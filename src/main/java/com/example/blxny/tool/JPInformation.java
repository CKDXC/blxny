package com.example.blxny.tool;

public  enum  JPInformation {

    //合伙人扩建审批
    PARTNER_UPDATE_SUCCESS("驿站扩建成功","您好，您申请的驿站扩建审批已通过"),
    PARTNER_UPDATE_ERROR("驿站扩建失败","您好，您申请的驿站扩建申请没有通过");


    private final String name;
    private final String info;
    private JPInformation(String name,String info)
    {
        this.name = name;
        this.info=info;
    }
    public String getInfo() {
        return info;
    }
    public String getName() {
        return name;
    }
}
