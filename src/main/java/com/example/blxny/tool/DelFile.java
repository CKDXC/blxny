package com.example.blxny.tool;

import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.FileNotFoundException;

public class DelFile {
    public static boolean del(String url, String fileName) {
        File path = null;
        try {
            path = new File(ResourceUtils.getURL("classpath:").getPath());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        File file = new File(path.getAbsolutePath(), "static/" + url + "/" + fileName);
        // 如果文件路径所对应的文件存在，并且是一个文件，则直接删除
        if (file.exists() && file.isFile()) {
            if (file.delete()) {
                System.out.println("删除单个文件" + file + "成功！");
                return true;
            } else {
                System.out.println("删除单个文件" + file + "失败！");
                return false;
            }
        } else {
            System.out.println("删除单个文件失败：" + file + "不存在！");
            return false;
        }
    }
}
