package com.example.blxny.tool;

import cn.jiguang.common.resp.APIConnectionException;
import cn.jiguang.common.resp.APIRequestException;
import cn.jpush.api.JPushClient;
import cn.jpush.api.push.PushResult;
import cn.jpush.api.push.model.Message;
import cn.jpush.api.push.model.Options;
import cn.jpush.api.push.model.Platform;
import cn.jpush.api.push.model.PushPayload;
import cn.jpush.api.push.model.audience.Audience;
import cn.jpush.api.push.model.notification.Notification;

import java.util.HashMap;
import java.util.Map;

public class JPushTool {
    // 设置好账号的app_key和masterSecret
    private static String APP_KEY = "90b499d6c23a16c26b3600eb";
    private static String MASTER_SECRET = "a8d118f68fd0e62ac0e8c9b1";
    public static String ONE="ONE";
    public static String ALL="ALL";
    public static String OTHER="OTHER";
    public static Object JPush(String JPid,//极光推送id
                        String infor,//发送的消息
                        String title,//消息抬头
                        String code) {//发送方式(ALL所有人,ONE指定JPid人，其他为合伙人)
        Map<String, String> parm = new HashMap<>();
        parm.put("id", JPid);
        parm.put("msg", infor);
        //创建JPushClient(极光推送的实例)
        JPushClient jpushClient = new JPushClient(MASTER_SECRET, APP_KEY);
        //推送的关键,构造一个payload
        PushPayload payload;
        if (code.equals("ALL")) {//发送所有
            payload = PushPayload.newBuilder()
                    .setPlatform(Platform.android())//指定android平台的用户
                    .setAudience(Audience.all())//你项目中的所有用户
                    .setNotification(Notification.android(parm.get("msg"), title, parm))
                    .setOptions(Options.newBuilder().setApnsProduction(false).build())
                    .setMessage(Message.content(parm.get("msg")))
                    .build();
            System.out.println("all");
        } else if (code.equals("ONE")) {//发送一人
            payload = PushPayload.newBuilder()
                    .setPlatform(Platform.android())//指定android平台的用户
                    .setAudience(Audience.registrationId(parm.get("id")))//registrationId指定用户
                    .setNotification(Notification.android(parm.get("msg"), title, parm))
                    .setOptions(Options.newBuilder().setApnsProduction(false).build())
                    .setMessage(Message.content(parm.get("msg")))//自定义信息
                    .build();
        } else {
            payload = PushPayload.newBuilder()
                    .setPlatform(Platform.android())//指定android平台的用户
                    .setAudience(Audience.alias("alias")).setNotification(Notification.alert(infor))//推送目标是别名为 "alias"，通知内容为 ALERT。
                    .setOptions(Options.newBuilder().setApnsProduction(false).build())
                    .setMessage(Message.content(parm.get("msg")))//自定义信息
                    .build();
            System.out.println("nom");
        }
        PushResult pu = null;
        try {
            pu = jpushClient.sendPush(payload);
        } catch (APIConnectionException e) {
            e.printStackTrace();
        } catch (APIRequestException e) {
            e.printStackTrace();
        }
        return pu;
    }
}
