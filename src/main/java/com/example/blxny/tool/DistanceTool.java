package com.example.blxny.tool;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * 计算两点距离，及，判断当前时间段
 * /* 时间点：
 *               0=  7:00-11:00
 *               1=  11:00-19:00
 *               2=  19:00-23:00
 *               3=  23:00-7:00
 */
public class DistanceTool {
    public  double distanceByLongNLat(String lat, String lng, String lngs, String lats) {
        /*计算两点距离*/
        double lat1=Double.parseDouble(lat);
        double lon1=Double.parseDouble(lng);
        double lat2=Double.parseDouble(lats);
        double lon2=Double.parseDouble(lngs);
        double a, b, R;
        R = 6378137;//地球半径
        lat1 = lat1 * Math.PI / 180.0;
        lat2 = lat2 * Math.PI / 180.0;
        a = lat1 - lat2;
        b = (lon1 - lon2) * Math.PI / 180.0;
        double d;
        double sa2, sb2;
        sa2 = Math.sin(a / 2.0);
        sb2 = Math.sin(b / 2.0);
        d = 2 * R * Math.asin(Math.sqrt(sa2 * sa2 + Math.cos(lat1) * Math.cos(lat2) * sb2 * sb2));
        return d;
    }

    public Map getDatabaseTimeForNow(){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:s");
        String time = new SimpleDateFormat("HH").format(new Date());
        Map js=new HashMap();
        int inttime = Integer.valueOf(time);
        String timeOB=null;
        if (inttime > 7 && inttime <= 11) {
            inttime = 1;
            timeOB="7:00-11:00";
        } else if (inttime > 11 && inttime <= 19) {
            inttime = 2;
            timeOB="11:00-19:00";
        } else if (inttime > 19 && inttime <= 23) {
            inttime = 3;
            timeOB="19:00-23:00";
        } else {
            inttime = 4;
            timeOB="23:00-7:00";
        }
        js.put("time",String.valueOf(inttime));
        js.put("timeInfo",timeOB);
        return js;
    }

}
