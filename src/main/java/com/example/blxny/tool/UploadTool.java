package com.example.blxny.tool;


import com.example.blxny.util.Base64MultipartFile;
import org.springframework.util.ResourceUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.util.UUID;

public class UploadTool {
    private static String URL = "http://120.78.172.33";
    //private static String URL="http://127.0.0.1:8888";

    public String upOne(String files) throws FileNotFoundException {
        files = "data:image/jpg;base64," + files;
        MultipartFile file = Base64MultipartFile.base64ToMultipart(files);
        String url = URL + "/img/";
        String oneimg = null;
        File path = new File(ResourceUtils.getURL("classpath:").getPath());
        if (!path.exists()) {
            path = new File("");
        }
        if (!file.isEmpty()) {
            try {
                File fi = new File(path.getAbsolutePath(), "static/img/");//获取img目录
                if (!fi.exists()) {
                    fi.mkdirs();
                }//不在就创建
                String fik = file.getOriginalFilename();
                int a = fik.indexOf(".");
                String hh = file.getOriginalFilename().substring(a);//截取字符串
                String luj = UUID.randomUUID().toString().replace("-", "").substring(0, 8);
                FileOutputStream f = new FileOutputStream(fi + "/" + luj + hh);
                BufferedOutputStream out = new BufferedOutputStream(f);
                System.out.println(url + luj + hh);
                oneimg = url + luj + hh;
                out.write(file.getBytes());
                out.flush();
                out.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                return "error";
            } catch (IOException e) {
                e.printStackTrace();
                return "error";
            }
            return oneimg;
        } else {
            return "error";
        }
    }

    //base64终极上传方法。前台不需要对base64做任何处理(换行问题未作测试)
    public static String upOneByYourUrlUseBase64(String files, String yourUrl) throws FileNotFoundException {
        int n = 0;
        //经过尝试.replace()不可用，替换空格方法如下
        StringBuffer str = new StringBuffer(files);
        while (str.indexOf(" ", n) != -1) {
            n = str.indexOf(" ", n);//获得空格的索引
            str.deleteCharAt(n);//删除对应索引的空格
            str.insert(n, "+");//在相应位置插入
            n = n + 1;
        }
        files = String.valueOf(str);
        if (files.indexOf("data:") == -1) {
            files = "data:image/jpg;base64," + files;
        }
        MultipartFile file = Base64MultipartFile.base64ToMultipart(files);
        String url = URL + "/" + yourUrl + "/";
        String oneimg = null;
        File path = new File(ResourceUtils.getURL("classpath:").getPath());
        if (!path.exists()) {
            path = new File("");
        }
        if (!file.isEmpty()) {
            try {
                File fi = new File(path.getAbsolutePath(), "static/" + yourUrl + "/");
                if (!fi.exists()) {
                    fi.mkdirs();
                }//不在就创建
                String fik = file.getOriginalFilename();
                int a = fik.indexOf(".");
                String hh = file.getOriginalFilename().substring(a);//截取字符串
                String luj = UUID.randomUUID().toString().replace("-", "").substring(0, 8);
                FileOutputStream f = new FileOutputStream(fi + "/" + luj + hh);
                BufferedOutputStream out = new BufferedOutputStream(f);
                System.out.println(url + luj + hh);
                oneimg = url + luj + hh;
                out.write(file.getBytes());
                out.flush();
                out.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                return "error";
            } catch (IOException e) {
                e.printStackTrace();
                return "error";
            }
            return oneimg;
        } else {
            return "error";
        }
    }

    public static String upFile(MultipartFile file, String ending, String body) throws FileNotFoundException {
        // files = "data:image/"+ending+";base64," + files;
        //MultipartFile file= Base64MultipartFile.base64ToMultipart(files);
        String url = URL + "/APP/";
        String oneimg = null;
        File path = new File(ResourceUtils.getURL("classpath:").getPath());
        if (!path.exists()) {
            path = new File("");
        }
        if (!file.isEmpty()) {
            try {
                File fi = new File(path.getAbsolutePath(), "static/APP/");//获取img目录
                if (!fi.exists()) {
                    fi.mkdirs();
                }//不在就创建
//                String fik=file.getOriginalFilename();
//                int a=fik.indexOf(".");
//                String hh=file.getOriginalFilename().substring(a);//截取字符串
                FileOutputStream f = new FileOutputStream(fi + "/" + body + "." + ending);
                BufferedOutputStream out = new BufferedOutputStream(f);
                System.out.println(url + body + "." + ending);
                oneimg = url + body + "." + ending;
                out.write(file.getBytes());
                out.flush();
                out.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                return "error";
            } catch (IOException e) {
                e.printStackTrace();
                return "error";
            }
            return oneimg;
        } else {
            return "error";
        }
    }

    public static String upFile(MultipartFile file, String headerPath, String ending, String body) throws FileNotFoundException {
        // files = "data:image/"+ending+";base64," + files;
        //MultipartFile file= Base64MultipartFile.base64ToMultipart(files);
        String header = URL + "/" + headerPath + "/";
        body = System.currentTimeMillis() + "";
        String url = null;
        File path = new File(ResourceUtils.getURL("classpath:").getPath());
        if (!path.exists()) {
            path = new File("");
        }
        if (!file.isEmpty()) {
            try {
                File fi = new File(path.getAbsolutePath(), "static/" + headerPath + "/");//获取img目录
                if (!fi.exists()) {
                    fi.mkdirs();
                }//不在就创建
                FileOutputStream f = new FileOutputStream(fi + "/" + body + "." + ending);
                BufferedOutputStream out = new BufferedOutputStream(f);
                System.out.println(header + body + "." + ending);
                url = header + body + "." + ending;
                out.write(file.getBytes());
                out.flush();
                out.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                return "error";
            } catch (IOException e) {
                e.printStackTrace();
                return "error";
            }
            return url;
        } else {
            return "error";
        }
    }
}
