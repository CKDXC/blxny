package com.example.blxny.tool;

import com.example.blxny.dao.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StationCollectedJudgeTool {
    @Autowired
    private UserDao dao;
    /**
     * 传入一个驿站station_id,用户useruuid
     * 输出此station_id是否被用户收藏
     * return:boolean
     */
    public boolean CollectedJudge(String useruuid,String station_id){
        boolean returnMassage=true;
        String[] arr;
        String chose=dao.findStation(useruuid);
        if (chose==null) {
            returnMassage=false;
        } else {
            arr = chose.split(",");
            for (int i = 0; i < arr.length; i++) {
                if (!station_id.equals(arr[i])) {
                    returnMassage=false;
                }
            }
        }
        return returnMassage;
    }
}
