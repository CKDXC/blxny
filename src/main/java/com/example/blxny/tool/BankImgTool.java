package com.example.blxny.tool;

public class BankImgTool {
    private String post = "http://120.78.172.33/bank/";
    private String[] img = {
            "上海银行.png",
            "东营市商业银行.png",
            "中信银行.png",
            "中原银行.png",
            "中国光大银行.png",
            "中国农业银行.png",
            "中国工商银行.png",
            "中国建设银行.png",
            "中国民生银行.png",
            "中国邮政.png",
            "中国银行.png",
            "交通银行.png",
            "众邦银行.png",
            "兴业银行.png",
            "北京银行.png",
            "华夏银行.png",
            "南京银行.png",
            "厦门国际银行.png",
            "台州银行.png",
            "嘉兴银行.png",
            "大连银行.png",
            "天津农商银行.png",
            "天津银行.png",
            "广发银行.png",
            "广州商业银行.png",
            "广州银行.png",
            "德州商业银行.png",
            "徽商银行.png",
            "恒丰银行.png",
            "成都银行.png",
            "招商银行.png",
            "昆山农商行.png",
            "晋商银行.png",
            "杭州商业银行.png",
            "杭州银行.png",
            "江苏银行.png",
            "泰隆银行.png",
            "浙商银行.png",
            "浙江民泰商业银行.png",
            "浦发银行.png",
            "深圳发展银行.png",
            "渤海银行.png",
            "温州银行.png",
            "湖州市商业银行.png",
            "盛京银行.png",
            "稠州商业银行.png",
            "绍兴银行.png",
            "鄞州银行.png",
            "金华银行.png",
            "青岛银行.png" };

    public String BankImgTool(String bank) {
        String url = "中国人民银行.png";
        for (int i = 0; i < img.length; i++) {
            if (img[i].indexOf(bank.substring(0,4)) != -1) {
                url = img[i];
                break;
            }
        }
        return post + url;
    }
}
