package com.example.blxny.controller;

import com.example.blxny.service.PowerService;
import com.example.blxny.util.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@CrossOrigin
@RestController
@RequestMapping("/blxny")
public class PowerWorkController extends BaseController {
    @Autowired
    private PowerService dao;

    /**
     * 扫描或输入终端号判断桩是否可用
     */
    @RequestMapping(value = "/scanQrcode", method = RequestMethod.POST)
    @ResponseBody
    public Object getpowernum(HttpServletRequest request, HttpServletResponse response,
                              @RequestParam("deviceid") String deviceid,
                              @RequestParam("Gunno") String Gunno) {
        return super.setSuccessMessage(request, response, dao.getPowerNum(deviceid, Gunno));
    }


    /**
     * 选择计费方式进入充电
     */
    @RequestMapping(value = "/startWork", method = RequestMethod.GET)
    @ResponseBody
    public Object startWork(HttpServletRequest request, HttpServletResponse response,
                            @RequestParam("deviceid") String deviceid,//设备号
                            @RequestParam("Gunno") String Gunno,//枪号
                            @RequestParam("user") String user,//用户uuid
                            @RequestParam("type") String type,//计费方式0自动充满，1多少分钟，2多少度，3多少元
                            @RequestParam("much") String much) {//计费方式输入的数量，自动“auto”，时间请将小时转化为分钟
        return null;
    }
}
