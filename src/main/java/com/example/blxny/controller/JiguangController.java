package com.example.blxny.controller;

import cn.jiguang.common.resp.APIConnectionException;
import cn.jiguang.common.resp.APIRequestException;
import cn.jpush.api.JPushClient;
import cn.jpush.api.push.PushResult;
import cn.jpush.api.push.model.Message;
import cn.jpush.api.push.model.Options;
import cn.jpush.api.push.model.Platform;
import cn.jpush.api.push.model.PushPayload;
import cn.jpush.api.push.model.audience.Audience;
import cn.jpush.api.push.model.notification.Notification;
import com.example.blxny.tool.JPushTool;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

@CrossOrigin
@RestController
@RequestMapping("/jiguang")
public class JiguangController {/*极光测试
    private static String APP_KEY = "90b499d6c23a16c26b3600eb";
    private static String MASTER_SECRET = "a8d118f68fd0e62ac0e8c9b1";*/
    @RequestMapping(value = "/jpush", method = RequestMethod.GET)
    @ResponseBody
    public void demo(HttpServletRequest request, HttpServletResponse response,
                    @RequestParam("JPid") String JPid,//极光推送id
                     @RequestParam("infor") String infor,//发送的消息
                     @RequestParam("title") String title,//消息抬头
                     @RequestParam("code") String code) {//发送方式(ALL所有人,ONE指定JPid人，其他为合伙人)

/*
        Object ob = new Object();
        Hallo number1 = new Hallo(ob);
        Hallo number2 = new Hallo(ob);

        number2.start();
        number2.setName("线程 1：");
*/
        JPushTool.JPush(JPid,infor,title,code);
    }

}