package com.example.blxny.controller;


import com.example.blxny.service.PowerService;
import com.example.blxny.service.UserLoginService;
import com.example.blxny.util.BaseController;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@CrossOrigin
@Controller
@RequestMapping("/power")
public class PowerController extends BaseController {
    @Autowired
    private PowerService dao;
    @Autowired
    private UserLoginService service;

    /*获取到附近的充电桩*/
    @RequestMapping(value = "/getpower", method = RequestMethod.POST)
    @ResponseBody
    public Object getPower(HttpServletRequest request, HttpServletResponse response, @RequestParam(value = "code") String code,
                           @RequestParam(value = "lat") String lat,
                           @RequestParam(value = "uuid") String uuid,
                           @RequestParam(value = "lng") String lng) {//lat:伟度，lng：经度\
        JSONObject data = new JSONObject();
        data.put("info", dao.findOtherPower(uuid, code, lat, lng));
        return this.setSuccessMessage(request, response, data);
    }

    /*获取到附近所有类型充电桩*/
    @RequestMapping(value = "/getallpower", method = RequestMethod.POST)
    @ResponseBody
    public Object getallpower(HttpServletRequest request, HttpServletResponse response,
                              @RequestParam(value = "lat") String lat,
                              @RequestParam(value = "lng") String lng,
                              @RequestParam(value = "uuid") String uuid,
                              @RequestParam(value = "city") String city) {
        JSONObject data = new JSONObject();
        data.put("info", dao.getAllPower(uuid, lat, lng, city));
        return this.setSuccessMessage(request, response, data);
    }

    /*获取此驿站的信息*/
    @RequestMapping(value = "/oneforstation", method = RequestMethod.POST)
    @ResponseBody
    public Object oneforstation(HttpServletRequest request, HttpServletResponse response,
                                @RequestParam(value = "id") String id) {
        JSONObject obj = new JSONObject();
        obj.put("fast", dao.findfastpower(id));
        obj.put("slow", dao.findlowpower(id));
        JSONObject object = JSONObject.fromObject(dao.findPowerPay(id));
        obj.put("time", object.getString("time"));/*当前时间段*/
        obj.put("fast_power_pay", object.getString("fastpay"));//快充电费
        obj.put("fast_service_pay", object.getString("fastservice"));//快充服务费
        obj.put("slow_power_pay", object.getString("lowpay"));//慢充电费
        obj.put("slow_service_pay", object.getString("lowservice"));//慢充服务费
        return this.setSuccessMessage(request, response, obj);
    }

    /**
     * 价格详情
     */
    @RequestMapping(value = "/price", method = RequestMethod.POST)
    @ResponseBody
    public Object price(HttpServletRequest request, HttpServletResponse response,
                        @RequestParam(value = "type") String type,
                        @RequestParam(value = "station") String station) {
        return this.setSuccessMessage(request, response, dao.findPay(type, station));
    }

    /**
     * 充电！
     * 客户端获取计费方式信息，用户uuid
     * 1，自动充满
     * 2，充多少分钟（如一小时12分格式‘01.12’；不选为0）
     * 3，充多少度，不选为0
     * 4，充多少元，不选为0
     * *注意*此项生成一个订单，需要返回一个订单号
     */
    @RequestMapping(value = "/price", method = RequestMethod.GET)
    @ResponseBody
    public Object price(HttpServletRequest request, HttpServletResponse response,
                        @RequestParam("type") String type,//计费方式
                        @RequestParam("useTime") String usetime,
                        @RequestParam("usePower") String usepower,
                        @RequestParam("useMuch") String usemuch,
                        @RequestParam("uuid") String uuid//用户uuid
    ) {
        dao.powerWorking(type, usetime, usepower, usemuch, uuid);
        return null;
    }

    /**
     * 确认拔枪，没拔为null，拔出则返回费用信息
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/powerend", method = RequestMethod.POST)
    @ResponseBody
    public Object powerend(HttpServletRequest request, HttpServletResponse response,
                           @RequestParam("orderid") String orderid) {//订单号
        return super.setSuccessMessage(request, response, dao.endPower(orderid));
    }

    /**
     * 找桩充电，返回桩号（deviceId）和状态（type），和预计剩余时间（time暂时剩余时间都为null）
     */
    @RequestMapping(value = "/touchFindPower", method = RequestMethod.POST)
    @ResponseBody
    public Object touchFindPower(HttpServletRequest request, HttpServletResponse response,
                                 @RequestParam("station") String station,//驿站的id
                                 @RequestParam("code") String code) {
        return super.setSuccessMessage(request, response, service.findDeviceType(station, code));
    }

    /**
     * 驿站导航
     */
        @RequestMapping(value = "/findGPS", method = RequestMethod.POST)
    @ResponseBody
    public Object findGPS(HttpServletRequest request, HttpServletResponse response,
                                 @RequestParam("id") String id) {//驿站的id
            return super.setSuccessMessage(request,response,dao.findGPS(id));

    }
}