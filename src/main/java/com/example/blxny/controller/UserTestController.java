package com.example.blxny.controller;

import com.example.blxny.model.*;
import com.example.blxny.service.AttachmentService;
import com.example.blxny.service.InformationService;
import com.example.blxny.service.UserLoginService;
import com.example.blxny.service.impl.UserServiceImpl;
import com.example.blxny.tool.UploadTool;
import com.example.blxny.util.BaseController;
import com.example.blxny.util.ErrorInfomation;
import com.example.blxny.util.FileUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Date;
import java.util.List;

@CrossOrigin
@Controller
@RequestMapping("/user")
public class UserTestController extends BaseController {


    @Autowired
    private UserLoginService userLoginService = new UserServiceImpl();
    @Autowired
    private AttachmentService attachmentService;
    @Autowired
    private InformationService informationService;

    /**
     * 注册短信发送
     *
     * @param request
     * @param response
     * @param phone
     * @return
     */
    @RequestMapping(value = "/verification", method = RequestMethod.POST)
    @ResponseBody
    public String verification(HttpServletRequest request, HttpServletResponse response, @RequestParam(value = "phone") String phone) {

        String returnMessage;
        returnMessage = userLoginService.verification(phone);
        System.out.println("====" + phone);
        //成功
        if (!returnMessage.equals(ErrorInfomation.Err1002.toString())) {
            returnMessage = this.setSuccessMessage(request, response, returnMessage);
        }
        //失败
        else {
            returnMessage = this.setFailureMessage(request, response, ErrorInfomation.Err1002);
        }
        System.out.println("====" + returnMessage);
        return returnMessage;
    }

    /*注册*/
    @RequestMapping(value = "/register", method = RequestMethod.POST)
    @ResponseBody
    public String reguster(HttpServletRequest request, HttpServletResponse response, @RequestParam(value = "phone") String phone,
                           @RequestParam(value = "password") String password) {
        System.out.println("得到电话" + phone + "密码" + password);
        String returnMessage;
        User user = new User();
        user.setPhone(phone);
        user.setPassword(password);
        String flg = userLoginService.reguster(phone, password);
        if (flg.equals("注册成功")) {
            returnMessage = this.setSuccessMessage(request, response, flg);
        } else {
            returnMessage = this.setFailureMessage(request, response, flg);
        }
        return returnMessage;
    }


    /*账户密码登录*/
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    @ResponseBody
    public String login(HttpServletRequest request, HttpServletResponse response, HttpSession session,
                        @RequestParam(value = "phone") String phone, @RequestParam(value = "password") String password) {
        String returnMassage;
        User user = userLoginService.FindUserAll(phone, password);
        if (user.getPhone().equals("密码错误")) {
            returnMassage = this.setFailureMessage(request, response, ErrorInfomation.Err10401);
        } else if (user.getPhone().equals("账号不存在")) {
            returnMassage = this.setFailureMessage(request, response, ErrorInfomation.Err1040);
        } else {
            returnMassage = this.setSuccessMessage(request, response, user);
        }
        return returnMassage;
    }

    /*短信验证码登录*/
    @RequestMapping(value = "/phonelogin", method = RequestMethod.POST)
    @ResponseBody
    public String phonelogin(HttpServletRequest request, HttpServletResponse response, HttpSession session,
                             @RequestParam(value = "phone") String phone) {
        String returnMassage;
        User user = userLoginService.PhoneLogin(phone);
        if (user == null) {
            returnMassage = this.setFailureMessage(request, response, ErrorInfomation.Err1040);
        } else {
            returnMassage = this.setSuccessMessage(request, response, user);
        }
        return returnMassage;
    }


    /**
     * 修改密码
     *
     * @param request
     * @param response
     * @param uid
     * @param newpwd
     * @return
     */
    @RequestMapping(value = "/modifypwd", method = RequestMethod.POST)
    @ResponseBody
    public String modifypwd(HttpServletRequest request, HttpServletResponse response,
                            @RequestParam(value = "uid") String uid,
                            @RequestParam(value = "newpwd") String newpwd) {
        String returnMessage = userLoginService.modifyPwd(uid, newpwd);
        System.out.println("uid:" + uid + "/n" + "newpwd:" + newpwd);
        if (returnMessage.equals("修改成功")) {
            returnMessage = this.setSuccessMessage(request, response, returnMessage);
        } else {
            returnMessage = this.setFailureMessage(request, response, returnMessage);
        }
        return returnMessage;
    }

    /**
     * 修改密码验证码验证
     *
     * @param request
     * @param response
     * @param phone
     * @return
     */
    @RequestMapping(value = "/verificating", method = RequestMethod.POST)
    @ResponseBody
    public String verificating(HttpServletRequest request, HttpServletResponse response,
                               @RequestParam(value = "phone") String phone) {
        String returnMessage = userLoginService.checkPhone(phone);
        if ("手机号不存在".equals(returnMessage)) {
            returnMessage = this.setFailureMessage(request, response, returnMessage);
        } else {
            returnMessage = this.setSuccessMessage(request, response, returnMessage);
        }
        return returnMessage;
    }


    /**
     * 上传头像base64
     *
     * @param request
     * @param response
     * @param id
     * @param file
     * @return
     * @throws IllegalStateException
     * @throws IOException
     */
    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    @ResponseBody
    public String imageUpload(HttpServletRequest request, HttpServletResponse response,
                              @RequestParam("id") Integer id,
                              @RequestParam("file") String file) throws IllegalStateException, IOException {
        System.out.println("id" + id);
        String fileName = FileUtils.convertStringtoImage(file, request);
        System.out.println("imgPath" + fileName);
        String imgPath = "http://192.168.1.17:8080/image/" + fileName;

        Attachment attachment = new Attachment();
        attachment.setFile(fileName);
        attachment.setCreated(new Date());
        String msg = attachmentService.addImg(id, attachment);

        if (msg.equals("success")) {
            return this.setSuccessMessage(request, response, imgPath);
        } else {
            return this.setFailureMessage(request, response, msg);
        }
    }

    //用户住家地址新增
    @RequestMapping(value = "/userInfor", method = RequestMethod.POST)
    @ResponseBody
    public String newUserInfor(HttpServletRequest request, HttpServletResponse response,
                               @RequestParam("user_uuid") String phone,
                               @RequestParam("address") String address,
                               @RequestParam("home_province") String homepro,
                               @RequestParam("home_city") String homecity,
                               @RequestParam("lat") String lat,
                               @RequestParam("lng") String lng) {
        String returnMessage;
        if (userLoginService.newUserInfor(address, lat, lng, homepro, homecity, phone) > 0) {
            returnMessage = this.setSuccessMessage(request, response, "修改成功");
        } else {
            returnMessage = this.setFailureMessage(request, response, "请求失败");
        }
        return returnMessage;
    }

    //用户公司地址新增
    @RequestMapping(value = "/workInfor", method = RequestMethod.POST)
    @ResponseBody
    public String workInfor(HttpServletRequest request, HttpServletResponse response,
                            @RequestParam("user_uuid") String phone,
                            @RequestParam("address") String address,
                            @RequestParam("work_province") String workpro,
                            @RequestParam("work_city") String workcity,
                            @RequestParam("lat") String lat,
                            @RequestParam("lng") String lng) {
        String returnMessage;
        if (userLoginService.workInfor(address, lat, lng, workpro, workcity, phone) > 0) {
            returnMessage = this.setSuccessMessage(request, response, "修改成功");
        } else {
            returnMessage = this.setFailureMessage(request, response, "请求失败");
        }
        return returnMessage;
    }

    /**
     * 通过手机,修改昵称
     *
     * @param request
     * @param response
     * @param uid
     * @param newname
     * @return
     */
    @RequestMapping(value = "/modifyName", method = RequestMethod.POST)
    @ResponseBody
    public String modifyName(HttpServletRequest request, HttpServletResponse response,
                             @RequestParam("uid") String uid,
                             @RequestParam("newname") String newname) {
        String msg = userLoginService.modifyNickNmae(uid, newname);
        if (msg.equals("success")) {
            return this.setSuccessMessage(request, response, "修改成功");
        } else {
            return this.setFailureMessage(request, response, "修改失败");
        }
    }

    /**
     * 获取车辆信息
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/getUserCar", method = RequestMethod.POST)
    @ResponseBody
    public String getUserCar(HttpServletRequest request, HttpServletResponse response) {
        return this.setFailureMessage(request, response, "fail");
    }

    //用户住家地址

    @RequestMapping(value = "/getUserStations", method = RequestMethod.POST)
    @ResponseBody
    public String getUserStations(HttpServletRequest request, HttpServletResponse response,
                                  @RequestParam("uid") String uid) {
        List<Station> list = userLoginService.getStationByUid(uid);
        return this.setSuccessMessage(request, response, list);
    }

    @RequestMapping(value = "/removeCollected", method = RequestMethod.POST)
    @ResponseBody
    public String removeCollected(HttpServletRequest request, HttpServletResponse response,
                                  @RequestParam("uid") String uid,
                                  @RequestParam("stationId") String stationId) {
        String msg = userLoginService.removeStationCollected(uid, stationId);
        if (msg.equals("success")) {
            return this.setSuccessMessage(request, response, msg);
        } else {
            return this.setFailureMessage(request, response, msg);
        }
    }

    /***
     * 收藏驿站
     */
    @RequestMapping(value = "/takeCollected", method = RequestMethod.POST)
    @ResponseBody
    public String takeCollected(HttpServletRequest request, HttpServletResponse response,
                                @RequestParam("uid") String uid,
                                @RequestParam("stationid") String stationId) {
        return super.setSuccessMessage(request, response, userLoginService.addCollected(uid, stationId));
    }

    @RequestMapping(value = "/getUserCoupon", method = RequestMethod.GET)
    @ResponseBody
    public String getUserCoupon(HttpServletRequest request, HttpServletResponse response,
                                @RequestParam("uid") String uid,
                                @RequestParam("isUsed") Integer isUsed) {
        return "";
    }

    @RequestMapping(value = "/upphoto", method = RequestMethod.POST)/*上传单个图片*/
    @ResponseBody
    public String upphoto(HttpServletRequest request, HttpServletResponse response,
                          @RequestParam("file") String files,
                          @RequestParam("id") String id) throws FileNotFoundException {
        UploadTool photo = new UploadTool();
        String url = photo.upOne(files);
        if ("error".equals(url)) {
            return super.setFailureMessage(request, response, "error");
        } else {
            userLoginService.upphoto(url, id);
            return super.setSuccessMessage(request, response, url);
        }
    }

    /**
     * 实名认证
     *
     * @throws FileNotFoundException
     */
    @RequestMapping(value = "/idcard", method = RequestMethod.POST)/*上传实名认证*/
    @ResponseBody
    public String idcard(HttpServletRequest request, HttpServletResponse response,
                         @RequestParam("face") String face,//身份证正面照片
                         @RequestParam("back") String back,//背面照片
                         @RequestParam("aspeople") String man,//持证照片
                         @RequestParam("name") String name,//用户姓名
                         @RequestParam("idcard") String idcard,//身份证号码
                         @RequestParam("useruuid") String uuid/*用户uuid*/) throws FileNotFoundException {
        //先保存图片
        UploadTool photo = new UploadTool();
        String faceidcard = photo.upOne(face);//正面路径
        String backidcard = photo.upOne(back);//背面路径
        String mans = photo.upOne(man);
        //再存储信息
        if ("error".equals(faceidcard) || "error".equals(backidcard)) {
            return super.setFailureMessage(request, response, "error");
        } else {
            IDcard iDcard = new IDcard();
            iDcard.setFace(faceidcard);
            iDcard.setBack(backidcard);
            iDcard.setIdcard(idcard);
            iDcard.setUseruuid(uuid);
            iDcard.setName(name);
            iDcard.setAspeople(mans);
            return super.setSuccessMessage(request, response, userLoginService.newIdCard(iDcard));
        }
    }

    /**
     * 判断是否实名认证
     *
     * @param request
     * @param response
     * @param useruuid
     * @return：0:未认证，1:认证中，2:已认证，3:认证失败
     */
    @RequestMapping(value = "/isrealman", method = RequestMethod.POST)
    @ResponseBody
    public Object isrealman(HttpServletRequest request, HttpServletResponse response,
                            @RequestParam("useruuid") String useruuid) {//用户uuid
        return super.setSuccessMessage(request, response, userLoginService.findrealman(useruuid));
    }

    /**
     * 换绑手机号
     *
     * @return 0:错误手机号已绑定 1：成功
     */
    @RequestMapping(value = "/changephone", method = RequestMethod.POST)
    @ResponseBody
    public Object changephone(HttpServletRequest request, HttpServletResponse response,
                              @RequestParam("phone") String phone,
                              @RequestParam("useruuid") String uuid) {
        return super.setSuccessMessage(request, response, userLoginService.changePhone(phone, uuid));
    }


    /**
     * 支付密码
     * 失败返回“error”成功返回用户信息
     */
    @RequestMapping(value = "/paypassword", method = RequestMethod.POST)
    @ResponseBody
    public Object paypassword(HttpServletRequest request, HttpServletResponse response,
                              @RequestParam("uuid") String uuid,
                              @RequestParam("password") String password) {
        userLoginService.changepaypassword(password, uuid);
        return super.setSuccessMessage(request, response, userLoginService.FindUserAllByUuid(uuid));
    }

    /**
     * 判断是否存在车辆信息
     *
     * @return car信息，没有返回(entity not fount.)
     */
    @RequestMapping(value = "/didhavecar", method = RequestMethod.POST)
    @ResponseBody
    public Object didhavecar(HttpServletRequest request, HttpServletResponse response,
                             @RequestParam("uuid") String uuid) {
        return super.setSuccessMessage(request, response, userLoginService.findCar(uuid));
    }

    /**
     * 保存车辆信息
     *
     * @return
     */
    @RequestMapping(value = "/insertCar", method = RequestMethod.POST)
    @ResponseBody
    public Object insertCar(HttpServletRequest request, HttpServletResponse response,
                            @RequestParam("uuid") String uuid,//用户uuid
                            @RequestParam("car") String car,//车名
                            @RequestParam("card") String card,//车牌
                            @RequestParam("year") String year) {//年份like 2019-04
        return super.setSuccessMessage(request, response, userLoginService.insertCar(car, card, year, uuid));
    }

    /**
     * 消息中心
     */
    @RequestMapping(value = "/informationCenter", method = RequestMethod.POST)
    @ResponseBody
    public Object informationCenter(HttpServletRequest request, HttpServletResponse response,
                                    @RequestParam("uuid") String uuid,//用户uuid
                                    @RequestParam("code") String code) {//当前页数
        return super.setSuccessMessage(request, response, informationService.findInformation(uuid, code));
    }

    /**
     * 清空消息
     */
    @RequestMapping(value = "/informationClean", method = RequestMethod.POST)
    @ResponseBody
    public Object informationClean(HttpServletRequest request, HttpServletResponse response,
                                   @RequestParam("uuid") String uuid) {//用户uuid
        if (informationService.informationClean(uuid)>0){
            return super.setSuccessMessage(request,response,"OK");
        }else {
            return super.setSuccessMessage(request,response,"NoDataChange");
        }

    }

    /**
     * 判断是否有消息未读,返回int类型是多少就有几条
     */
    @RequestMapping(value = "/didHaveInfo", method = RequestMethod.POST)
    @ResponseBody
    public String  didHaveInfo(HttpServletRequest request, HttpServletResponse response,
                                   @RequestParam("uuid") String uuid,
                                    @RequestParam("jpid")String jpid) {//用户uuid
        return super.setSuccessMessage(request,response,userLoginService.didHaveInfo(uuid,jpid));
    }
    /**
     * 微信登录
     * 成功返回用户数据，失败返回空
     */
    @RequestMapping(value = "/wxlogin", method = RequestMethod.POST)
    @ResponseBody
    public Object wxlogin(HttpServletRequest req, HttpServletResponse resp,
                          @RequestParam("nickname") String nickname,
                          @RequestParam("headimgurl") String img,
                          @RequestParam("unionid") String uuid) {
        User user = userLoginService.wxlogin(uuid);
        if (user == null) {
            User users = new User();
            return super.setSuccessMessage(req, resp, users);
        }
        return super.setSuccessMessage(req, resp, user);
    }

    /**
     * 微信绑定电话号码，侧边栏中第三方绑定也可以使用该接口
     * 成功返回用户数据，失败返回空,如输入手机号已注册，该手机号账户将与此微信关联
     */
    @RequestMapping(value = "/wxreg", method = RequestMethod.POST)
    @ResponseBody
    public Object wxreg(HttpServletRequest req, HttpServletResponse resp,
                        @RequestParam("nickname") String nickname,
                        @RequestParam("headimgurl") String img,
                        @RequestParam("unionid") String uuid,
                        @RequestParam("phone") String phone) {
        User user = userLoginService.wxRegWithFhone(nickname, img, uuid, phone);
        if (user != null) {
            return super.setSuccessMessage(req, resp, user);
        } else {
            User users = new User();
            return super.setSuccessMessage(req, resp, users);
        }
    }

    /**
     * 第三方解绑，删除第三方与其账号关系
     * 返回成功后，前端缓存user需将token或integral变成null
     * @param req
     * @param resp
     * @param uuid
     * @param code
     * @return
     */
    @RequestMapping(value = "/close", method = RequestMethod.POST)
    @ResponseBody
    public Object wxreg(HttpServletRequest req, HttpServletResponse resp,
                        @RequestParam("uuid") String uuid,//用户uuid
                        @RequestParam("code") String code){//选择，2为支付宝，1为微信
                return super.setSuccessMessage(req,resp,userLoginService.closeOtherLogin(code,uuid));
    }

    /**
     * 城市获取，无选择字段返回0（如获取四川省所有市=？province=四川省&city=0），
     * 默认返回省（所有字段为0）
     * @param req
     * @param resp
     * @param province 省
     * @param city 市/县
     * @return
     */
    @RequestMapping(value = "/city", method = RequestMethod.POST)
    @ResponseBody
    public Object city(HttpServletRequest req, HttpServletResponse resp,
                        @RequestParam("province") String province,
                        @RequestParam("city") String city){
      return super.setSuccessMessage(req,resp,userLoginService.findCity(province,city));
    }

    /**
     * 判断是否有未支付订单或者是正在运行的订单
     * 返回null则无未结算订单，返回0则有订单正在运行，返回1则有订单未支付
     */
    @RequestMapping(value="/seeYourOrder",method = RequestMethod.POST)
    @ResponseBody
    public Object seeYourOrder(HttpServletRequest request,HttpServletResponse response ,@RequestParam("uuid") String uuid){
        String string= (String) userLoginService.findOrderNutPay(uuid);
        return super.setSuccessMessage(request,response,string);
    }


    /**新闻//user/news
     *返回headline为标题，source为出版社，time为发表时间，content为类容，img为图片
     * ！content中有些许html标签，img为直接网络地址，如无图片返回为‘null’
     */
    @RequestMapping(value="/news",method = RequestMethod.POST)
    @ResponseBody
    public Object news(HttpServletRequest request,HttpServletResponse response ){
        return super.setSuccessMessage(request,response,userLoginService.news());
    }

    /**
     * 查看是否更新
     */
    @RequestMapping(value="/update",method = RequestMethod.POST)
    @ResponseBody
    public Object update(HttpServletRequest request,HttpServletResponse response,
                         @RequestParam("version")String version){
        return super.setSuccessMessage(request,response,userLoginService.update(version));
    }

    /**
     * 上传新版本
     */
    @RequestMapping(value = "/addFile", method = RequestMethod.POST)
    @ResponseBody
    public String addFile(HttpServletRequest request, HttpServletResponse response,
                          @RequestParam("file") MultipartFile files,
                          /*@RequestParam("ending")String ending,*/
                          @RequestParam("info")String info,
                          @RequestParam("version")String s) throws FileNotFoundException {//版本
        String url;
        String ending="apk";
        if(!ending.equals("apk")){
            url="后缀不要改";
        }else {
            UploadTool photo = new UploadTool();
            url = photo.upFile(files, ending, s);
            if(!url.equals("error")){
                userLoginService.addUpdate(url,info,s);
            }
        }
        return super.setSuccessMessage(request,response,url);
    }
}