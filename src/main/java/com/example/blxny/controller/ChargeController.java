package com.example.blxny.controller;

import com.example.blxny.service.ChargeService;
import com.example.blxny.util.BaseController;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@CrossOrigin
@RestController
@RequestMapping("/charge")
public class ChargeController extends BaseController {

    @Autowired
    private ChargeService chargeService;

    @RequestMapping(value = "/sweetCode", method = RequestMethod.GET)
    public String sweetCode(HttpServletRequest request, HttpServletResponse response,
                            @RequestParam("deviceId") String deviceId) {
        return "";
    }

    /**
     * 开始充电
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/startCharge", method = RequestMethod.POST)
    public String startCharge(HttpServletRequest request, HttpServletResponse response,
                              @RequestParam("deviceId") String deviceId,
                              @RequestParam("gunNo") String gunNo,
                              @RequestParam("chargeType") String chargeType,
                              @RequestParam("chargeData") int chargeData,
                              @RequestParam("uid") String uid) {
        JSONObject msg = (JSONObject) chargeService.startCharge(deviceId, gunNo, chargeType, chargeData, uid);
        return this.setSuccessMessage(request, response, msg);
    }

    @RequestMapping(value = "/endCharge", method = RequestMethod.POST)
    public String endCharge(HttpServletRequest request, HttpServletResponse response,
                            @RequestParam("orderId") String orderId) {
        JSONObject msg = (JSONObject) chargeService.endCharge(orderId, "01");
        return this.setSuccessMessage(request, response, msg);
    }

    @RequestMapping(value = "/getChargeData", method = RequestMethod.POST)
    public String getChargeData(HttpServletRequest request, HttpServletResponse response,
                                @RequestParam("orderId") String orderId) {
        JSONObject msg = (JSONObject) chargeService.getChargeData(orderId);
        return this.setSuccessMessage(request, response, msg);
    }
}