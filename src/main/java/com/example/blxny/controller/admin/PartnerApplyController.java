package com.example.blxny.controller.admin;/*
 *项目名: blxny
 *文件名: PartnerApplyController
 *创建者: SCH
 *创建时间:2019/5/6 14:49
 *描述: TODO
 */

import com.example.blxny.service.admin.PartnerApplyService;
import com.example.blxny.util.BaseController;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@CrossOrigin
@RestController
public class PartnerApplyController extends BaseController {

    @Autowired
    private PartnerApplyService partnerApplyService;

    @GetMapping("/partnerApplys")
    public String tableData(HttpServletRequest request, HttpServletResponse response,
                            @RequestParam(value = "pageNumber", defaultValue = "1") Integer pageNumber,
                            @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize) {
        PageInfo pageInfo = partnerApplyService.findPage(pageNumber, pageSize);
        return this.setSuccessMessage(request, response, pageInfo);
    }

    @PostMapping("/partnerApply")
    public String post(HttpServletRequest request, HttpServletResponse response,
                       @RequestParam("id") String id,
                       @RequestParam("state") Integer state,
                       @RequestParam("type") Integer type) {
        if (state != 1 && state != 2 && type != 1 && type != 2) {
            return super.setFailureMessage(request, response, "参数数值异常");
        }
        String msg = partnerApplyService.updatePartnerAppoly(id, state, type);
        if ("处理成功".equals(msg)) {
            return super.setSuccessMessage(request, response, msg);
        } else {
            return super.setFailureMessage(request, response, msg);
        }
    }
}