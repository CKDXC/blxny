package com.example.blxny.controller.admin;

import com.example.blxny.service.admin.IndexInforationService;
import com.example.blxny.util.BaseController;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@CrossOrigin
@RestController
@RequestMapping("/index")
public class IndexInformation extends BaseController {
    @Resource(name = "information")
    private IndexInforationService informationService;

    @RequestMapping(value = "/information", method = RequestMethod.POST)
    public String post(HttpServletRequest request, HttpServletResponse response,
                       @RequestParam("info") String info,
                       @RequestParam("time") String time) {
        String message = informationService.addNewInformation(info, time);
        if (message.equals("OK")) {
            return this.setSuccessMessage(request, response, message);
        } else {
            return this.setFailureMessage(request, response, message);
        }
    }

    @PutMapping("/information/{id}")
    public String put(HttpServletRequest request, HttpServletResponse response,
                      @PathVariable("id") Integer id) {
        if (informationService.oneInformationDown(id) > 0) {
            return setSuccessMessage(request, response, "OK");
        } else {
            return setFailureMessage(request, response, "ERROR");
        }
    }

    @GetMapping("/information")
    public String get(HttpServletRequest request, HttpServletResponse response,
                      @RequestParam("pageSize") Integer pageSize,
                      @RequestParam("pageNumber") Integer pageNumber) {
        return this.setSuccessMessage(request, response, informationService.getAdminInforMation(pageSize, pageNumber));
    }

    /**以下为极光推送，极光推送仅只能提供推送功能*/
    @PostMapping("/JPinformation")
    public String JPinformation(HttpServletRequest request, HttpServletResponse response,
                                @RequestParam("info") String info){
        return this.setSuccessMessage(request,response,informationService.AdminJPinformation(info));
    }
}
