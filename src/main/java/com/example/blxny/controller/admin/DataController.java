package com.example.blxny.controller.admin;

import com.example.blxny.service.admin.DataService;
import com.example.blxny.util.BaseController;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@CrossOrigin
@RestController
@RequestMapping("/data")
public class DataController extends BaseController {
    @Resource(name = "DataService")
    private DataService dataService;

    @GetMapping("/CityStationNumber")
    public String getCityStation(HttpServletRequest request, HttpServletResponse response) {
        return setSuccessMessage(request, response,dataService.getCityStationNumber());
    }

    @GetMapping("/settlement/{city}")
    public String settlement(HttpServletRequest request, HttpServletResponse response,
                             @PathVariable("city") String city,
                             @RequestParam("startDay")String start,
                             @RequestParam("endDay")String end){
        return setSuccessMessage(request, response,dataService.getSettlement(city,start,end));
    }

}
