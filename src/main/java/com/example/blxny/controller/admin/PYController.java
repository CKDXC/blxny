package com.example.blxny.controller.admin;

import com.example.blxny.service.admin.NewsService;
import com.example.blxny.util.BaseController;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
@CrossOrigin
public class PYController extends BaseController {//爬虫管理
    @Resource(name = "NewsService")
    private NewsService newsService;

    @PutMapping("/PYnews")
    public String put(HttpServletRequest request, HttpServletResponse response) throws InterruptedException {
        if (newsService.flashNewS()) {
            return this.setSuccessMessage(request, response, "OK");
        } else {
            return this.setFailureMessage(request, response, "ERROR");
        }
    }

    @PostMapping("PYnews")
    public String put(HttpServletRequest request, HttpServletResponse response,
                      @RequestParam("time")String time){
        String message=newsService.newNews(time);
        if(!message.equals("OK")){
            return this.setFailureMessage(request,response,message);
        }else{
            return this.setSuccessMessage(request,response, "创建成功，将在"+time+"分钟后执行");
        }
    }

    @DeleteMapping("PYnews")
    public String del(HttpServletRequest request, HttpServletResponse response){
        String message=newsService.delNews();
        if(!message.equals("OK")){
            return this.setFailureMessage(request,response,message);
        }else{
            return this.setSuccessMessage(request,response, message);
        }
    }

    @GetMapping("PYnews")
    public String get(HttpServletRequest request, HttpServletResponse response){
        String message=newsService.getNews();
        if("ERROR".equals(message)){
            return this.setFailureMessage(request,response,"缓存错误");
        }else{
            return this.setSuccessMessage(request,response,newsService.getNews());}
    }
}
