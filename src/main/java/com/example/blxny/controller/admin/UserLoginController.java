package com.example.blxny.controller.admin;

import com.example.blxny.model.City;
import com.example.blxny.service.admin.UserLoginService;
import com.example.blxny.util.BaseController;
import com.sun.org.apache.regexp.internal.RE;
import net.sf.json.JSON;
import net.sf.json.JSONObject;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@CrossOrigin
@RestController
@RequestMapping("/blxny/adminUser")
public class UserLoginController extends BaseController {
    @Resource(name = "adminUserLogin")
    private UserLoginService service;

    /**
     * admin登录
     */
    @RequestMapping("/login")
    public Object login(HttpServletRequest request, HttpServletResponse response,
                        @RequestParam("username") String username,
                        @RequestParam("password") String password) {
        JSONObject js = service.AdminUserLogin(username, password);
        if (String.valueOf(js.get("result")).equals("0")) {
            return super.setSuccessMessage(request, response,js);
        } else {
            return super.setFailureMessage(request, response,js);
        }
    }








}