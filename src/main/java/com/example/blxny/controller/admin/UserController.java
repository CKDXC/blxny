package com.example.blxny.controller.admin;
/*
 *项目名: blxny
 *文件名: UserController
 *创建者: SCH
 *创建时间:2019/5/6 18:35
 *描述: TODO
 */

import com.example.blxny.service.admin.UserService;
import com.example.blxny.util.BaseController;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@CrossOrigin
@RestController
public class UserController extends BaseController {

    @Resource(name = "adminUserService")
    private UserService userService;

    @RequestMapping("/user")
    public String tableData(HttpServletRequest request, HttpServletResponse response,
                            @RequestParam(value = "pageNumber", defaultValue = "1") Integer pageNumber,
                            @RequestParam(value = "pageSize", defaultValue = "10") Integer pagrSize) {
        PageInfo pageInfo = userService.findPage(pageNumber, pagrSize);
        return this.setSuccessMessage(request, response, pageInfo);
    }

    /**
     * 0级账户(master)等级赋予普通账户
     */
    @RequestMapping(value = "/master/authorization", method = RequestMethod.POST)
    public Object authorization(HttpServletRequest request, HttpServletResponse response,
                                @RequestParam("master") String master,
                                @RequestParam("gave") String gave,
                                @RequestParam("level") Integer level) {
        if (level.equals(1) || level.equals(2)) {
            String message=String.valueOf(userService.authorization(master, gave, level));
            if ("修改成功".equals(message)) {
                return this.setSuccessMessage(request, response, "OK");
            }else {
                return this.setFailureMessage(request, response, message);
            }
        } else {
            return this.setFailureMessage(request, response, "请正确填写权限等级");
        }
    }
}