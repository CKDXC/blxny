package com.example.blxny.controller.admin;

import com.example.blxny.service.admin.RuleService;
import com.example.blxny.util.BaseController;
import com.github.pagehelper.PageInfo;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.example.blxny.model.vo.RuleVo;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/*
 *项目名: blxny
 *文件名: RuleController
 *创建者: SCH
 *创建时间:2019/5/14 10:40
 *描述: TODO
 */
@CrossOrigin
@RestController
public class RuleController extends BaseController {

    @Resource(name = "adminRuleService")
    private RuleService ruleService;

    @GetMapping("/rule")
    public String tableData(HttpServletRequest request, HttpServletResponse response,
                            @RequestParam("pageNumber") Integer pageNumber,
                            @RequestParam("pageSize") Integer pageSize) {
        PageInfo pageInfo = ruleService.findPage(pageNumber, pageSize);
        return super.setSuccessMessage(request, response, pageInfo);
    }

    @PostMapping("/rule")
    public String post(HttpServletRequest request, HttpServletResponse response,
                       RuleVo ruleVo) {
        String message = (String) ruleService.addNewRule(ruleVo);
        if (message.equals("OK")) {
            return this.setSuccessMessage(request, response, message);
        } else {
            return this.setSuccessMessage(request, response, message);
        }
    }

    @PutMapping("/rule/{stationId}/{type}")
    public String put(HttpServletRequest request, HttpServletResponse response,
                      @RequestParam("type") Integer type,
                      @RequestParam("number") Double number,
                      @PathVariable("stationId") String id,
                      @PathVariable("type") String type2) {
        String message = (String) ruleService.changeRule(type, number, id, type2);
        if (message.equals("OK")) {
            return this.setSuccessMessage(request, response, message);
        } else {
            return this.setSuccessMessage(request, response, message);
        }
    }
}
