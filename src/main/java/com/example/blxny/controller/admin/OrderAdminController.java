package com.example.blxny.controller.admin;

import com.example.blxny.service.admin.OrderService;
import com.example.blxny.util.BaseController;
import com.fasterxml.jackson.databind.ser.Serializers;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
@CrossOrigin
public class OrderAdminController extends BaseController {
    @Resource(name = "adminOrder")
    private OrderService orderService;

    /**
     * 查询所有订单
     */
    @GetMapping("/Order")
    public String get(HttpServletRequest request, HttpServletResponse response,
                      @RequestParam("pageNumber") Integer pageNumber,
                      @RequestParam("pageSize") Integer pageSize) {
        return this.setSuccessMessage(request,response,orderService.findAllOrderAdmin(pageNumber,pageSize));
    }

    @GetMapping("/Order/{id}")
    public String idGet(HttpServletRequest request, HttpServletResponse response,
                        @RequestParam("pageNumber") Integer pageNumber,
                        @RequestParam("pageSize") Integer pageSize,
                        @PathVariable("id")String id){
        return this.setSuccessMessage(request,response,orderService.findDeviceOrder(pageNumber,pageSize,id));
    }
}
