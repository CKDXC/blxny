package com.example.blxny.controller.admin;

import com.example.blxny.model.Rule;
import com.example.blxny.model.Station;
import com.example.blxny.service.admin.StationService;
import com.example.blxny.util.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@CrossOrigin
@RestController
public class StationController extends BaseController {
    @Autowired
    private StationService stationService;

    /*** 查询所有驿站信息并分页*/
    @GetMapping("/station")
    public Object tableData(HttpServletRequest request, HttpServletResponse response,
                            @RequestParam(value = "stationPageSize", defaultValue = "10") Integer pageNumber,
                            @RequestParam(value = "stationPageNumber", defaultValue = "1") Integer pageSize) {
        return this.setSuccessMessage(request, response, stationService.adminFindAllStation(pageSize, pageNumber));
    }

    /*** 查询驿站计费信息，使用PowerController  price方法*/

    /*** 新增驿站（单独增加驿站主页端将无法查看）*/
    @PostMapping(value = "/station")
    public Object post(HttpServletRequest request, HttpServletResponse response,
                       Station station) {
        boolean filter = (station.getAddressInfo().indexOf("省") != -1 && station.getAddressInfo().indexOf("市") != -1&&
                station.getAddressInfo().indexOf("区") != -1);
        String message = (String) stationService.adminAddNewStation(station);
        if (!filter) {
            return this.setFailureMessage(request, response, "请输入有效的地址");
        } else {
            if (message.equals("OK")) {
                return this.setSuccessMessage(request, response, message);
            } else {
                return this.setFailureMessage(request, response, message);
            }
        }
    }

    /**修改驿站基本信息*/
    @PutMapping("/station/{stationId}")
    public Object put(HttpServletRequest request, HttpServletResponse response,
                       Station station){
        String message= (String) stationService.updateBasicStationInfo(station);
        if (message.equals("OK")) {
            return this.setSuccessMessage(request, response, message);
        } else {
            return this.setFailureMessage(request, response, message);
        }
    }

}
