package com.example.blxny.controller.admin;

import com.example.blxny.service.admin.CouponService;
import com.example.blxny.util.BaseController;
import com.github.pagehelper.PageInfo;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/*
 *项目名: blxny
 *文件名: CouponController
 *创建者: SCH
 *创建时间:2019/5/14 9:26
 *描述: TODO
 */
@CrossOrigin
@RestController
public class CouponController extends BaseController {

    @Resource(name = "adminCouponService")
    private CouponService couponService;

    @GetMapping("/coupon")
    public String tableData(HttpServletRequest request, HttpServletResponse response,
                            @RequestParam(value = "pageNumber", defaultValue = "1") Integer pageNumber,
                            @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize) {
        PageInfo pageInfo = couponService.findPage(pageNumber, pageSize);
        return super.setSuccessMessage(request, response, pageInfo);
    }
}
