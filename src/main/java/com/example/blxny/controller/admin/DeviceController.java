package com.example.blxny.controller.admin;


import com.example.blxny.model.vo.DeviceVO;
import com.example.blxny.service.admin.DeviceService;
import com.example.blxny.util.BaseController;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@CrossOrigin
@RestController
public class DeviceController extends BaseController {

    @Resource(name = "adminDeviceService")
    private DeviceService deviceService;


    /**
     * 查询所有设备信息并分页
     */
    @GetMapping("/device")
    public Object device(HttpServletRequest request, HttpServletResponse response,
                         @RequestParam(value = "devicePageSize", defaultValue = "10") Integer pageNumber,
                         @RequestParam(value = "devicePageNumber", defaultValue = "1") Integer pageSize) {
        return this.setSuccessMessage(request, response, deviceService.adminFindAllDevice(pageSize, pageNumber));
    }

    /*** 驿站内新增设备*/
    @PostMapping(value = "/device")
    public Object post(HttpServletRequest request, HttpServletResponse response,
                       @RequestBody DeviceVO deviceVO) {
        if (deviceService.addDevice(deviceVO) > 0) {
            return this.setSuccessMessage(request, response, "OK");
        } else {
            return this.setFailureMessage(request, response, "ERROR");
        }
    }

    @RequestMapping(value = "/device/{deviceId}", method = RequestMethod.PUT)
    public String postUpdate(HttpServletRequest request, HttpServletResponse response,
                             @PathVariable String deviceId,
                             @RequestBody DeviceVO deviceVO) {
        deviceVO.setDeviceId(deviceId);
        String msg = deviceService.updateDevice(deviceVO);
        if ("修改成功".equals(msg)) {
            return super.setSuccessMessage(request, response, msg);
        } else {
            return super.setFailureMessage(request, response, msg);
        }
    }

}
