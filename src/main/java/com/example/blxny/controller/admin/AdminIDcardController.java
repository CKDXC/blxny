package com.example.blxny.controller.admin;

import com.example.blxny.service.admin.IDcardService;
import com.example.blxny.util.BaseController;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@CrossOrigin
@RestController
public class AdminIDcardController extends BaseController {
    @Resource(name = "IDcard")
    private IDcardService iDcardService;

    @RequestMapping(value = "/IDcard" ,method = RequestMethod.GET)
    public String get(HttpServletRequest request, HttpServletResponse response,
                      @RequestParam("pageNumber")Integer pageNumber,
                      @RequestParam("pageSize")Integer pageSize,
                      @RequestParam("type")Integer type){
            return this.setSuccessMessage(request,response,iDcardService.findAllIdcardApproval(type,pageSize,pageNumber));
    }
    @PutMapping("/IDcard/{uuid}")
    public String put(HttpServletRequest request, HttpServletResponse response,
                      @PathVariable("uuid")String uuid,
                        @RequestParam("type")Integer type) {
        if (iDcardService.ApprovalIdCard(uuid, type).equals("OK")) {
            return  this.setSuccessMessage(request, response, "OK");
        } else {
            return this.setFailureMessage(request, response, "ERROR");
        }
    }
}
