package com.example.blxny.controller.admin;

import com.example.blxny.service.admin.partnerUpdateService;
import com.example.blxny.util.BaseController;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@CrossOrigin
@RestController
public class StationUpdateController extends BaseController {

    @Resource(name = "partnerUpdateService")
    private partnerUpdateService partnerUpdateService;

    /**
     *查看合伙人驿站扩建申请单
     */
    @GetMapping("/partnerUpdate")
    public Object get(HttpServletRequest request, HttpServletResponse response,
                      @RequestParam(value = "pageNumber", defaultValue = "1") Integer pageNumber,
                      @RequestParam(value = "type") Integer type) {
        return this.setSuccessMessage(request,response,partnerUpdateService.findPartnerUpdate(pageNumber,type));
    }

    /**修改合伙人驿站扩建状态*/
    @RequestMapping(value = "/partnerUpdate/{updateId:^[0-9]*$}",method = RequestMethod.PUT)
    public Object put(HttpServletRequest request, HttpServletResponse response,
                      @PathVariable(value = "updateId") String updateId,
                      @RequestParam(value = "state") Integer state) {
            if(state==1|state==2){
                return this.setSuccessMessage(request,response,(String)partnerUpdateService.changPartnerUpdate(state,updateId));
            }else{
                return this.setFailureMessage(request,response,"请输入正确的state");
            }

    }
}
