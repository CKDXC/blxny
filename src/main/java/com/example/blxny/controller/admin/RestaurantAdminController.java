package com.example.blxny.controller.admin;

import com.example.blxny.model.Hotel;
import com.example.blxny.model.CateringProduct;
import com.example.blxny.model.Restaurant;
import com.example.blxny.model.dto.CateringProductDTO;
import com.example.blxny.model.Shop;
import com.example.blxny.service.admin.RestaurantService;
import com.example.blxny.tool.UploadTool;
import com.example.blxny.util.BaseController;
import com.github.pagehelper.PageInfo;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileNotFoundException;
import java.util.List;


@CrossOrigin
@RestController
public class RestaurantAdminController extends BaseController {
    @Resource(name = "Restaurant")
    private RestaurantService restaurantService;

    @PostMapping("/changeQRCode")//调整二维码利率
    public String post(HttpServletRequest request, HttpServletResponse response,
                       @RequestParam("number") String number) {
        if (restaurantService.change(number)) {
            return setSuccessMessage(request, response, "OK");
        } else {
            return setFailureMessage(request, response, "ERROR");
        }
    }

    @GetMapping("/changeQRCode")//查看当前二维码利率
    public String get(HttpServletRequest request, HttpServletResponse response) {
        return setSuccessMessage(request, response, restaurantService.get());
    }

    @PostMapping("/list")//添加列表信息
    public String postList(HttpServletRequest request, HttpServletResponse response,
                           Restaurant restaurant) throws FileNotFoundException {
        if (restaurantService.addNewStore(restaurant).equals("OK")) {
            return setSuccessMessage(request, response, "OK");
        } else {
            return setFailureMessage(request, response, "ERROR");
        }
    }

    @PutMapping("/list")
    public String putList(HttpServletRequest request, HttpServletResponse response,
                          Restaurant restaurant) {
        String message = restaurantService.changeStore(restaurant);
        if (message.equals("OK")) {
            return setSuccessMessage(request, response, message);
        } else {
            return setFailureMessage(request, response, message);
        }
    }

    @GetMapping("/hotel")
    public String getHotel(HttpServletRequest request, HttpServletResponse response,
                           @RequestParam("message") String message) {
        List<Hotel> hotels = restaurantService.listHotel(message);
        if (hotels == null || hotels.size() == 0) {
            return setFailureMessage(request, response, "未找到");
        } else {
            return setSuccessMessage(request, response, hotels);
        }
    }

    @DeleteMapping("/list/{id}")//下架一个店铺
    public String delList(HttpServletRequest request, HttpServletResponse response,
                          @PathVariable("id") Integer id) {
        if (restaurantService.delList(id)) {
            return setSuccessMessage(request, response, "OK");
        } else {
            return setFailureMessage(request, response, "ERROR");
        }

    }

    @PostMapping("/hotel")//通过id新增一个酒店的图片
    public String postHotel(HttpServletRequest request, HttpServletResponse response,
                            @RequestParam("img") String img,
                            @RequestParam("info") String info,
                            @RequestParam("id") Integer id) throws FileNotFoundException {
        String message = restaurantService.addHotelImg(img, info, id);
        if (message.equals("OK")) {
            return setSuccessMessage(request, response, message);
        } else {
            return setFailureMessage(request, response, message);
        }
    }

    @PostMapping("/shop/{restaurantId}")//新增一个商店商品
    public String postShop(HttpServletRequest request, HttpServletResponse response,
                           @PathVariable("restaurantId")Integer restaurantId,
                           Shop shop) throws FileNotFoundException {
        String message=restaurantService.addNewShop(shop,restaurantId);
        if (message.equals("OK")) {
            return setSuccessMessage(request, response, message);
        } else {
            return setFailureMessage(request, response, message);
        }
    }

    @GetMapping("/catering/{id}")
    public String coffee(HttpServletRequest request, HttpServletResponse response,
                         @PathVariable("id") Integer id,
                         @RequestParam("pageNumber") Integer pageNumber,
                         @RequestParam("pageSize") Integer pageSize) {
        PageInfo pageInfo = restaurantService.findPage(id, pageNumber, pageSize);
        return super.setSuccessMessage(request, response, pageInfo);
    }

    @PostMapping("/catering")
    public String coffeeCateringPost(HttpServletRequest request, HttpServletResponse response,
                                     CateringProduct cateringProduct,
                                     MultipartFile multipartFile) {

        String url = "";
        if (null == multipartFile) {
            return super.setFailureMessage(request, response, "图片上传失败");
        }
        try {
            url = UploadTool.upFile(multipartFile, "catering", "jpg", System.currentTimeMillis() + "");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        cateringProduct.setImg(url);
        String msg = restaurantService.addCateringProduct(cateringProduct);
        return super.setSuccessMessage(request, response, msg);
    }

    @PutMapping("/catering")
    public String coffeeCateringUpdate(HttpServletRequest request, HttpServletResponse response,
                                       CateringProductDTO cateringProductDTO,
                                       MultipartFile multipartFile) {
        String url = "";
        if (null == multipartFile) {
            return super.setFailureMessage(request, response, "图片上传失败");
        }
        try {
            url = UploadTool.upFile(multipartFile, "catering", "jpg", System.currentTimeMillis() + "");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        cateringProductDTO.setImg(url);
        String msg = restaurantService.updateCateringProduct(cateringProductDTO);
        return super.setSuccessMessage(request, response, msg);
    }

    @DeleteMapping("/shop/{id}")//下架一个商品
    public String delShop(HttpServletRequest request, HttpServletResponse response,
                          @PathVariable("id")Integer id){
        String message=restaurantService.delShop(id);
        if (message.equals("OK")) {
            return setSuccessMessage(request, response, message);
        } else {
            return setFailureMessage(request, response, message);
        }
    }
}
