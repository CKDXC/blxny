package com.example.blxny.controller.admin;

import com.example.blxny.model.vo.BankPosVO;
import com.example.blxny.service.admin.BankPosService;
import com.example.blxny.service.admin.DeviceService;
import com.example.blxny.util.BaseController;
import com.github.pagehelper.PageInfo;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@CrossOrigin
@RestController
public class BankPosController extends BaseController {
    @Resource(name = "posService")
    private BankPosService bankPosService;

    /**
     * 提现申请查询
     * @param request
     * @param response
     * @param state
     * @param phone
     * @param number
     * @return
     */
    @GetMapping("/bankPos")
    public Object get(HttpServletRequest request, HttpServletResponse response,
                      @RequestParam("state") Integer state,
                      @RequestParam(value = "phone", defaultValue = "0") String phone,
                      @RequestParam(value = "pageNumber", defaultValue = "1") Integer number) {
        PageInfo<BankPosVO> message = null;
        switch (state) {
            case 1://查询所有订单
                message = bankPosService.findAllBankPos(number, 9, phone);
                break;
            case 2://查询已审批
                message = bankPosService.findAllBankPos(number, 0, phone);
                break;
            case 3://查询未审批
                message = bankPosService.findAllBankPos(number, 1, phone);
                break;
            case 4://按电话号码查询
                message = bankPosService.findAllBankPos(number, 8, phone);
                break;

        }
        return super.setSuccessMessage(request, response, message);
    }
/**
 * 提现申请审批
 */
        @RequestMapping(value = "/bankPos/{posId}",method = RequestMethod.PUT)
        public Object get(HttpServletRequest request, HttpServletResponse response,
                                 @PathVariable("posId") String posId){
            String message=bankPosService.sureBankPos(posId);
            if (message.equals("OK")){
                return this.setSuccessMessage(request,response,message);
            }else{
                return this.setFailureMessage(request,response,message);}
        }
}
