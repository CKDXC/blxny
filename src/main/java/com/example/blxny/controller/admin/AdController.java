package com.example.blxny.controller.admin;

import com.example.blxny.model.Ad;
import com.example.blxny.service.admin.AdService;
import com.example.blxny.util.BaseController;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileNotFoundException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@CrossOrigin
@RestController
public class AdController extends BaseController {
    @Resource(name = "AD")
    private AdService adService;
    @RequestMapping(value = "/AD",method = RequestMethod.POST)
    public Object post(HttpServletRequest request, HttpServletResponse response,
                       @RequestParam("imgFile")String file,
                       @RequestParam("endTime")String endtime,
                       Ad ad) throws FileNotFoundException, ParseException {
        Date date=new SimpleDateFormat("yyyy-MM-dd").parse(endtime);
        ad.setEndtime(date);
        String message= (String) adService.addNewAD(file,ad);
        if(message.equals("ERROR")){
            return setFailureMessage(request,response,message);
        }else {
            return setSuccessMessage(request,response,message);
        }
    }

    @DeleteMapping("/AD/{id}")
    public String del(HttpServletRequest request, HttpServletResponse response,@PathVariable("id")Integer id){
        if(adService.delAD(id).equals("ERROR")){
                return setFailureMessage(request,response,"ERROR");
            }else {
                return setSuccessMessage(request,response,"OK");
            }
        }
    }

