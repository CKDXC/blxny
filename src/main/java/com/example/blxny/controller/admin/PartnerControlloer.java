package com.example.blxny.controller.admin;

import com.example.blxny.service.admin.PartnerService;
import com.example.blxny.util.BaseController;
import com.github.pagehelper.PageInfo;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/*
 *项目名: blxny
 *文件名: PartnerControlloer
 *创建者: SCH
 *创建时间:2019/5/7 10:43
 *描述: TODO
 */
@CrossOrigin
@RestController
public class PartnerControlloer extends BaseController {

    @Resource(name = "adminPartnerService")
    private PartnerService partnerService;

    @GetMapping("/partner")
    public String tableData(HttpServletRequest request, HttpServletResponse response,
                            @RequestParam(value = "pageNumber",defaultValue = "1") Integer pageNumber,
                            @RequestParam(value = "pageSize", defaultValue = "10") Integer pagrSize) {
        PageInfo pageInfo = partnerService.findPage(pageNumber, pagrSize);
        return super.setSuccessMessage(request, response, pageInfo);
    }


}
