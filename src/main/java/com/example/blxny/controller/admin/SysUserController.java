package com.example.blxny.controller.admin;

import com.example.blxny.model.vo.UserSysRoleVO;
import com.example.blxny.service.admin.SysUserService;
import com.example.blxny.util.BaseController;
import com.github.pagehelper.PageInfo;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/*
 *项目名: blxny
 *文件名: SysUserController
 *创建者: SCH
 *创建时间:2019/5/7 11:25
 *描述: TODO
 */
@CrossOrigin
@RestController
public class SysUserController extends BaseController {

    @Resource(name = "adminSysUserService")
    private SysUserService sysUserService;

    @GetMapping("/sysUser")
    public String tableData(HttpServletRequest request, HttpServletResponse response,
                            @RequestParam(value = "pageNumber", defaultValue = "1") Integer pageNumber,
                            @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize) {
        PageInfo<UserSysRoleVO> pageInfo = sysUserService.findPage(pageNumber, pageSize);
        return super.setSuccessMessage(request, response, pageInfo);
    }

    @PostMapping("/sysUser")
    public String update(HttpServletRequest request, HttpServletResponse response,
                         @RequestParam("uid") String uid,
                         @RequestParam("oldPwd") String oldPwd,
                         @RequestParam("newPwd") String newPwd) {
        String msg = sysUserService.updatePwd(uid, oldPwd, newPwd);
        if ("修改密码成功".equals(msg)) {
            return super.setSuccessMessage(request, response, msg);
        } else {
            return super.setFailureMessage(request, response, msg);
        }
    }
}
