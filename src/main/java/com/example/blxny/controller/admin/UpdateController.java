package com.example.blxny.controller.admin;

import com.example.blxny.service.admin.UpdateVersionService;
import com.example.blxny.util.BaseController;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileNotFoundException;

@CrossOrigin
@RestController
public class UpdateController extends BaseController {
    @Resource(name = "UpdateVersion")
    private UpdateVersionService updateVersionService;

    @GetMapping("/update/information")
    public Object get(HttpServletRequest request, HttpServletResponse response) {
        return this.setSuccessMessage(request, response, updateVersionService.updateVersion());
    }

    @DeleteMapping("/update/information/{version}")
    public String del(HttpServletRequest request, HttpServletResponse response,
                      @PathVariable("version") String version) throws FileNotFoundException {
        if(updateVersionService.delVersion(version).equals("OK")){
            return this.setSuccessMessage(request,response,"OK");
        }else{
            return this.setFailureMessage(request,response,"ERROR");
        }
    }
}
