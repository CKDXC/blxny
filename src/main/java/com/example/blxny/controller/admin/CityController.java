package com.example.blxny.controller.admin;

import com.example.blxny.model.City;
import com.example.blxny.service.admin.CityServcie;
import com.example.blxny.util.BaseController;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.security.MessageDigest;

@CrossOrigin
@RestController
public class CityController extends BaseController {

    @Resource(name = "CityAdmin")
    private CityServcie cityServcie;

    /**
     * 添加城市
     */
    @PostMapping("city")
    public Object post(HttpServletRequest request, HttpServletResponse response,
                       City city) {
        boolean filter = (city.getProvince().indexOf("省") != -1 && city.getCity().indexOf("市") != -1 &&
                city.getArea().indexOf("区") != -1);
        if (filter) {
            String mssage = (String) cityServcie.AddNewCity(city);
            if (mssage.equals("ERROR")) {
                return setFailureMessage(request, response, mssage);
            } else {
                return setSuccessMessage(request, response, mssage);
            }
        } else {
            return setFailureMessage(request, response, "ERROR");
        }
    }

    /**
     * 删除城市
     */
    @DeleteMapping("city/{id}")
    public String del(HttpServletRequest request, HttpServletResponse response,
                      @PathVariable("id") String id) {
        if(cityServcie.delCity(id)>0){
            return  this.setSuccessMessage(request,response,"OK");
        }else {
            return this.setFailureMessage(request,response,"ERROR");
        }
    }
}
