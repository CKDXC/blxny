package com.example.blxny.controller;


import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.domain.AlipayTradeAppPayModel;
import com.alipay.api.internal.util.AlipaySignature;
import com.alipay.api.request.AlipaySystemOauthTokenRequest;
import com.alipay.api.request.AlipayTradeAppPayRequest;
import com.alipay.api.request.AlipayUserInfoShareRequest;
import com.alipay.api.response.AlipaySystemOauthTokenResponse;
import com.alipay.api.response.AlipayTradeAppPayResponse;
import com.alipay.api.response.AlipayUserInfoShareResponse;
import com.example.blxny.model.User;
import com.example.blxny.service.UserLoginService;
import com.example.blxny.util.BaseController;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

//import com.alipay.sdk.app.EnvUtils;

/**
 * 支付宝专用
 * 此类大多未封装，请勿修改参数
 */
@CrossOrigin
@RestController
@RequestMapping("/pay")
public class PayController extends BaseController {
    @Autowired
    private UserLoginService service;
    private static final String privateKey = "MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCdhVrab5dN+SyHFjQWsfSyLdsGWIU+oMBqB6Q5gkq0bDOhhtLkuY6LlLVlUSYlTdFx+CSYFLKCoX2B7GQwHgp3KkEbSCOTMTunm3MDREldvrtsApUUqUKgoi2A973bxJFkChuc7LMQDR3pyEOimBg007dio57ZQ8OFyY1n+D7CwxkXytkpmMwVngFpqgm1I4A6dsXLzLvv7AIa7Ozmeco7ooBjJzaxr220yXdMNwdZ6TyS0N6XekCDXl1UUDuTtJ8OpjewHjXHkNHwwEsFXlEPj/itCHUCdlCGIzEbTbu6sPqsEgnlR82kv6liCmNxUF3Tj1xTWUQVSdnaW0Hxa5sbAgMBAAECggEAaB3a/YLA6aMHUYrVhyK88IDqwnkEKW8rf2npnLGIG1VhkySAhPo5Q/1ZIMzQf9u90q0H0xXrH/kJ8jSDzGIu3LErM5MtIH9sRM606LXcIICPdUcHrw5poa9QkGwwK/DzgMJFmiXB/ZwN55adTf1mEpTWI/auakEmHWopc01cfnJW3z8lN33klH/ttXqSw6uAegXFX92y39vM/x2M6VEIPk6JiQjQiiHzxqr0z0KCnixisz5USUQeRK32Fpp1Lgjyx2LHD1YEAiiFrCKIG6PwcRRols4F+RBaFNLwYLnh4/yuWsxP0PiOnf6z4mwpanLUljtk3DkeaqkI/saHNjdrgQKBgQDikJVmLWN6p+prpW1y6jJka4ygxJimB/m6ORbIg1v42eG2w2f3qI0JyrUbIGUav+Xd+wzRr8HVqxHSeVBkyberFredy0LpOqCNl3oTSC9Jl9JUULXwPZ/hqiK8lhSTKqTfdUOMNPWAnrOwVrHRHCFlqquZNTHuvQAgy6uayKI+uwKBgQCx/GlYnKoGKMFFVoCM09EWq2g9Ra/qMCXtX9RwoUyvYYAQiDlbUHZngPzIePtvbakD/Hlpoc4k/I9gpPsQsK6AcSr/GrA7fm7L5EJmCQSMleMx23k/KzqGtE4+k+/lqipHlntoWMuejT406mSLuNUxkqnB0eTYM1rmFEeU7dy/IQKBgQCozs1h6iOaug3h0sovuut9At0u1AxgeMTKycp979HRIpYYlJOcZUtgplY2Nfbe7WgZ8vnkKi4kQ2fuCFGsJtfgc/avbUkXeYUyulBMmZYSITqnc9N7go7jlt524OPHmxhpHngLuVHKWXTMyGuIwIrpszZT2tyvYsb7qITduOuaDQKBgEpC1nFybD5Fot4bSWd4uTSML+HgwZUkoa543Vpg8m36cER1kfjXiY+F8fJzB/Vl+67+GGpKaIhmGCxqqZY9IZHVperqZ71q8y9+/XU1LgC6SSD0QZJppvGtKThS4TUh2qIj1zmiUtLEe7/4TBs/8zPLzahguuq6AqMcswEgfYoBAoGBAOBgPXg30B4GgpePqLiaPW8rFS0rcNMiYnR81ln8ETd9t/dwqkPKU+Lnc8ubtJJEEcJ4QRRoiRzs5+5lMOeVX7ul62mWOELbjau1jIpE08ovYslFfE6dF7WC7mG6XsRaidHt6x10loSfgyZSvRpF71PW9X9ONejB+uUYeU5jJ5b1";
    private static final String publickKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAnYVa2m+XTfkshxY0FrH0si3bBliFPqDAagekOYJKtGwzoYbS5LmOi5S1ZVEmJU3RcfgkmBSygqF9gexkMB4KdypBG0gjkzE7p5tzA0RJXb67bAKVFKlCoKItgPe928SRZAobnOyzEA0d6chDopgYNNO3YqOe2UPDhcmNZ/g+wsMZF8rZKZjMFZ4BaaoJtSOAOnbFy8y77+wCGuzs5nnKO6KAYyc2sa9ttMl3TDcHWek8ktDel3pAg15dVFA7k7SfDqY3sB41x5DR8MBLBV5RD4/4rQh1AnZQhiMxG027urD6rBIJ5UfNpL+pYgpjcVBd049cU1lEFUnZ2ltB8WubGwIDAQAB";
    private static final String publickAliKEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAtNgqbNTnioQxb6zisCOYnUdrh3j2FfSrTNHTw25DsPxWALSKi/NSJ8JxJ9+3IM9eY9naDo6S0aTwSpLXChoMRnkqeardcolH9yBo2rpoJ9XazE2nPqfHbxaIwgYRs3fhsP9hv+n06S9IIdRD6zmwtnbUlrbjrJ5aV8AcBWixkSwdLW/+iQDaJO+PtHdvfnaG5tkJXVn0AckuA4XulBdP3+jZKuV/3utw+cBu53mCxfvobUJwFBtotv7x3KwtxoXE8zQkIV6xrIr1Zo4crGPwYnYnuIX4hxF2O+ELp7vlXABDZ4CW9IlIMBwQd0LML1ox90ULkwJPwyjlYreQnyXmMQIDAQAB";
    private static final String appid = "2019041663912129";
    private static final String pid = "2088431693199434";

    @RequestMapping(value = "/pay", method = RequestMethod.GET)//支付宝GET
    public void getAuthCode(HttpServletRequest request, HttpServletResponse response, String app_auth_code, String app_id, String source) {
        System.out.println(app_id);
        System.out.println(source);
        System.out.println(app_auth_code);
    }

    /**
     * 回调地址测试
     *
     * @param httpRequest
     * @param httpResponse
     */
    @RequestMapping(value = "/alibaba", method = RequestMethod.POST)
    public void url(HttpServletRequest httpRequest, HttpServletResponse httpResponse) {
        System.out.println("returnurl");
    }

    /**
     * 支付宝登录获取权限，返回inforStr
     *
     * @param httpRequest
     * @param httpResponse
     * @return
     * @throws AlipayApiException
     */
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public String login(HttpServletRequest httpRequest, HttpServletResponse httpResponse) throws AlipayApiException, UnsupportedEncodingException {
        String infoStr =
                "apiname=com.alipay.account.auth" +
                        "&method=alipay.open.auth.sdk.code.get" +
                        "&app_id=" + appid +
                        "&app_name=mc" +
                        "&biz_type=openservice" +
                        "&pid=" + pid +
                        "&product_id=APP_FAST_LOGIN" +
                        "&scope=kuaijie" +
                        "&target_id=123456" +
                        "&auth_type=AUTHACCOUNT" +
                        "&sign_type=RSA2";
        String sign = AlipaySignature.rsaSign(infoStr, privateKey, "UTF-8", "RSA2");
        infoStr = infoStr + "&sign=" + URLEncoder.encode(sign, "utf-8");
        //infoStr=infoStr+"&sing=ND54aM7eaeMwcp2WqxhWhRrX0PnBAIbbeFqe9jREL59C/fOi0xf4E5ai7BF0orOUfMbqrnf7Guo+4/vFuk8bSkVFw+IoORFSmWx0SAg7I+YuizO0CBqZrxy9falX526AjinqEhNSLn7lj/lC6yoiO6yUIF3MHkASMwL/vRRHzWHQCVph7KGz6NJa/xGTjGiHtmt5F4l84NH9RnPEthCnnnNPJJlBIQ3bC1wurcMTHX09go0SSMTST";
        // System.out.println("加签后"+infoStr);
        return super.setSuccessMessage(httpRequest, httpResponse, infoStr);

    }

    /**
     * 通过code获取支付宝 access_token
     * 返回user，如果有值直接登录，如果没有进入手机号注册，！！如果其中phone为”systemError“为系统错误
     *
     * @param code
     * @return
     */
    @RequestMapping(value = "/getCode", method = RequestMethod.POST)
    public Object getCode(HttpServletRequest httpRequest, HttpServletResponse httpResponse, @RequestParam("code") String code) {
        System.out.println(code);
        String uuid = null;
        User user = new User();
        User us = new User();
        try {
            AlipayClient alipayClient = new DefaultAlipayClient(
                    "https://openapi.alipay.com/gateway.do",
                    appid,//支付宝分配给开发者的应用ID
                    privateKey,
                    "json",
                    "utf-8",
                    publickAliKEY,
                    "RSA2");
            AlipaySystemOauthTokenRequest request = new AlipaySystemOauthTokenRequest();
            request.setGrantType("authorization_code");
            request.setCode(code);
            AlipaySystemOauthTokenResponse response = alipayClient.execute(request);
         /*   System.out.println("标记"+response.getBody());
            System.out.println("code"+response.getAccessToken());*/
            if (response.isSuccess()) {
                /*AlipayTokenDto alipayTokenDto = new AlipayTokenDto(response);*/
                /* System.out.println("成功了"+response.getAccessToken());*/
                uuid = getAlipayUserInfo(response.getAccessToken());
            } else {
                us.setPhone("systemError");
            }
        } catch (Exception e) {
            us.setPhone("systemError");
        } finally {
            user = service.zfblogin(uuid);
            if (user == null) {
                us.setPhone(uuid);
                return super.setSuccessMessage(httpRequest, httpResponse, us);
            }
        }
        return super.setSuccessMessage(httpRequest, httpResponse, user);
    }

    /**
     * 根据token获取用户信息
     *
     * @param alipayToken
     * @return
     */
    private String getAlipayUserInfo(String alipayToken) {
        try {
            AlipayClient alipayClient = new DefaultAlipayClient(
                    "https://openapi.alipay.com/gateway.do",
                    appid,//支付宝分配给开发者的应用ID
                    privateKey,
                    "json",
                    "utf-8",
                    publickAliKEY,
                    "RSA2");
            AlipayUserInfoShareRequest request = new AlipayUserInfoShareRequest();
            AlipayUserInfoShareResponse response = alipayClient.execute(request, alipayToken);
            if (response.isSuccess()) {
                /*AlipayUserInfoDto alipayUserInfoDto = new AlipayUserInfoDto(response);*/
                System.out.println(response.getUserId());
                return response.getUserId();
            } else {
                System.out.println("支付宝获取失败1");
            }
        } catch (AlipayApiException e) {
            System.out.println("支付宝获取失败2");
        }
        return null;
    }


    /**
     * 支付宝短信验证通过后
     */
    @RequestMapping(value = "/zfbReg", method = RequestMethod.POST)
    public Object zfbReg(HttpServletRequest httpRequest, HttpServletResponse httpResponse,
                         @RequestParam("ZFuuid") String ZFuuid,//上个页面
                         @RequestParam("phone") String phone) {
        //先看这个手机号有没有被注册，如果被注册则关联此账号
        User user = service.ZFRegWithFhone(ZFuuid, phone);
        if (user != null) {
            return super.setSuccessMessage(httpRequest, httpResponse, user);
        } else {
            User users = new User();
            return super.setSuccessMessage(httpRequest, httpResponse, users);
        }
    }


    /**
     * 支付测试
     *
     * @param httpRequest
     * @param httpResponse
     * @param money
     * @param name
     * @param orderid
     * @param uid
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/alipay", method = RequestMethod.POST)//支付宝支付
    public Object doPost(HttpServletRequest httpRequest, HttpServletResponse httpResponse,
                         @RequestParam("money") String money,
                         @RequestParam("name") String name,
                         @RequestParam("orderid") String orderid,
                         @RequestParam("uid") String uid
    ) throws ServletException, IOException {
//        EnvUtils.setEnv(EnvUtils.EnvEnum.SANDBOX);
        AlipayClient alipayClient = new DefaultAlipayClient(
                "https://openapi.alipaydev.com/gateway.do",
                appid,//支付宝分配给开发者的应用ID
                privateKey,
                "json",
                "utf-8",
                publickAliKEY,
                "RSA2");
        AlipayTradeAppPayRequest request = new AlipayTradeAppPayRequest();
        AlipayTradeAppPayModel model = new AlipayTradeAppPayModel();
        model.setBody("转账");//对一笔交易的具体描述信息。如果是多种商品，请将商品描述字符串累加传给body。
        model.setSubject(name);//商品的标题/交易标题/订单标题/订单关键字等。
        model.setOutTradeNo(orderid);//商户网站唯一订单号
        model.setTimeoutExpress("15m");//该笔订单允许的最晚付款时间，逾期将关闭交易。
        model.setTotalAmount(money);//订单总金额，单位为元，精确到小数点后两位，取值范围[0.01,100000000]
        model.setProductCode("QUICK_MSECURITY_PAY");//销售产品码，商家和支付宝签约的产品码，为固定值QUICK_MSECURITY_PAY
        request.setBizModel(model);
        request.setNotifyUrl("商户外网可以访问的异步地址");
        JSONObject returnMassage = new JSONObject();
        try {
            //这里和普通的接口调用不同，使用的是sdkExecute
            AlipayTradeAppPayResponse response = alipayClient.sdkExecute(request);
            System.out.println(response.getBody());//就是orderString 可以直接给客户端请求，无需再做处理。
            returnMassage.put("returnMassage", response.getBody());
        } catch (AlipayApiException e) {
            e.printStackTrace();
        }
        return super.setSuccessMessage(httpRequest, httpResponse, returnMassage);
    }

    /**
     * 充值后续(为充值后用户加钱）
     */
    @RequestMapping(value = "/didPayForAli", method = RequestMethod.POST)
    public String didPayForAli(HttpServletRequest request, HttpServletResponse response,
                               @RequestParam("money") String money,
                               @RequestParam("uid") String uid) {
        return super.setSuccessMessage(request, response, service.addMoneyToUser(money, uid));
    }

    /**
     * 支付后续
     */
    @RequestMapping(value = "/payOver", method = RequestMethod.POST)
    public String payOver(HttpServletRequest request, HttpServletResponse response,
                          @RequestParam("id") String id,
                          @RequestParam("money") String money) {
        return super.setSuccessMessage(request, response, service.payOver(id, money));
    }

    /**
     * 支付宝内部绑定，需要先调用/pay/login接口
     *
     * @param code
     * @return
     */
    @RequestMapping(value = "/joinAlibaba", method = RequestMethod.POST)
    public String test(HttpServletRequest httpRequest, HttpServletResponse httpResponse,
                       @RequestParam("code") String code,//支付宝授权得到的code值
                       @RequestParam("phone") String phone) {//用户电话号码
        User returnMassage = new User();
        try {
            AlipayClient alipayClient = new DefaultAlipayClient(
                    "https://openapi.alipay.com/gateway.do",
                    appid,//支付宝分配给开发者的应用ID
                    privateKey,
                    "json",
                    "utf-8",
                    publickAliKEY,
                    "RSA2");
            AlipaySystemOauthTokenRequest request = new AlipaySystemOauthTokenRequest();
            request.setGrantType("authorization_code");
            request.setCode(code);
            AlipaySystemOauthTokenResponse response = alipayClient.execute(request);
            //  System.out.println(response);
            if (response.isSuccess()) {
                //System.out.println(response.getAccessToken());
                String uuid = getuser(response.getAccessToken());
                if (service.changeOtherUid(uuid, phone) > 0) {
                    returnMassage = service.zfblogin(uuid);
                    System.out.println(returnMassage);
                }
            }
        } catch (Exception e) {
            System.out.println("支付宝授权绑定登录系统错误");
        }
        System.out.println("asd" + returnMassage);
        return super.setSuccessMessage(httpRequest, httpResponse, returnMassage);
    }


    /**
     * 根据token获取用户信息
     *
     * @param alipayToken
     * @return
     */
    private String getuser(String alipayToken) {
        try {
            AlipayClient alipayClient = new DefaultAlipayClient(
                    "https://openapi.alipay.com/gateway.do",
                    appid,//支付宝分配给开发者的应用ID
                    privateKey,
                    "json",
                    "utf-8",
                    publickAliKEY,
                    "RSA2");
            AlipayUserInfoShareRequest request = new AlipayUserInfoShareRequest();
            AlipayUserInfoShareResponse response = alipayClient.execute(request, alipayToken);
            if (response.isSuccess()) {
                return response.getUserId();
            } else {
                System.out.println("支付宝获取失败1");
            }
        } catch (AlipayApiException e) {
            System.out.println("支付宝获取失败2");
        }
        return null;
    }


    /**
     * 充电成功支付后增加积分
     */
    @RequestMapping(value = "/addGold", method = RequestMethod.POST)
    public String addGold(HttpServletRequest httpRequest, HttpServletResponse httpResponse,
                          @RequestParam("uuid") String uuid,
                          @RequestParam("money") String money,
                          @RequestParam("type") String type) {
        int returnMassage = service.addgold(uuid, type, money);
        return super.setSuccessMessage(httpRequest, httpResponse, returnMassage);
    }

    /**
     * 用户提现申请
     */
    @RequestMapping(value = "/posInfo", method = RequestMethod.POST)
    public String posInfo(HttpServletRequest httpRequest, HttpServletResponse httpResponse,
                          @RequestParam("money")String money,
                          @RequestParam("uuid")String uuid,
                          @RequestParam("card")String card){
        return super.setSuccessMessage(httpRequest,httpResponse,service.pos(uuid,money,card));
    }
}