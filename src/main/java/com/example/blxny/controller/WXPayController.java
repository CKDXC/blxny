package com.example.blxny.controller;


import com.example.blxny.tool.DateUtil;
import com.example.blxny.tool.MD5Util;
import com.example.blxny.tool.PayCommonUtil;
import com.example.blxny.tool.StringUtil;
import com.example.blxny.util.BaseController;
import net.sf.json.JSONObject;
import org.jdom2.JDOMException;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.*;


@CrossOrigin
@Controller
@RequestMapping("/WXpay")
public class WXPayController extends BaseController {
private static String APPID="wxe5cda898168800af";
private static String KEY="12dc05a6a35b9a6c446f90e9b4701691";
private static String MUCHID="1532835011";//商户号
private static String NEWKEY="blxnyblxnyblxnyblxnyblxnyblxnybl";
    String randomString = PayCommonUtil.getRandomString(32);
    //支付成功后的回调函数
    public static String wxnotify = "http://"+
            "120.78.172.33"
            +"/WXpay/notifyWeiXinPay";

    public WXPayController() {
    }
    public static String createSign(String characterEncoding, SortedMap<String, Object> parameters) {
        StringBuffer sb = new StringBuffer();
        Set es = parameters.entrySet();
        Iterator it = es.iterator();
        while (it.hasNext()) {
            Map.Entry entry = (Map.Entry) it.next();
            String k = (String) entry.getKey();
            Object v = entry.getValue();
            if (null != v && !"".equals(v)
                    && !"sign".equals(k) && !"key".equals(k)) {
                sb.append(k + "=" + v + "&");
            }
        }
        //sb.append("key=" + KEY);
        sb.append("key="+NEWKEY);
        System.out.println("签名字符串:"+sb.toString());
        String sign = MD5Util.MD5Encode(sb.toString(), characterEncoding).toUpperCase();
        return sign;
    }

    /**
     * @param request
     * @return
     */
    @RequestMapping(value = "/wxpay", method = RequestMethod.POST)
    @ResponseBody
    public Object wxpay(HttpServletRequest request, HttpServletResponse response,
                        @RequestParam("totalPrice")String totalPrice,//总金额
                        @RequestParam("orderId")String orderid,//订单号
                        @RequestParam("goods")String goods) {//商品名称
        BigDecimal totalAmount = BigDecimal.valueOf(Double.valueOf(totalPrice));
        String trade_no = orderid;
        String openId = goods;//商品名称
        Map<String, String> map = weixinPrePay(trade_no,totalAmount,openId,request);
        JSONObject jsonObject = new JSONObject();
        SortedMap<String, Object> parameterMap = new TreeMap<String, Object>();
        parameterMap.put("appid", "wxe5cda898168800af");
        parameterMap.put("partnerid",MUCHID);
        parameterMap.put("prepayid",map.get("prepay_id"));
        parameterMap.put("package", "Sign=WXPay");
        parameterMap.put("noncestr", map.get("nonce_str"));
        parameterMap.put("timestamp", String.valueOf(System.currentTimeMillis()));
        String signs = createSign("UTF-8", parameterMap);
        parameterMap.put("sign", signs);
        jsonObject.put("parameterMap", parameterMap);
        System.out.println("微信return"+jsonObject.toString());
        return super.setSuccessMessage(request,response,jsonObject);
    }


    /**
     * 统一下单
     * 应用场景：商户系统先调用该接口在微信支付服务后台生成预支付交易单，返回正确的预支付交易回话标识后再在APP里面调起支付。
     * @return
     */

    public Map<String, String> weixinPrePay(String orderUUid,
                                            BigDecimal totalAmount,
                                            String description,
                                            HttpServletRequest request) {
        SortedMap<String, Object> parameterMap = new TreeMap<String, Object>();
        parameterMap.put("appid", "wxe5cda898168800af");  //应用appid
        parameterMap.put("mch_id",MUCHID);  //商户号
        parameterMap.put("nonce_str", randomString);
        parameterMap.put("body", "BLXNY-"+description);
        parameterMap.put("out_trade_no", orderUUid);
        parameterMap.put("fee_type", "CNY");
        BigDecimal total = totalAmount.multiply(new BigDecimal(100));  //接口中参数支付金额单位为【分】，参数值不能带小数，所以乘以100
        java.text.DecimalFormat df=new java.text.DecimalFormat("0");
        parameterMap.put("total_fee", df.format(total));
        parameterMap.put("spbill_create_ip", PayCommonUtil.getRemoteHost(request));//终端号码
        parameterMap.put("notify_url", wxnotify);
        parameterMap.put("trade_type", "APP");//"JSAPI"
        //trade_type为JSAPI是 openid为必填项
        //parameterMap.put("openid", openid);
        String sign = PayCommonUtil.createSign("UTF-8", parameterMap);
        parameterMap.put("sign", sign);
        String requestXML = PayCommonUtil.getRequestXml(parameterMap);
        System.out.println(requestXML);
        String result = PayCommonUtil.httpsRequest(
                "https://api.mch.weixin.qq.com/pay/unifiedorder", "POST",
                requestXML);
        System.out.println("requser返回"+result);
        Map<String, String> map = null;
        try {
            map = PayCommonUtil.doXMLParse(result);
        } catch (JDOMException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        System.out.println("同一下单"+map);
        return map;
    }



    /**
     * 此函数会被执行多次，如果支付状态已经修改为已支付，则下次再调的时候判断是否已经支付，如果已经支付了，则什么也执行
     * @param request
     * @param response
     * @return
     * @throws IOException
     * @throws JDOMException
     */
    @RequestMapping(value = "notifyWeiXinPay", produces = MediaType.APPLICATION_JSON_VALUE)
    // @RequestDescription("支付回调地址")
    @ResponseBody
    public String notifyWeiXinPay(HttpServletRequest request, HttpServletResponse response) throws IOException, JDOMException {
        System.out.println("微信支付回调");
        InputStream inStream = request.getInputStream();
        ByteArrayOutputStream outSteam = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int len = 0;
        while ((len = inStream.read(buffer)) != -1) {
            outSteam.write(buffer, 0, len);
        }
        String resultxml = new String(outSteam.toByteArray(), "utf-8");
        Map<String, String> params = PayCommonUtil.doXMLParse(resultxml);
        outSteam.close();
        inStream.close();


        Map<String,String> return_data = new HashMap<String,String>();
        if (!PayCommonUtil.isTenpaySign(params)) {
            // 支付失败
            return_data.put("return_code", "FAIL");
            return_data.put("return_msg", "return_code不正确");
            return StringUtil.GetMapToXML(return_data);
        } else {
            System.out.println("===============付款成功==============");
            // ------------------------------
            // 处理业务开始
            // ------------------------------
            // 此处处理订单状态，结合自己的订单数据完成订单状态的更新
            // ------------------------------
            String total_fee = params.get("total_fee");
            double v = Double.valueOf(total_fee) / 100;
            String out_trade_no = String.valueOf(Long.parseLong(params.get("out_trade_no").split("O")[0]));
            Date accountTime = DateUtil.stringtoDate(params.get("time_end"), "yyyyMMddHHmmss");
            String ordertime = DateUtil.dateToString(new Date(), "yyyy-MM-dd HH:mm:ss");
            String totalAmount = String.valueOf(v);
            String appId = params.get("appid");
            String tradeNo = params.get("transaction_id");

            return_data.put("return_code", "SUCCESS");
            return_data.put("return_msg", "OK");
            System.out.println(StringUtil.GetMapToXML(return_data));
            return StringUtil.GetMapToXML(return_data);
        }
    }



}
