package com.example.blxny.controller;


import com.example.blxny.model.Order;
import com.example.blxny.service.ChargeService;
import com.example.blxny.service.OrderService;
import com.example.blxny.util.BaseController;
import com.example.blxny.util.DateJsonValueProcessor;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.List;
@CrossOrigin
@Controller
@RequestMapping("/order")
public class OrderController extends BaseController {

    @Autowired
    private OrderService orderService;
    @Autowired
    private ChargeService chargeService;

    /**
     * 获取预约订单订单
     *
     * @param request
     * @param response
     * @param uid        用户id
     * @param pageNumber
     * @param pageSize
     * @return
     */
    @RequestMapping(value = "/getPage", method = RequestMethod.POST)
    @ResponseBody
    public String getPage(HttpServletRequest request, HttpServletResponse response,
                          @RequestParam("uid") Integer uid,
                          @RequestParam("orderType") int orderType,
                          @RequestParam("pageNumber") Integer pageNumber,
                          @RequestParam("pageSize") Integer pageSize) {
        //订单类型
        List<Order> list = orderService.getPageByUid(uid, orderType, pageNumber, pageSize);
        return this.setSuccessMessage(request, response, list);
    }


    /**
     * 查询所有预约
     *
     */
    @RequestMapping(value = "/findappo", method = RequestMethod.POST)
    @ResponseBody
    public Object findappo(HttpServletRequest request, HttpServletResponse response,
                           @RequestParam("num") String num,
                           @RequestParam("uid") String uid) {
        System.out.println(num);
        System.out.println(uid);
        JSONObject json = new JSONObject();
        JsonConfig jsonConfig = new JsonConfig();
        jsonConfig.registerJsonValueProcessor(Date.class, new DateJsonValueProcessor("MM-dd HH:mm:ss"));
        json.put("top", JSONArray.fromObject(orderService.findAppoOne(uid),jsonConfig));
//        json.put("appointment", orderService.findAppo(num, uid));
        json.put("appointment", JSONArray.fromObject(orderService.findAppo(num, uid), jsonConfig));
        System.out.println("查询所有预约返回" + json);
        return super.setSuccessMessage(request, response, json);
    }

    /**
     * 清除无效预约
     *
     * @param request
     * @param response
     * @param uid
     * @return
     */
    @RequestMapping(value = "/clean", method = RequestMethod.POST)
    @ResponseBody
    public Object clean(HttpServletRequest request, HttpServletResponse response,
                        @RequestParam("uid") String uid) {
        return super.setSuccessMessage(request, response, orderService.cleanAppo(uid));
    }

    /**
     * 取消预约
     *
     * @param request
     * @param response
     * @param uid
     * @return
     */
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    @ResponseBody
    public Object del(HttpServletRequest request, HttpServletResponse response,
                      @RequestParam("uid") String uid) {//预约订单号
        return super.setSuccessMessage(request, response, orderService.quxiao(uid));
    }

    /**
     * 查看是否预约
     *
     * @return trun false
     */
    @RequestMapping(value = "/isappo", method = RequestMethod.POST)
    @ResponseBody
    public Object isappo(HttpServletRequest request, HttpServletResponse response,
                         @RequestParam("uid") String uid) {
        return super.setSuccessMessage(request, response, orderService.isAppo(uid));
    }


    /*以上为预约*/
    /*以下为订单*/

    /**
     * 查看订单
     * deviceid：桩编号
     * stationName：驿站名字
     * time:订单时间
     * powertime：充电时间
     * orderId：订单号
     * type：订单状态（0进行中、1待支付、2已结束）
     * **working为进行中的订单，over为已结束的订单
     */

    @RequestMapping(value = "/myorder", method = RequestMethod.POST)
    @ResponseBody
    public Object myoder(HttpServletRequest request, HttpServletResponse response,
                         @RequestParam("uid") String uid,//用户uuid
                         @RequestParam("code") String code) {//分页，0开始
        JSONObject js = new JSONObject();
        //返回正在充电，待支付订单
        js.put("working", orderService.findorderIng(uid));
        //返回结束订单
        js.put("over", orderService.findorder(uid, code));
        System.out.println(js);
        return super.setSuccessMessage(request, response, js);
    }

    /**
     * 清空三个月订单
     */
    @RequestMapping(value = "/threeMonthClean", method = RequestMethod.POST)
    @ResponseBody
    public Object myoder(HttpServletRequest request, HttpServletResponse response,
                         @RequestParam("uid") String uid) {//用户uuid
        return super.setSuccessMessage(request, response, orderService.changeMonththree(uid));
    }

    /**
     * 订单详情
     * 所反参数与页面相近
     */
    @RequestMapping(value = "/orderInfo", method = RequestMethod.POST)
    @ResponseBody
    public Object orderInfo(HttpServletRequest request, HttpServletResponse response,
                            @RequestParam("id") String id) {//订单id
        return super.setSuccessMessage(request, response, orderService.findAllOrderInfor(id));
    }

    /**
     * 添加预约
     *
     * @param request
     * @param response
     * @param uid
     * @param time
     * @param deviceId
     * @param money
     * @return
     */
    @RequestMapping(value = "/addAppo", method = RequestMethod.POST)
    @ResponseBody
    public String addAppo(HttpServletRequest request, HttpServletResponse response,
                          @RequestParam("uid") String uid,
                          @RequestParam("time") String time,
                          @RequestParam("deviceId") String deviceId,
                          @RequestParam("money") String money,
                          @RequestParam("payType") String payType) {
        JSONObject msg = (JSONObject) chargeService.addAppointment(uid, time, deviceId, money, payType);
        return this.setSuccessMessage(request, response, msg);

    }

    /**
     * 取消预约
     *
     * @param request
     * @param response
     * @param appoId
     * @return
     */
    @RequestMapping(value = "/cancelAppo", method = RequestMethod.POST)
    @ResponseBody
    public String cancelAppo(HttpServletRequest request, HttpServletResponse response,
                             @RequestParam("appoId") String appoId) {
        String msg = chargeService.cancelAppointment(appoId);
        if ("success".equals(msg)) {
            return this.setSuccessMessage(request, response, msg);
        } else {
            return this.setFailureMessage(request, response, msg);
        }
    }
}
