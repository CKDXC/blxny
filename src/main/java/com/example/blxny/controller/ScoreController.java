package com.example.blxny.controller;

import com.example.blxny.model.Coupon;
import com.example.blxny.service.CouponService;
import com.example.blxny.util.BaseController;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Controller
@RequestMapping("/score")
public class ScoreController extends BaseController {

    @Autowired
    private CouponService couponService;


    @RequestMapping(value = "/exchange", method = RequestMethod.GET)
    @ResponseBody
    public String exchange(HttpServletRequest request, HttpServletResponse response,
                           @RequestParam("uid") String uid,
                           @RequestParam("scoreProductId") int scoreProductId) {
        return "";
    }

    /**
     * 获取用户自己拥有的优惠券
     *
     * @param request
     * @param response
     * @param uid
     * @return
     */
    @RequestMapping(value = "/getUserCoupon", method = RequestMethod.POST)
    @ResponseBody
    public String getUserCoupon(HttpServletRequest request, HttpServletResponse response,
                                @RequestParam("uid") String uid,
                                @RequestParam("type") int type) {//type:1-优惠券，2-代金券
        List<Coupon> list = couponService.getCoupon(uid, type, 0);
        return this.setSuccessMessage(request, response, list);
    }


    @RequestMapping(value = "getScoreProduct", method = RequestMethod.GET)
    @ResponseBody
    public String getScoreProduct(HttpServletRequest request, HttpServletResponse response,
                                  @RequestParam("type") int type) {
        return "";
    }

    /**
     * 积分兑换
     */
    @RequestMapping(value = "/gold", method = RequestMethod.POST)
    @ResponseBody
    public Object gole(HttpServletRequest request, HttpServletResponse response,
                       @RequestParam("uuid") String uuid,//用户uuid
                       @RequestParam("gold") String gold,//需要得积分
                       @RequestParam("money") String money,//代金卷价格
                       @RequestParam("type") String type,//类型1-优惠券2-代金券3-积分商品
                       @RequestParam("day") String day) {//过期时间，单位天
        JSONObject retMassage = new JSONObject();
        retMassage.put("type", couponService.getGold(uuid, money, gold, type, day));
        return super.setSuccessMessage(request, response, retMassage);
    }

    /**
     * 积分消费明细
     */
    @RequestMapping(value = "/goldUse", method = RequestMethod.POST)
    @ResponseBody
    public Object goldUse(HttpServletRequest request, HttpServletResponse response,
                          @RequestParam("uuid") String uuid) {//用户uuid
        List<Coupon> list = (List<Coupon>) couponService.getGoldUse(uuid);
        return this.setSuccessMessage(request, response, list);
    }

    /**
     * 清空兑换明细
     */
    @RequestMapping(value = "/cleanGoldUse", method = RequestMethod.POST)
    @ResponseBody
    public Object cleanGoldUse(HttpServletRequest request, HttpServletResponse response,
                          @RequestParam("uuid") String uuid) {//用户uuid
            return super.setSuccessMessage(request,response,couponService.cleanGoldUse(uuid));
    }
}