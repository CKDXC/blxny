package com.example.blxny.controller;

import com.example.blxny.model.Bank;
import com.example.blxny.service.UserLoginService;
import com.example.blxny.tool.HttpUtils;
import com.example.blxny.tool.IDNumValidatyUtil;
import com.example.blxny.util.BaseController;
import net.sf.json.JSONObject;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@CrossOrigin
@RestController
@RequestMapping("/BankCard")
public class IDcardController extends BaseController {
    @Autowired
    private UserLoginService service;
    //阿里云appcode,如出现403错误说明次数不够请到以下链接添加次数
    // https://market.aliyun.com/products/57000002/cmapi013074.html?spm=5176.2020520132.101.7.73a27218surkZE#sku=yuncode707400008
    //且请修改appid 如下
    private static final String appcode = "704bd764fbfa4f8781f8b7ecdd207dbe";

    /**
     * 银行卡验证url=？？？/BankCard/findId？name&idcard&phone
     * 每次成功请求都会扣费，请正确判定格式后再调用此接口
     * 身份证后台已经算法判断，调用时可不做验证
     * 银行卡后台已做Luhm算法验证。
     * @param name//真实姓名，请简单判定一下
     * @param idcard//身份证号，
     * @param bankcard/银行卡号，请验证位数后传输
     * @param phone/请将电话号码验证完整格式后传过来
     * @return /out:此字段返回说明，可直接显示在用户提示。如（认证通过，银行卡格式错误，身份证错误。。。等）
     * 但只有返回认证通过才为正确。
     * /bankNumber：返回的银行卡号，
     * /bankName：如认证成功，则返回为银行名称
     * /cardType：如认证成功，则返回此卡的卡种（如：借记卡）
     * /cardVar：如认证成功，则返回银行卡类别（如：牡丹卡普卡，龙卡等）
     * /cardAddress：如认证成功，则返回办卡地址（如：四川省-成都）
     */
    @RequestMapping(value = "/findId", method = RequestMethod.POST)
    public Object idcard(HttpServletRequest request, HttpServletResponse response,
                         @RequestParam("useruuid") String uuid,//用户uuid
                         @RequestParam("name") String name,
                         @RequestParam("idcard") String idcard,
                         @RequestParam("bankcard") String bankcard,
                         @RequestParam("phone") String phone,
                         @RequestParam("where") String where) {//0普通用户1合伙人用户
        String out = null;
        String bankNumber = "fail";
        String bankName = "fail";
        String cardType = "fail";
        String cardVar = "fail";
        String cardAddress = "fail";
        JSONObject returnMassage = new JSONObject();
        IDNumValidatyUtil card = new IDNumValidatyUtil();
        boolean bool = card.checkID(idcard);//身份证判定
        boolean bool2 = card.BankCard(bankcard);//银行卡判定
        boolean bool3 = service.isHavaBankCard(bankcard, where);//判断是否已被绑定;
        if (!bool3) {
            if (bool) {
                if (bool2) {
                    String host = "https://ali-bankcard4.showapi.com";
                    String path = "/bank4";
                    String method = "GET";
                    Map<String, String> headers = new HashMap<String, String>();
                    //最后在header中的格式(中间是英文空格)为Authorization:APPCODE 83359fd73fe94948385f570e3c139105
                    headers.put("Authorization", "APPCODE " + appcode);
                    Map<String, String> querys = new HashMap<String, String>();
                    querys.put("acct_name", name);
                    querys.put("acct_pan", bankcard);
                    querys.put("cert_id", idcard);
                    querys.put("cert_type", "01");
                    querys.put("needBelongArea", "true");
                    querys.put("phone_num", phone);
                    try {
                        HttpResponse responses = HttpUtils.doGet(host, path, method, headers, querys);
                        System.out.println(responses.toString());
                        //获取response的body
                        String i = EntityUtils.toString(responses.getEntity());
                        String str = i.replaceAll(" ", "");
                        JSONObject js = JSONObject.fromObject(str);
                        JSONObject jsw = JSONObject.fromObject(js.get("showapi_res_body"));
                        JSONObject info = JSONObject.fromObject(jsw.get("belong"));
                        out = (String) jsw.get("msg");
                        if (out.equals("认证通过")) {
                            bankNumber = (String) info.get("cardNum");
                            try {
                                String bank = String.valueOf(info.get("bankName")).split("银行")[1];
                                bankName = String.valueOf(info.get("bankName")).split("银行")[0] + "银行";
                            } catch (ArrayIndexOutOfBoundsException a) {
                                bankName = String.valueOf(info.get("bankName"));
                            }
                            cardType = (String) info.get("cardType");
                            cardVar = (String) info.get("brand");
                            cardAddress = (String) info.get("area");
                            Bank bank = new Bank();
                            bank.setBackName(bankName);
                            bank.setNumber(bankNumber);
                            bank.setCardType(cardType);
                            bank.setCardVar(cardVar);
                            bank.setCardAddress(cardAddress);
                            bank.setPhone(phone);
                            bank.setUserUuid(uuid);
                            bank.setWhere(where);
                            bank.setIdcard(idcard);
                            service.addBank(bank);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {//银行卡格式错误
                    out = "银行卡格式错误";
                }
            } else {//身份证错误
                out = "身份证错误";
                bankNumber = bankcard;
            }
        } else {
            out = "银行卡已被绑定";
            bankNumber = bankcard;
        }
        returnMassage.put("out", out);
        returnMassage.put("bankNumber", bankNumber);
        returnMassage.put("bankName", bankName);
        returnMassage.put("cardType", cardType);
        returnMassage.put("cardVar", cardVar);
        returnMassage.put("cardAddress", cardAddress);
        return returnMassage;
    }

    /**
     * 解绑银行卡,返回大于0则成功
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/closeBankCard", method = RequestMethod.POST)
    public Object closeBankCard(HttpServletRequest request, HttpServletResponse response,
                                @RequestParam("bankNum") String bankNum,//银行卡号码
                                @RequestParam("userUuid") String uuid,//用户uuid
                                @RequestParam("where") String where) {
        return super.setSuccessMessage(request, response, service.closeBankCard(bankNum, uuid, where));
    }

    /**
     * 查询所有绑定,没有为空
     */
    @RequestMapping(value = "/findBank", method = RequestMethod.POST)
    public Object findBankAll(HttpServletRequest request, HttpServletResponse response,
                              @RequestParam("uuid") String uuid,//用户
                              @RequestParam("where") String where) {
        List<Bank> bank = service.fingAllBank(uuid, where);
        if (bank == null) {
            return super.setSuccessMessage(request, response, new Bank());
        } else {
            return super.setSuccessMessage(request, response, bank);
        }
    }
}
