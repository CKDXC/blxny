package com.example.blxny.controller;

import com.alibaba.fastjson.JSONObject;
import com.example.blxny.dao.CateringProductDao;
import com.example.blxny.model.Hotel;
import com.example.blxny.service.RestaurantService;
import com.example.blxny.service.RestaurantService;
import com.example.blxny.util.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.sun.org.apache.regexp.internal.RE;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/*
 *项目名: blxny
 *文件名: RestaurantController
 *创建者: SCH
 *创建时间:2019/5/28 14:08
 *描述: TODO
 */
@CrossOrigin
@RestController
@RequestMapping("/restaurant")
public class RestaurantController extends BaseController {

    @Autowired
    private RestaurantService restaurantService;

    @RequestMapping(value = "/{type:[1-4]}/getlist", method = RequestMethod.POST)
    public String getList(HttpServletRequest request, HttpServletResponse response,
                          @PathVariable("type") Integer type,
                          @RequestParam("popular") Integer popular,
                          @RequestParam("lastId") Integer lastId,
                          @RequestParam("ps") Integer pageSize) {
        return super.setSuccessMessage(request, response, (JSONObject) restaurantService.getList(type, popular, lastId, pageSize));
    }

    @RequestMapping(value = "/hotel", method = RequestMethod.POST)//酒店详情展示
    public Object Hotel(HttpServletRequest request, HttpServletResponse response,
                        @RequestParam("id") Integer id) {
        List<Hotel> hotels = restaurantService.getHotel(id);
        if (hotels == null) {
            hotels.add(new Hotel());
        }
        return setSuccessMessage(request, response, hotels);
    }

    @RequestMapping(value = "/shop", method = RequestMethod.POST)//首页展示商品列表
    public Object Shop(HttpServletRequest request, HttpServletResponse response,
                       @RequestParam("id") Integer id,
                       @RequestParam("pageSize") Integer size,
                       @RequestParam("pageNumber") Integer number) {
        return setSuccessMessage(request, response, restaurantService.getShop(id, size, number));
    }

    @RequestMapping(value = "/shop/{type:[1-8]}", method = RequestMethod.POST)//根据种类查看商品列表
    public String findShopByType(HttpServletRequest request, HttpServletResponse response,
                                 @RequestParam("id") Integer id,
                                 @PathVariable("type") Integer type,
                                 @RequestParam("size") Integer size,
                                 @RequestParam("number") Integer number) {
        return setSuccessMessage(request, response, restaurantService.getShopByType(type, id, size, number));
    }

    @RequestMapping(value = "/{restaurantId}/getProduct", method = RequestMethod.POST)
    public String getProducts(HttpServletRequest request, HttpServletResponse response,
                              @PathVariable("restaurantId") Integer restaurantId,
                              @RequestParam("lastId") Integer lastId,
                              @RequestParam("type") Integer type,
                              @RequestParam("ps") Integer ps) {
        return super.setSuccessMessage(request, response, (JSONObject) restaurantService.getCateringProducts(restaurantId, lastId, type, ps));
    }

    @RequestMapping(value = "/qrCode/{useruuid}", method = RequestMethod.POST)//优惠二维码生成
    public String score(HttpServletRequest request, HttpServletResponse response,
                        @PathVariable("useruuid") String uuid,
                        @RequestParam("service") String type) {//1酒店，2咖啡厅，3餐厅，4商店
        return setSuccessMessage(request, response, restaurantService.getMessage(uuid, type));
    }

    @RequestMapping(value = "/getQRCode", method = RequestMethod.POST)//获取二维码信息
    public String getQr(HttpServletRequest request, HttpServletResponse response,
                        @RequestParam("uuid") String uid,
                        @RequestParam("code") String code) {
        net.sf.json.JSONObject js = net.sf.json.JSONObject.fromObject(restaurantService.getQRCode(uid, code));
        if (js.get("message").equals("OK")) {
            return setSuccessMessage(request, response, js);
        } else {
            return setFailureMessage(request, response, js);
        }
    }

    @RequestMapping(value = "/sureUseQRCode", method = RequestMethod.POST)//确认使用该二维码
    public String sureUseQRCode(HttpServletResponse response, HttpServletRequest request,
                                @RequestParam("code") String code,
                                @RequestParam("uid") String uid) {
        if (restaurantService.endQRCode(code, uid)) {
            return setSuccessMessage(request, response, "OK");
        } else {
            return setFailureMessage(request, response, "ERROR");
        }
    }

    @RequestMapping(value = "/checkUse/{id}", method = RequestMethod.GET)
    public String checkUse(HttpServletResponse response, HttpServletRequest request,
                           @PathVariable("id") Integer id) {
        if (restaurantService.checkUse(id) == true) {
            return super.setSuccessMessage(request, response, "success");
        } else {
            return super.setSuccessMessage(request, response, "error");
        }
    }

    @RequestMapping(value = "/evaluat/{id}", method = RequestMethod.POST)
    public String evaluat(HttpServletResponse response, HttpServletRequest request,
                          @PathVariable("id") Integer id,
                          @RequestParam("score") Integer score) {
        String msg = restaurantService.evaluat(id, score);
        return super.setSuccessMessage(request, response, msg);
    }
}
