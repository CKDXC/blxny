package com.example.blxny.controller;

import com.example.blxny.model.SaleMan;
import com.example.blxny.model.SalesOder;
import com.example.blxny.model.Service;
import com.example.blxny.service.FindSalesService;
import com.example.blxny.util.BaseController;
import com.sun.org.apache.regexp.internal.RE;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.MessageFormat;
import java.text.ParseException;

@CrossOrigin
@RestController
@RequestMapping("/Sales")
public class SalesmanController extends BaseController {

    @Autowired
    private FindSalesService services;

    /**
     * 用户确认洗车或充电，后台派单，请让用户知晓此接口会定位当前最近的驿站位置且无法修改
     * 返回业务员信息，和最近的驿站信息
     * 此接口通过地理位置派发人员
     */
    @RequestMapping(value = "/findSalesMan", method = RequestMethod.POST)
    public String findSalesMan(HttpServletRequest request, HttpServletResponse response,
                               @RequestParam("lat") String lat,
                               @RequestParam("lng") String lng) {//驿站信息
        Object obj = services.findSales(lat, lng);
        return super.setSuccessMessage(request, response, obj);
    }


    /**
     * 用户确认服务订单，在扣费成功之后调取
     */
    @RequestMapping(value = "/addService", method = RequestMethod.POST)
    public String addService(HttpServletRequest request, HttpServletResponse response,
                             @RequestParam("useruuid") String useruuid,//服务用户id
                             @RequestParam("userphone") String userphone,//服务用户电话
                             @RequestParam("username") String username,//用户昵称
                             @RequestParam("stationid") String stationid,//驿站编号
                             @RequestParam("address") String address,//驿站地址
                             @RequestParam("service") String service,//服务名称（洗车服务，充电服务）
                             @RequestParam("office") String office,//服务公司名称
                             @RequestParam("salesId") String salesid,//业务员id
                             @RequestParam("type") String type,//服务类型（默认自助）
                             @RequestParam("invoice") String invoice,//0开发票1不开发票
                             @RequestParam("money") String money) {//价格
        Service service1 = new Service();
        service1.setUserUuid(useruuid);
        service1.setUserPhone(userphone);
        service1.setUserName(username);
        service1.setServiceStation(stationid);
        service1.setServiceAddress(address);
        service1.setService(service);
        service1.setServiceOffice(office);
        service1.setSalesId(salesid);
        service1.setServiceType(type);
        service1.setInvoice(Integer.valueOf(invoice));
        service1.setMoney(money);
        int i = services.addService(service1);
        if (i > 0) {
            services.changeSalesType("3", salesid);
        }
        return super.setSuccessMessage(request, response, i);
    }

/************************************************以上过期**************************/

    /**
     * 查看他是不是业务员，如果是则显示业务员链接图标
     * status: "OK",则为业务员，status: "ERROR",则此为普通用户
     * data返回如为’nomore‘ 则此为普通用户，如返回字符（此字符为业务员id）则为业务员id
     */
    @RequestMapping(value = "/isSales", method = RequestMethod.POST)
    public String addService(HttpServletRequest request, HttpServletResponse response,
                             @RequestParam("useruuid") String useruuid) {//用户id
        String string = services.isSales(useruuid);
        if (string == null) {
            return super.setFailureMessage(request, response, "nomore");
        } else {
            return super.setSuccessMessage(request, response, string);
        }
    }

    /**
     * 业务员点击上班/下班按钮
     */
    @RequestMapping(value = "/goToWork", method = RequestMethod.POST)
    public Object goToWork(HttpServletRequest request, HttpServletResponse response,
                           @RequestParam("JPid") String JPid,//此设备极光推送id
                           @RequestParam("uuid") String uuid,//用户id
                           @RequestParam("type") Integer type,//1上班2下班
                           @RequestParam("lng") String lng,
                           @RequestParam("lat") String lat) {
        if (services.changeSalesTypeNow(uuid, type, JPid, lng, lat).equals("error")) {
            return super.setFailureMessage(request, response, "error");
        } else {
            return super.setSuccessMessage(request, response, "OK");
        }
    }

    /**
     * 用户点击生成订单,返回订单个别信息less
     */
    @RequestMapping(value = "/oderInfo", method = RequestMethod.POST)
    public Object oderInfo(HttpServletRequest request, HttpServletResponse response,
                           @RequestParam("uuid") String uuid,//用户uuid
                           @RequestParam("userService") String userService,//什么服务，0爱洗车1随带充
                           @RequestParam("lat") String lat,//当前位置伟度
                           @RequestParam("lng") String lng//当前位置经度
    ) {
        return super.setSuccessMessage(request, response, services.oderInfo(uuid, userService, lat, lng));
    }

    /**
     * 用户点击提交订单，开始寻找业务员
     * 传入为整个订单信息
     * http:///Sales/findSaleForUs?salesId=123123&salesTime=2019-04-25%2013:15&beginTime=13:30&endTime=14:00&money=19.99&carCard=asdasd&phone=2134566&serviceType=1&bill=1&serviceInfo=1&other=&stationId=0000050104002&useruuid=1
     */
    @RequestMapping(value = "/findSaleForUs", method = RequestMethod.POST)
    public Object findSaleForUs(HttpServletRequest request, HttpServletResponse response,
                                @RequestParam("beginTime") String beginTime,
                                @RequestParam("endTime") String endTime,
                                @RequestParam("money") String money,
                                @RequestParam("carCard") String carCard,
                                @RequestParam("phone") String phone,
                                @RequestParam("serviceInfo") String serviceInfo,
                                @RequestParam("other") String other,
                                @RequestParam("stationId") String stationId,
                                @RequestParam("useruuid") String useruuid,
                                @RequestParam("car") String car) throws ParseException {
        String massage = (String) services.findSalesForUs(beginTime, endTime, money, carCard, phone, serviceInfo, other, stationId, useruuid, car);
        if ("OK".equals(massage)) {
            return super.setSuccessMessage(request, response, massage);
        } else {
            return setFailureMessage(request, response, massage);
        }
    }

    /**
     * 开始抢单
     */
    @RequestMapping(value = "/fate", method = RequestMethod.GET)
    public Object fate(HttpServletRequest request, HttpServletResponse response,
                       @RequestParam("uuid") String uuid,//用户uuid
                       @RequestParam("id") String id/*订单号*/) {
        SalesOder i = (SalesOder) services.fate(uuid, id);
        return super.setSuccessMessage(request, response, i);
    }

    /**
     * 客户循环请求查看订单
     */
    @RequestMapping(value = "findSalesEndliss", method = RequestMethod.GET)
    public Object findSalesEndliss(HttpServletRequest request, HttpServletResponse response,
                                   @RequestParam("uuid") String uuid,
                                   @RequestParam("oderId") String id) {
        SalesOder returnMassage = (SalesOder) services.findSalesEndliss(uuid, id);
        return super.setSuccessMessage(request, response, returnMassage);
    }
    /*********************以上是全部抢单过程***********************/


    /***
     *广告轮播获取
     */
    @RequestMapping(value = "/adEndless", method = RequestMethod.POST)
    public Object adEndless(HttpServletRequest request, HttpServletResponse response) {
        return super.setSuccessMessage(request, response, services.adEndless());
    }


    /**驻点订单*/
    @RequestMapping("/saleOrder")
    public String SaleOrder(HttpServletRequest request,HttpServletResponse response,
                            @RequestParam("JPid")String uuid){
        String message=services.findFateOrder(uuid);
        return message;
    }

}