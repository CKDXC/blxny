package com.example.blxny.controller;

import com.example.blxny.model.Partner;
import com.example.blxny.model.PartnerUpdate;
import com.example.blxny.service.PartnerService;
import com.example.blxny.service.impl.PartnerServiceImpl;
import com.example.blxny.tool.MD5Util;
import com.example.blxny.util.BaseController;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileNotFoundException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

@CrossOrigin
@Controller
@RequestMapping("/partner")
public class PartnerController extends BaseController {

    @Autowired
    private PartnerService partnerService;

    /**
     * 添加合伙人
     *
     * @param request
     * @param response
     * @param uid
     * @param partnerName
     * @param opreatorName
     * @param userName
     * @param partnerPwd
     * @return
     */
    @RequestMapping(value = "/addPartner", method = RequestMethod.GET)
    @ResponseBody
    public String addPartner(HttpServletRequest request, HttpServletResponse response,
                             @RequestParam("uid") String uid,
                             @RequestParam("partnerName") String partnerName,
                             @RequestParam("opreatorName") String opreatorName,
                             @RequestParam("userName") String userName,
                             @RequestParam("partnerPwd") String partnerPwd) {
        String msg = partnerService.addPartner(uid, partnerName, opreatorName, userName, partnerPwd);
        if ("success".equals(msg)) {
            return this.setSuccessMessage(request, response, msg);
        } else {
            return this.setFailureMessage(request, response, msg);
        }
    }

    /**
     * 添加合伙人申请
     *
     * @param request
     * @param response
     * @param uid
     * @param partnerName
     * @param opreatorName
     * @param userName
     * @param partnerPwd
     * @return
     */
    @RequestMapping(value = "/addPartnerApply", method = RequestMethod.POST)
    @ResponseBody
    public String addPartnerApply(HttpServletRequest request, HttpServletResponse response,
                                  @RequestParam("uid") String uid,
                                  @RequestParam("partnerName") String partnerName,
                                  @RequestParam("opreatorName") String opreatorName,
                                  @RequestParam("userName") String userName,
                                  @RequestParam("partnerPwd") String partnerPwd) {
        System.out.println("uid" + uid);
        System.out.println("partnerName" + partnerName);
        System.out.println("opreatorName" + opreatorName);
        System.out.println("userName" + userName);
        System.out.println("partnerPwd" + partnerPwd);
        String msg = partnerService.addPartnerApply(uid, partnerName, opreatorName, userName, partnerPwd);
        if ("success".equals(msg)) {
            return this.setSuccessMessage(request, response, msg);
        } else {
            return this.setFailureMessage(request, response, msg);
        }
    }

    /**
     * 修改合伙人密码
     *
     * @param request
     * @param response
     * @param partnerId
     * @param newPwd
     * @return
     */
    @RequestMapping(value = "/modifyPwd", method = RequestMethod.POST)
    @ResponseBody
    public String modifyPwd(HttpServletRequest request, HttpServletResponse response,
                            @RequestParam("partnerId") Integer partnerId,
                            @RequestParam("newPwd") String newPwd) {
        String returnMessage = partnerService.modifyPwd(partnerId, newPwd);
        System.out.println("uid:" + partnerId + "/n" + "newpwd:" + newPwd);
        if (returnMessage.equals("修改成功")) {
            returnMessage = this.setSuccessMessage(request, response, returnMessage);
        } else {
            returnMessage = this.setFailureMessage(request, response, returnMessage);
        }
        return returnMessage;
    }

    /**
     * 合伙人登录
     *
     * @param request
     * @param response
     * @param uid
     * @param partnerPwd
     * @return
     */
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    @ResponseBody
    public String login(HttpServletRequest request, HttpServletResponse response,
                        @RequestParam("uid") String uid,
                        @RequestParam("partnerPwd") String partnerPwd) {
        String msg = partnerService.login(uid, partnerPwd);
        Partner returnMassage = new Partner();
        if (msg.equals("success")) {
            returnMassage = partnerService.findPartner(uid);
            if (returnMassage != null) {
                MessageDigest md = null;
                try {
                    md = MessageDigest.getInstance("MD5");
                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                }
                return this.setSuccessMessage(request, response, returnMassage);
            } else {
                return this.setSuccessMessage(request, response, returnMassage);
            }
        } else {
            return this.setSuccessMessage(request, response, returnMassage);
        }
    }

    /**
     * 修改合伙人支付密码
     *
     * @param request
     * @param response
     * @param partnerId
     * @param payPwd
     * @return
     */
    @RequestMapping(value = "/modifyPayPwd", method = RequestMethod.POST)
    @ResponseBody
    public String modifyPayPwd(HttpServletRequest request, HttpServletResponse response,
                               @RequestParam("partnerId") Integer partnerId,
                               @RequestParam("payPwd") String payPwd) {
        String msg = partnerService.modifyPayPwd(partnerId, payPwd);
        if ("ERROR".equals(msg)) {
            return this.setFailureMessage(request, response, msg);
        } else {
            return this.setSuccessMessage(request, response, msg);
        }
    }

    /**
     * 合伙人支付密码判断
     */
    @RequestMapping(value = "/password", method = RequestMethod.POST)
    @ResponseBody
    public String modifyPayPwd(HttpServletRequest request, HttpServletResponse response,
                               @RequestParam("password") String password,
                               @RequestParam("pwd") String pwd) {
        if (partnerService.password(password,pwd)) {
            return this.setSuccessMessage(request, response, "OK");
        } else {
            return this.setFailureMessage(request, response, "ERROR");
        }
    }
    /**
     * 我的驿站4.25update
     *
     * @param request
     * @param response
     * @param partnerId
     * @return
     */
    @RequestMapping(value = "/myStation", method = RequestMethod.POST)
    @ResponseBody
    public String myStation(HttpServletRequest request, HttpServletResponse response,
                            @RequestParam("partnerId") int partnerId) {
        JSONObject msg = (JSONObject) partnerService.getStations(partnerId);
        return this.setSuccessMessage(request, response, msg);
    }

    /**
     * 绑定手机号
     *
     * @param request
     * @param response
     * @param partnerId
     * @param phone
     * @return
     */
    @RequestMapping(value = "/bindPhone", method = RequestMethod.POST)
    @ResponseBody
    public String bindPhone(HttpServletRequest request, HttpServletResponse response,
                            @RequestParam("partnerId") int partnerId,
                            @RequestParam("phone") String phone) {
        String msg = partnerService.bindPhone(partnerId, phone);
        if ("success".equals(msg)) {
            return this.setSuccessMessage(request, response, msg);
        } else {
            return this.setFailureMessage(request, response, msg);
        }
    }

    /**
     * 获取device
     *
     * @param request
     * @param response
     * @param partnerId
     * @param stationId
     * @return
     */
    @RequestMapping(value = "/myStation/checkStation", method = RequestMethod.POST)
    @ResponseBody
    public String checkStation(HttpServletRequest request, HttpServletResponse response,
                               @RequestParam("partnerId") int partnerId,
                               @RequestParam("stationId") String stationId) {
        JSONObject msg = (JSONObject) partnerService.getStations(partnerId, stationId);
        return this.setSuccessMessage(request, response, msg);
    }

    /**
     * 切换设备
     *
     * @param request
     * @param response
     * @param deviceId
     * @return
     */
    @RequestMapping(value = "/myStation/checkDevice", method = RequestMethod.POST)
    @ResponseBody
    public String checkDevice(HttpServletRequest request, HttpServletResponse response,
                              @RequestParam("deviceId") String deviceId) {
        JSONObject msg = (JSONObject) partnerService.getDevices(deviceId);
        return this.setSuccessMessage(request, response, msg);
    }

    /**
     * 切换月份
     *
     * @param request
     * @param response
     * @param deviceId
     * @param month
     * @return
     */
    @RequestMapping(value = "/myStation/checkMonth", method = RequestMethod.POST)
    @ResponseBody
    public String checkMonth(HttpServletRequest request, HttpServletResponse response,
                             @RequestParam("deviceId") String deviceId,
                             @RequestParam("year") int year,//4.25update i:新增年份
                             @RequestParam("month") int month) {//4.25update
        JSONObject msg = (JSONObject) partnerService.getCount(deviceId, month, year);
        return this.setSuccessMessage(request, response, msg);
    }

    /**
     * 获取合伙人申请的状态
     *
     * @param request
     * @param response
     * @param uid
     * @return
     */
    @RequestMapping(value = "/partnerApply", method = RequestMethod.POST)
    @ResponseBody
    public String partnerApply(HttpServletRequest request, HttpServletResponse response,
                               @RequestParam("uid") String uid) {
        JSONObject msg = (JSONObject) partnerService.getPartnerApplyStatus(uid);
        return this.setSuccessMessage(request, response, msg);
    }


    /**
     * 头像
     */
    @RequestMapping(value = "/addImg", method = RequestMethod.POST)
    @ResponseBody
    public String addImg(HttpServletRequest request, HttpServletResponse response,
                         @RequestParam("file") String file,
                         @RequestParam("partner_id") String pid) throws FileNotFoundException {//partner_id
        String reMassage = (String) partnerService.addImg(file, pid);
        if (reMassage.equals("error")) {
            return super.setFailureMessage(request, response, reMassage);
        } else {
            return super.setSuccessMessage(request, response, reMassage);
        }
    }


    /**
     * 合伙人扩建
     * 填写资料表，再通过验证码后调用此接口，将信息一并传入
     */
    @RequestMapping(value = "/updateStation", method = RequestMethod.POST)
    @ResponseBody
    public String updateStation(HttpServletRequest request, HttpServletResponse response,
                                @RequestParam("name") String name,//真实姓名
                                @RequestParam("phone") String phone,//电话
                                @RequestParam("workname") String workname,//运营商名称
                                @RequestParam("workaddress") String workaddress,//营运商地址
                                @RequestParam("type") String type,//电流类型 DC直流 AC交流
                                @RequestParam("power") String power,//功率，string
                                @RequestParam("num") String number,//需求数量
                                @RequestParam("useruuid") String useruuid,//用户uuid
                                @RequestParam("partnerid") String partnerid) {//合伙人id
        PartnerUpdate update = new PartnerUpdate();
        update.setUsername(name);
        update.setPhone(phone);
        update.setWorkname(workname);
        update.setWorkaddress(workaddress);
        update.setType(type);
        update.setPower(power);
        update.setNumber(Integer.valueOf(number));
        update.setUseruuid(useruuid);
        update.setPartnerid(partnerid);
        int i = partnerService.partnerUpdate(update);
        if (i > 0) {
            return super.setSuccessMessage(request, response, String.valueOf(i));
        } else {
            return super.setFailureMessage(request, response, "0");
        }
    }

    /**
     * 查看扩建申请单子
     */
    @RequestMapping(value = "/findUpdateStation", method = RequestMethod.POST)
    @ResponseBody
    public String findUpdateStation(HttpServletRequest request, HttpServletResponse response,
                                    @RequestParam("id") String id) {
        List list = partnerService.findPartnerUpdate(id);
        return super.setSuccessMessage(request, response, list);
    }
}