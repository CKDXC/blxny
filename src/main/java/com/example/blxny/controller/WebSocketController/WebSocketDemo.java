package com.example.blxny.controller.WebSocketController;


import com.example.blxny.service.FindSalesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 消息传输规则：
 * service：
 * AA:初始判断是否上班 0为没上班，1为上班
 * AB:初始化判断是否有新的订单0 没有 1 有（有的时候后面会继续跟上参数）
 * view：
 * 01：调整为上班状态
 * 02：调整为下班状态
 */

@ServerEndpoint("/websocket/{userno}")
@Component
public class WebSocketDemo {
    private static FindSalesService service11;

    @Autowired
    private void setService11(FindSalesService service11) {
        this.service11 = service11;
    }

    //静态变量，用来记录当前在线连接数。应该把它设计成线程安全的。
    private static int onlineCount = 0;
    //concurrent包的线程安全Set，用来存放每个客户端对应的MyWebSocket对象。若要实现服务端与单一客户端通信的话，可以使用Map来存放，其中Key可以为用户标识
    private static ConcurrentHashMap<String, WebSocketDemo> webSocketSet = new ConcurrentHashMap<String, WebSocketDemo>();
    //与某个客户端的连接会话，需要通过它来给客户端发送数据
    private Session session;
    //当前发消息的人员编号
    private String userno = "";

    /**
     * 连接建立成功调用的方法
     *
     * @param session 可选的参数。session为与某个客户端的连接会话，需要通过它来给客户端发送数据
     */
    @OnOpen
    public void onOpen(@PathParam(value = "userno") String param, Session session, EndpointConfig config) throws IOException {
        userno = param;//接收到发送消息的人员编号
        this.session = session;
        webSocketSet.put(param, this);//加入map中
        addOnlineCount();             //在线数加1
        System.out.println("有新连接加入" + getOnlineCount() + "用户为" + param);
        //判断是否在上班状态
        String workType = service11.findTypeNow(param);
        //判断有没有接到新的订单
        String orderInfo = service11.findOrderInfo(param);
        webSocketSet.get(param).sendMessage("AA|" + workType);
    }

    /**
     * 连接关闭调用的方法
     */
    @OnClose
    public void onClose() {
        if (!userno.equals("")) {
            webSocketSet.remove(userno);  //从set中删除
            subOnlineCount();           //在线数减1
            System.out.println("有一连接关闭" + getOnlineCount());
        }
    }

    /**
     * 收到客户端消息后调用的方法
     */
    @OnMessage
    public void onMessage(String message, Session session) {
        System.out.println("来自网页的消息:" + message);
        String[] info = message.split("\\|");
        messageTool(info);
    }

    public String messageTool(String[] info) {
        switch (info[0]) {
            case "01":
                service11.changeSalesTypeNow(info[1], 1, null, info[2], info[3]);
                return null;
            case "02":
                service11.changeSalesTypeNow(info[1], 2, null, null, null);
                return null;
            default:
                return null;
        }
    }

    public static synchronized int getOnlineCount() {
        return onlineCount;
    }

    public static synchronized void addOnlineCount() {
        WebSocketDemo.onlineCount++;
    }

    public static synchronized void subOnlineCount() {
        WebSocketDemo.onlineCount--;
    }

    public void sendMessage(String message) throws IOException {//正在在发消息的东西
        //this.session.getBasicRemote().sendText(message);
        this.session.getAsyncRemote().sendText(message);
    }

    /**
     * 发生错误时调用
     */
    @OnError
    public void onError(Session session, Throwable error) {
        System.out.println("发生错误");
        error.printStackTrace();
    }

}