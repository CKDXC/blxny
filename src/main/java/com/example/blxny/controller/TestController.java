package com.example.blxny.controller;

import com.example.blxny.service.ChargeService;
import com.example.blxny.service.OrderService;
import com.example.blxny.util.BaseController;
import com.example.blxny.util.MQUtil;
import com.example.blxny.util.RedisUtil;
import com.example.blxny.util.SendThread;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.Socket;

@RestController
@RequestMapping("/test")
public class TestController extends BaseController {

    @Autowired
    private ChargeService chargeService;
    @Autowired
    private RedisUtil redisUtil;

    /**
     * 开始充电
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "startCharge", method = RequestMethod.GET)
    public String startCharge(HttpServletRequest request, HttpServletResponse response,
                              @RequestParam("uid") String uid) {
        JSONObject msg = (JSONObject) chargeService.startCharge("0000050104001001", "01", "01", 0, uid);
        return this.setSuccessMessage(request, response, msg);
    }

    @RequestMapping(value = "socket", method = RequestMethod.GET)
    public String socket(HttpServletRequest request, HttpServletResponse response) {
        try {
            SendThread sendThread = new SendThread(new Socket("192.168.1.16", 9010));
            sendThread.setCmd("1231231");
            MQUtil.putMsg("/192.168.1.16:9010", "");
            sendThread.start();
            String flg = "";
            int times = 0;
            while ("".equals(flg) || times > 5) {
                flg = MQUtil.getMsg().get("/192.168.1.16:9010");
                Thread.sleep(1000);
                times++;
                System.out.println(MQUtil.getMsg().toString());
                System.out.println("msg\t" + flg);
            }
            return this.setSuccessMessage(request, response, flg);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return "失败";
    }

    @RequestMapping(value = "setRule", method = RequestMethod.GET)
    public String setRule(HttpServletRequest request, HttpServletResponse response,
                          @RequestParam("deviceId") String deviceId,
                          @RequestParam("ruleId") int ruleId) {
        return this.setSuccessMessage(request, response, chargeService.ruleSetToDevice(deviceId, ruleId));
    }

    @RequestMapping(value = "endCharge", method = RequestMethod.GET)
    public String endCharge(HttpServletRequest request, HttpServletResponse response,
                            @RequestParam("orderId") String orderId) {
        JSONObject msg = (JSONObject) chargeService.endCharge(orderId, "01");
        return this.setSuccessMessage(request, response, msg);
    }

//    @GetMapping(value = "appoCharge")
//    public String appoCharge(HttpServletRequest request, HttpServletResponse response) {
//        JSONObject msg = (JSONObject) orderService.addAppointment("6", "30", "0000050104001001", "2");
//        return this.setSuccessMessage(request, response, msg);
//    }

    @GetMapping(value = "cancelAppo")
    public String cancelAppo(HttpServletRequest request, HttpServletResponse response,
                             @RequestParam("appoId") String appoId) {
        String msg = chargeService.cancelAppointment(appoId);
        return this.setSuccessMessage(request, response, msg);
    }

    @GetMapping(value = "proofTime")
    public String proofTime(HttpServletRequest request, HttpServletResponse response,
                            @RequestParam("deviceId") String deviceId) {
        return this.setSuccessMessage(request, response, chargeService.proofTime(deviceId));
    }

    @GetMapping(value = "reStart")
    public String reStart(HttpServletRequest request, HttpServletResponse response,
                          @RequestParam("deviceId") String deviceId) {
        return this.setSuccessMessage(request, response, chargeService.reStart(deviceId));
    }

    @GetMapping(value = "upGrade")
    public String upGrade(HttpServletRequest request, HttpServletResponse response,
                          @RequestParam("deviceId") String deviceId,
                          @RequestParam("ruleId") int ruleId) {
        return this.setSuccessMessage(request, response, chargeService.upgrade(deviceId, ruleId));
    }

    @RequestMapping(value = "/addAppo", method = RequestMethod.GET)
    public String addAppo(HttpServletRequest request, HttpServletResponse response,
                          @RequestParam("uid") String uid,
                          @RequestParam("time") String time,
                          @RequestParam("deviceId") String deviceId,
                          @RequestParam("money") String money,
                          @RequestParam("payType") String payType) {
        JSONObject msg = (JSONObject) chargeService.addAppointment(uid, time, deviceId, money, payType);
        return this.setSuccessMessage(request, response, msg);
    }

    @GetMapping(value = "/redis")
    public String redi(HttpServletRequest request, HttpServletResponse response){
        String str = redisUtil.get("a");
        return this.setSuccessMessage(request, response, str);
    }

    @RequestMapping(value = "/getChargeData", method = RequestMethod.GET)
    public String getChargeData(HttpServletRequest request, HttpServletResponse response,
                                @RequestParam("orderId") String orderId) {
        JSONObject msg = (JSONObject) chargeService.getChargeData(orderId);
        return this.setSuccessMessage(request, response, msg);
    }
}
