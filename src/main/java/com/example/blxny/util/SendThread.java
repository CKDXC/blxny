package com.example.blxny.util;

import java.io.*;
import java.net.Socket;
import java.util.Scanner;
/**
 * 发送消息类
 */
public class SendThread extends Thread{
    private PrintWriter pw;
    private Socket socket;
    private String cmd;//发送的消息

    public SendThread() {    }

    public SendThread(Socket socket, String cmd) {//传入Socket和用户名
        this.socket = socket;
        this.cmd = cmd;
    }

    public SendThread(Socket socket) {
        this.socket = socket;
    }

    public String getCmd() {
        return cmd;
    }

    public void setCmd(String cmd) {
        this.cmd = cmd;
    }

    @Override
    public void run() {
        try {
            if (null!=cmd) {
//                pw = new PrintWriter(socket.getOutputStream(), true);//打印流 通过Socket用于发送消息
//                pw.println(cmd);//将消息发送出去

                InputStream is = new ByteArrayInputStream(cmd.getBytes());
                DataOutputStream out = new DataOutputStream(socket.getOutputStream());
                String s = new BufferedReader(new InputStreamReader(is)).readLine();
                System.out.print("服务器端发出报文：" + s + "\n");
                out.write(HexConvert.parseHexStr2Byte(s.replace(" ", "")));
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (pw != null)
                pw.close();
        }
    }
}
