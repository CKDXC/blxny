package com.example.blxny.util;

/**
 * Created by wly on 2018/4/17.
 */
public class HexConvert {
 
	public static String  convertStringToHex(String str){
	 
	    char[] chars = str.toCharArray();
	 
	    StringBuffer hex = new StringBuffer();
	    for(int i = 0; i < chars.length; i++){
	        hex.append(Integer.toHexString((int)chars[i]));
	    }
	 
	    return hex.toString();
	}
	 
	public static String convertHexToString(String hex){
	 
	    StringBuilder sb = new StringBuilder();
	    StringBuilder sb2 = new StringBuilder();
	 
	    for( int i=0; i<hex.length()-1; i+=2 ){
	 
	        String s = hex.substring(i, (i + 2));           
	        int decimal = Integer.parseInt(s, 16);          
	        sb.append((char)decimal);
	        sb2.append(decimal);//调用未返回值？SB2值的作用？
	    }
	 
	    return sb.toString();
	}
	public static byte[] hexStringToBytes(String hexString) {
	    if (hexString == null || hexString.equals("")) {
	        return null;
	    }
	    // toUpperCase将字符串中的所有字符转换为大写
	    hexString = hexString.toUpperCase();
	    int length = hexString.length() / 2;
	    // toCharArray将此字符串转换为一个新的字符数组。
	    char[] hexChars = hexString.toCharArray();
	    byte[] d = new byte[length];
	    for (int i = 0; i < length; i++) {
	        int pos = i * 2;
	        d[i] = (byte) (charToByte(hexChars[pos]) << 4 | charToByte(hexChars[pos + 1]));
	    }
	    return d;
	}
	//返回匹配字符
	private static byte charToByte(char c) {
	    return (byte) "0123456789ABCDEF".indexOf(c);
	}
	 
	//将字节数组转换为short类型，即统计字符串长度
	public static short bytes2Short2(byte[] b) {
	    short i = (short) (((b[1] & 0xff) << 8) | b[0] & 0xff);
	    return i;
	}
	//将字节数组转换为16进制字符串
	public static String BinaryToHexString(byte[] bytes) {
	    String hexStr = "0123456789ABCDEF";
	    String result = "";
	    String hex = "";
	    for (byte b : bytes) {
	        hex = String.valueOf(hexStr.charAt((b & 0xF0) >> 4));
	        hex += String.valueOf(hexStr.charAt(b & 0x0F));
	        result += hex + " ";
	    }
	    return result;
	}
	
	public static boolean isHave(String[] str,String s){   
	    int i = str.length;  
	    while (i-- > 0){  
	        if(str[i].equals(s)){
	            return true;  
	        }  
	    }  
	    return false;  
	} 
    
    private static String getCrc(byte[] data) {
		int high;
		int flag;
		// 16位寄存器，所有数位均为1
		int wcrc = 0xffff;
		for (int i = 0; i < data.length; i++) {
			// 16 位寄存器的高位字节
			high = wcrc >> 8;
			// 取被校验串的一个字节与 16 位寄存器的高位字节进行“异或”运算
			wcrc = high ^ data[i];
			for (int j = 0; j < 8; j++) {
				flag = wcrc & 0x0001;
				// 把这个 16 寄存器向右移一位
				wcrc = wcrc >> 1;
				// 若向右(标记位)移出的数位是 1,则生成多项式 1010 0000 0000 0001 和这个寄存器进行“异或”运算
				if (flag == 1)
					wcrc ^= 0xa001;
			}
		}
		return Integer.toHexString(wcrc);
    }

	/**
	 * hex转byte数组
	 * @param hex
	 * @return
	 */
	public static byte[] hexToByte(String hex){
		int m = 0, n = 0;
		int byteLen = hex.length() / 2;
		byte[] ret = new byte[byteLen];
		for (int i = 0; i < byteLen; i++) {
			m = i * 2 + 1;
			n = m + 1;
			int intVal = Integer.decode("0x" + hex.substring(i * 2, m) + hex.substring(m, n));
			ret[i] = Byte.valueOf((byte)intVal);
		}
		return ret;
	}

	public static byte[] parseHexStr2Byte(String hexStr) {
        if (hexStr.length() < 1)
            return null;
            byte[] result = new byte[hexStr.length() / 2];
            for (int i = 0; i < hexStr.length() / 2; i++) {
                int high = Integer.parseInt(hexStr.substring(i * 2, i * 2 + 1), 16);
                int low = Integer.parseInt(hexStr.substring(i * 2 + 1, i * 2 + 2), 16);
                result[i] = (byte) (high * 16 + low);
            }
            return result;
        }
	 
    public static byte[] subBytes(byte[] src, int begin, int count) {
        byte[] bs = new byte[count];
        System.arraycopy(src, begin, bs, 0, count);
        return bs;
    }
    
    
}