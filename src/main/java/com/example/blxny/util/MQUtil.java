package com.example.blxny.util;

import java.util.HashMap;
import java.util.Map;

public class MQUtil {
    private static Map<String,String> msg = new HashMap<>();

    public static Map<String, String> getMsg() {
        return msg;
    }

    public static void setMsg(Map<String, String> msg) {
        MQUtil.msg = msg;
    }

    public static void putMsg(String key, String value){
        msg.put(key,value);
        System.out.println("设置KV\t"+"key\t"+key+"\tvalue\t"+value);
    }

    public static void removeMsg(String key){
        msg.remove(key);
    }
}
