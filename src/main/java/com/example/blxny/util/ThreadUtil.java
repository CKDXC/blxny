package com.example.blxny.util;

public class ThreadUtil {
    public static Thread getThreadByName(String threadName) {
        ThreadGroup currentGroup = Thread.currentThread().getThreadGroup();
        int noThreads = currentGroup.activeCount();
        Thread[] Threads = new Thread[noThreads];
        currentGroup.enumerate(Threads);
        for (int i = 0; i < noThreads; i++) {
            if (Threads[i].getName().equals(threadName)) {
                System.out.println("找到线程\t" + threadName);
                return Threads[i];
            }
        }
        return null;
    }
}
