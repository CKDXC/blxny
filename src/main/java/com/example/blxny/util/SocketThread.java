package com.example.blxny.util;

import com.example.blxny.service.ChargeService;
import com.example.blxny.service.impl.ChargeServiceImpl;
import org.springframework.context.ApplicationContext;
import java.io.*;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Socket多线程处理类 用来处理服务端接收到的客户端请求（处理Socket对象）
 */

public class SocketThread extends Thread {
    private Socket socket;
    private boolean isLogin = false;
    private boolean isStart = false;
    private ArrayList<String> reqs = new ArrayList<>();
    private ApplicationContext applicationContext;

    public SocketThread(Socket socket, ApplicationContext applicationContext) {
        this.socket = socket;
        this.applicationContext = applicationContext;
    }

    public Socket getSocket() {
        return socket;
    }

    public void setSocket(Socket socket) {
        this.socket = socket;
    }

    public void run() {
        // 根据输入输出流和客户端连接
        try {
            System.out.println("已接收到客户端");
            String[] momery = {};
            InputStream inputStream = socket.getInputStream();
            // 得到一个输入流，接收客户端传递的信息
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);// 提高效率，将自己字节流转为字符流
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);// 加入缓冲区
            byte[] b = new byte[1024];
            int len;
            int heartTimes = 0;
            int reConnectTimes = 0;
            String address = socket.getRemoteSocketAddress().toString();
            while ((len = inputStream.read(b)) != -1) {
                String hxestring = HexConvert.BinaryToHexString(b);
                String hxestring1 = HexConvert.BinaryToHexString(HexConvert.subBytes(b, 0, len));
                System.out.print("客户端说：" + hxestring1 + "\n");
                String hexs = hxestring1.replace(" ", "");
                String subStrhex = hexs.substring(2, hexs.length() - 4);
                byte[] c = CRC16.hexToByte(subStrhex);
                String strCrc = HexConvert.BinaryToHexString(CRC16.GetCRC(c)).replace(" ", "");
                String strCrc16 = hexs.substring(hexs.length() - 4, hexs.length());
                // CRc校验
                if (strCrc.equals(strCrc16)) {
                    System.out.print("c1:" + strCrc16 + "===c2：" + strCrc + "\n");
                    String cmdCode = hexs.substring(2, 4);
                    String[] arrCmdCode = {"01", "82", "83", "86", "87", "08", "09", "0B", "0C", "0D", "8E", "1C", "9D", "9E"};
                    if (!HexConvert.isHave(arrCmdCode, cmdCode)) {
                        reConnectTimes++;
                        System.out.println("无效指令\t" + cmdCode + "\t" + reConnectTimes + "次");
                    }
                    //是否登录
                    //01请求--C-S--启动登录--需要调用--登录校验OX
                    if (cmdCode.equals("01")) {
                        String str1 = hexs.substring(4, 8);
                        String reqContent = hexs.substring(12, hexs.length() - 4);
                        String deviceId = reqContent.substring(0, 16);
                        String deviceType = reqContent.substring(16, 18);
                        ChargeService chargeService = applicationContext.getBean(ChargeServiceImpl.class);
                        boolean flg = chargeService.setAddress(deviceId, address);
                        String resContent = chargeService.login(reqContent);
                        String hexStr02 = "81 " + substrs(str1) + "00 09 " + substrs(resContent);
                        byte[] serverStr02 = CRC16.hexToByte(hexStr02.replace(" ", ""));
                        String serverstrCrc02 = HexConvert.BinaryToHexString(CRC16.GetCRC(serverStr02));
                        String hexserverStr02 = "68 " + HexConvert.BinaryToHexString(serverStr02) + serverstrCrc02;
                        InputStream is = new ByteArrayInputStream(hexserverStr02.getBytes());
                        DataOutputStream out = new DataOutputStream(socket.getOutputStream());
                        String s = new BufferedReader(new InputStreamReader(is)).readLine();
                        out.write(HexConvert.parseHexStr2Byte(s.replace(" ", "")));
                        chargeService.ruleSetToDevice(socket, deviceId, deviceType);
                        String result = resContent.substring(16, 18);
                        if ("00".equals(result)) {
                            System.out.print("服务器端说：" + s + "\n");
                            System.out.print("登录成功,是否启动\n");// 磊加
                            isLogin = true;
                        } else {
                            System.out.print("服务器端说：" + s + "\n");
                            System.out.print("登录失败\n");// 磊加
                            isLogin = false;
                        }
                    } else {
                        //登陆判定
                        if (isLogin == true) {
                            //0C心跳
                            if (cmdCode.equals("0C")) {
                                heartTimes++;
                                System.out.println("heartTimes\t" + heartTimes);
                                String str1 = hexs.substring(4, 8);
                                Date now = new Date();
                                SimpleDateFormat dateFormat = new SimpleDateFormat("yyMMddHHmmss");// 可以方便地修改日期格式
                                String createtime = dateFormat.format(now);
                                String BCD = "";
                                for (int i = 0; i < 6; i++) {
                                    String temp = createtime.substring(i * 2, i * 2 + 2);
                                    byte[] bcd = BCDUtil.str2Bcd(temp);
                                    BCD += fullZeroStr(HexConvert.BinaryToHexString(bcd).replace(" ", ""), 2);
//                            BCD += fullZeroStr(Integer.toHexString(Integer.parseInt(createtime.substring(i * 2, i * 2 + 2))).toUpperCase(), 2);
                                }
                                String StrXT = "8C " + substrs(str1) + "00 06 " + substrs(BCD);
                                byte[] xt = CRC16.hexToByte(StrXT.replace(" ", ""));
                                String strXTCrc = HexConvert.BinaryToHexString(CRC16.GetCRC(xt));
                                String hexXTStr = "68 " + StrXT + strXTCrc;
                                InputStream isxt = new ByteArrayInputStream(hexXTStr.getBytes());
                                DataOutputStream outxt = new DataOutputStream(socket.getOutputStream());
                                String sxt = new BufferedReader(new InputStreamReader(isxt)).readLine();
                                System.out.print("服务器端说：" + sxt + "\n");
                                outxt.write(HexConvert.parseHexStr2Byte(sxt.replace(" ", "")));
                            }
                            //心跳以外
                            else {
                                //是否启动
//                                if (isStart == false) {
                                //82请求--预约充电回复--没有回复--待测
                                if (cmdCode.equals("82")) {
                                    System.out.println("82请求--预约充电回复");
                                    String resContent = hexs.substring(12, hexs.length() - 4);
                                    String msgCode = hexs.substring(4, 8);
                                    MQUtil.putMsg(address + msgCode, resContent);//设置桩对应key的消息
                                }
                                //83请求--预约充电取消回复--没有回复--返回值给主线程
                                else if (cmdCode.equals("83")) {
                                    String resContent = hexs.substring(12, hexs.length() - 4);
                                    String msgCode = hexs.substring(4, 8);
                                    MQUtil.putMsg(address + msgCode, resContent);
                                }
                                //86请求--远程启动充电回复--没有回复--这个要有 返回值
                                else if (cmdCode.equals("86")) {
                                    System.out.println("进入86------------");
                                    String resContent = hexs.substring(12, hexs.length() - 4);
                                    String msgCode = hexs.substring(4, 8);
                                    MQUtil.putMsg(address + msgCode, resContent);
                                    String result = resContent.substring(16, 18);
                                    if ("00".equals(result)) {
                                        isStart = true;
                                    }
                                }
                                //0D请求--告警--这里有一个警告--保存到数据库
                                else if (cmdCode.equals("0D")) {
                                    System.out.println("进入0D--------");
                                    String reqContent = hexs.substring(12, hexs.length() - 4);
                                    ChargeService chargeService = applicationContext.getBean(ChargeServiceImpl.class);
                                    chargeService.alarmDevice(reqContent);
                                    String str1 = hexs.substring(4, 8);
                                    String hexStr08 = "0D " + substrs(str1) + "00 00";
                                    byte[] serverStr8 = CRC16.hexToByte(hexStr08.replace(" ", ""));
                                    String serverstrCrc08 = HexConvert.BinaryToHexString(CRC16.GetCRC(serverStr8));
                                    String hexserverStr08 = "68 " + HexConvert.BinaryToHexString(serverStr8) + serverstrCrc08;
                                    InputStream is08 = new ByteArrayInputStream(hexserverStr08.getBytes());
                                    DataOutputStream out08 = new DataOutputStream(socket.getOutputStream());
                                    String s08 = new BufferedReader(new InputStreamReader(is08)).readLine();
                                    System.out.print("服务器端说：" + s08 + "\n");
                                    out08.write(HexConvert.parseHexStr2Byte(s08.replace(" ", "")));
                                }
                                //8E请求--校时回复--不用回复
                                else if (cmdCode.equals("8E")) {
                                    System.out.println("进入8E-------");
                                    String resContent = hexs.substring(12, hexs.length() - 4);
                                    String msgCode = hexs.substring(4, 8);
                                    MQUtil.putMsg(address + msgCode, resContent);
                                }
                                //1C请求--设置二维码--
                                else if (cmdCode.equals("1C")) {
                                    String str1 = hexs.substring(4, 8);
                                    String reqContent = hexs.substring(12, hexs.length() - 4);
                                    ChargeService chargeService = applicationContext.getBean(ChargeServiceImpl.class);
                                    String resContent = chargeService.setQRcode(hexs);
                                    InputStream is08 = new ByteArrayInputStream(resContent.getBytes());
                                    DataOutputStream out08 = new DataOutputStream(socket.getOutputStream());
                                    String s08 = new BufferedReader(new InputStreamReader(is08)).readLine();
                                    System.out.print("服务器端说：" + s08 + "\n");
                                    out08.write(HexConvert.parseHexStr2Byte(s08.replace(" ", "")));
                                }
                                //9D请求--重启回复处理--
                                else if (cmdCode.equals("9D")) {
                                    System.out.println("进入9D------------");
                                    String resContent = hexs.substring(12, hexs.length() - 4);
                                    String msgCode = hexs.substring(4, 8);
                                    MQUtil.putMsg(address + msgCode, resContent);
                                    isLogin = false;
                                }
                                //9E请求--远程升级回复处理--
                                else if (cmdCode.equals("9E")) {
                                    System.out.println("进入9D------------");
                                    String resContent = hexs.substring(12, hexs.length() - 4);
//                                    address = socket.getLocalSocketAddress();
                                    String msgCode = hexs.substring(4, 8);
                                    MQUtil.putMsg(address + msgCode, resContent);
                                }
                                //------ 分割线
                                //87请求--远程结束充电回复--不用回复--调用service 数据库交互
                                if (cmdCode.equals("87")) {
                                    System.out.println("进入87-------------");
                                    String resContent = hexs.substring(12, hexs.length() - 4);
                                    String msgCode = hexs.substring(4, 8);
                                    MQUtil.putMsg(address + msgCode, resContent);
                                    isStart = false;
                                }
                                //08请求--上传充电记录--调用service 数据库交互
                                else if (cmdCode.equals("08")) {
                                    System.out.println("进入08请求");
                                    String str1 = hexs.substring(4, 8);
                                    String resContent = hexs.substring(12, hexs.length() - 4);
                                    ChargeService chargeService = applicationContext.getBean(ChargeServiceImpl.class);
                                    int result = chargeService.upChargeLog(resContent);
//                                    String hexStr08 = "88 " + substrs(str1) + "00 09 " + substrs(resContent.substring(2, 18)) + " 0" + (result == 0 ? 1 : 0);
                                    String hexStr08 = "88 " + substrs(str1) + "00 09 " + substrs(resContent.substring(2, 18)) + " 00";
                                    byte[] serverStr8 = CRC16.hexToByte(hexStr08.replace(" ", ""));
                                    String serverstrCrc08 = HexConvert.BinaryToHexString(CRC16.GetCRC(serverStr8));
                                    String hexserverStr08 = "68 " + HexConvert.BinaryToHexString(serverStr8) + serverstrCrc08;
                                    InputStream is08 = new ByteArrayInputStream(hexserverStr08.getBytes());
                                    DataOutputStream out08 = new DataOutputStream(socket.getOutputStream());
                                    String s08 = new BufferedReader(new InputStreamReader(is08)).readLine();
                                    System.out.print("服务器端说：" + s08 + "\n");
                                    out08.write(HexConvert.parseHexStr2Byte(s08.replace(" ", "")));
                                }
                                //09请求--上传充电进度--没有回复
                                else if (cmdCode.equals("09")) {
                                    System.out.println("进入09请求");
                                    String msgCode = hexs.substring(4, 8);
                                    String resContent = hexs.substring(12, hexs.length() - 4);
                                    ChargeService chargeService = applicationContext.getBean(ChargeServiceImpl.class);
                                    chargeService.upChargeData(resContent);

                                }
                                //8B请求--计费规则设置回复--不用回复--调用service 数据库交互
                                else if (cmdCode.equals("8B")) {
                                    System.out.println("进入8B------------");
                                    String resContent = hexs.substring(12, hexs.length() - 4);
                                    ChargeService chargeService = applicationContext.getBean(ChargeServiceImpl.class);
                                    chargeService.dealRuleSet(resContent, address);
                                }
                                //0D请求--告警--这里有一个警告--推送到用户手机吧
                                else if (cmdCode.equals("0D")) {
                                    System.out.println("进入0D--------");
                                    String reqContent = hexs.substring(12, hexs.length() - 4);
                                    ChargeService chargeService = applicationContext.getBean(ChargeServiceImpl.class);
                                    chargeService.alarmDevice(reqContent);
                                    String str1 = hexs.substring(4, 8);
                                    String hexStr08 = "0D " + substrs(str1) + "00 00";
                                    byte[] serverStr8 = CRC16.hexToByte(hexStr08.replace(" ", ""));
                                    String serverstrCrc08 = HexConvert.BinaryToHexString(CRC16.GetCRC(serverStr8));
                                    String hexserverStr08 = "68 " + HexConvert.BinaryToHexString(serverStr8) + serverstrCrc08;
                                    InputStream is08 = new ByteArrayInputStream(hexserverStr08.getBytes());
                                    DataOutputStream out08 = new DataOutputStream(socket.getOutputStream());
                                    String s08 = new BufferedReader(new InputStreamReader(is08)).readLine();
                                    System.out.print("服务器端说：" + s08 + "\n");
                                    out08.write(HexConvert.parseHexStr2Byte(s08.replace(" ", "")));
                                }
                            }
                        } else {
                            reConnectTimes++;
                            System.out.println("未登录\t" + reConnectTimes);
                        }
                    }
                } else {
                    System.out.println("CRc校验失败被丢弃");
                }
            }
            OutputStream outputStream = socket.getOutputStream();// 获取一个输出流，向服务端发送信息
            PrintWriter printWriter = new PrintWriter(outputStream);// 将输出流包装成打印流
            printWriter.print("你好，服务端已接收到您的信息");
            printWriter.flush();
            socket.shutdownOutput();// 关闭输出流
            bufferedReader.close();
            inputStream.close();
            printWriter.close();
            outputStream.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    //------------util

    public static String substrs(String replace) {
        String regex = "(.{2})";
        replace = replace.replaceAll(regex, "$1 ");
        return replace;
    }

    //字符串转ASCLL码
    public static String stringToAscii(String value) {
        StringBuffer sbu = new StringBuffer();
        char[] chars = value.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            if (i != chars.length - 1) {
                sbu.append((int) chars[i]).append(",");
            } else {
                sbu.append((int) chars[i]);
            }
        }
        return sbu.toString();
    }

    public static String substrspace(String replace) {
        String regex = "(.{4})";
        replace = replace.replaceAll(regex, "$1 ");
        return replace;
    }

    //16进制字符串转10进制FLOAT
    public float stringToFloat(String str) {
        return Long.parseLong(str, 16) / 1000;
    }

    //YYMMDDHHMMSS转换成date
    public static Date stringToDate(String str) {
        SimpleDateFormat df = new SimpleDateFormat("yyMMddhhmmss");
        try {
            Date date = df.parse(str);
            System.out.println(date.getTime());
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return new Date();
    }

    //补完指定个数的零
    public static String fullZeroStr(String str, int zero) {
        String msg = "0000000000000000000" + str;
        return msg.substring(msg.length() - zero);
    }

    /**
     * 10进制转bcd
     *
     * @param str 10进制数字 String.valueOf(int number);将10进制数字转成字符串传入此参数
     * @return bcd码
     */
    public static String DecimaltoBcd(String str) {
        String b_num = "";
        for (int i = 0; i < str.length(); i++) {
            String b = Integer.toBinaryString(Integer.parseInt(str.valueOf(str.charAt(i))));
            int b_len = 4 - b.length();
            for (int j = 0; j < b_len; j++) {
                b = "0" + b;
            }
            b_num += b;
        }
        return b_num;
    }


    /**
     * 16进制转bcd
     * 将16进制转成10进制，再将10进制转成bcd
     *
     * @param hex 16进制数字String.valueOf(int number);这里忽略16进制的前缀0x，只转后面的数字为字符串类型，将16进制数字转成字符串传入此参数
     * @return bcd码
     */
    public static String HextoBcd(String hex) {
        int decimal = Integer.parseInt(hex, 16);
        String bcd = DecimaltoBcd(String.valueOf(decimal));
        return bcd;
    }

    //数组合并
    public static byte[] arrayApend(byte[] byte1, byte[] byte2) {
        int len1 = byte1.length;
        int len2 = byte2.length;
        byte[] byte3 = new byte[len1 + len2];
        for (int i = 0; i < len1; i++) {
            byte3[i] = byte1[i];
        }
        for (int j = len1; j < byte3.length; j++) {
            byte3[j] = byte2[j - len1];
        }
        return byte3;
    }

    /** */
    /**
     * 把字节数组转换成16进制字符串
     *
     * @param bArray
     * @return
     */
    public static final String bytesToHexString(byte[] bArray) {
        StringBuffer sb = new StringBuffer(bArray.length);
        String sTemp;
        for (int i = 0; i < bArray.length; i++) {
            sTemp = Integer.toHexString(0xFF & bArray[i]);
            if (sTemp.length() < 2)
                sb.append(0);
            sb.append(sTemp.toUpperCase());
        }
        return sb.toString();
    }

    public static byte[] StrToBCDBytes(String s) {
        if (s.length() % 2 != 0) {
            s = "0" + s;
        }
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        char[] cs = s.toCharArray();
        for (int i = 0; i < cs.length; i += 2) {
            int high = cs[i] - 48;
            int low = cs[i + 1] - 48;
            baos.write(high << 4 | low);
        }
        return baos.toByteArray();
    }

    /**
     * 把16进制字符串转换成字节数组
     *
     * @param hex
     * @return
     */
    public static byte[] hexStringToByte(String hex) {
        int len = (hex.length() / 2);
        byte[] result = new byte[len];
        char[] achar = hex.toCharArray();
        for (int i = 0; i < len; i++) {
            int pos = i * 2;
            result[i] = (byte) (toByte(achar[pos]) << 4 | toByte(achar[pos + 1]));
        }
        return result;
    }

    private static byte toByte(char c) {
        byte b = (byte) "0123456789ABCDEF".indexOf(c);
        return b;
    }

    public static String Bytes2HexString(byte[] b) {
        String ret = "";
        for (int i = 0; i < b.length; i++) {
            String hex = Integer.toHexString(b[i] & 0xFF);
            if (hex.length() == 1) {
                hex = '0' + hex;
            }
            ret += " 0x" + hex.toUpperCase();
        }
        return ret;
    }
}