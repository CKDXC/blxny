package com.example.blxny.util;

import java.beans.PropertyEditor;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSON;
import net.sf.json.JSONObject;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.beans.propertyeditors.PropertiesEditor;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;

public class BaseController
{
	public class DoubleEditor extends PropertiesEditor
	{
		public void setAsText(String text) throws IllegalArgumentException
		{
			if (text == null || text.equals(""))
			{
				text = "0";
			}
			setValue(Double.parseDouble(text));
		}

		@Override
		public String getAsText()
		{
			return getValue().toString();
		}
	}
	
	public class FloatEditor extends PropertiesEditor
	{
		@Override
		public void setAsText(String text) throws IllegalArgumentException
		{
			if (text == null || text.equals(""))
			{
				text = "0";
			}
			setValue(Float.parseFloat(text));
		}

		@Override
		public String getAsText()
		{
			return getValue().toString();
		}
	}
	
	public class LongEditor extends PropertiesEditor
	{
		@Override
		public void setAsText(String text) throws IllegalArgumentException
		{
			if (text == null || text.equals(""))
			{
				text = "0";
			}
			setValue(Long.parseLong(text));
		}

		@Override
		public String getAsText()
		{
			return getValue().toString();
		}
	}
	
	public class IntegerEditor extends PropertiesEditor
	{
		@Override
		public void setAsText(String text) throws IllegalArgumentException
		{
			if (text == null || text.equals(""))
			{
				text = "0";
			}
			setValue(Integer.parseInt(text));
		}

		@Override
		public String getAsText()
		{
			return getValue().toString();
		}
	}

	@InitBinder
	public void dataBinder(WebDataBinder binder)
	{
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		dateFormat.setLenient(false);
		PropertyEditor propertyEditor = new CustomDateEditor(dateFormat, true); // 第二个参数表示是否允许为空
		binder.registerCustomEditor(Date.class, propertyEditor);
		binder.registerCustomEditor(Double.class, new DoubleEditor());    
		binder.registerCustomEditor(Float.class, new FloatEditor());    
		binder.registerCustomEditor(Long.class, new LongEditor());    
		binder.registerCustomEditor(Integer.class, new IntegerEditor()); 
	}

	public void setValue(double parseDouble) {
		// TODO Auto-generated method stub
		
	}

	/**
	 * 设置跨域访问头信息
	 */
	private void setCrossSite(HttpServletRequest request, HttpServletResponse response)
	{
		ServletContext application = request.getSession().getServletContext();
		Object obj = application.getAttribute("sites");
		String sites = "*";
		if(obj != null)
		{
			sites = obj.toString();
		}
		
		response.setCharacterEncoding("utf-8");
		response.setContentType("application/json;charset=utf-8");
		response.addHeader("Access-Control-Allow-Origin", sites);
		response.addHeader("Access-Control-Allow-Headers", "Content-Type, Authorization, Accept,X-Requested-With");
		response.addHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS");
	}

	protected String setFailureMessage(HttpServletRequest request, HttpServletResponse response, String msg)
	{
		setCrossSite(request, response);
		return "{\"status\":\"ERROR\",\"data\":\"" + msg + "\"}";
	}
	
	protected String setFailureMessage(HttpServletRequest request, HttpServletResponse response, ErrorInfomation error)
	{
		setCrossSite(request, response);
		return "{\"status\": \"ERROR\", data: {\"code\": " + error.getCode() + ", \"description\": \"" + error.getDescription() + "\"}}";
	}



	protected String setSuccessMessage(HttpServletRequest request, HttpServletResponse response, boolean data)
	{
		setCrossSite(request, response);
		return "{\"status\":\"OK\",\"data\":\"" + data + "\"}";
	}
	
	protected String setSuccessMessage(HttpServletRequest request, HttpServletResponse response, Integer data)
	{
		setCrossSite(request, response);
		return "{\"status\":\"OK\",\"data\":\"" + data + "\"}";
	}
	
	protected String setSuccessMessage(HttpServletRequest request, HttpServletResponse response, Long data)
	{
		setCrossSite(request, response);
		return "{\"status\":\"OK\",\"data\":\"" + data + "\"}";
	}
	
	protected String setSuccessMessage(HttpServletRequest request, HttpServletResponse response, String data)
	{
		setCrossSite(request, response);
		return "{\"status\":\"OK\",\"data\":\"" + data + "\"}";
	}

	protected String setFailureMessage(HttpServletRequest request, HttpServletResponse response, Object data)
	{
		setCrossSite(request, response);

		if (data == null)
		{
			return setFailureMessage(request, response, "entity not fount.");
		}
		try
		{
			return "{\"status\":\"ERROR\",\"data\":" + JsonUtil.objectToJson(data) + "}";
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return setFailureMessage(request, response, e.getMessage());
		}
	}
	protected String setSuccessMessage(HttpServletRequest request, HttpServletResponse response, Object data)
	{
		setCrossSite(request, response);

		if (data == null)
		{
			return setFailureMessage(request, response, "entity not fount.");
		}
		try
		{
			return "{\"status\":\"OK\",\"data\":" + JsonUtil.objectToJson(data) + "}";
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return setFailureMessage(request, response, e.getMessage());
		}
	}

	protected String setSuccessMessage(HttpServletRequest request, HttpServletResponse response, Object data, String[] excludes)
	{
		setCrossSite(request, response);

		if (data == null)
		{
			return setFailureMessage(request, response, "entity not fount.");
		}
		try
		{
			return "{\"status\":\"OK\",\"data\":" + JsonUtil.objectToJson(data, excludes) + "}";
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return setFailureMessage(request, response, e.getMessage());
		}
	}

	protected String setSuccessMessage(HttpServletRequest request, HttpServletResponse response, List<?> data)
	{
		setCrossSite(request, response);

		if (data == null)
		{
			return setFailureMessage(request, response, "entity not fount.");
		}
		try
		{
			return "{\"status\":\"OK\",\"data\":" + JsonUtil.collectionToJsonArray(data) + "}";
		}
		catch (Exception e)
		{
			return setFailureMessage(request, response, e.getMessage());
		}
	}

	protected String setSuccessMessage(HttpServletRequest request, HttpServletResponse response, List<?> data, String[] excludes)
	{
		setCrossSite(request, response);

		if (data == null)
		{
			return setFailureMessage(request, response, "entity not fount.");
		}
		try
		{
			return "{\"status\":\"OK\",\"data\":" + JsonUtil.collectionToJsonArray(data, excludes) + "}";
		}
		catch (Exception e)
		{
			return setFailureMessage(request, response, e.getMessage());
		}
	}
	protected String setSuccessMessage(HttpServletRequest request, HttpServletResponse response, Object[] data)
	{
		setCrossSite(request, response);

		if (data == null)
		{
			return setFailureMessage(request, response, "entity not fount.");
		}
		try
		{
			return "{\"status\":\"OK\",\"data\":" + JsonUtil.collectionToJsonArray(data) + "}";
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return setFailureMessage(request, response, e.getMessage());
		}
	}

	protected String setSuccessMessage(HttpServletRequest request, HttpServletResponse response, Object[] data, String[] excludes)
	{
		setCrossSite(request, response);

		if (data == null)
		{
			return setFailureMessage(request, response, "entity not fount.");
		}
		try
		{
			return "{\"status\":\"OK\",\"data\":" + JsonUtil.collectionToJsonArray(data, excludes) + "}";
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return setFailureMessage(request, response, e.getMessage());
		}
	}
}
