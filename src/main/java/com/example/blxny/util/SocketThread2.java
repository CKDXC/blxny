package com.example.blxny.util;

import com.example.blxny.model.Alarm;
import com.example.blxny.model.Order;
import com.example.blxny.service.ChargeService;
import com.example.blxny.service.OrderService;
import com.example.blxny.service.impl.ChargeServiceImpl;
import com.example.blxny.service.impl.OrderServiceImpl;
import org.springframework.context.ApplicationContext;

import java.io.*;
import java.math.BigDecimal;
import java.net.Socket;
import java.net.SocketAddress;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Socket多线程处理类 用来处理服务端接收到的客户端请求（处理Socket对象）
 */


public class SocketThread2 extends Thread {
    private Socket socket;
    private boolean isLogin = false;
    private boolean isStart = false;
    private ArrayList<String> reqs = new ArrayList<>();
    private ApplicationContext applicationContext;
    private String cmd;

    public SocketThread2(Socket socket, ApplicationContext applicationContext) {
        this.socket = socket;
        this.applicationContext = applicationContext;
    }

    public SocketThread2(Socket socket, ApplicationContext applicationContext, String cmd) {
        this.socket = socket;
        this.applicationContext = applicationContext;
        this.cmd = cmd;
    }

    public String getCmd() {
        return cmd;
    }

    public void setCmd(String cmd) {
        this.cmd = cmd;
        System.out.println(cmd);
    }

    public void run() {
        // 根据输入输出流和客户端连接
        try {
            System.out.println("已接收到客户端");
            String[] momery = {};
            InputStream inputStream = socket.getInputStream();
            // 得到一个输入流，接收客户端传递的信息
            // System.out.println(inputStream.toString());
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);// 提高效率，将自己字节流转为字符流
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);// 加入缓冲区
            byte[] b = new byte[1024];
            int len;
//                DataOutputStream out = new DataOutputStream(socket.getOutputStream());
//                out.write(HexConvert.parseHexStr2Byte(cmd.replace(" ", "")));
            while ((len = inputStream.read(b)) != -1) {
//                    System.out.println("socketThread\t" + cmd);
//                    if (null != cmd) {
////                DataOutputStream out = new DataOutputStream(socket.getOutputStream());
////                out.write(HexConvert.parseHexStr2Byte(cmd.replace(" ", "")));
//                        System.out.println("socketThread2\t" + cmd);
//                        InputStream is08 = new ByteArrayInputStream(cmd.getBytes());
//                        DataOutputStream out08 = new DataOutputStream(socket.getOutputStream());
//                        String s08 = new BufferedReader(new InputStreamReader(is08)).readLine();
//                        System.out.print("服务器端说：" + s08 + "\n");
//                        out08.write(HexConvert.parseHexStr2Byte(s08.replace(" ", "")));
//                        System.out.println(cmd);
//                        MQUtil.putMsg("123", "1");
//                    }

                // String str = new String(b, 0, len);?
//                    System.out.print("len：" + len + "\n");
                SocketAddress address = socket.getLocalSocketAddress();
                int port = socket.getLocalPort();
//                System.out.println("address\t" + address);
                String hxestring = HexConvert.BinaryToHexString(b);
                String hxestring1 = HexConvert.BinaryToHexString(HexConvert.subBytes(b, 0, len));
                System.out.print("客户端说：" + hxestring1 + "\n");
                String hexs = hxestring1.replace(" ", "");
                String subStrhex = hexs.substring(2, hexs.length() - 4);
                byte[] c = CRC16.hexToByte(subStrhex);
                String strCrc = HexConvert.BinaryToHexString(CRC16.GetCRC(c)).replace(" ", "");
                String strCrc16 = hexs.substring(hexs.length() - 4, hexs.length());
                // CRc校验
                if (strCrc.equals(strCrc16)) {
                    System.out.print("c1:" + strCrc16 + "===c2：" + strCrc + "\n");
                    String str = hexs.substring(2, 4);
                    String[] arrStr = {"01", "02", "03", "04", "05", "06", "07", "08", "09", "0B", "0C", "1C", "1D","82","83"};
                    String[] strs = {"02", "03", "08", "09", "0A", "0B", "0C", "1C", "1D"};
//                    isLogin = true;
//                    if (status == false && str.equals("01")) {
//                        System.out.print("请求登录\n");
//                    } else if (status == true && start == true && str.equals("07")) {
//                        System.out.print("结束\n");
//                    } else if (status == true && str.equals("0C")) {
//                        System.out.print("0C心跳\n");
//                    } else if (status == true && str.equals("01")) {
//                        System.out.print("已登录\n");
//                    } else if (status == true && start == true && HexConvert.isHave(strs, str)) {
//                        System.out.print("已登录，已启动\n");
//                    } else if (status == true && start == false && (str.equals("06"))) {
//                        System.out.print("已登录，发送启动请求\n");
//                    } else if (status == true && start == false && (str.equals("1C"))) {
//                        System.out.print("已登录，扫码请求\n");
//                    } else if (status == true && start == true && (str.equals("02") || str.equals("03")
//                            || str.equals("08") || str.equals("09") || str.equals("0A") || str.equals("0B")
//                            || str.equals("0D") || str.equals("1D"))) {
//                        System.out.print("已登录，已启动\n");
//                    } else if (status == false) {
//                        System.out.print("登录失败，请重新登录\n");
//                        return;
//                    } else {
//                        System.out.print("其他情况\n");
//                    }
                    //未登录
                    isLogin=true;
                    if (isLogin == false) {
                        //01请求--C-S--启动登录--需要调用--登录校验--还没写
                        if (str.equals("01")) {
                            String str1 = hexs.substring(4, 8);
                            String reqContent = hexs.substring(12,hexs.length()-4);
                            ChargeService chargeService = applicationContext.getBean(ChargeServiceImpl.class);
                            String resContent = chargeService.login(reqContent);
                            String hexStr02 = "81 " + substrs(str1) + "00 09 "+substrs(resContent);
                            byte[] serverStr02 = CRC16.hexToByte(hexStr02.replace(" ", ""));
                            String serverstrCrc02 = HexConvert.BinaryToHexString(CRC16.GetCRC(serverStr02));
                            String hexserverStr02 = "68 " + HexConvert.BinaryToHexString(serverStr02) + serverstrCrc02;
                            InputStream is = new ByteArrayInputStream(hexserverStr02.getBytes());
                            DataOutputStream out = new DataOutputStream(socket.getOutputStream());
                            String s = new BufferedReader(new InputStreamReader(is)).readLine();
                            System.out.print("服务器端说：" + s + "\n");
                            out.write(HexConvert.parseHexStr2Byte(s.replace(" ", "")));
                            System.out.print("登录成功,是否启动\n");// 磊加
                            System.out.print("===" + str);
                            System.out.print("===" + HexConvert.isHave(arrStr, "0C"));
                            isLogin = true;
                        }else{
                            System.out.println("未登录，不处理01以外的请求");
                        }
                    }
                    //已经登录
                    else {
                        System.out.println("已经登陆");
                        //心跳以外的情况
                        if (!str.equals("0C")) {
                            //已经登陆，未启动
                            if (isStart == false) {
//                                //1C请求--C-S--二维码设置请求--设置消息--写在service
//                                if (str.equals("1C") && HexConvert.isHave(arrStr, "1C")) {
//                                    String reqContent = hexs.substring(12, hexs.length() - 4);
//                                    System.out.println("gunNo---");
//                                    address = socket.getLocalSocketAddress();
//                                    port = socket.getLocalPort();
//                                    System.out.println("address\t" + address + "\tport\t" + port);
//                                    MQUtil.putMsg(address.toString(), reqContent);
////                                    String strr = hexs.replace(" ", "");
////                                    String str1 = hexs.substring(4, 8);
////                                    String Str1c = strr.substring(2, 14);
////                                    String str1c04 = strr.substring(strr.length() - 4, strr.length());
////                                    byte[] serverStr1c = CRC16.hexToByte(Str1c);
////                                    String serverstrCrc1c = HexConvert.BinaryToHexString(CRC16.GetCRC(serverStr1c)).replace(" ", "");
////                                    if (str1c04.equals(serverstrCrc1c)) {
////                                        String urls = "http://120.78.172.33:9010";
////                                        byte[] byte1c = urls.getBytes();
////                                        String hexStr1c = HexConvert.BinaryToHexString(byte1c);
////
////                                        String[] strArr = hexStr1c.split(" ");
////                                        String strAsc = "\\u00" + strArr[0] + " ";
////                                        for (int i = 1; i <= strArr.length - 1; i++) {
////                                            strAsc = strAsc + "\\u00" + strArr[i] + " ";
////                                        }
//////                    				    System.out.print("strAsc==="+strAsc+'\n');
//////                    				    System.out.print("hexStr1c==="+hexStr1c+'\n');
////                                        String hexStrc1 = "9C " + substrs(str1) + "00 1C 01 00 19 " + hexStr1c;
//////                    					System.out.print("hexStrc1==="+hexStrc1+'\n');
////
////                                        byte[] serverStrc1 = CRC16.hexToByte(hexStrc1.replace(" ", ""));
////                                        String serverstrCrcc1 = HexConvert.BinaryToHexString(CRC16.GetCRC(serverStrc1));
////                                        String hexserverStrc1 = "68 " + HexConvert.BinaryToHexString(serverStrc1) + serverstrCrcc1;
////                                        InputStream isc1 = new ByteArrayInputStream(hexserverStrc1.getBytes());
////                                        DataOutputStream outc1 = new DataOutputStream(socket.getOutputStream());
////                                        String sc1 = new BufferedReader(new InputStreamReader(isc1)).readLine();
////                                        String hexStrc11 = "68 9C " + substrs(str1) + "00 1C 01 00 19 " + strAsc + " " + serverstrCrcc1;
////                                        System.out.print("服务器端说：" + hexStrc11 + "\n");
////                                        outc1.write(HexConvert.parseHexStr2Byte(sc1.replace(" ", "")));
//
//                                    //1C扫码成功 直接06启动
////                                        Date now = new Date();
////                                        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");// 可以方便地修改日期格式
////                                        String createtime = dateFormat.format(now);
////                                        String StrQd = "06 05 01 00 16 01 01 00 00 00 00 00 00 11 " + substrs(createtime) + "00 00 05 01 04 00 10 01 00 01 ";
////                                        byte[] qd = CRC16.hexToByte(StrQd.replace(" ", ""));
////                                        String strQdCrc = HexConvert.BinaryToHexString(CRC16.GetCRC(qd));
////                                        String hexQdStr = "68 " + StrQd + strQdCrc;
////                                        InputStream Qdis = new ByteArrayInputStream(hexQdStr.getBytes());
////                                        DataOutputStream Qdout = new DataOutputStream(socket.getOutputStream());
////                                        String Qds = new BufferedReader(new InputStreamReader(Qdis)).readLine();
////                                        System.out.print("服务器端说启动:" + Qds + "\n");
////                                        Qdout.write(HexConvert.parseHexStr2Byte(Qds.replace(" ", "")));
////                                        // 启动回复
////                                        String qdhf = hxestring1.replace(" ", "");
////                                        System.out.print("客户端启动回复:" + hxestring1 + "\n");
////                                        String hfstr = "68 86 00 03 00 0D 11 19 03 23 00 00 05 01 04 00 10 01 00 01 00 4C 47".replace(" ", "");
////                                        System.out.println("\n" + "qdhf: " + qdhf);
////                                        System.out.println("hfstr:" + hfstr);
////                                        System.out.println("===" + Qdout);
////
////                                        System.out.print("客户端启动对比:68 86 00 03 00 0D 11 19 03 23 00 00 05 01 04 00 10 01 00 01 00 4C 47\n");
////                                        //qdhf请求指令未处理，无法比较
////                                        if (true || qdhf.equals(hfstr)) {
////                                            isStart = true;
////                                            System.out.print("启动成功" + "\n");
////                                        }
//                                }
//                                //06请求--S-C--远程启动充电--设置消息--写在service--XXXX
//                                else if (str.equals("06") && HexConvert.isHave(arrStr, "06")) {
//                                    Date now = new Date();
//                                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");// 可以方便地修改日期格式
//                                    String createtime = dateFormat.format(now);
//                                    String StrQd = "06 05 01 00 16 01 01 00 00 00 00 00 00 11 " + substrs(createtime) + "00 00 05 01 04 00 10 01 00 01 ";
//                                    byte[] qd = CRC16.hexToByte(StrQd.replace(" ", ""));
//                                    String strQdCrc = HexConvert.BinaryToHexString(CRC16.GetCRC(qd));
//                                    String hexQdStr = "68 " + StrQd + strQdCrc;
//                                    InputStream Qdis = new ByteArrayInputStream(hexQdStr.getBytes());
//                                    DataOutputStream Qdout = new DataOutputStream(socket.getOutputStream());
//                                    String Qds = new BufferedReader(new InputStreamReader(Qdis)).readLine();
//                                    System.out.print("服务器端说启动:" + Qds + "\n");
//                                    Qdout.write(HexConvert.parseHexStr2Byte(Qds.replace(" ", "")));
//
//                                    // 启动回复
//                                    String qdhf = hxestring1.replace(" ", "");
//                                    System.out.print("客户端启动回复:" + hxestring1 + "\n");
//                                    String hfstr = "68 86 00 03 00 0D 11 19 03 23 00 00 05 01 04 00 10 01 00 01 00 4C 47".replace(" ", "");
//                                    System.out.println("\n" + "qdhf: " + qdhf);
//                                    System.out.println("hfstr:" + hfstr);
//                                    System.out.println("===" + Qdout);
//
//                                    System.out.print("客户端启动对比:68 86 00 03 00 0D 11 19 03 23 00 00 05 01 04 00 10 01 00 01 00 4C 47\n");
//                                    //qdhf请求指令未处理，无法比较
//                                    if (true || qdhf.equals(hfstr)) {
//                                        isStart = true;
//                                        System.out.print("启动成功" + "\n");
//                                    }
//                                }
                                System.out.println("已经登陆，未启动"+str);
                                //1C请求--设置二维码--
                                if (str.equals("1C") && HexConvert.isHave(arrStr, "1C")) {
                                    String str1 = hexs.substring(4, 8);
                                    String reqContent = hexs.substring(12, hexs.length() - 4);
                                    ChargeService chargeService = applicationContext.getBean(ChargeServiceImpl.class);
                                    String resContent = chargeService.setQRcode(reqContent);
                                    InputStream is08 = new ByteArrayInputStream(resContent.getBytes());
                                    DataOutputStream out08 = new DataOutputStream(socket.getOutputStream());
                                    String s08 = new BufferedReader(new InputStreamReader(is08)).readLine();
                                    System.out.print("服务器端说：" + s08 + "\n");
                                    out08.write(HexConvert.parseHexStr2Byte(s08.replace(" ", "")));
                                }
                                //82请求--预约充电回复--没有回复--待测
                                else if (str.equals("82") && HexConvert.isHave(arrStr, "82")) {
                                    String resContent = hexs.substring(12, hexs.length() - 4);
                           /*       String appoUid = reqContent.substring(0, 16);
                                    String result = reqContent.substring(16, 18);//
                                    System.out.println("appoUid---" + appoUid + "\nresult---" + result);
                                    AppointmentService appointmentService = (AppointmentServiceImpl) applicationContext.getBean(AppointmentServiceImpl.class);
                                    appointmentService.dealAppoCharge(appoUid, result);*/
                                    address = socket.getLocalSocketAddress();//192.。
                                    String msgCode = hexs.substring(4, 8);
                                    MQUtil.putMsg(address.toString() + msgCode, resContent);//设置桩对应key的消息
                                    System.out.println(resContent);

                                    // out.close();
                                    // is.close();
                                }
                                //83请求--预约充电取消回复--没有回复--返回值给主线程
                                else if (str.equals("83") && HexConvert.isHave(arrStr, "83")) {
                                    String resContent = hexs.substring(12, hexs.length() - 4);
                                    address = socket.getLocalSocketAddress();//192.。
                                    String msgCode = hexs.substring(4, 8);
                                    MQUtil.putMsg(address.toString() + msgCode, resContent);

                                }
                                //86请求--远程启动充电回复--没有回复--这个要有 返回值
                                else if (str.equals("86") && HexConvert.isHave(arrStr, "86")) {
                                    System.out.println("进入86------------");
                                    String resContent = hexs.substring(12, hexs.length() - 4);
                                    address = socket.getLocalSocketAddress();
                                    String msgCode = hexs.substring(4, 8);
                                    MQUtil.putMsg(address.toString() + msgCode, resContent);
                                    isStart = true;
                                }
                                //8E请求--校时回复--不用回复
                                else if (str.equals("8E") && HexConvert.isHave(arrStr, "0E")) {
                                    System.out.println("进入8E-------");
                                    String resContent = hexs.substring(12, hexs.length() - 4);
                                    address = socket.getLocalSocketAddress();
                                    String msgCode = hexs.substring(4, 8);
                                    MQUtil.putMsg(address.toString() + msgCode, resContent);
                                }
                                //9D请求--重启回复处理--
                                else if (str.equals("9D") && HexConvert.isHave(arrStr, "9D")) {
                                    System.out.println("进入9D------------");
                                    String resContent = hexs.substring(12, hexs.length() - 4);
                                    address = socket.getLocalSocketAddress();
                                    String msgCode = hexs.substring(4, 8);
                                    MQUtil.putMsg(address.toString() + msgCode, resContent);
                                    isStart = true;
                                }
                                //9E请求--远程升级回复处理--
                                else if (str.equals("9E") && HexConvert.isHave(arrStr, "9E")) {
                                    System.out.println("进入9D------------");
                                    String resContent = hexs.substring(12, hexs.length() - 4);
                                    address = socket.getLocalSocketAddress();
                                    String msgCode = hexs.substring(4, 8);
                                    MQUtil.putMsg(address.toString() + msgCode, resContent);
                                }
                                //0D请求--告警--这里有一个警告--推送到用户手机吧
                                else if (str.equals("0D") && HexConvert.isHave(arrStr, "0D")) {
                                    System.out.println("进入0D--------");
                                    String reqContent = hexs.substring(12, hexs.length() - 4);
                                    System.out.println(reqContent);
                                    String gunNo = reqContent.substring(0, 2);
                                    String code = reqContent.substring(2, 10);
                                    String alarmTime = reqContent.substring(10, 22);
                                    String alarmStatus = reqContent.substring(22, 24);
                                    String alarmValue = reqContent.substring(24, 32);
                                    Alarm alarm = new Alarm();
                                    alarm.setGunNo(Integer.parseInt(gunNo));
                                    alarm.setCode(code);
                                    alarm.setAlarmTime(alarmTime);
                                    alarm.setStatus(Integer.parseInt(alarmStatus));
                                    alarm.setAlarmValue(Double.parseDouble(alarmValue));
//                                    System.out.println("gunNo---" + gunNo + "\nalarmCode---" + alarmCode +
//                                            "\nalarmTime---" + "\nalarmStatus" + alarmStatus + alarmTime + "\nalarmValue" + alarmValue);
                                    ChargeService chargeService = applicationContext.getBean(ChargeServiceImpl.class);
//                                    chargeService.alarmDevice(alarm);
                                    String str1 = hexs.substring(4, 8);
                                    String hexStr08 = "0D " + substrs(str1) + "00 00";
                                    byte[] serverStr8 = CRC16.hexToByte(hexStr08.replace(" ", ""));
                                    String serverstrCrc08 = HexConvert.BinaryToHexString(CRC16.GetCRC(serverStr8));
                                    String hexserverStr08 = "68 " + HexConvert.BinaryToHexString(serverStr8) + serverstrCrc08;
                                    InputStream is08 = new ByteArrayInputStream(hexserverStr08.getBytes());
                                    DataOutputStream out08 = new DataOutputStream(socket.getOutputStream());
                                    String s08 = new BufferedReader(new InputStreamReader(is08)).readLine();
                                    System.out.print("服务器端说：" + s08 + "\n");
                                    out08.write(HexConvert.parseHexStr2Byte(s08.replace(" ", "")));
                                }
//                                //01请求--C-S--登录请求--回复XXXX
//                                else if (str.equals("01") && HexConvert.isHave(arrStr, "01")) {
//                                    String str1 = hexs.substring(4, 8);
//                                    String reqContent = hexs.substring(12, hexs.length() - 4);
//                                    String deviceId = reqContent.substring(0, 16);
//                                    String resContent = deviceId + "00";
//                                    byte[] serverStr8 = CRC16.hexToByte(resContent);
//                                    String serverstrCrc08 = HexConvert.BinaryToHexString(CRC16.GetCRC(serverStr8));
//                                    String hexserverStr08 = "68 81 " + str1 + "00 09" + HexConvert.BinaryToHexString(serverStr8) + serverstrCrc08;
//                                    InputStream is08 = new ByteArrayInputStream(hexserverStr08.getBytes());
//                                    DataOutputStream out = new DataOutputStream(socket.getOutputStream());
//                                    String s = new BufferedReader(new InputStreamReader(is08)).readLine();
//                                    System.out.print("服务器端说：" + s + "\n");
//                                    out.write(HexConvert.parseHexStr2Byte(s.replace(" ", "")));
//                                    System.out.print("登录成功,是否启动\n");// 磊加
//                                    System.out.print("===" + str);
//                                    System.out.print("===" + HexConvert.isHave(arrStr, "0C"));
//                                }
                            }
                            //已经启动
                            else {
//                                //02请求
//                                if (str.equals("02") && HexConvert.isHave(arrStr, "02")) {
//                                    String str2 = hexs.substring(26, 34);
//                                    String str1 = hexs.substring(4, 8);
//                                    String hexStr02 = "82 " + substrs(str1) + "00 0D " + substrs(str2) + "00 00 05 01 04 00 10 01 00 01 00";
//                                    byte[] serverStr02 = CRC16.hexToByte(hexStr02.replace(" ", ""));
//                                    String serverstrCrc02 = HexConvert.BinaryToHexString(CRC16.GetCRC(serverStr02));
//                                    String hexserverStr02 = "68 " + HexConvert.BinaryToHexString(serverStr02) + serverstrCrc02;
//                                    InputStream is02 = new ByteArrayInputStream(hexserverStr02.getBytes());
//                                    DataOutputStream out02 = new DataOutputStream(socket.getOutputStream());
//                                    String s02 = new BufferedReader(new InputStreamReader(is02)).readLine();
//                                    System.out.print("服务器端说：" + s02 + "\n");
//                                    out02.write(HexConvert.parseHexStr2Byte(s02.replace(" ", "")));
//                                }
//                                //03请求
//                                else if (str.equals("03") && HexConvert.isHave(arrStr, "03")) {
//                                    String str1 = hexs.substring(4, 8);
//                                    String str3 = hexs.substring(14, 22);
//                                    String hexStr03 = "83 " + substrs(str1) + "00 0D " +
//                                            substrs(str3) + "00 00 05 01 04 00 10 01 00 01 00";
//                                    byte[] serverStr03 = CRC16.hexToByte(hexStr03.replace(" ", ""));
//                                    String serverstrCrc03 =
//                                            HexConvert.BinaryToHexString(CRC16.GetCRC(serverStr03));
//                                    String hexserverStr03 = "68 " +
//                                            HexConvert.BinaryToHexString(serverStr03) + serverstrCrc03;
//                                    InputStream is03 = new
//                                            ByteArrayInputStream(hexserverStr03.getBytes());
//                                    DataOutputStream out03 = new
//                                            DataOutputStream(socket.getOutputStream());
//                                    String s03 = new BufferedReader(new
//                                            InputStreamReader(is03)).readLine();
//                                    System.out.print("服务器端说：" + s03 + "\n");
//                                    out03.write(HexConvert.parseHexStr2Byte(s03.replace(" ", "")));
//                                }
                                //07请求
//                                else if (str.equals("07") && HexConvert.isHave(arrStr, "07")) {
//                                    String str1 = hexs.substring(4, 8);
//                                    String str5 = hexs.substring(14, 22);
//                                    String reqContent = hexs.substring(12, hexs.length() - 4);
//                                    System.out.println(reqContent);
//                                    String hexStr07 = "87 " + substrs(str1) + "00 0D " + substrs(str5) + "00 00 05 01 04 00 10 01 00 01 00";
//                                    byte[] serverStr7 = CRC16.hexToByte(hexStr07.replace(" ", ""));
//                                    String serverstrCrc07 = HexConvert.BinaryToHexString(CRC16.GetCRC(serverStr7));
//                                    String hexserverStr07 = "68 " + HexConvert.BinaryToHexString(serverStr7) + serverstrCrc07;
//                                    InputStream is07 = new ByteArrayInputStream(hexserverStr07.getBytes());
//                                    DataOutputStream out07 = new DataOutputStream(socket.getOutputStream());
//                                    String s07 = new BufferedReader(new InputStreamReader(is07)).readLine();
//                                    System.out.print("服务器端说：" + s07 + "\n");
//                                    out07.write(HexConvert.parseHexStr2Byte(s07.replace(" ", "")));
//                                    isStart = false;
//                                    // out.close();
//                                    // is.close();
//                                }
                                //87请求--远程结束充电回复--不用回复--调用service 数据库交互
                                if (str.equals("87") && HexConvert.isHave(arrStr, "87")) {
                                    System.out.println("进入87-------------");
                                    String resContent = hexs.substring(12, hexs.length() - 4);
                                    address = socket.getLocalSocketAddress();
                                    String msgCode = hexs.substring(4, 8);
                                    MQUtil.putMsg(address.toString() + msgCode, resContent);
                                    isStart = false;
                                }
                                //08请求--回复88--上传充电记录--调用service 数据库交互
                                else if (str.equals("08") && HexConvert.isHave(arrStr, "08")) {
                                    System.out.println("进入08请求");
                                    String str1 = hexs.substring(4, 8);
                                    String str6 = hexs.substring(14, 22);
                                    System.out.println("hexs" + hexs);
                                    String resContent = hexs.substring(12, hexs.length() - 4);
                                    ChargeService chargeService = applicationContext.getBean(ChargeServiceImpl.class);
                                    int result = chargeService.upChargeLog(resContent);
                                    String hexStr08 = "88 " + substrs(str1) + "00 09 " + substrs(resContent.substring(2, 18)) + " 0" + (result == 0 ? 1 : 0);
                                    byte[] serverStr8 = CRC16.hexToByte(hexStr08.replace(" ", ""));
                                    String serverstrCrc08 = HexConvert.BinaryToHexString(CRC16.GetCRC(serverStr8));
                                    String hexserverStr08 = "68 " + HexConvert.BinaryToHexString(serverStr8) + serverstrCrc08;
                                    InputStream is08 = new ByteArrayInputStream(hexserverStr08.getBytes());
                                    DataOutputStream out08 = new DataOutputStream(socket.getOutputStream());
                                    String s08 = new BufferedReader(new InputStreamReader(is08)).readLine();
                                    System.out.print("服务器端说：" + s08 + "\n");
                                    out08.write(HexConvert.parseHexStr2Byte(s08.replace(" ", "")));
                                    // out.close();
                                    // is.close();
                                }
                                //09请求--上传充电进度--没有回复
                                else if (str.equals("09") && HexConvert.isHave(arrStr, "09")) {
                                    System.out.println("进入09请求");
                                    String str1 = hexs.substring(4, 8);
                                    String str6 = hexs.substring(14, 22);
                                    System.out.println("hexs" + hexs);
                                    String reqContent = hexs.substring(12, hexs.length() - 4);
                                    System.out.println(reqContent);
                                    Order order = new Order();
                                    order.setGunNo(Integer.parseInt(reqContent.substring(0, 2)));
                                    order.setOrderId(reqContent.substring(2, 18));
                                    order.setChargeType(Integer.parseInt(reqContent.substring(18, 20)));
                                    order.setCardId(reqContent.substring(20, 36));//BCD 转 10进制4位
                                    order.setVin(reqContent.substring(36, 70));
                                    order.setSoc(Integer.parseInt(reqContent.substring(70, 72), 16));
                                    order.setEndReason(Integer.parseInt(reqContent.substring(72, 84)));
                                    order.setStartTime(new Date());//reqContent.substring(74,86)
                                    order.setEndTime(new Date());//reqContent.substring(86,98)
                                    System.out.println("时间" + reqContent.substring(84, 96));
//                                    order.setStartMeter(stringToFloat(reqContent.substring(98, 106)));
//                                    order.setEndMeter(stringToFloat(reqContent.substring(106, 114)));
                                    order.setChargeAmount(stringToFloat(reqContent.substring(96, 104)));
                                    order.setCuspElectricity(stringToFloat(reqContent.substring(104, 112)));
                                    order.setHighElectricity(stringToFloat(reqContent.substring(112, 120)));
                                    order.setNormalElectricity(stringToFloat(reqContent.substring(120, 128)));
                                    order.setValleyElectricity(stringToFloat(reqContent.substring(128, 136)));
                                    order.setTotalAmount(BigDecimal.valueOf(Double.parseDouble(reqContent.substring(136, 144))));
                                    order.setCuspAmount(BigDecimal.valueOf(Double.parseDouble(reqContent.substring(144, 152))));
                                    order.setHighAmount(BigDecimal.valueOf(Double.parseDouble(reqContent.substring(152, 160))));
                                    order.setNormalAmount(BigDecimal.valueOf(Double.parseDouble(reqContent.substring(160, 168))));
                                    order.setValleyAmount(BigDecimal.valueOf(Double.parseDouble(reqContent.substring(168, 176))));
                                    order.setBookingAmount(BigDecimal.valueOf(Double.parseDouble(reqContent.substring(176, 184))));
                                    order.setServiceAmount(BigDecimal.valueOf(Double.parseDouble(reqContent.substring(184, 192))));
                                    order.setPickAmount(BigDecimal.valueOf(Double.parseDouble(reqContent.substring(192, 200))));

                                    OrderService orderService = applicationContext.getBean(OrderServiceImpl.class);
                                    boolean flg = orderService.addOrder(order);
                                    System.out.println(flg);
                                    // out.close();
                                    // is.close();
                                }
                                //8B请求--计费规则设置回复--不用回复--调用service 数据库交互
                                else if (str.equals("8B") && HexConvert.isHave(arrStr, "8B")) {
                                    System.out.println("进入8B------------");
                                    String resContent = hexs.substring(12, hexs.length() - 4);
                                    address = socket.getLocalSocketAddress();
                                    String msgCode = hexs.substring(4, 8);
                                    MQUtil.putMsg(address.toString() + msgCode, resContent);
                                }
                                //0D请求--告警--这里有一个警告--推送到用户手机吧
                                else if (str.equals("0D") && HexConvert.isHave(arrStr, "0D")) {
                                    System.out.println("进入0D--------");
                                    String reqContent = hexs.substring(12, hexs.length() - 4);
                                    System.out.println(reqContent);
                                    Alarm alarm = new Alarm();
                                    alarm.setGunNo(Integer.parseInt(reqContent.substring(0, 2)));
                                    alarm.setCode(reqContent.substring(2, 10));
                                    alarm.setAlarmTime(reqContent.substring(10, 22));
                                    alarm.setStatus(Integer.parseInt(reqContent.substring(22, 24)));
                                    alarm.setAlarmValue(Double.parseDouble(reqContent.substring(24, 32)));
//                                    System.out.println("gunNo---" + gunNo + "\nalarmCode---" + alarmCode +
//                                            "\nalarmTime---" + "\nalarmStatus" + alarmStatus + alarmTime + "\nalarmValue" + alarmValue);
                                    ChargeService chargeService = applicationContext.getBean(ChargeServiceImpl.class);
//                                    chargeService.alarmDevice(alarm);
                                    String str1 = hexs.substring(4, 8);
                                    String hexStr08 = "0D " + substrs(str1) + "00 00";
                                    byte[] serverStr8 = CRC16.hexToByte(hexStr08.replace(" ", ""));
                                    String serverstrCrc08 = HexConvert.BinaryToHexString(CRC16.GetCRC(serverStr8));
                                    String hexserverStr08 = "68 " + HexConvert.BinaryToHexString(serverStr8) + serverstrCrc08;
                                    InputStream is08 = new ByteArrayInputStream(hexserverStr08.getBytes());
                                    DataOutputStream out08 = new DataOutputStream(socket.getOutputStream());
                                    String s08 = new BufferedReader(new InputStreamReader(is08)).readLine();
                                    System.out.print("服务器端说：" + s08 + "\n");
                                    out08.write(HexConvert.parseHexStr2Byte(s08.replace(" ", "")));
                                }
//                                //0B请求
//                                else if (str.equals("0B") && HexConvert.isHave(arrStr, "0B")) {
//                                    String str1 = hexs.substring(4, 8);
//                                    String str8B = hexs.substring(12, 28);
//                                    System.out.print("8B：" + str8B + "\n");
//                                    String hexStr8B = "8B " + substrs(str1) + "00 09 " + substrs(str8B) + "00";
//                                    byte[] serverStr8B = CRC16.hexToByte(hexStr8B.replace(" ", ""));
//                                    String serverstrCrc8B = HexConvert.BinaryToHexString(CRC16.GetCRC(serverStr8B));
//                                    String hexserverStr8B = "68 " + HexConvert.BinaryToHexString(serverStr8B) + serverstrCrc8B;
//                                    InputStream is8B = new ByteArrayInputStream(hexserverStr8B.getBytes());
//                                    DataOutputStream out8B = new DataOutputStream(socket.getOutputStream());
//                                    String s8B = new BufferedReader(new InputStreamReader(is8B)).readLine();
//                                    System.out.print("服务器端说：" + s8B + "\n");
//                                    out8B.write(HexConvert.parseHexStr2Byte(s8B.replace(" ", "")));
//                                    // out.close();
//                                    // is.close();
//                                }
//                                //1D请求
//                                else if (str.equals("1D") && HexConvert.isHave(arrStr, "1D")) {
//                                    String str1 = hexs.substring(4, 8);
//                                    String hexStr9D = "9D " + substrs(str1) + "00 01 00";
//                                    byte[] serverStr9D = CRC16.hexToByte(hexStr9D.replace(" ", ""));
//                                    String serverstrCrc9D = HexConvert.BinaryToHexString(CRC16.GetCRC(serverStr9D));
//                                    String hexserverStr9D = "68 " + HexConvert.BinaryToHexString(serverStr9D) + serverstrCrc9D;
//                                    InputStream is9D = new ByteArrayInputStream(hexserverStr9D.getBytes());
//                                    DataOutputStream out9D = new DataOutputStream(socket.getOutputStream());
//                                    String s9D = new BufferedReader(new InputStreamReader(is9D)).readLine();
//                                    System.out.print("服务器端说：" + s9D + "\n");
//                                    out9D.write(HexConvert.parseHexStr2Byte(s9D.replace(" ", "")));
//                                }
                            }
                        }
                        //心跳
                        else {
                            if (str.equals("0C")) {
                                String str1 = hexs.substring(4, 8);
                                Date now = new Date();
                                SimpleDateFormat dateFormat = new SimpleDateFormat("yyMMddHHmmss");// 可以方便地修改日期格式
                                String createtime = dateFormat.format(now);
                                String StrXT = "8C " + substrs(str1) + "00 06 " + substrs(createtime);
                                byte[] xt = CRC16.hexToByte(StrXT.replace(" ", ""));
                                String strXTCrc = HexConvert.BinaryToHexString(CRC16.GetCRC(xt));
                                String hexXTStr = "68 " + StrXT + strXTCrc;
                                InputStream isxt = new ByteArrayInputStream(hexXTStr.getBytes());
                                DataOutputStream outxt = new DataOutputStream(socket.getOutputStream());
                                String sxt = new BufferedReader(new InputStreamReader(isxt)).readLine();
                                System.out.print("服务器端说：" + sxt + "\n");
                                outxt.write(HexConvert.parseHexStr2Byte(sxt.replace(" ", "")));
                            } else {
                                System.out.println("心跳异常");
                            }
                        }
                    }
                }
                // if(str.equals("0C")){
                // String str1 = hexs.substring(4, 8);
                // Date now = new Date();
                // SimpleDateFormat dateFormat = new
                // SimpleDateFormat("yyyyMMddHHmm");//可以方便地修改日期格式
                // String createtime = dateFormat.format( now );
                // String StrXT = "8C "+ substrs(str1) +"00 06 "+
                // substrs(createtime);
                // byte[] xt =CRC16.hexToByte(StrXT.replace( " ","" ));
                // String strXTCrc =
                // HexConvert.BinaryToHexString(CRC16.GetCRC(xt));
                // String hexXTStr = "68 "+ StrXT + strXTCrc;
                // InputStream isxt = new
                // ByteArrayInputStream(hexXTStr.getBytes());
                // DataOutputStream outxt = new
                // DataOutputStream(socket.getOutputStream());
                // String sxt = new BufferedReader(new
                // InputStreamReader(isxt)).readLine();
                // System.out.print("服务器端说："+sxt+"\n");
                // outxt.write(HexConvert.parseHexStr2Byte(sxt.replace( " ",""
                // )));
                // }else{
                // System.out.println("心跳异常");
                // }
                //
                // if(str.equals("01") && HexConvert.isHave(arrStr,"01")){
                // String str1 = hexs.substring(4, 8);
                // String hexStr = "68 81 "+ substrs(str1)
                // +"00 0A "+"00 00 05 01 04 00 10 01 00 01 00 CB 24";
                // InputStream is = new ByteArrayInputStream(hexStr.getBytes());
                // DataOutputStream out = new
                // DataOutputStream(socket.getOutputStream());
                // String s = new BufferedReader(new
                // InputStreamReader(is)).readLine();
                // System.out.print("服务器端说："+s+"\n");
                // out.write(HexConvert.parseHexStr2Byte(s.replace( " ","" )));
                // System.out.print("登录成功,是否启动\n");//磊加
                // System.out.print("==="+str);
                // System.out.print("==="+HexConvert.isHave(arrStr,"0C"));
                //
                // }else{
                // System.out.print("登录失败，请重新登录\n");
                // }
                // }
                //
                // //启动
                // if(HexConvert.isHave(arrStr,"06")){
                // Date now = new Date();
                // SimpleDateFormat dateFormat = new
                // SimpleDateFormat("yyyyMMdd");//可以方便地修改日期格式
                // String createtime = dateFormat.format( now );
                // String StrQd = "06 05 01 00 16 01 01 00 00 00 00 00 00 11 "+
                // substrs(createtime) +"00 00 05 01 04 00 10 01 00 01 ";
                // byte[] qd =CRC16.hexToByte(StrQd.replace( " ","" ));
                // String strQdCrc =
                // HexConvert.BinaryToHexString(CRC16.GetCRC(qd));
                // String hexQdStr = "68 "+ StrQd + strQdCrc;
                // InputStream Qdis = new
                // ByteArrayInputStream(hexQdStr.getBytes());
                // DataOutputStream Qdout = new
                // DataOutputStream(socket.getOutputStream());
                // String Qds = new BufferedReader(new
                // InputStreamReader(Qdis)).readLine();
                // System.out.print("服务器端说启动:"+Qds+"\n");
                // Qdout.write(HexConvert.parseHexStr2Byte(Qds.replace( " ",""
                // )));
                // // 启动回复
                // String qdhf = hxestring1.replace( " ","" );
                // System.out.print("客户端启动回复:"+hxestring1+"\n");
                // String hfstr =
                // "68 86 00 03 00 0D 11 19 03 23 00 00 05 01 04 00 10 01 00 01 00 4C 47".replace(
                // " ","" );
                // System.out.print("客户端启动对比:"+"68 86 00 03 00 0D 11 19 03 23 00 00 05 01 04 00 10 01 00 01 00 4C 47"+"\n");
                // if(qdhf.equals(hfstr)){
                // System.out.print("启动成功"+"\n");
//                         if(str.equals("02") && HexConvert.isHave(arrStr,"02")){
//                         String str2 = hexs.substring(26, 34);
//                         String hexStr02 = "82 "+ substrs(str1)
//                         +"00 0D "+substrs(str2)+"00 00 05 01 04 00 10 01 00 01 00";
//                         byte[] serverStr02 =CRC16.hexToByte(hexStr02.replace( " ",""
//                         ));
//                         String serverstrCrc02 = HexConvert.BinaryToHexString(CRC16.GetCRC(serverStr02));
//                         String hexserverStr02 = "68 "+HexConvert.BinaryToHexString(serverStr02) + serverstrCrc02;
//                         InputStream is02 = new ByteArrayInputStream(hexserverStr02.getBytes());
//                         DataOutputStream out02 = new
//                         DataOutputStream(socket.getOutputStream());
//                         String s02 = new BufferedReader(new InputStreamReader(is02)).readLine();
//                         System.out.print("服务器端说："+s02+"\n");
//                         out02.write(HexConvert.parseHexStr2Byte(s02.replace( " ",""
//                         )));
                // }else if(str.equals("03") && HexConvert.isHave(arrStr,"03")){
                // String str3 = hexs.substring(14, 22);
                // String hexStr03 = "83 "+ substrs(str1) +"00 0D "+
                // substrs(str3)+"00 00 05 01 04 00 10 01 00 01 00";
                // byte[] serverStr03 =CRC16.hexToByte(hexStr03.replace( " ",""
                // ));
                // String serverstrCrc03 =
                // HexConvert.BinaryToHexString(CRC16.GetCRC(serverStr03));
                // String hexserverStr03 = "68 "+
                // HexConvert.BinaryToHexString(serverStr03) + serverstrCrc03;
                // InputStream is03 = new
                // ByteArrayInputStream(hexserverStr03.getBytes());
                // DataOutputStream out03 = new
                // DataOutputStream(socket.getOutputStream());
                // String s03 = new BufferedReader(new
                // InputStreamReader(is03)).readLine();
                // System.out.print("服务器端说："+s03+"\n");
                // out03.write(HexConvert.parseHexStr2Byte(s03.replace( " ",""
                // )));
                // }else if(str.equals("07") && HexConvert.isHave(arrStr,"07")){
                // String str5 = hexs.substring(14, 22);
                // String hexStr07 = "87 "+ substrs(str1) +"00 0D "+
                // substrs(str5)+"00 00 05 01 04 00 10 01 00 01 00";
                // byte[] serverStr7 =CRC16.hexToByte(hexStr07.replace( " ",""
                // ));
                // String serverstrCrc07 =
                // HexConvert.BinaryToHexString(CRC16.GetCRC(serverStr7));
                // String hexserverStr07 = "68 "+
                // HexConvert.BinaryToHexString(serverStr7) + serverstrCrc07;
                // InputStream is07 = new
                // ByteArrayInputStream(hexserverStr07.getBytes());
                // DataOutputStream out07 = new
                // DataOutputStream(socket.getOutputStream());
                // String s07 = new BufferedReader(new
                // InputStreamReader(is07)).readLine();
                // System.out.print("服务器端说："+s07+"\n");
                // out07.write(HexConvert.parseHexStr2Byte(s07.replace( " ",""
                // )));
                // // out.close();
                // // is.close();
                // }else if(str.equals("08") && HexConvert.isHave(arrStr,"08")){
                // // String str1 = hexs.substring(4, 8);
                // String str6 = hexs.substring(14, 22);
                // String hexStr08 = "88 "+ substrs(str1)+"00 0D "
                // +substrs(str6)+"00 00 05 01 04 00 10 01 00 01 00";
                // byte[] serverStr8 =CRC16.hexToByte(hexStr08.replace( " ",""
                // ));
                // String serverstrCrc08 =
                // HexConvert.BinaryToHexString(CRC16.GetCRC(serverStr8));
                // String hexserverStr08 = "68 "+
                // HexConvert.BinaryToHexString(serverStr8) + serverstrCrc08;
                // InputStream is08 = new
                // ByteArrayInputStream(hexserverStr08.getBytes());
                // DataOutputStream out08 = new
                // DataOutputStream(socket.getOutputStream());
                // String s08 = new BufferedReader(new
                // InputStreamReader(is08)).readLine();
                // System.out.print("服务器端说："+s08+"\n");
                // out08.write(HexConvert.parseHexStr2Byte(s08.replace( " ",""
                // )));
                // // out.close();
                // // is.close();
                // }else if(str.equals("0B") && HexConvert.isHave(arrStr,"0B")){
                // String str8B = hexs.substring(12, 28);
                // System.out.print("8B："+str8B+"\n");
                // String hexStr8B = "8B "+ substrs(str1) +"00 09 "+
                // substrs(str8B)+"00";
                // byte[] serverStr8B =CRC16.hexToByte(hexStr8B.replace( " ",""
                // ));
                // String serverstrCrc8B =
                // HexConvert.BinaryToHexString(CRC16.GetCRC(serverStr8B));
                // String hexserverStr8B = "68 "+
                // HexConvert.BinaryToHexString(serverStr8B) + serverstrCrc8B;
                // InputStream is8B = new
                // ByteArrayInputStream(hexserverStr8B.getBytes());
                // DataOutputStream out8B = new
                // DataOutputStream(socket.getOutputStream());
                // String s8B = new BufferedReader(new
                // InputStreamReader(is8B)).readLine();
                // System.out.print("服务器端说："+s8B+"\n");
                // out8B.write(HexConvert.parseHexStr2Byte(s8B.replace( " ",""
                // )));
                // // out.close();
                // // is.close();
                // }else if(str.equals("1D") && HexConvert.isHave(arrStr,"1D")){
                // String hexStr9D = "9D "+ substrs(str1) +"00 01 00";
                // byte[] serverStr9D =CRC16.hexToByte(hexStr9D.replace( " ",""
                // ));
                // String serverstrCrc9D =
                // HexConvert.BinaryToHexString(CRC16.GetCRC(serverStr9D));
                // String hexserverStr9D = "68 "+
                // HexConvert.BinaryToHexString(serverStr9D) + serverstrCrc9D;
                // InputStream is9D = new
                // ByteArrayInputStream(hexserverStr9D.getBytes());
                // DataOutputStream out9D = new
                // DataOutputStream(socket.getOutputStream());
                // String s9D = new BufferedReader(new
                // InputStreamReader(is9D)).readLine();
                // System.out.print("服务器端说："+s9D+"\n");
                // out9D.write(HexConvert.parseHexStr2Byte(s9D.replace( " ",""
                // )));
                // }
                // }
                // }else{
                // System.out.print("启动失败"+"\n");
                // }
                // }else{
                // System.out.println("登陆失败");//
                // }
                // }
                else {
                    System.out.println("CRc校验失败被丢弃");
                }
            }
            OutputStream outputStream = socket.getOutputStream();// 获取一个输出流，向服务端发送信息
            PrintWriter printWriter = new PrintWriter(outputStream);// 将输出流包装成打印流
            printWriter.print("你好，服务端已接收到您的信息");
            printWriter.flush();
            socket.shutdownOutput();// 关闭输出流
            bufferedReader.close();
            inputStream.close();
            printWriter.close();
            outputStream.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public static String substrs(String replace) {
        String regex = "(.{2})";
        replace = replace.replaceAll(regex, "$1 ");
        return replace;
    }

    //字符串转ASCLL码
    public static String stringToAscii(String value) {
        StringBuffer sbu = new StringBuffer();
        char[] chars = value.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            if (i != chars.length - 1) {
                sbu.append((int) chars[i]).append(",");
            } else {
                sbu.append((int) chars[i]);
            }
        }
        return sbu.toString();
    }

    //16进制字符串转10进制FLOAT
    public float stringToFloat(String str) {
        return Long.parseLong(str, 16) / 1000;
    }

    //YYMMDDHHMMSS转换成date
    public static Date stringToDate(String str) {
        SimpleDateFormat df = new SimpleDateFormat("yyMMddhhmmss");
        try {
            Date date = df.parse(str);
            System.out.println(date.getTime());
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return new Date();
    }
}