package com.example.blxny.util;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.oreilly.servlet.multipart.FilePart;
import com.oreilly.servlet.multipart.MultipartParser;
import com.oreilly.servlet.multipart.ParamPart;
import com.oreilly.servlet.multipart.Part;
import org.apache.commons.codec.binary.Base64;
import org.springframework.util.ClassUtils;
import org.springframework.web.multipart.MultipartFile;


public class FileUtils {

    public static Map<String, String> upload(HttpServletRequest request, int maxSize, String path) {

        //以map形式保存数据 key对应保存的是获取界面上的name名称 value保存的是获取界面上的name对应的值
        Map<String, String> map = new HashMap<String, String>();
        Part part = null;
        try {
            MultipartParser mrequest = new MultipartParser(request, maxSize);
            mrequest.setEncoding("utf-8");
            //遍历所有的part组
            while ((part = mrequest.readNextPart()) != null) {
                if (part.isFile()) {  //判断是否是文件
                    FilePart filepart = (FilePart) part;//转化成文件组
                    String fileName = filepart.getFileName();//得到文件名
                    if (fileName != null && fileName.length() > 0) {
                        // 取得扩展名
                        String fileExtName = fileName.substring(fileName.lastIndexOf(".") + 1).toLowerCase();
                        // 只上传图片  //判断图片上传的格式是否符合 后缀名是否有效
                        if (fileExtName.equalsIgnoreCase("jpeg")
                                || fileExtName.equalsIgnoreCase("png")
                                || fileExtName.equalsIgnoreCase("jpg")
                                || fileExtName.equalsIgnoreCase("gif")
                                || fileExtName.equalsIgnoreCase("ico")
                                || fileExtName.equalsIgnoreCase("bmp")
                                || fileExtName.equalsIgnoreCase("flv")
                                || fileExtName.equalsIgnoreCase("mp4")
                                || fileExtName.equalsIgnoreCase("mp3")) {
                            /*String newFileName = new Date().getTime() + "."+ fileExtName;//重新改文件名  文件名+扩展名 */
                            String newFileName = new Date().getTime() + fileName;//不改图片名字
                            String newPath = path + "/" + newFileName; //文件处理文件上传的路径
                            File newFile = new File(newPath);
                            filepart.writeTo(newFile);  //将文件真正写入到对应的文件夹中
                            //filepart.getName()  得到 request 要接收的参数的名字
                            map.put(filepart.getName(), newFileName);//把文件信息保存到map中
                            map.put("newFile", newFile.toString());
                        } else {
                            map.put("geshi", "geshi");
                            continue;
                        }// 说明上传的不是图片
                    } else {
                        map.put("yes", "yes");
                        continue; // 说明没有选择上传图片
                    }
                }
                //判断是否是参数
                else if (part.isParam()) {
                    ParamPart paramPart = (ParamPart) part;
                    map.put(paramPart.getName(), paramPart.getStringValue());
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return map;
    }


    public String ImageUpload(MultipartFile file, HttpServletRequest request, HttpServletResponse response, HttpSession session) throws IllegalStateException, IOException {
        String savePaths = "";
        if (file != null) {// 判断上传的文件是否为空
            // String path="";// 文件路径
            String type = null;// 文件类型
            String fileName = file.getOriginalFilename();// 文件原名称
            //System.out.println("上传的文件原名称:"+fileName);
            // 判断文件类型

            type = fileName.indexOf(".") != -1 ? fileName.substring(fileName.lastIndexOf(".") + 1, fileName.length()) : null;
            if (type != null) {// 判断文件类型是否为空

                if ("GIF".equals(type.toUpperCase()) || "PNG".equals(type.toUpperCase()) || "JPG".equals(type.toUpperCase())) {
                    // 项目在容器中实际发布运行的根路径
                    String realPath = request.getSession().getServletContext().getRealPath("/resource/images/");
                    // 自定义的文件名称
                    String trueFileName = String.valueOf(System.currentTimeMillis()) + fileName; //15312196403485.jpg
                    // 设置存放图片文件的路径
                    String path = realPath +/*System.getProperty("file.separator")+*/trueFileName;
     /*    imageuploads.setTrueFileName(trueFileName);
         System.out.println(imageuploads.getTrueFileName());
         img.setImgname(imageuploads.getTrueFileName());
         System.out.println(img.getImgname());
         imgMapper.addimg(img);*/
                    // 转存文件到指定的路径
                    file.transferTo(new File(path));
                    savePaths = path;
                } else {
                    System.out.println("不是我们想要的文件类型,请按要求重新上传");
                    return null;
                }
            } else {
                System.out.println("文件类型为空");
                return null;
            }
        } else {
            System.out.println("没有找到相对应的文件");
            return null;
        }
        return savePaths;
    }

    /**
     * 通过BASE64Decoder解码，并生成图片
     *
     * @param encodedImageStr
     * @return url地址
     */
    public static String convertStringtoImage(String encodedImageStr,HttpServletRequest request) {
        if (encodedImageStr == null)
            return "图片不存在";
        try {
            // Base64解码图片
            System.out.println(encodedImageStr);
            String[] arr = encodedImageStr.split(",");
            String data = encodedImageStr.substring(encodedImageStr.indexOf(","),encodedImageStr.length()-1);
            byte[] imageByteArray;
            imageByteArray = Base64.decodeBase64(data);
            //时间戳命名
            String fileName = String.valueOf(System.currentTimeMillis()) + ".jpg";
            //项目路径
            String path = ClassUtils.getDefaultClassLoader().getResource("").getPath();
            Calendar date = Calendar.getInstance();
            String saveDirectory = request.getSession().getServletContext().getRealPath("/upload/images");
            String webUrl = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath() + "/upload/images/";

            String saveFilePath = ""; // 图片地址

            // 建年的文件夹
            int iYear = date.get(Calendar.YEAR);
            saveDirectory += File.separator + iYear;
            webUrl += iYear + "/";
            int iMonth = date.get(Calendar.MONTH) + 1;
            saveDirectory += File.separator + iMonth;
            webUrl += iMonth + "/";
            int iDay = date.get(Calendar.DATE);
            saveDirectory += File.separator + iDay;
            webUrl += iDay + "/";

            File f = new File(saveDirectory);
            if (!f.isDirectory())
            {
                f.mkdirs();
            }
            String saveFileName = iYear + ""+ iMonth + "" + iDay+ "" + date.get(Calendar.HOUR) + date.get(Calendar.MINUTE) + "" + date.get(Calendar.SECOND) + ".jpg";
            webUrl += saveFileName;

            saveFilePath = saveDirectory + File.separator + saveFileName;
            System.out.println("绝对地址"+saveFilePath);
            FileOutputStream imageOutFile = new FileOutputStream(saveFilePath);
            imageOutFile.write(imageByteArray);

            imageOutFile.close();
            System.out.println("Image Successfully Stored");
            return webUrl;
        } catch (FileNotFoundException fnfe) {
            System.out.println("Image Path not found" + fnfe);
        } catch (IOException ioe) {
            System.out.println("Exception while converting the Image " + ioe);
        }
        return "fail";
    }
}