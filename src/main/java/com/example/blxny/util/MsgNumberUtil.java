package com.example.blxny.util;


import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;


@Component
@ConfigurationProperties(prefix = "msgnumber")
public class MsgNumberUtil {
    private int number;

    public String getNumber() {
        if (number==9999){
            number=0;
            return "9999";
        }
        int temp = number;
        ++number;
        String tempReturn=subString();
        return tempReturn;
    }

    public void setNumber(int number) {
        this.number = number;
    }


    public String subString(){
        String str = "0000"+number;
        String numStr = number+"";
        return str.substring(numStr.length(),numStr.length()+4);
    }
}


