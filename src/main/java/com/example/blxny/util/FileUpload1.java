package com.example.blxny.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.tomcat.util.http.fileupload.FileItemIterator;
import org.apache.tomcat.util.http.fileupload.FileItemStream;
import org.apache.tomcat.util.http.fileupload.FileUploadException;
import org.apache.tomcat.util.http.fileupload.disk.DiskFileItemFactory;
import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;
import org.apache.tomcat.util.http.fileupload.util.Streams;


public class FileUpload1 extends HttpServlet
{
	private static final long serialVersionUID = 1L;

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		response.setCharacterEncoding("UTF-8");
		response.setContentType("application/json;charset=UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		// response.addHeader("Access-Control-Allow-Credentials","true");
		response.addHeader("Access-Control-Allow-Headers", "apptypeid,platformid,city,district,Content-Type, Authorization, Accept,X-Requested-With");
		response.addHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS");
		response.getWriter().print("");
	}

	@Override
	public void doOptions(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		response.setCharacterEncoding("UTF-8");
		response.setContentType("application/json;charset=UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		// response.addHeader("Access-Control-Allow-Credentials","true");
		response.addHeader("Access-Control-Allow-Headers", "Content-Type, Authorization, Accept,X-Requested-With");
		response.addHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS");
		response.getWriter().print("");
	}

	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		response.setCharacterEncoding("UTF-8");
		response.setContentType("application/json;charset=UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		// response.addHeader("Access-Control-Allow-Credentials","true");
		response.addHeader("Access-Control-Allow-Headers", "Content-Type, Authorization, Accept,X-Requested-With");
		response.addHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS");

		Calendar date = Calendar.getInstance();
		String saveDirectory = request.getSession().getServletContext().getRealPath("/upload/images");
		String webUrl = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath() + "/upload/images/";

		// 建年的文件夹
		int iYear = date.get(Calendar.YEAR);
		saveDirectory += File.separator + iYear;
		webUrl += iYear + "/";
		int iMonth = date.get(Calendar.MONTH) + 1;
		saveDirectory += File.separator + iMonth;
		webUrl += iMonth + "/";
		int iDay = date.get(Calendar.DATE);
		saveDirectory += File.separator + iDay;
		webUrl += iDay + "/";

		File f = new File(saveDirectory);
		if (!f.isDirectory())
		{
			f.mkdirs();
		}

		if (ServletFileUpload.isMultipartContent(request))
		{
			DiskFileItemFactory dff = new DiskFileItemFactory();
			dff.setRepository(f);
			dff.setSizeThreshold(1024000);
			ServletFileUpload sfu = new ServletFileUpload(dff);
			FileItemIterator fii = null;
			try
			{
				fii = sfu.getItemIterator(request);
			}
			catch (FileUploadException e1)
			{
				e1.printStackTrace();
			}
			String saveFilePath = ""; // 图片地址
			boolean isUEditor = false;
			String state = "SUCCESS";
			try
			{
				int iIndex = 0;
				while (fii.hasNext())
				{
					FileItemStream fis = fii.next();

					try
					{
						if (!fis.isFormField() && fis.getName().length() > 0)
						{
							iIndex++;
							String saveFileName = iYear + ""+ iMonth + "" + iDay+ "" + date.get(Calendar.HOUR) + date.get(Calendar.MINUTE) + "" + date.get(Calendar.SECOND) + "-" + iIndex + ".png";
							webUrl += saveFileName;
							saveFilePath = saveDirectory + File.separator + saveFileName;
							BufferedInputStream in = new BufferedInputStream(fis.openStream());// 获得文件输入流
							FileOutputStream a = new FileOutputStream(new File(saveFilePath));
							BufferedOutputStream output = new BufferedOutputStream(a);
							Streams.copy(in, output, true);// 开始把文件写到你指定的上传文件夹
						}
						else
						{
							String fname = fis.getFieldName();

							if (fname.indexOf("isueditor") != -1)
							{
								BufferedInputStream in = new BufferedInputStream(fis.openStream());
								byte c[] = new byte[10];
								int n = 0;
								while ((n = in.read(c)) != -1)
								{
									String temp = new String(c, 0, n);
									if (temp.trim().equals("1"))
									{
										isUEditor = true;
									}
									break;
								}
							}
						}

					}
					catch (Exception e)
					{
						e.printStackTrace();
						state = "FALSE";
					}
				}
			}
			catch (FileUploadException e)
			{
				e.printStackTrace();
				state = "FALSE";
			}

			if (isUEditor)
			{
				response.getWriter().print("{'url':'" + webUrl + "','title':'','state':'" + state + "'}");
			}
			else
			{
				response.getWriter().print("{status: 'OK',data: '" + webUrl + "'}");
			}
		}
	}
}
