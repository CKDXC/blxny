package com.example.blxny.util;

public enum ErrorInfomation
{
	Err1000(1000, "获取magazine模块出现异常，请联系管理员。"),
	Err1001(1001, "无法从数据库中查询到杂志信息。"),
	Err1002(1002, "保存失败!"),

	Err1040(1040, "登录失败,用户名错误!"),
	Err10401(10401,"登录失败,密码错误!"),
	Err1041(1041, "登录模块出现异常，请联系管理员!"),
	Err1042(1042, "用户名已注册,请选择其他用户名!"),
	Err1043(1043, "登录模块出现异常，请联系管理员!"),
	Err1044(1044, "您修改的用户不存在!"),
	Err1045(1045, "用户更新失败!"),
	Err1046(1046, "用户更新模块出现异常,请联系管理员!"),
	Err1047(1047, "您查询的用户不存在!"),
	Err1048(1048, "用户查询模块出现异常,请联系管理员!"),
	Err1049(1049, "密码修改失败!"),
	Err1050(1050, "修改密码模块出现异常,请联系管理员"),
	Err1051(1051, "资料修改失败!"),
	Err1052(1052, "资料完善模块出现异常,请联系管理员"),


	Err1080(1080, "deviceaccesslog保存失败!"),
	Err1081(1081, "deviceaccesslog保存出现异常,请联系管理员!"),
	Err1082(1082, "您更新的deviceaccesslog不存在"),
	Err1083(1083, "deviceaccesslog更新失败!"),
	Err1084(1084, "deviceaccesslog更新模块出现异常,请联系管理员!"),
	
	Err9999(9999, "未知错误。");

	// 成员变量
	private String description;
	private int code;

	// 构造方法
	private ErrorInfomation(int code, String description)
	{
		this.description = description;
		this.code = code;
	}
	
	public int getCode()
	{
		return this.code;
	}
	
	public String getDescription()
	{
		return this.description;
	}

	// 覆盖方法
	@Override
	public String toString()
	{
		return this.code + "_" + this.description;
	}
}
