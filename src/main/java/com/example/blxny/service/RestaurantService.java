package com.example.blxny.service;

import com.example.blxny.model.Shop;

import java.util.List;

/*
 *项目名: blxny
 *文件名: RestaurantService
 *创建者: SCH
 *创建时间:2019/5/28 14:14
 *描述: TODO
 */
public interface RestaurantService {
    //获取对应分类的 分页数据
    Object getList(int type, int popular, int lastId, int pageSize);

    Object getCateringProducts(int restaurantId, int lastId, int type, int ps);

    //酒店详情获取
    List getHotel(int id);

    //商店货品获取
    Object getShop(int id, int size, int number);

    //商店货品按种类获取
    List<Shop> getShopByType(int type, int id, int size, int page);

    //二维码加密信息
    Object getMessage(String uuid, String type);

    //二维码解密获取信息
    String getQRCode(String uuid, String code);

    //确认使用该二维码优惠
    boolean endQRCode(String code, String uid);

    //确认是否使用了二维码
    boolean checkUse(int id);

    String evaluat(int id, int socre);
}
