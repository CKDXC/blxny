package com.example.blxny.service;

public interface AppointmentService {

    String dealAppoCharge(String appoId,String result);

    void cancelAppoCharge(String appoId,String result);

}
