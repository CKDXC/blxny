package com.example.blxny.service.impl;


import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.IdUtil;
import com.example.blxny.dao.CouponDao;
import com.example.blxny.dao.ScoreDiscountDao;
import com.example.blxny.dao.ScoreProductDao;
import com.example.blxny.dao.UserDao;
import com.example.blxny.model.Coupon;
import com.example.blxny.model.Information;
import com.example.blxny.model.ScoreDiscount;
import com.example.blxny.model.ScoreProduct;
import com.example.blxny.service.CouponService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Service
public class CouponServiceImpl implements CouponService {

    @Autowired
    private CouponDao couponDao;
    @Autowired
    private UserDao userDao;
    @Autowired
    private ScoreProductDao scoreProductDao;
    @Autowired
    private ScoreDiscountDao scoreDiscountDao;
    @Override
    public List<Coupon> getCoupon(String uid,int type, int isUsed){
        try {
            couponDao.changeDate();
            List<Coupon> list = couponDao.getCoupon(uid, type, isUsed, DateUtil.currentSeconds());
            couponDao.changeUserVoucher(list.size(),uid);//user变
            return list;
        }catch (Exception e){
            return null;
        }
    }

    @Override
    @Transactional
    public String exchange(String uid, int scoreProductId) {
        ScoreProduct scoreProduct = scoreProductDao.getProductById(scoreProductId);
        if (null == scoreProduct) {
            return "数据异常";
        }
        int price = scoreProduct.getPrice();
        int stock = scoreProduct.getStock();
        int effectiveTime = scoreProduct.getEffectiveTime();
        if (stock <= 0) {
            return "库存不足";
        }
        int userScore = userDao.getScoreByUid(uid);
        if (userScore < price) {
            return "用户积分不足";
        }
        Date expireDate = DateUtil.offsetDay(new Date(), effectiveTime);
      /*  if (couponDao.addOne(uid, scoreProductId, expireDate) <= 0) {
            return "处理异常";
        }*/
        int productScore = scoreProduct.getPrice();
        if (userDao.updateScore(uid, productScore) <= 0) {
            return "处理异常";
        }
        return "success";
    }

    @Override
    public List<ScoreProduct> getScoreProducts() {
        return null;
    }



    @Override
    public Object getGold(String uid,
                          String money,
                          String cold,//多少积分
                          String type,//卷的类型
                          String day) {//过期时间
        String returnMassage="error";
        //先看用户是否满足获取代金卷条件
        int scord=userDao.findUserScore(uid);
        if(scord>Integer.valueOf(cold)){
        //再添加代金卷
            int id = 0;
            for(int j = 0; j< 100; j++){
                id= (int)((Math.random()*9+1)*100000);
            }
            Date date=new Date();
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            Calendar now = Calendar.getInstance();
            now.setTime(date);
            now.set(Calendar.DATE, now.get(Calendar.DATE) + Integer.valueOf(day));//+后 -前
            Date overday= now.getTime();
            if(couponDao.addOne(uid,id,Integer.valueOf(type),date,cold,Double.valueOf(money),overday)>0){
                //再扣减积分
                userDao.updateUserScore(Integer.valueOf(cold),uid);
                //再添加待金卷张数
                Information information=new Information();
                information.setTitle("积分兑换");
                information.setInfo("您的积分已经兑换成功，请确认兑换金额，感谢您的使用。");
                information.setUserUuid(uid);
                userDao.addInformation(information);
                ScoreDiscount sc=new ScoreDiscount();
                sc.setAmount(-Integer.valueOf(cold));
                sc.setFrom(5);
                sc.setProvider(uid);
                sc.setUid(uid);
                sc.setScoreId(IdUtil.simpleUUID());
                scoreDiscountDao.newTable(sc);
                returnMassage="OK";
            }
        }
        return returnMassage;
    }

    @Override
    public List<Coupon> getGoldUse(String uuid) {
        List<Coupon> list=couponDao.getGoldUse(uuid);
        return list;
    }

    @Override
    public String cleanGoldUse(String uuid) {
        if(couponDao.cleanGoldUse(uuid)>0){
            return "OK";
        }else{
            return "error";
        }

    }
}
