package com.example.blxny.service.impl;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.RandomUtil;
import com.example.blxny.dao.PowerDao;
import com.example.blxny.dao.SalesDao;
import com.example.blxny.dao.UserDao;
import com.example.blxny.model.*;
import com.example.blxny.service.FindSalesService;
import com.example.blxny.tool.DateUtil;
import com.example.blxny.tool.DistanceTool;
import com.example.blxny.tool.JPushTool;
import com.example.blxny.tool.PayCommonUtil;
import com.example.blxny.util.RedisUtil;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static com.example.blxny.tool.JPushTool.ONE;

@Service
public class FindSalesServiceImpl implements FindSalesService {
    @Autowired
    private SalesDao dao;
    @Autowired
    private RedisUtil redisUtil;
    @Autowired
    private PowerDao powerDao;
    @Autowired
    private UserDao userDao;

    @Override
    public String findOrderInfo(String user) {
        //先看有谁新增了订单

        return null;
    }

    @Override
    public List<Ad> adEndless() {
        List<Ad> ad = dao.findAd();
        return ad;
    }

    @Override
    public Object findSales(String lat, String lng) {
        JSONObject js = new JSONObject();
        DistanceTool tool = new DistanceTool();
        //找到最近的业务员信息
        List<SaleMan> man = dao.findSales();
        String manlat = null;
        String manlng = null;
        double longs = 0;
        String manId = null;
        String manPhone = null;
        String office = null;
        for (SaleMan i : man) {
            manlat = i.getLat();
            manlng = i.getLng();
            manId = i.getSeleid();
            manPhone = i.getPhone();
            office = i.getServiceOffice();
            longs = tool.distanceByLongNLat(lat, lng, manlng, manlat);
            break;
        }
        for (SaleMan i : man) {
            double lon = tool.distanceByLongNLat(lat, lng, i.getLng(), i.getLat());
            if (longs > lon) {
                manlat = i.getLat();
                manlng = i.getLng();
                manId = i.getSeleid();
                manPhone = i.getPhone();
                office = i.getServiceOffice();
                longs = lon;
            }
        }
        js.put("salesId", manId);
        js.put("salesPhone", manPhone);
        js.put("office", office);
        return js;
    }

    @Override
    public int addService(com.example.blxny.model.Service service) {
        if (dao.addService(service) > 0) {
            return dao.addService(service);
        } else {
            return 0;
        }
    }

    @Override
    public int changeSalesType(String type, String id) {
        return dao.changeSalesType(Integer.valueOf(type), id);
    }

    @Override
    public String isSales(String uuid) {
        String isSales = dao.isSales(uuid);
        return dao.isSales(uuid);
    }

    //业务员上班
    @Override
    public String changeSalesTypeNow(String uuid, int type, String JPid,String lng,String lat) {
        JSONObject js = new JSONObject();
        //JPid = userDao.FindUserAllByUid(uuid).getStarttime();//通过uuid获取jpid
        List list = redisUtil.lRange("workMan", 0, redisUtil.lLen("workMan"));//获取所有的上班的人
        boolean bool = false;
        for (Object lis : list) {
            JSONObject json = JSONObject.fromObject(lis);
            if (json.get("uuid") == null || json.get("uuid").equals(uuid)) {
                bool = true;
            }
        }
        if (type == 1) {
            if (!bool) {
                if (dao.updateWork(uuid, JPid,lng,lat) > 0) {//修改数据库上班状态
                    //存入缓存
                    SaleMan man = dao.findSalesNew(uuid);
                    js.put("uuid", uuid);
                    js.put("id", man.getSeleid());
                    js.put("lat", man.getLat());
                    js.put("lng", man.getLng());
                    redisUtil.lRightPush("workMan", js.toString());//将此人存入上班者集合
                    //System.out.println("在上班的人还有"+redisUtil.lRange("workMan", 0, redisUtil.lLen("workMan")));
                    return "OK";
                }
            }
        } else {
            if (dao.offWork(uuid) > 0) {
                SaleMan man = dao.findSalesWork(uuid);
                js.put("uuid", uuid);
                js.put("id", man.getSeleid());
                js.put("lat", man.getLat());
                js.put("lng", man.getLng());
                redisUtil.lRemove("workMan", 0, js.toString());
                // System.out.println("休息,在上班的人还有"+redisUtil.lRange("workMan", 0, redisUtil.lLen("workMan")));
                return "OK";
            }
        }
        return "error";
    }

    @Override
    public Object oderInfo(String uuid, String type, String lat, String lng) {
        String SaleId = null;
        String time = null;
        String month = null;
        String getTherStr = null;
        String falishJobStr = null;
        String returnStation = null;
        String returnStationId = null;
        String money = null;
        //最初查找自己有没有订单
        List ls = redisUtil.lRange("fate", 0, redisUtil.lLen("fate"));
        boolean chose = true;
        for (Object list : ls) {
            JSONObject js = JSONObject.fromObject(list);
            if (js.get("user").equals(uuid)) {
                chose = false;
            }
        }
        JSONObject js = new JSONObject();
        if (chose) {
            //先生成订单号
            SaleId = PayCommonUtil.getRandomNumber(12);
            //时间运算
            Date date = new Date();
            time = new SimpleDateFormat("yyyy-MM-dd hh:mm").format(date);
            month = new SimpleDateFormat("MM月dd日").format(date);
            Date getTher = DateUtil.adjustDateByMinutes(date, 15, 1);
            Date falishJob = DateUtil.adjustDateByMinutes(getTher, 30, 1);
            getTherStr = new SimpleDateFormat("hh:mm").format(getTher);
            falishJobStr = new SimpleDateFormat("hh:mm").format(falishJob);
            //获取最近的驿站
            if (redisUtil.lLen("station") == 0) {
                List<Station> station = powerDao.findAllStationNoMore();
                for (Station stationone : station) {
                    JSONObject json = new JSONObject();
                    json.put("id", stationone.getStationId());
                    json.put("name", stationone.getName());
                    json.put("type", stationone.getType());
                    json.put("lat", stationone.getLat());
                    json.put("lng", stationone.getLng());
                    redisUtil.lRightPush("station", json.toString());
                    System.out.println("初始化station缓存");
                }
            }
            List<String> stationAll = redisUtil.lRange("station", 0, redisUtil.lLen("station"));
            DistanceTool tool = new DistanceTool();
            double longs = 99999999;
            for (String sta : stationAll) {
                JSONObject stationJson = JSONObject.fromObject(sta);
                double newlongs = tool.distanceByLongNLat((String) stationJson.get("lat"), (String) stationJson.get("lng"), lng, lat);
                if (longs > newlongs) {
                    longs = newlongs;
                    returnStation = (String) stationJson.get("name");
                    returnStationId = (String) stationJson.get("id");
                }
            }
            //付款金额（暂时写死）
            money = null;
            if ("0".equals(type)) {//洗车
                money = "25";
            } else {
                money = "15";
            }
        }
        js.put("orderId", SaleId);
        js.put("nowTime", time);
        js.put("nowMonth", month);
        js.put("getTher", getTherStr);
        js.put("falishJob", falishJobStr);
        js.put("stationName", returnStation);
        js.put("stationId", returnStationId);
        js.put("money", money);
        return js;
    }

    @Override
    public Object findSalesForUs(String beginTime, String endTime, String money, String carCard, String phone,  String serviceInfo, String other,
                                 String stationId,String  useruuid,String car) throws ParseException {
        beginTime+=":00";
        endTime+=":00";
//先找到那五个人
        //找到这个桩的信息经纬度
        Station newSales = powerDao.findOneStation(stationId);
        if(newSales==null){
            return "no station";
        }
        String lat = newSales.getLat();
        String lng = newSales.getLng();
        //找到在上班的人
        List<String> AllMan = redisUtil.lRange("workMan", 0, redisUtil.lLen("workMan"));
        DistanceTool tool = new DistanceTool();
        //判断经纬度最近的几个人
        List<SaleMan> findMan = new ArrayList<SaleMan>();
        for (String allman : AllMan) {
            JSONObject js = JSONObject.fromObject(allman);
            String StationLat = (String) js.get("lat");
            String StationLng = (String) js.get("lng");
            double far = tool.distanceByLongNLat(lat, lng, StationLng, StationLat);
            findMan.add(new SaleMan((int) far, (String) js.get("id")));
        }
        Collections.sort(findMan, new Comparator<SaleMan>() {//集合排序
            @Override
            public int compare(SaleMan o1, SaleMan o2) {
                int i = o1.getId() - o2.getId();
                return i;
            }
        });
        List list = new ArrayList();
        int i = 0;
        for (SaleMan stu : findMan) {//找到前五个
            JSONObject json = new JSONObject();
            json.put("far", stu.getId());
            json.put("id", stu.getSeleid());
            list.add(json);
            i++;
            if (i == 5) {
                break;
            }
        }
        if (list.size() != 0) {
//最后生成一个订单
            Date date = new Date();
            SalesOder oder = new SalesOder();
            oder.setSalesTime(date);//订单生成时间
            oder.setBeginTime(cn.hutool.core.date.DateUtil.parse(beginTime));//客户设置的订单开始时间
            oder.setEndTime(cn.hutool.core.date.DateUtil.parse(endTime));//客户设置的订单结束时间
            oder.setMoney(Convert.toBigDecimal(money));//订单的价格
            oder.setCarCard(carCard);
            oder.setPhone(phone);
            oder.setServiceInfo(Integer.valueOf(serviceInfo));
            oder.setOther(other);
            oder.setStationId(stationId);
            oder.setUseruuid(useruuid);
            oder.setCar(car);
            oder.setSalesId(Convert.toStr(RandomUtil.randomInt(999999999)));//订单的编号
//缓存抢单数据
            JSONObject order = new JSONObject();
            order.put("user", useruuid);
            order.put("sales",list);
            order.put("id", oder);
            redisUtil.lRightPush("fate", order.toString());
            if (0 < dao.addSalesOder(oder)) {
                //再发送推送给他们
                for (Object lists : list) {
                    JSONObject js = JSONObject.fromObject(lists);
                    JPushTool.JPush(js.getString("id"), "单号" + oder.getSalesId(), "您有新的抢单信息", ONE);
                }
                return "OK";
            } else {
                return "error";
            }
        } else {
            return "暂时没有业务员在线";
        }
    }

    @Override
    public Object fate(String uuid, String id) {
        SalesOder oder = new SalesOder();
        //先找这个订单还在不在
        List list = redisUtil.lRange("fate", 0, redisUtil.lLen("fate"));
        for (Object lists : list) {
            JSONObject js = JSONObject.fromObject(lists);
            if (js.get("id").equals(id)) {
                String orderid = (String) js.get("id");
                redisUtil.lRemove("fate", 0, js.toString());//删除此抢单
                if (dao.getFate(uuid, String.valueOf(js.get("id"))) > 0) {
                    oder = dao.getOrderSales(orderid);
                    if (dao.goToWork(uuid) > 0) {//改变有业务状态
                        SaleMan man = dao.findSalesOne(uuid);//查询此人
                        JSONObject jss = new JSONObject();
                        jss.put("uuid", uuid);
                        jss.put("id", man.getSeleid());
                        jss.put("lat", man.getLat());
                        jss.put("lng", man.getLng());
                        redisUtil.lRemove("workMan", 0, jss.toString());//清除缓存
                        System.out.println(jss.toString());
                        break;
                    }
                }
            }
        }
        return oder;
    }

    @Override
    public Object findSalesEndliss(String uuid, String id) {
        SalesOder salesOder = new SalesOder();
        //先看这个单子有没有被人抢
        List list = redisUtil.lRange("fate", 0, redisUtil.lLen("fate"));
        int i = 0;
        for (Object lists : list) {
            JSONObject js = JSONObject.fromObject(lists);
            String id1 = js.getString("id");
            if (id1.equals(id)) {
                i++;
            }
        }
        if (i == 0) {
            salesOder = dao.getOrderSales(id);
        }
        return salesOder;
    }

    @Override
    public String findTypeNow(String uuid)  {
        //先看他的是不是在上班
        List list=new ArrayList();
        try {
           list= redisUtil.lRange("workMan", 0,redisUtil.lLen("workMan"));
        }catch (NullPointerException e){
            list=null;
        }
        String bool = "0";
        if (list != null) {
            for (Object lis : list) {
                JSONObject json = JSONObject.fromObject(lis);
                if (json.get("uuid") == null || json.get("uuid").equals(uuid)) {
                    bool = "1";
                }
            }
        }
        return bool;
    }

    @Override
    public String findFateOrder(String uuid) {
        List fate=redisUtil.lRange("fate", 0,redisUtil.lLen("fate"));//所有要抢的单子
        for(Object list:fate){
            JSONObject js=JSONObject.fromObject(list);
            List sale=(List)js.get("sales");
            for(Object s:sale){
                JSONObject json=JSONObject.fromObject(s);
                System.out.println(json);
                if(json.get("id").equals(uuid)){
                    return js.get("id").toString();//找到他自己，获取这个订单信息
                }
            }
        }
        return null;
    }


}
