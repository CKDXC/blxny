package com.example.blxny.service.impl;

import cn.hutool.core.date.DateField;
import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;
import com.example.blxny.dao.*;
import com.example.blxny.model.*;
import com.example.blxny.service.OrderService;
import com.example.blxny.tool.DistanceTool;
import com.example.blxny.tool.DoubleUtil;
import com.example.blxny.util.*;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

@Transactional
@Service
public class OrderServiceImpl implements OrderService {
    @Autowired
    private OrderDao orderDao;
    @Autowired
    private StationDao stationDao;
    @Autowired
    private MsgNumberUtil msgNumberUtil;
    @Autowired
    private DeviceDao deviceDao;
    @Autowired
    private UserDao userDao;
    @Autowired
    private RuleDao ruleDao;

    @Override
    public Object findAllOrderInfor(String id) {
        String time = new SimpleDateFormat("HH").format(new Date());
        Order order = orderDao.findAllOrderInfor(id);
        Device device = deviceDao.getById(order.getDeviceId());
        Rule rule = ruleDao.getRule(device.getStationId(), String.valueOf(device.getChargeType()));
        double powerNum = 0;
        DistanceTool tool = new DistanceTool();
        String inttime = (String) tool.getDatabaseTimeForNow().get("time");
        if (inttime.equals("1")) {
            powerNum = rule.getCuspPrice();
        } else if (inttime.equals("2")) {
            powerNum = rule.getHighPrice();
        } else if (inttime.equals("3")) {
            powerNum = rule.getNormalPrice();
        } else {
            powerNum = rule.getValleyPrice();
        }
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("stationName", orderDao.findStationOne(device.getStationId()).getName());
        //驿站名字

        jsonObject.put("orderId", order.getOrderId());
        //订单号

        jsonObject.put("deviceId", order.getDeviceId());
        //桩号

        jsonObject.put("powerTime", String.valueOf(DateUtil.between(order.getStartTime(), order.getEndTime(), DateUnit.MINUTE)));
        //时间

        jsonObject.put("allPower", order.getChargeAmount());
        //充电总电量

        String salePower = "0";
        jsonObject.put("salePower", salePower);
        //优惠电量#

        jsonObject.put("time", new SimpleDateFormat("yyyy-MM-dd").format(order.getCreated()));
        //消费时间

        jsonObject.put("powerPay", String.valueOf(powerNum));
        //电费单价

        //jsonObject.put("servicePay", DoubleUtil.mul(Double.valueOf(charging.getServiceNum()), Double.valueOf(order.getChargeAmount())));
        jsonObject.put("servicePay", order.getServiceAmount());
        //服务费

        double saloPay = DoubleUtil.round(DoubleUtil.mul(Double.valueOf(salePower), powerNum), 2);//计算优惠金额并保留两位=优惠电量*电价
        jsonObject.put("salePay", saloPay);
        //优惠价格=优惠电量*电价

        BigDecimal overTime = order.getOverPay();
        if (overTime == null) {
            overTime = new BigDecimal(0);
        }
        jsonObject.put("overTime", overTime);
        //延时费

        BigDecimal allpay = order.getTotalAmount().add(order.getServiceAmount());
        jsonObject.put("oldPay", allpay);
        //无优惠金额（充电费用）

        double oldpay = DoubleUtil.add(Double.valueOf(String.valueOf(order.getTotalAmount())), Double.valueOf(saloPay));
        jsonObject.put("AllPay", DoubleUtil.round(Double.valueOf(String.valueOf(allpay.subtract(BigDecimal.valueOf(saloPay)))), 2));
        //总金额=（充电费用）-优惠费用

        jsonObject.put("orderType", order.getOrderStatus());
        //支付状态

        jsonObject.put("chargeType", device.getChargeType());
        //快充慢充
        return jsonObject;
    }

    @Override
    public String changeMonththree(String uuid) {
        if (orderDao.changeMonththree(uuid) > 0) {
            return "OK";
        } else {
            return "error";
        }
    }

    @Override
    public Object findorder(String useruuid, String code) {
        List<Order> orders = orderDao.findorder(useruuid, Integer.valueOf(code) * 10);
        List retmassage = new ArrayList();
        for (Order list : orders) {
            Map ret = new HashMap();
            String Stationid = list.getDeviceId().substring(0, 13);
            Date s = list.getStartTime();
            Date n = list.getEndTime();
            long times = DateUtil.between(s, n, DateUnit.MINUTE);//数据库时间转化分钟
/*          Calendar cal = Calendar.getInstance();
            cal.setTime(list.getStartTime());
            long time1 = cal.getTimeInMillis();
            cal.setTime(list.getEndTime());
            long time2 = cal.getTimeInMillis();
            long between_days=(time2-time1)/(1000*60);
            System.out.println("时间测试"+between_days);*/
            ret.put("deviceid", list.getDeviceId());//put桩编号deviceid
            String time = new SimpleDateFormat("yyyy-MM-dd").format(list.getStartTime());
            ret.put("time", time);//put时间time
            ret.put("stationName", orderDao.findStationOne(Stationid).getName());//驿站名stationName
            ret.put("powertime", times);//充电时间//数据库时间转化分钟powertime
            ret.put("orderId", list.getOrderId());//订单号orderId
            ret.put("type", list.getOrderStatus());//订单状态type
            ret.put("pay", String.valueOf(list.getTotalAmount()));//获取价格
            retmassage.add(ret);
        }
        return retmassage;
    }

    @Override
    public Object findorderIng(String useruuid) {
        List<Order> oders = new ArrayList<>();
        oders = orderDao.findorderIng(useruuid);
        JSONObject jsonObject = new JSONObject();
        if (oders.size() == 0) {
            return null;
        } else {
            for (Order oder : oders) {
                jsonObject.put("deviceid", oder.getDeviceId());//put桩编号
                String time = new SimpleDateFormat("yyyy-MM-dd").format(oder.getStartTime());
                jsonObject.put("time", time);//put时间
                String Stationid = oder.getDeviceId().substring(0, 13);
                jsonObject.put("stationName", orderDao.findStationOne(Stationid).getName());//驿站名
                Date bg = oder.getStartTime();
                Date ed = oder.getEndTime();
                if (ed == null) {
                    ed = new Date();
                }
                long times = DateUtil.between(bg, ed, DateUnit.MINUTE);//数据库时间转化分钟
                jsonObject.put("powertime", times);//充电时间
                jsonObject.put("orderId", oder.getOrderId());//订单号
                jsonObject.put("type", oder.getOrderStatus());//订单状态
                jsonObject.put("pay", String.valueOf(oder.getTotalAmount()));
            }
        }

        return jsonObject;
    }

    @Override
    @Transactional
    public String quxiao(String id) {
        //找到对应的桩ip和端口号
        Appointment appo = orderDao.findAppoByAppoUid(id);
        if (null == appo) {
            return "数据异常，预约号错误";
        }
        Device device = orderDao.findDeviceById(appo.getDevice());
        if (device == null) {
            return "数据异常，未找到对应电桩";
        }
        String address = device.getClientAddress();
        String msgCode = msgNumberUtil.getNumber();
        String key = address + msgCode;
        SocketThread socketThread = (SocketThread) ThreadUtil.getThreadByName(address);
        if (null == socketThread) {
            return "电桩未连接到服务器";
        }
        SendThread sendThread = new SendThread(socketThread.getSocket());
        String hexStr02 = "03" + msgCode + "00 09 01" + substrs(appo.getAppoCode());
        byte[] serverStr02 = CRC16.hexToByte(hexStr02.replace(" ", ""));
        String serverstrCrc02 = HexConvert.BinaryToHexString(CRC16.GetCRC(serverStr02));
        String hexserverStr02 = "68 " + HexConvert.BinaryToHexString(serverStr02) + serverstrCrc02;
        sendThread.setCmd(hexserverStr02);
        MQUtil.putMsg(key, "");
        sendThread.start();
        String msg = null;
        int currentWaitTimes = 0;
        int maxWaitTimes = 4000;
        //有回复才会或者超过等待时间会跳出
        while (null == msg || "".equals(msg)) {
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            msg = MQUtil.getMsg().get(key);
            currentWaitTimes += 10;
            //这里面写超时处理
            if (currentWaitTimes >= maxWaitTimes) {
                System.out.println("超时处理");
                return "超时处理";
            }
        }
        MQUtil.removeMsg(key);//删掉无用KV
        String msgOrderId = msg.substring(0, 16);
        String msgResult = msg.substring(16, 18);
        if ("00".equals(msgResult)) {
            if (orderDao.del(id) > 0 && deviceDao.updateStatus(device.getDeviceId(), "0") > 0) {
                return "success";
            } else {
                return "记录更新失败";
            }
        } else if ("01".equals(msgResult)) {
            return "预约号错误";
        } else {
            return "其他原因错误";
        }
    }

    @Override
    public boolean addOne(Order order) {
        if (orderDao.addOne(order) > 0) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public List<Order> getPageByUid(int uid, int orderType, int pageNumber, int pageSize) {
        List<Order> list = orderDao.getPageByUid(uid, orderType, pageNumber, pageSize);
        System.out.println(list.get(0).toString());
        return list;
    }

    @Override
    @Transactional
    public Object addAppointment(String uid, String time, String deviceId, String much, String payType) {
        Device device = null;
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("money", userDao.getMoney(uid));
        jsonObject.put("result", "000000");
        jsonObject.put("appointment", JSONArray.fromObject(new Appointment()));
        //先找到桩，返回桩名/桩号/充电方式
        if (deviceId.length() != 16) {
            deviceId = deviceId + "%";
            device = orderDao.findDeviceByLike(deviceId);
        } else {
            device = orderDao.findDeviceById(deviceId);
        }
        //计算时间
//        Date d = new Date();
//        String df = new SimpleDateFormat("yyyy-MM-dd HH:mm").format(new Date());
//        String now = new SimpleDateFormat("yyMMdd").format(new Date());
//        String endtime = new SimpleDateFormat("HH:mm").format(new Date(d.getTime() + Integer.valueOf(time) * 60 * 1000));
        Date beginTime = new Date();
        Date endTime = DateUtil.offset(beginTime, DateField.MINUTE, 30);
        //用户信息一起add
        Appointment appointment = new Appointment();
        appointment.setType(0);
        appointment.setBeginTime(beginTime);
        appointment.setEndTime(endTime);
        appointment.setDevice(device.getDeviceId());
        appointment.setDeviceType(String.valueOf(device.getChargeType()));
        appointment.setName(device.getName());
        appointment.setUseruuid(uid);
        String appouid = deviceId.substring(4, 10) + new SimpleDateFormat("yyMMdd").format(beginTime);
        String num = orderDao.findId(appouid + "%");
        if (null == num) {
            appouid = appouid + "0001";
        } else {
            num = String.valueOf(Integer.valueOf(num.substring(12)) + 1);
            String temp = "00000" + num;
            num = temp.substring(temp.length() - 4);
            appouid = appouid + num;
        }
        appointment.setAppoUid(appouid);
        //--------------------------------------
        //找到对应的桩ip和端口号
        String address = device.getClientAddress();
        String msgCode = msgNumberUtil.getNumber();
        String key = address + msgCode;
        SocketThread socketThread = (SocketThread) ThreadUtil.getThreadByName(address);
        if (null == socketThread) {
            jsonObject.put("result", "电桩未连接到服务器");
            return jsonObject;
        }
        SendThread sendThread = new SendThread(socketThread.getSocket());
        String times = fullZeroStr(String.valueOf(Integer.valueOf(time) * 60), 4);
        String money = fullZeroStr((much + "000"), 8);
//        String str = "01" + HexConvert.convertStringToHex(times) + HexConvert.convertStringToHex(money) + HexConvert.convertStringToHex(appouid);
        String str = "01" + times + money + appouid;
        String hexStr02 = "02" + msgCode + "000F " + str;
        hexStr02 = substrs(hexStr02);
        byte[] serverStr02 = CRC16.hexToByte(hexStr02.replace(" ", ""));
        String serverstrCrc02 = HexConvert.BinaryToHexString(CRC16.GetCRC(serverStr02));
        String hexserverStr02 = "68 " + HexConvert.BinaryToHexString(serverStr02) + serverstrCrc02;
        sendThread.setCmd(hexserverStr02);
        MQUtil.putMsg(key, "");
        sendThread.start();
        String msg = null;
        int currentWaitTimes = 0;
        int maxWaitTimes = 10000;
        //有回复才会或者超过等待时间会跳出
        while (null == msg || "".equals(msg)) {
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            msg = MQUtil.getMsg().get(key);
            currentWaitTimes += 10;
            //这里面写超时处理
            if (currentWaitTimes >= maxWaitTimes) {
                jsonObject.put("result", "联通超时");
                return jsonObject;
            }
        }
        MQUtil.removeMsg(key);//删掉无用KV
        String msgOrderId = msg.substring(0, 16);
        appointment.setAppoCode(msgOrderId);
        String msgResult = msg.substring(16, 18);
        //------------------------------------
        if ("00".equals(msgResult)) {
            //预约成功
            if (orderDao.addAppointment(appointment) > 0 && orderDao.changeDeviceUse(device.getDeviceId()) > 0) {
                if ("ye".equals(payType)) {
                    BigDecimal yeMoney = new BigDecimal(userDao.getMoney(uid)).setScale(2, BigDecimal.ROUND_HALF_UP);
                    BigDecimal nowMoney = yeMoney.subtract(new BigDecimal(much));
                    if (orderDao.updateMoney(nowMoney, uid) > 0) {
                        jsonObject.put("result", "success");
//                        jsonObject.put("money", yeMoney);
                        jsonObject.put("money", nowMoney);
                        jsonObject.put("appointment", JSONObject.fromObject(appointment));
                    } else {
                        jsonObject.put("result", "扣费失败");
                    }
                } else {
                    jsonObject.put("result", "success");
                    jsonObject.put("appointment", JSONObject.fromObject(appointment));
                }
            } else {
                jsonObject.put("result", "添加订单失败");
            }
        } else if ("01".equals(msgResult)) {
            jsonObject.put("result", "预约号错误");
        } else {
            jsonObject.put("result", "其他原因错误");
        }
        return jsonObject;
    }

    @Override
    public List<Appointment> findAppo(String number, String uuid) {
        int numbers = Integer.valueOf(number) * 10;
        System.out.println("数值" + number);
        return orderDao.findAppo(numbers, uuid);
    }

    @Override
    public List<Appointment> findAppoOne(String uuid) {
        List<Appointment> ls = orderDao.findAppoOne(uuid);
        for (Appointment l : ls) {
            String time = new SimpleDateFormat("HH:mm").format(new Date());
            Date date = l.getEndTime();
            Date date2 = new Date();
            if (date2.after(date)) {
                if (0 < orderDao.del(String.valueOf(l.getAppoUid()))) {
                    deviceDao.updateStatus(l.getDevice(),"0");
                    List<Appointment> lss = orderDao.findAppoOne(uuid);
                    //设置appo状态
                    return lss;
                }
            }
        }
        return ls;
    }

    @Override
    public String cleanAppo(String id) {
        if (orderDao.clean(id) > 0) {
            return "Ok";
        } else {
            return "error";
        }
    }

    @Override
    public String isAppo(String uuid) {
        if (orderDao.isAppo(uuid) > 0) {
            return "false";
        } else {
            return "true";
        }

    }

    @Override
    public boolean addOrder(Order order) {
        if (orderDao.addOrder(order) > 0) {
            return true;
        } else {
            return false;
        }
    }


    public static String substrs(String replace) {
        String regex = "(.{2})";
        replace = replace.replaceAll(regex, "$1 ");
        return replace;
    }


    //补完指定个数的零
    public static String fullZeroStr(String str, int zero) {
        String msg = "0000000000000000000" + str;
        return msg.substring(msg.length() - zero);
    }
}