package com.example.blxny.service.impl;

import com.example.blxny.dao.AttachmentDao;
import com.example.blxny.dao.UserDao;
import com.example.blxny.model.Attachment;
import com.example.blxny.service.AttachmentService;
import com.example.blxny.util.MQUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AttachmentServiceImpl implements AttachmentService {

    @Autowired
    private AttachmentDao attachmentDao;
    @Autowired
    private UserDao userDao;

    @Override
    public String addImg(int id, Attachment attachment) {
        String flg = userDao.hasId(id);
        if (flg.isEmpty()){
            return "用户不存在";
        }else{
            attachment.setUid(id);
            if (attachmentDao.addImg(attachment)){
                return "success";
            }else{
              /*  String asd=MQUtil.getMsg().get("/192.168.1.1:9010");
                */
                return "faile";
            }
        }

    }

}
