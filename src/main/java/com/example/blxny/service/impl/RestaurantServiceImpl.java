package com.example.blxny.service.impl;


import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.CharsetUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.crypto.SecureUtil;
import cn.hutool.crypto.symmetric.AES;
import com.alibaba.fastjson.JSONObject;
import com.example.blxny.dao.*;
import com.example.blxny.model.*;
import com.example.blxny.dao.RestaurantDao;
import com.example.blxny.service.RestaurantService;
import com.example.blxny.util.RedisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.*;

import java.text.SimpleDateFormat;

import static com.example.blxny.tool.DoubleUtil.round;

/*
 *项目名: blxny
 *文件名: RestaurantServiceImpl
 *创建者: SCH
 *创建时间:2019/5/28 14:27
 *描述: TODO
 */
@Service
public class RestaurantServiceImpl implements RestaurantService {

    private static byte[] KEY = {19, 28, 37, 64, 55, 64, 73, 82, 91, 100, 101, 110, 111, 127, 127, 127};
    @Autowired
    private RestaurantDao restaurantDao;
    @Autowired
    private CateringProductDao cateringProductDao;
    @Autowired
    private UserDao userDao;
    @Autowired
    private ScoreDiscountDao scoreDiscountDao;
    @Autowired
    private RedisUtil redisUtil;

    @Override
    public Object getList(int type, int popular, int lastId, int pageSize) {
        List<Restaurant> list = null;
        if (popular == 0) {
            list = restaurantDao.getList(type, lastId, pageSize);
        } else {
            list = restaurantDao.getPopular(type, lastId, pageSize);
        }
        com.alibaba.fastjson.JSONObject jsonObject = new JSONObject();
        jsonObject.put("list", JSONObject.toJSON(list));
        return jsonObject;
    }

    @Override
    public List<Hotel> getHotel(int id) {
        List<Hotel> list = restaurantDao.findHotelInfo(id);
        return list;
    }

    @Override
    public Object getShop(int id, int size, int number) {
        number = (number - 1) * size;
        JSONObject js = new JSONObject();
        List<Shop> newGoods = restaurantDao.findShopInfo(id, 5, 0, 0);
        if (newGoods == null) {
            newGoods.add(new Shop());
        }
        js.put("newGoods", newGoods);
        List<Shop> goods = restaurantDao.findShopInfo(id, size, number + 5, 0);
        if (goods == null) {
            goods.add(new Shop());
        }
        js.put("goods", goods);
        return js;
    }

    @Override
    public Object getCateringProducts(int restaurantId, int lastId, int type, int ps) {
        //type: 0:默认, 1:热门推荐, 2:新品推荐, 3:小众新品
        //from: 1:咖啡厅, 2:餐厅
        JSONObject jsonObject = new JSONObject();
        List<CateringProduct> list = new ArrayList<>();
        list.add(new CateringProduct());
        jsonObject.put("list", list);
        if (type == 1 || type == 2 || type == 3) {
            list = cateringProductDao.getList(restaurantId, 0, type, ps);
        } else {
            list = cateringProductDao.getList(restaurantId, lastId, type, ps);
        }
        if (list.size() > 0) {
            jsonObject.put("list", JSONObject.toJSON(list));
        }
        return jsonObject;
    }


//    public Object getCoffeeProducts(int from, int type) {
//        //type: 0:默认, 1:热门推荐, 2:新品推荐, 3:小众新品
//        //from: 1:咖啡厅, 2:餐厅
//        JSONObject jsonObject = new JSONObject();
//        List<CateringProduct> list = new ArrayList<CateringProduct>();
//        list.add(new CateringProduct());
//        jsonObject.put("热门推荐", list);
//        jsonObject.put("新品推荐", list);
//        jsonObject.put("小众新品", list);
//        List<CateringProduct> list1 = cateringProductDao.getCateringProductByType(from, 1);
//        if (list1.size() > 0) {
//            jsonObject.put("热门推荐", JSONObject.toJSON(list1));
//        }
//        List<CateringProduct> list2 = cateringProductDao.getCateringProductByType(from, 2);
//        if (list2.size() > 0) {
//            jsonObject.put("新品推荐", JSONObject.toJSON(list2));
//        }
//        List<CateringProduct> list3 = cateringProductDao.getCateringProductByType(from, 3);
//        if (list3.size() > 0) {
//            jsonObject.put("小众新品", JSONObject.toJSON(list3));
//        }
//        return jsonObject;
//    }

    @Override
    public List<Shop> getShopByType(int type, int id, int size, int page) {
        page = (page - 1) * size;
        List<Shop> goods = restaurantDao.findShopInfo(id, size, page, type);
        return goods;
    }

    @Override
    public Object getMessage(String uuid, String type) {
        //二维码信息加密
        //加密中含有：用户uuid（user）；领取时间（date）；哪一个服务领取的（type）；现在可兑换的金额(money)；
        //二维码所使用的积分(sc),二维码id（id）
        net.sf.json.JSONObject js = new net.sf.json.JSONObject();//二维码内容
        AES aes = SecureUtil.aes(KEY);
        String date = new SimpleDateFormat("yyyyMMdd").format(new Date());//获取现在时间
        net.sf.json.JSONObject jsonObject = new net.sf.json.JSONObject();//返出内容
        int a = userDao.getScoreByUid(uuid);
        String b = scoreDiscountDao.getAmount(uuid);
        int bb = 0;
        if (b != null) {
            bb = Integer.valueOf(b);
        }
        int score = a - bb;//获取现在可以兑换的积分
        if (score < 200) {//200积分起用
            score = 0;
        }
        double i = Double.valueOf(redisUtil.get("theInterestRateOfqrCode"));//获取现在的利率
        double money = round(score * i, 4);//积分转换为金额
        js.put("user", uuid);//二维码拥有者
        js.put("type", type);//商家类型
        js.put("date", date);//二维码生成时间
        js.put("money", money);//二维码价值
        js.put("sc", score);//二维码所使用的积分
        String uid=IdUtil.simpleUUID();
        js.put("id", uid);//二维码id
        jsonObject.put("money", money);
        jsonObject.put("uid",uid);
        String encryptHex = aes.encryptHex(js.toString());//二维码加密
        jsonObject.put("qrCode", encryptHex);
        return jsonObject;
    }

    @Override
    public String getQRCode(String uuid, String code) {
        AES aes = SecureUtil.aes(KEY);
        String decryptStr = aes.decryptStr(code, CharsetUtil.CHARSET_UTF_8);
        net.sf.json.JSONObject js = net.sf.json.JSONObject.fromObject(decryptStr);
        //看这个二维码能不能用
        String id = (String) js.get("id");
        String time = (String) js.get("date");
        String date = new SimpleDateFormat("yyyyMMdd").format(new Date());//获取现在时间
        js.put("message", "OK");
        if (!scoreDiscountDao.getByScoreId(id).equals("0") || !time.equals(date)) {
            js.put("message", "二维码已失效");
        }
        //看现在的积分和二维码上的能不能对应
        double price = Double.valueOf(Convert.toStr(js.get("sc")));
        String uid = (String) js.get("user");
        String b = scoreDiscountDao.getAmount(uid);
        int bb = 0;
        if (b != null) {
            bb = Integer.valueOf(b);
        }
        int score = userDao.getScoreByUid(uid) - bb;//获取现在可以兑换的积分
        if (price != score) {
            js.put("message", "金额变化,请刷新二维码");
        }
        //现在返回数据
        return js.toString();
    }
    
    @Override
    public boolean endQRCode(String code, String uid) {
        ScoreDiscount sc = new ScoreDiscount();
        AES aes = SecureUtil.aes(KEY);
        String decryptStr = aes.decryptStr(code, CharsetUtil.CHARSET_UTF_8);
        net.sf.json.JSONObject js = net.sf.json.JSONObject.fromObject(decryptStr);
        String id = (String) js.get("id");
        if (scoreDiscountDao.getByScoreId(id).equals("0")) {
            String uuid = (String) js.get("user");
            int score = (int) js.get("sc");//获取现在可以兑换的积分
            sc.setAmount(score);
            sc.setFrom(Integer.valueOf((String) js.get("type")));
            sc.setProvider(uid);
            sc.setUid(uuid);
            sc.setPrice(BigDecimal.valueOf(Double.valueOf((String) js.get("money"))));
            sc.setScoreId((String) js.get("id"));
            if (0 < scoreDiscountDao.newTable(sc)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean checkUse(int id) {
        if (scoreDiscountDao.getCountById(id) > 0) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String evaluat(int id, int score) {
        if (scoreDiscountDao.updateScore(id, score) > 0) {
            String star = scoreDiscountDao.getSocreAvg(id);
            restaurantDao.updateStar(id, new BigDecimal(star));
            return "评分成功";
        } else {
            return "评分失败";
        }
    }
}
