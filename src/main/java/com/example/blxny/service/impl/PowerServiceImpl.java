package com.example.blxny.service.impl;

import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;
import com.example.blxny.dao.OrderDao;
import com.example.blxny.dao.PowerDao;
import com.example.blxny.model.*;
import com.example.blxny.service.PowerService;
import com.example.blxny.tool.DistanceTool;
import com.example.blxny.tool.DoubleUtil;
import com.example.blxny.tool.StationCollectedJudgeTool;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class PowerServiceImpl implements PowerService {
    @Autowired
    private PowerDao dao;
    @Autowired
    private OrderDao orderDao;


    @Override
    public Object findGPS(String id) {
        Station station=dao.findGPS(id);
        JSONObject js=new JSONObject();
        if(station!=null){
            js.put("lng",station.getLng());
            js.put("lat",station.getLat());
        }else {
            js.put("lng","0");
            js.put("lat","0");
        }
        return js;
    }

    @Override
    public Object powerWorking(String type, String usetime, String usepower, String usemuch, String uuid) {
        JSONObject retunmassage = new JSONObject();
        return null;
    }

    @Override
    public Object findOtherPower(String uuid, String code, String lat, String lng) {
        double distance = 0;
        double far = 99999999;
        List<IndexSort> reobject = new ArrayList<IndexSort>();
       // List reobject2 = new ArrayList();
        String[] codes = code.split("");
        String code1 = null;
        String code2 = null;
        for (int i = 0; i < codes.length; i++) {
            code1 = codes[0];
            code2 = codes[1];
        }
        List chouse = dao.findType(code2);
        //System.out.println(chouse);
        //查找有快|慢充电的驿站id
        Object choseS = null;
        List<Station> station = new ArrayList<>();
        for (Object s : chouse) {
            Station chose = dao.findStation(code1, String.valueOf(s));
            Station stations = new Station();
            if (chose != null) {
                stations = chose;
                station.add(stations);
            }
        }
        //System.out.println(station.size() != 0);getPowerNum
        if (station.size() != 0) {
            if (station.get(0) != null) {
                //System.out.println("得到匹配的驿站信息" + station);
                //System.out.println("长度"+f);
                for (Station sta : station) {
                    String lats = sta.getLat();
                    String lngs = sta.getLng();
                    DistanceTool distanceByLongNLat = new DistanceTool();
                    double dou = distanceByLongNLat.distanceByLongNLat(lat, lng, lngs, lats);
                    if (dou == distance) {
                        dou += 1;
                    } else if (dou < far) {
                        far = dou;
                        choseS = sta;
                    }
                    distance = dou;
                    //计算自己与驿站距离
                   // System.out.println("本次长度"+f);
                    if (distance <= 10000) {
                       // System.out.println("是否进入"+f);
                        //System.out.println(sta);
                        String returnlat = sta.getLat();
                        String retyrnlng = sta.getLng();
                        //获取当前时间点
                        DistanceTool tools = new DistanceTool();
                        String timeOB = (String) tools.getDatabaseTimeForNow().get("time");
                        String nowTime = "%" + timeOB + "%";
                        //查找到此驿站快/慢充电电费信息表//获取当前时间点的电费信息表
                        //System.out.println(sta.getStationId()+nowTime+code2);
                        Rule cha = dao.changeMoney(sta.getStationId(), nowTime, code2);
                        //获取当前电费
                        double power;
                        // System.out.println(cha);
                        if (timeOB.equals("1")) {
                            power = cha.getCuspPrice();
                        } else if (timeOB.equals("2")) {
                            power = cha.getHighPrice();
                        } else if (timeOB.equals("3")) {
                            power = cha.getNormalPrice();
                        } else {
                            power = cha.getValleyPrice();
                        }
//                        JSONObject object = new JSONObject();
////                        object.put("far", (int) distance);
////                        object.put("id", sta.getStationId());
////                        object.put("name", sta.getName());
////                        object.put("null_device", dao.countNull(sta.getStationId()));
////                        object.put("powerpay", String.valueOf(power));//计费表获取电费
////                        object.put("servicepay", String.valueOf(cha.getServicePrice()));//计费表获取服务费
////                        object.put("address", sta.getAddressInfo());
////                        object.put("timetool", sta.getWorkTime());
////                        object.put("range", sta.getRange());
////                        object.put("lat", returnlat);
////                        object.put("lng", retyrnlng);
////                        object.put("type", sta.getType());
                        IndexSort object = new IndexSort();
                        object.setFar((int) distance);
                        object.setId( sta.getStationId());
                        object.setName( sta.getName());
                        object.setNull_device( dao.countNull(sta.getStationId()));
                        object.setPowerpay( String.valueOf(power));//计费表获取电费
                        object.setServicepay( String.valueOf(cha.getServicePrice()));//计费表获取服务费
                        object.setAddress( sta.getAddressInfo());
                        object.setTimetool( sta.getWorkTime());
                        object.setRange( sta.getRange());
                        object.setLat( returnlat);
                        object.setLng( retyrnlng);
                        object.setType( sta.getType());
                        if (!uuid.equals("null")) {
                            StationCollectedJudgeTool tool = new StationCollectedJudgeTool();
                            object.setIsCollected(String.valueOf(tool.CollectedJudge(uuid, sta.getStationId())));
                        } else {
                            object.setIsCollected( "error");
                        }
                        reobject.add(object);
                    }
                }
                /* 集合排序*/
                Collections.sort(reobject, new Comparator<IndexSort>() {
                    @Override
                    public int compare(IndexSort o1, IndexSort o2) {
                        int i = o1.getFar() - o2.getFar();
                        return i;
                    }
                });
              /*  for(IndexSort obj:reobject){
                        JSONObject object = new JSONObject();
                        object.put("far", obj.getFar());
                        object.put("id",obj.getId());
                        object.put("name",obj.getName());
                        object.put("null_device",obj.getNull_device());
                        object.put("powerpay", obj.getPowerpay());//计费表获取电费
                        object.put("servicepay",obj.getServicepay());//计费表获取服务费
                        object.put("address",obj.getAddress());
                        object.put("timetool",obj.getTimetool());
                        object.put("range",obj.getRange());
                        object.put("lat",obj.getLat());
                        object.put("lng", obj.getLng());
                        object.put("type",obj.getType());
                        object.put( "isCollected",obj.getIsCollected());
                         reobject2.add(object);
                }*/

/*                List um = new ArrayList();
                for (Object obj : reobject) {
                    JSONObject objects = JSONObject.fromObject(obj);
                    um.add(Integer.valueOf(objects.getString("far")));
                    Collections.sort(um);
                }

                for (Object i : um) {
                    for (Object obj : reobject) {
                        JSONObject objects = JSONObject.fromObject(obj);
                        if (i.equals(Integer.valueOf(objects.getString("far")))) {
                            reobject2.add(objects);
                        }
                    }
                }*/
            }
        }
        return reobject;
    }

    @Override//注释为旧版本
    public Object getAllPower(String uuid, String lat, String lng, String city) {
        System.out.println(lat + "/n" + lng);
        double distance = 0;
        //List reobject = new ArrayList();
        List<IndexSort> reobject = new ArrayList<IndexSort>();
        //List reobject2 = new ArrayList();
        List<Station> station = new ArrayList<>();
        station = dao.findAllStation(city + "%");
        for (Station sta : station) {
            String lats = sta.getLat();
            String lngs = sta.getLng();
            DistanceTool distanceByLongNLat = new DistanceTool();
            double dou = distanceByLongNLat.distanceByLongNLat(lat, lng, lngs, lats);
            if (dou == distance) {
                dou += 1;
            }
            distance = dou;
            //计算自己与驿站距离
            if (distance <= 10000) {
                String returnlat = sta.getLat();
                String retyrnlng = sta.getLng();
//                JSONObject object = new JSONObject();
//                object.put("far", (int) distance);
//                object.put("id", sta.getStationId());
//                object.put("name", sta.getName());
//                object.put("null_device", dao.countNull(sta.getStationId()));
//                object.put("powerpay", "snull");//计费表获取电费
//                object.put("servicepay", "snull");//服务费
//                object.put("address", sta.getAddressInfo());
//                object.put("timetool", sta.getWorkTime());
//                object.put("range", sta.getRange());
//                object.put("lat", returnlat);
//                object.put("lng", retyrnlng);
//                object.put("type", sta.getType());
                IndexSort object = new IndexSort();
                object.setFar((int) distance);
                object.setId( sta.getStationId());
                object.setName( sta.getName());
                object.setNull_device( dao.countNull(sta.getStationId()));
                object.setPowerpay( "snull");//计费表获取电费
                object.setServicepay( "snull");//计费表获取服务费
                object.setAddress( sta.getAddressInfo());
                object.setTimetool( sta.getWorkTime());
                object.setRange( sta.getRange());
                object.setLat( returnlat);
                object.setLng( retyrnlng);
                object.setType( sta.getType());
                String chose = dao.findStationOne(uuid);
                boolean returnMassage = true;
                String[] arr;
                if (chose == null) {
                    returnMassage = false;
                } else {
                    arr = chose.split(",");
                    for (int i = 0; i < arr.length; i++) {
                        if (!sta.getStationId().equals(arr[i])) {
                            returnMassage = false;
                        } else {
                            returnMassage = true;
                            //object.put("isCollected", returnMassage);
                            object.setIsCollected(String.valueOf(returnMassage));
                        }
                    }
                }
                reobject.add(object);
            }
        }
        /* 集合排序*/
        Collections.sort(reobject, new Comparator<IndexSort>() {
            @Override
            public int compare(IndexSort o1, IndexSort o2) {
                int i = o1.getFar() - o2.getFar();
                return i;
            }
        });
        /*for(IndexSort obj:reobject){//此为结合旧版本所需，如前端获取能改可直接返回reobject
            JSONObject object = new JSONObject();
            object.put("far", obj.getFar());
            object.put("id",obj.getId());
            object.put("name",obj.getName());
            object.put("null_device",obj.getNull_device());
            object.put("powerpay", obj.getPowerpay());//计费表获取电费
            object.put("servicepay",obj.getServicepay());//计费表获取服务费
            object.put("address",obj.getAddress());
            object.put("timetool",obj.getTimetool());
            object.put("range",obj.getRange());
            object.put("lat",obj.getLat());
            object.put("lng", obj.getLng());
            object.put("type",obj.getType());
            object.put( "isCollected",obj.getIsCollected());
            reobject2.add(object);
        }*/
/*        List um = new ArrayList();
        for (Object obj : reobject) {
            JSONObject objects = JSONObject.fromObject(obj);
            um.add(Integer.valueOf(objects.getString("far")));
            Collections.sort(um);
        }
        for (Object i : um) {
            JSONObject obje = JSONObject.fromObject(i);
            for (Object obj : reobject) {
                JSONObject objects = JSONObject.fromObject(obj);
                if (i.equals(Integer.valueOf(objects.getString("far")))) {
                    reobject2.add(objects);
                    break;
                }
            }
        }*/
        return reobject;
    }

    @Override
    public List findlowpower(String id) {
        List object = new ArrayList();
        for (Device obj : dao.findlowpower(id)) {
            object.add(obj.getDeviceId());
        }
        return object;
    }

    @Override
    public List findfastpower(String id) {
        List object = new ArrayList();
        for (Device obj : dao.findfastpower(id)) {
            object.add(obj.getDeviceId());
        }
        return object;
    }

    @Override
    public Object findPowerPay(String id) {
        //获取当前时间点
        DistanceTool tools = new DistanceTool();
        String timeOB = (String) tools.getDatabaseTimeForNow().get("time");
        String nowTime = "%" + timeOB + "%";
        //查找到此驿站快/慢充电电费信息表//获取当前时间点的电费信息表
        Rule low = dao.changeMoney(id, nowTime, "0");//慢充信息
        Rule fast = dao.changeMoney(id, nowTime, "1");//快充信息
        //获取当前电费
        double lowpower;
        double fastpower;
        if (timeOB.equals("1")) {
            lowpower = low.getCuspPrice();
            fastpower = fast.getCuspPrice();
        } else if (timeOB.equals("2")) {
            lowpower = low.getHighPrice();
            fastpower = fast.getHighPrice();
        } else if (timeOB.equals("3")) {
            lowpower = low.getNormalPrice();
            fastpower = fast.getNormalPrice();
        } else {
            lowpower = low.getValleyPrice();
            fastpower = fast.getValleyPrice();
        }
        /*之后移动充信息所留地*/
        JSONObject obj = new JSONObject();
        obj.put("time", tools.getDatabaseTimeForNow().get("timeInfo"));
        obj.put("lowpay", lowpower);
        obj.put("lowservice", low.getServicePrice());
        obj.put("fastpay", fastpower);
        obj.put("fastservice", fast.getServicePrice());
        //System.out.println("通过驿站id找到" + obj);
        return obj;
    }

    @Override
    public Object findPay(String type, String station) {
        Rule charging = dao.findPay(type, station);
        //System.out.println(charging);
        JSONObject obj = new JSONObject();
        List noon = new ArrayList();
        List morning = new ArrayList();
        List affternoon = new ArrayList();
        List night = new ArrayList();
        morning.add(charging.getCuspPrice());
        morning.add(charging.getServicePrice());

        noon.add(charging.getHighPrice());
        noon.add(charging.getServicePrice());

        affternoon.add(charging.getNormalPrice());
        affternoon.add(charging.getServicePrice());

        night.add(charging.getValleyPrice());
        night.add(charging.getServicePrice());
        obj.put("morning", morning);
        obj.put("noon", noon);
        obj.put("affternoon", affternoon);
        obj.put("night", night);
        return obj;
    }

    @Override
    public Object endPower(String orderid) {
        Order order = orderDao.getOrderById(orderid);
        Device device = orderDao.findDeviceById(order.getDeviceId());
        DistanceTool tool = new DistanceTool();
        Charging charging = orderDao.findPowerPay(device.getStationId(), (String) tool.getDatabaseTimeForNow().get("time"), String.valueOf(device.getChargeType()));
        JSONObject returnMassage = new JSONObject();
        //先确认是否有超时费用
        double overPay = 0;
        Date date = new Date();
        if (DateUtil.between(date, order.getEndTime(), DateUnit.MINUTE) > 15) {
            overPay = DoubleUtil.mul(Double.valueOf(DateUtil.between(date, order.getEndTime(), DateUnit.MINUTE)), 0.3);//超时费用
        }
        //电费单价=电费单价+服务费单价
        double powerpay = DoubleUtil.add(Double.valueOf(charging.getPowerNum()), Double.valueOf(charging.getServiceNum()));
        returnMassage.put("useTime", new SimpleDateFormat("yyyy-MM-dd").format(order.getStartTime()));//使用时间
        returnMassage.put("usePower", DoubleUtil.round(order.getChargeAmount(), 2));
        double sale = 0;//优惠度数
        returnMassage.put("salePower", sale);
        returnMassage.put("salePay", DoubleUtil.mul(sale, powerpay));
        returnMassage.put("powerMuch", powerpay);
        returnMassage.put("overPay", overPay);
        double allpay = DoubleUtil.round(DoubleUtil.sub(DoubleUtil.add(DoubleUtil.mul(Double.valueOf(order.getChargeAmount()), powerpay), overPay), DoubleUtil.mul(sale, powerpay)), 2);
        returnMassage.put("allPay", allpay);//总费用=电量（usePower）*电单价（powerpay）+延时费用（overPay）-优惠（salePay）
        dao.setAllPay(String.valueOf(allpay), orderid);
        //测试是否拔枪，如没拔把returnMassage赋值为null；
        return returnMassage;
    }

    @Override
    public Object getPowerNum(String id, String num) {
        //找到桩是否被用了
        String V = "empty";
        String A = "empty";
        String W = "empty";
        JSONObject js = new JSONObject();
        Device device = dao.finddevice(id);
        if (device == null) {
            Device d = new Device();
            js.put("device", "");
            js.put("power", "");
            js.put("V", V);
            js.put("A", A);
            js.put("W", W);
            js.put("type", "");
            return js;
        } else {
            if (device.getState().equals("0")) {
                V = device.getVoltage();
                A = device.getElectric();
                W = device.getPower();
            }
            js.put("device", device.getDeviceId());
            js.put("power", device.getChargeType());
            js.put("V", V);
            js.put("A", A);
            js.put("W", W);
            js.put("type", device.getState());
            return js;
        }
    }
}

