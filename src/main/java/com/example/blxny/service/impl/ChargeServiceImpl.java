package com.example.blxny.service.impl;

import cn.hutool.core.date.DateField;
import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.core.util.StrUtil;
import com.example.blxny.dao.*;
import com.example.blxny.model.*;
import com.example.blxny.service.ChargeService;
import com.example.blxny.util.*;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public class ChargeServiceImpl implements ChargeService {

    @Autowired
    private DeviceDao deviceDao;
    @Autowired
    private StationDao stationDao;
    @Autowired
    private OrderDao orderDao;
    @Autowired
    private RuleDao ruleDao;
    @Autowired
    private AlarmDao alarmDao;
    @Autowired
    private MsgNumberUtil msgNumberUtil;
    @Autowired
    private VersionDao versionDao;
    @Autowired
    private UserDao userDao;
    @Autowired
    private RedisUtil redisUtil;

    @Override
    public String sweetCode(String deviceId, int gunNo) {
        Device device = deviceDao.getById(deviceId);
        Station station = stationDao.getInfoById(device.getStationId());
        String ipAddress = station.getIp();
        int port = device.getPort();
        String msgCode = msgNumberUtil.getNumber();
        String key = "/" + ipAddress + ":" + port + msgCode;
        SendThread sendThread = null;
        try {
            sendThread = new SendThread(new Socket(ipAddress, port));
        } catch (IOException e) {
            e.printStackTrace();
        }
        //拼接并格式化server发送的消息
        String linkAscllStr = stringToAscii("192.168.1.10:8888/test");
        String length = Integer.toHexString(linkAscllStr.length());
        String msgContent = "0" + gunNo + linkAscllStr;
        if (length.length() == 1) {
            msgContent = "0" + length;
        } else {
            msgContent += length;
        }
        msgContent += linkAscllStr;
        String hexStr02 = " 1C " + msgCode + "00 12 " + substrs(msgContent);
        byte[] serverStr02 = CRC16.hexToByte(hexStr02.replace(" ", ""));
        String serverstrCrc02 = HexConvert.BinaryToHexString(CRC16.GetCRC(serverStr02));
        String hexserverStr02 = "68 " + HexConvert.BinaryToHexString(serverStr02) + serverstrCrc02;
        //给send线程设置 发送消息
        sendThread.setCmd(hexserverStr02);
        MQUtil.putMsg(key, "");
        sendThread.start();
        String msg = null;
        int currentWaitTimes = 0;
        int maxWaitTimes = 2000;
        //有回复才会或者超过等待时间会跳出
        while (null == msg || "".equals(msg)) {
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            msg = MQUtil.getMsg().get(key);
            currentWaitTimes += 10;
            //这里面写超时处理
            if (currentWaitTimes >= maxWaitTimes) {
                System.out.println("超时处理");
                return "联通超时";
            }
        }
        MQUtil.removeMsg(key);//删掉无用KV
        //电桩回复处理
        String msgGunNo = msg.substring(0, 2);
        if (gunNo == Integer.parseInt(msgGunNo)) {
            //业务处理,返回电桩信息
            return "扫码成功";
        } else {
            return "二维码内容错误";
        }
    }

    @Override
    public Object startCharge(String deviceId, String gunNo, String chargeType, int chargeData, String uid) {
        String orderId = deviceId.substring(4, 10) + new SimpleDateFormat("yyMMdd").format(new Date());
        JSONObject jsonObject = new JSONObject();
        Order order = new Order();
        jsonObject.put("order", order);
        jsonObject.put("result", "000000");
        jsonObject.put("deviceType", deviceDao.getChargeType(deviceId) + "");
        String lastOrderId = orderDao.getLastOrder(orderId);//获取当天最新的订单号
        if (null != orderDao.getOrderByStatus(uid, 1)) {
            jsonObject.put("result", "有未支付订单");
        }
        if (null != lastOrderId) {
            String No = lastOrderId.substring(12, 16);
            if ("9999".equals(No)) {
                jsonObject.put("result", "已达订单上限");
                return jsonObject;
            } else {
                String no = ("0000" + (Integer.parseInt(No) + 1));
                orderId += no.substring(no.length() - 4);
            }
        } else {
            orderId += "0001";
        }
        Device device = deviceDao.getById(deviceId);
        if (null == device) {
            jsonObject.put("result", "设备不存在");
            return jsonObject;
        }
        JSONObject deviceJSON = new JSONObject();
        deviceJSON.put("powerRating", device.getPowerRating());
        deviceJSON.put("currentRated", device.getCurrentRated());
        deviceJSON.put("ratedVoltage", device.getRatedVoltage());
        jsonObject.put("device", deviceJSON);
        //获取 socketThread 线程
        String msgCode = msgNumberUtil.getNumber();
        String address = device.getClientAddress();
        String key = address + msgCode;
        SocketThread socketThread = (SocketThread) ThreadUtil.getThreadByName(address);
        if (null == socketThread) {
            jsonObject.put("result", "设备未连接服务器");
            return jsonObject;
        }
        SendThread sendThread = new SendThread(socketThread.getSocket());
        //充电数据处理
        String chargeDataStr = "";
        switch (chargeType) {
            case "01":
                chargeDataStr = "00000000";
                break;
            case "02":
            case "04":
                chargeDataStr = fullZeroStr(chargeData * 1000 + "", 8);
                break;
            case "03":
                chargeDataStr = fullZeroStr(chargeData + "", 8);
                break;
        }
        int ranNum = RandomUtil.randomInt(0, 9999); //停止码处理
        String stopCode = fullZeroStr(ranNum + "", 4);//生成停止码
        //拼接并格式化 server 发送的消息
        String msgContent = gunNo + chargeType + chargeDataStr + stopCode.replace(" ", "") + orderId;
        String hexStr01 = "06 " + substrs(msgCode) + "00 10 " + substrs(msgContent);
        byte[] serverStr01 = CRC16.hexToByte(hexStr01.replace(" ", ""));
        String serverstrCrc01 = HexConvert.BinaryToHexString(CRC16.GetCRC(serverStr01));
        String hexserverStr01 = "68 " + HexConvert.BinaryToHexString(serverStr01) + serverstrCrc01;

        sendThread.setCmd(hexserverStr01);
        MQUtil.putMsg(key, "");
        try {
            sendThread.start();
        } catch (Exception e){
            e.printStackTrace();
        }
        String msg = null;
        int currentWaitTimes = 0;
        int maxWaitTimes = 10000;
        //有回复才会或者超过等待时间会跳出
        while (StrUtil.isBlank(msg)) {
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            msg = MQUtil.getMsg().get(key);
            currentWaitTimes += 10;
            //这里面写超时处理
            if (currentWaitTimes >= maxWaitTimes) {
                String msgContent1 = gunNo + orderId;
                String hexStr02 = "07 " + substrs(msgCode) + "00 09 " + substrs(msgContent1);
                byte[] serverStr02 = CRC16.hexToByte(hexStr02.replace(" ", ""));
                String serverstrCrc02 = HexConvert.BinaryToHexString(CRC16.GetCRC(serverStr02));
                String hexserverStr02 = "68 " + HexConvert.BinaryToHexString(serverStr02) + serverstrCrc02;
                sendThread.setCmd(hexserverStr02);
                try {
                    sendThread.start();     //给send线程设置 发送消息
                }catch (Exception ex){
                    ex.printStackTrace();
                }
                jsonObject.put("result", "通信超时");
                return jsonObject;
            }
        }
        MQUtil.removeMsg(key);//删掉无用KV
        String msgOrderId = msg.substring(0, 16);
        String msgResult = msg.substring(16, 18);
        if ("00".equals(msgResult)) {
            order.setOrderId(msgOrderId);
            order.setDeviceId(deviceId);
            order.setStartTime(new Date());
            order.setGunNo(Integer.parseInt(gunNo));
            order.setStopCode(stopCode);
            order.setCreated(new Date());
            order.setStartTime(new Date());
            order.setUid(uid);
            order.setOverPay(new BigDecimal(0));
            deviceDao.updateStatus(deviceId, "1");
            orderDao.addOrder(order);//隐患
            jsonObject.put("order", com.alibaba.fastjson.JSONObject.toJSONString(order));
            jsonObject.put("result", "success");
        } else if ("01".equals(msgResult)) {
            jsonObject.put("result", "枪被预约");
        } else {
            jsonObject.put("result", "其他原因失败");
        }
        return jsonObject;
    }

    //    @Override
//    public int upChargeData(String resContent) {
//        Order order = new Order();
//        System.out.println("reqContent:\t" + resContent.length());
//        order.setGunNo(Integer.parseInt(resContent.substring(0, 2)));
//        order.setOrderId(resContent.substring(2, 18));
//        order.setChargeType(Integer.parseInt(resContent.substring(18, 20)));
//        order.setCardId(resContent.substring(20, 36));//BCD 转 10进制4位-------------------------------------
//        order.setVin(resContent.substring(36, 70));
//        order.setSoc(Integer.parseInt(resContent.substring(70, 72), 16));
////        order.setEndReason(Integer.parseInt(resContent.substring(72, 74)));
//        order.setStartTime(stringToDate(resContent.substring(72, 84)));//reqContent.substring(74,86)
//        order.setEndTime(stringToDate(resContent.substring(84, 96)));//reqContent.substring(86,98)
////        order.setStartMeter(stringToFloat(resContent.substring(96, 104)));
////        order.setEndMeter(stringToFloat(resContent.substring(104, 112)));
//        order.setChargeAmount(stringToFloat(resContent.substring(96, 104)));
//        order.setCuspElectricity(stringToFloat(resContent.substring(104, 112)));
//        order.setHighElectricity(stringToFloat(resContent.substring(112, 120)));
//        order.setNormalElectricity(stringToFloat(resContent.substring(120, 128)));
//        order.setValleyElectricity(stringToFloat(resContent.substring(128, 136)));
//        order.setTotalAmount(BigDecimal.valueOf(Double.parseDouble(resContent.substring(136, 144))/1000));
//        order.setCuspAmount(BigDecimal.valueOf(Double.parseDouble(resContent.substring(144, 152))/1000));
//        order.setHighAmount(BigDecimal.valueOf(Double.parseDouble(resContent.substring(152, 160))/1000));
//        order.setNormalAmount(BigDecimal.valueOf(Double.parseDouble(resContent.substring(160, 168))/1000));
//        order.setValleyAmount(BigDecimal.valueOf(Double.parseDouble(resContent.substring(168, 176))/1000));
//        order.setBookingAmount(BigDecimal.valueOf(Double.parseDouble(resContent.substring(176, 184))/1000));
//        order.setServiceAmount(BigDecimal.valueOf(Double.parseDouble(resContent.substring(184, 192))/1000));
//        order.setPickAmount(BigDecimal.valueOf(Double.parseDouble(resContent.substring(190,200))/1000));
//        return orderDao.update(order);
//    }
    @Override
    public void upChargeData(String resContent) {
        Order order = new Order();
        order.setGunNo(Integer.parseInt(resContent.substring(0, 2)));
        order.setOrderId(resContent.substring(2, 18));
        order.setChargeType(Integer.parseInt(resContent.substring(18, 20)));
        order.setCardId(resContent.substring(20, 36));//BCD 转 10进制4位-------------------------------------
        order.setVin(resContent.substring(36, 70));
        order.setSoc(Integer.parseInt(resContent.substring(70, 72), 16));
        order.setStartTime(stringToDate(resContent.substring(72, 84)));//reqContent.substring(74,86)
        order.setEndTime(stringToDate(resContent.substring(84, 96)));//reqContent.substring(86,98)
        order.setChargeAmount(stringToFloat(Integer.parseInt(resContent.substring(96, 104), 16) + ""));
        order.setCuspElectricity(stringToFloat(Integer.parseInt(resContent.substring(104, 112), 16) + ""));
        order.setHighElectricity(stringToFloat(Integer.parseInt(resContent.substring(112, 120), 16) + ""));
        order.setNormalElectricity(stringToFloat(Integer.parseInt(resContent.substring(120, 128), 16) + ""));
        order.setValleyElectricity(stringToFloat(Integer.parseInt(resContent.substring(128, 136), 16) + ""));
        order.setTotalAmount(BigDecimal.valueOf(Double.parseDouble(Integer.parseInt(resContent.substring(136, 144), 16) + "") / 1000));
        order.setCuspAmount(BigDecimal.valueOf(Double.parseDouble(Integer.parseInt(resContent.substring(144, 152), 16) + "") / 1000));
        order.setHighAmount(BigDecimal.valueOf(Double.parseDouble(Integer.parseInt(resContent.substring(152, 160), 16) + "") / 1000));
        order.setNormalAmount(BigDecimal.valueOf(Double.parseDouble(Integer.parseInt(resContent.substring(160, 168), 16) + "") / 1000));
        order.setValleyAmount(BigDecimal.valueOf(Double.parseDouble(Integer.parseInt(resContent.substring(168, 176), 16) + "") / 1000));
        order.setBookingAmount(BigDecimal.valueOf(Double.parseDouble(Integer.parseInt(resContent.substring(176, 184), 16) + "") / 1000));
        order.setServiceAmount(BigDecimal.valueOf(Double.parseDouble(Integer.parseInt(resContent.substring(184, 192), 16) + "") / 1000));
        order.setPickAmount(BigDecimal.valueOf(Double.parseDouble(Integer.parseInt(resContent.substring(192, 200), 16) + "") / 1000));
        order.setDeviceId(orderDao.getDeviceIdByOrderId(order.getOrderId()));
        com.alibaba.fastjson.JSONObject jsonObject = (com.alibaba.fastjson.JSONObject) com.alibaba.fastjson.JSONObject.toJSON(order);
        String deviceId = orderDao.getDeviceIdByOrderId(order.getOrderId());
        jsonObject.put("deviceType", deviceDao.getChargeType(deviceId));
        Device device = deviceDao.getById(order.getDeviceId());
        double price;
        Rule rule;
        if (null == device) {
            price = 1;
        } else {
            rule = ruleDao.getRule(stationDao.getInfoById(device.getStationId()).getStationId(), order.getChargeType() + "");
            String formatTime = DateUtil.formatTime(new Date());
            int hhmm = Integer.parseInt(formatTime.replace("", ":").substring(0, 4));
            String start[] = rule.getStartTime().split(",");
            String end[] = rule.getEndTime().split(",");
            String type[] = rule.getTimeType().split(",");
            String nowType = "1";
            int timeSlot = rule.getTimeSlot();
            for (int i = 0; i < timeSlot; i++) {
                int startTime = Integer.parseInt(start[i]);
                int endTime = Integer.parseInt(end[i]);
                if (startTime < hhmm && endTime > hhmm) {
                    nowType = type[i];
                }
            }
            switch (nowType) {
                case "1":
                    price = rule.getCuspPrice();
                    break;
                case "2":
                    price = rule.getHighPrice();
                    break;
                case "3":
                    price = rule.getNormalPrice();
                    break;
                case "4":
                    price = rule.getValleyPrice();
                    break;
                default:
                    price = 1;
            }
        }

        double chargeNum = order.getTotalAmount().divide(new BigDecimal(price), 3, BigDecimal.ROUND_CEILING).doubleValue();
        order.setChargeAmount((float) chargeNum);
        Order order1 = orderDao.getOrderById(order.getOrderId());
        long betweenMin = DateUtil.between(order1.getStartTime(), new Date(), DateUnit.MINUTE);
        jsonObject.put("chargeTime", betweenMin);
        jsonObject.put("chargeStatus", true);
        redisUtil.set(order.getOrderId(), jsonObject.toString());
    }

    @Override
    public Object getChargeData(String orderId) {
        JSONObject jsonObject = new JSONObject();
        String chargeData = redisUtil.get(orderId);
        JSONObject deviceJSON = new JSONObject();
        deviceJSON.put("powerRating", 0);
        deviceJSON.put("currentRated", 0);
        deviceJSON.put("ratedVoltage", 0);
        String deviceId = orderDao.getDeviceIdByOrderId(orderId);
        Device device = deviceDao.getById(deviceId);
        if (null != device) {
            jsonObject.put("result", "success");
            deviceJSON.put("powerRating", device.getPowerRating());
            deviceJSON.put("currentRated", device.getCurrentRated());
            deviceJSON.put("ratedVoltage", device.getRatedVoltage());
        }

        jsonObject.put("chargeData", chargeData);
        jsonObject.put("device", deviceJSON);
        if (StrUtil.isBlank(chargeData)) {
            jsonObject.put("result", "fail");
            JSONObject orderJSON = JSONObject.fromObject(new Order());
            orderJSON.put("chargeStatus", true);
            orderJSON.put("deviceType", "");
            orderJSON.put("chargeTime", 0);
            jsonObject.put("chargeData", orderJSON);
        }
        return jsonObject;
    }

    @Override
    public int upChargeLog(String resContent) {
        Order order = new Order();
        order.setGunNo(Integer.parseInt(resContent.substring(0, 2)));
        order.setOrderId(resContent.substring(2, 18));
        order.setChargeType(Integer.parseInt(resContent.substring(18, 20)));
        order.setCardId(resContent.substring(20, 36));//BCD 转 10进制4位-------------------------------------
        order.setVin(resContent.substring(36, 70));
        order.setSoc(Integer.parseInt(resContent.substring(70, 72), 16));
        order.setEndReason(Integer.parseInt(resContent.substring(72, 74)));
        order.setStartTime(stringToDate(resContent.substring(86, 98)));//reqContent.substring(74,86)
        order.setEndTime(stringToDate(resContent.substring(86, 98)));//reqContent.substring(86,98)
        order.setStartMeter(stringToFloat(Integer.parseInt(resContent.substring(98, 106), 16) + ""));
        order.setEndMeter(stringToFloat(Integer.parseInt(resContent.substring(106, 114), 16) + ""));
        order.setChargeAmount(stringToFloat(Integer.parseInt(resContent.substring(114, 122), 16) + ""));
        order.setCuspElectricity(stringToFloat(Integer.parseInt(resContent.substring(122, 130), 16) + ""));
        order.setHighElectricity(stringToFloat(Integer.parseInt(resContent.substring(130, 138), 16) + ""));
        order.setNormalElectricity(stringToFloat(Integer.parseInt(resContent.substring(138, 146), 16) + ""));
        order.setValleyElectricity(stringToFloat(Integer.parseInt(resContent.substring(146, 154), 16) + ""));
        order.setTotalAmount(BigDecimal.valueOf(Double.parseDouble(Integer.parseInt(resContent.substring(154, 162), 16) + "") / 1000));
        order.setCuspAmount(BigDecimal.valueOf(Double.parseDouble(Integer.parseInt(resContent.substring(162, 170), 16) + "") / 1000));
        order.setHighAmount(BigDecimal.valueOf(Double.parseDouble(Integer.parseInt(resContent.substring(170, 178), 16) + "") / 1000));
        order.setNormalAmount(BigDecimal.valueOf(Double.parseDouble(Integer.parseInt(resContent.substring(178, 186), 16) + "") / 1000));
        order.setValleyAmount(BigDecimal.valueOf(Double.parseDouble(Integer.parseInt(resContent.substring(186, 194), 16) + "") / 1000));
        order.setBookingAmount(BigDecimal.valueOf(Double.parseDouble(Integer.parseInt(resContent.substring(194, 202), 16) + "") / 1000));
        order.setServiceAmount(BigDecimal.valueOf(Double.parseDouble(Integer.parseInt(resContent.substring(202, 210), 16) + "") / 1000));
        order.setPickAmount(BigDecimal.valueOf(Double.parseDouble(Integer.parseInt(resContent.substring(210, 218), 16) + "") / 1000));
        //redisUtil.delete(order.getOrderId());   //删除 redis 缓存中的 充电数据
        String chargeData = redisUtil.get(order.getOrderId());
        com.alibaba.fastjson.JSONObject chargeDataJSON = com.alibaba.fastjson.JSONObject.parseObject(chargeData);
        order.setChargeAmount(Float.parseFloat(chargeDataJSON.get("chargeAmount").toString()));
        int flg = orderDao.update(order);
        Order nowOrder = orderDao.getOrderById(order.getOrderId());
        com.alibaba.fastjson.JSONObject jsonObject = (com.alibaba.fastjson.JSONObject) com.alibaba.fastjson.JSONObject.toJSON(nowOrder);
        jsonObject.put("chargeStatus", false);
        com.alibaba.fastjson.JSONObject redisJSON = (com.alibaba.fastjson.JSONObject) com.alibaba.fastjson.JSONObject.parse(redisUtil.get(order.getOrderId()));
        long betweenMin = DateUtil.between(nowOrder.getCreated(), DateUtil.parse(redisJSON.get("startTime").toString()), DateUnit.MINUTE);
        jsonObject.put("chargeTime", betweenMin);
        redisUtil.set(order.getOrderId(), jsonObject.toString());
        Information information = new Information();
        information.setTitle("充电结束");
        information.setInfo("尊敬的用户，您的爱车电量已充满，请您拔枪挪车，否则我们将在15分钟后收取延时费，感谢您的使用。");
        if ("0000000000000000".equals(order.getOrderId())) {
            return flg;
        }
        information.setUserUuid(orderDao.getOrderById(order.getOrderId()).getUid());
        userDao.addInformation(information);
        Device device = deviceDao.getById(orderDao.getDeviceIdByOrderId(order.getOrderId()));
        if (null == device) {
            jsonObject.put("result", "数据异常");
        }
        String addres = device.getClientAddress();
        SocketThread socketThread = (SocketThread) ThreadUtil.getThreadByName(addres);
        SendThread sendThread = new SendThread(socketThread.getSocket());
        //拼接并格式化server发送的消息
        String msgContent = order.getGunNo() + order.getOrderId();
        String msgCode = msgNumberUtil.getNumber();
        String hexStr02 = "07 " + substrs(msgCode) + "00 09 " + substrs(msgContent);
        byte[] serverStr02 = CRC16.hexToByte(hexStr02.replace(" ", ""));
        String serverstrCrc02 = HexConvert.BinaryToHexString(CRC16.GetCRC(serverStr02));
        String hexserverStr02 = "68 " + HexConvert.BinaryToHexString(serverStr02) + serverstrCrc02;
        sendThread.setCmd(hexserverStr02);        //给send线程设置 发送消息
        sendThread.start();
        return flg;
    }

    @Override
    public Object endCharge(String orderId, String gunNo) {
        Order order = orderDao.getOrderById(orderId);
        JSONObject jsonObject = new JSONObject();
        if (null == order) {
            jsonObject.put("result", "订单号不存在");
            return jsonObject;
        }
        Device device = deviceDao.getById(order.getDeviceId());
        if (null == device) {
            jsonObject.put("result", "数据异常");
            return jsonObject;
        }
        //获取 socketThread 线程
        String addres = device.getClientAddress();
        String msgCode = msgNumberUtil.getNumber();
        String key = addres + msgCode;
        SocketThread socketThread = (SocketThread) ThreadUtil.getThreadByName(addres);
        if (null == socketThread) {
            jsonObject.put("result", "电桩未连接到服务器");
            return jsonObject;
        }
        //拼接并格式化 server 发送的消息
        String msgContent = gunNo + orderId;
        String hexStr = "07 " + substrs(msgCode) + "00 09 " + substrs(msgContent);
        byte[] serverStr = CRC16.hexToByte(hexStr.replace(" ", ""));
        String serverstrCrc = HexConvert.BinaryToHexString(CRC16.GetCRC(serverStr));
        String hexserverStr = "68 " + HexConvert.BinaryToHexString(serverStr) + serverstrCrc;
        //给 sendThread 线程设置 发送消息
        SendThread sendThread = new SendThread(socketThread.getSocket());
        sendThread.setCmd(hexserverStr);
        try {
            sendThread.start();
        }catch (Exception e){
            e.printStackTrace();
        }
        //有回复才会或者超过等待时间会跳出
        MQUtil.putMsg(key, "");
        String msg = null;
        int currentWaitTimes = 0;
        int maxWaitTimes = 5000;
        while (StrUtil.isBlank(msg)) {
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            msg = MQUtil.getMsg().get(key);
            currentWaitTimes += 10;
            //这里面写超时处理
            if (currentWaitTimes >= maxWaitTimes) {
                System.out.println("超时处理");
                jsonObject.put("result", "通信超时");
                return jsonObject;
            }
        }
        MQUtil.removeMsg(key);  //删掉无用KV
        //电桩回复处理
        String msgOrderId = msg.substring(0, 16);
        String msgResult = msg.substring(16, 18);
        if ("00".equals(msgResult)) {
            order.setOrderId(orderId);
            deviceDao.updateStatus(device.getDeviceId(), "0");
            orderDao.updateStatus(msgOrderId, 1);   //设置成未付款 1-未付款
            jsonObject.put("result", "success");
            return jsonObject;
        } else if ("01".equals(msgResult)) {
            jsonObject.put("result", "订单号错误");
            return jsonObject;
        } else {
            jsonObject.put("result", "异常");
            return jsonObject;
        }
    }

    @Override
    public String ruleSetToDevice(String deviceId, int ruleId) {
        Rule rule = ruleDao.getRule(String.valueOf(ruleId),String.valueOf(deviceDao.getById(deviceId).getChargeType()));
        if (null == rule) {
            return "计费规则不存在";
        }
        //获取 socketThread 线程
        String address = deviceDao.getClientAddress(deviceId);
        String msgCode = msgNumberUtil.getNumber();
        String key = address + msgCode;
        SocketThread socketThread = (SocketThread) ThreadUtil.getThreadByName(address);
        if (null == socketThread) {
            return "电桩未连接到服务器";
        }
        String id = fullZeroStr(rule.getId() + "", 8);
        String ruleVersion = fullZeroStr(rule.getRuleVersion() + "", 8);
        String BCDstr = rule.getEffectiveTime();
        String bookingPrice = fullZeroStr((int) (rule.getBookingPrice() * 1000) + "", 8);
        String servicePrice = fullZeroStr((int) (rule.getServicePrice() * 1000) + "", 8);
        String pickPrice = fullZeroStr((int) (rule.getPickPrice() * 1000) + "", 8);
        String cuspPrice = fullZeroStr((int) (rule.getCuspPrice() * 1000) + "", 8);
        String highPrice = fullZeroStr((int) (rule.getHighPrice() * 1000) + "", 8);
        String normalPrice = fullZeroStr((int) (rule.getNormalPrice() * 1000) + "", 8);
        String valleyPrice = fullZeroStr((int) (rule.getValleyPrice() * 1000) + "", 8);
        int timeSlot = rule.getTimeSlot();
        String startTime = rule.getStartTime();
        String endTime = rule.getEndTime();
        String timeType = rule.getTimeType();
        String start[] = startTime.split(",");
        String end[] = endTime.split(",");
        String type[] = timeType.split(",");
        String timeStr = fullZeroStr(timeSlot + "", 2);
        for (int i = 0; i < timeSlot; i++) {
            timeStr += (start[i].substring(0, 2) + start[i].substring(2, 4));
            timeStr += (end[i].substring(0, 2) + end[i].substring(2, 4));
            timeStr += ("0" + type[i]);
        }
        //拼接并格式化server发送的消息
        String msgContent = id + ruleVersion + BCDstr + bookingPrice + servicePrice + pickPrice + cuspPrice + highPrice + normalPrice + valleyPrice + timeStr;
        String msgLength = fullZeroStr(Integer.toHexString(msgContent.getBytes().length), 4);   //报文内容的长度
        String hexStr = "0B " + substrs(msgCode) + substrs(msgLength) + substrs(msgContent);
        byte[] serverStr = CRC16.hexToByte(hexStr.replace(" ", ""));
        String serverstrCrc = HexConvert.BinaryToHexString(CRC16.GetCRC(serverStr));
        String hexserverStr = "68 " + HexConvert.BinaryToHexString(serverStr) + serverstrCrc;
        //给send线程设置 发送消息
        SendThread sendThread = new SendThread(socketThread.getSocket());
        sendThread.setCmd(hexserverStr);
        try {
            sendThread.start();
        }catch (Exception e) {
            e.printStackTrace();
        }
        //获取回复
        MQUtil.putMsg(key, "");
        String msg = null;
        int currentWaitTimes = 0;
        int maxWaitTimes = 10000;
        //有回复才会或者超过等待时间会跳出
        while (StrUtil.isBlank(msg)) {
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            msg = MQUtil.getMsg().get(key);
            currentWaitTimes += 10;
            //这里面写超时处理
            if (currentWaitTimes >= maxWaitTimes) {
                System.out.println("超时处理");
                return "联通超时";
            }
        }
        MQUtil.removeMsg(key);//删掉无用KV
        //电桩回复处理
        String msgRuleId = msg.substring(0, 8);
        String msgRuleVersion = msg.substring(8, 16);
        String msgResult = msg.substring(16, 18);
        if ("00".equals(msgResult)) {
            //这里应该有一个数据库交互--device 的计费规则ID和版本--没有建表
            return "设置成功";
        } else {
            return "设置失败";
        }
    }

    @Override
    public void ruleSetToDevice(Socket socket, String deviceId, String deviceType) {
        String stationId = deviceDao.getStationByDeviceId(deviceId);
        Rule rule = ruleDao.getRule(stationId, deviceType);
        if (null == rule) {
            System.out.println("计费规则不存在");
            return;
        }
        String msgCode = msgNumberUtil.getNumber();
        String id = fullZeroStr(rule.getId() + "", 8);
        String ruleVersion = fullZeroStr(rule.getRuleVersion() + "", 8);
        String BCDstr = rule.getEffectiveTime();
        String bookingPrice = fullZeroStr((int) (rule.getBookingPrice() * 1000) + "", 4);
        String servicePrice = fullZeroStr((int) (rule.getServicePrice() * 1000) + "", 4);
        String pickPrice = fullZeroStr((int) (rule.getPickPrice() * 1000) + "", 4);
        String cuspPrice = fullZeroStr((int) (rule.getCuspPrice() * 1000) + "", 4);
        String highPrice = fullZeroStr((int) (rule.getHighPrice() * 1000) + "", 4);
        String normalPrice = fullZeroStr((int) (rule.getNormalPrice() * 1000) + "", 4);
        String valleyPrice = fullZeroStr((int) (rule.getValleyPrice() * 1000) + "", 4);
        int timeSlot = rule.getTimeSlot();
        String startTime = rule.getStartTime();
        String endTime = rule.getEndTime();
        String timeType = rule.getTimeType();
        String[] start = startTime.split(",");
        String[] end = endTime.split(",");
        String[] type = timeType.split(",");
        String timeStr = fullZeroStr(timeSlot + "", 2);
        for (int i = 0; i < timeSlot; i++) {
            timeStr += (start[i].substring(0, 2) + start[i].substring(2, 4));
            timeStr += (end[i].substring(0, 2) + end[i].substring(2, 4));
            timeStr += ("0" + type[i]);
        }
        //拼接并格式化 server 发送的消息
        String msgContent = id + ruleVersion + BCDstr + bookingPrice + servicePrice + pickPrice + cuspPrice + highPrice + normalPrice + valleyPrice + timeStr;
        String msgLength = fullZeroStr(Integer.toHexString(29 + 5 * timeSlot), 4);  //报文内容的长度
        String hexStr02 = "0B " + substrs(msgCode) + substrs(msgLength) + substrs(msgContent);
        byte[] serverStr02 = CRC16.hexToByte(hexStr02.replace(" ", ""));
        String serverstrCrc02 = HexConvert.BinaryToHexString(CRC16.GetCRC(serverStr02));
        String hexserverStr02 = "68 " + HexConvert.BinaryToHexString(serverStr02) + serverstrCrc02;
        //给 sendThread 线程设置 发送消息
        SendThread sendThread = new SendThread(socket);
        sendThread.setCmd(hexserverStr02);
        try {
            sendThread.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void dealRuleSet(String response, String address) {
        String ruleId = response.substring(0, 8);
        String ruleVersion = response.substring(8, 16);
        String result = response.substring(16, 18);
        if ("00".equals(result)) {
            deviceDao.updateStatusByAddress(address, "0");
        } else {
            deviceDao.updateStatusByAddress(address, "2");
        }
    }

    @Override
    public void alarmDevice(String reqContent) {
        Alarm alarm = new Alarm();
        alarm.setGunNo(Integer.parseInt(reqContent.substring(0, 2)));
        alarm.setCode(reqContent.substring(2, 6));
        alarm.setAlarmTime(reqContent.substring(6, 18));
        alarm.setStatus(Integer.parseInt(reqContent.substring(18, 20)));
        alarm.setAlarmValue(Double.parseDouble(reqContent.substring(20, 28)) / 1000);
        alarmDao.addOne(alarm);
    }

    @Override
    public String proofTime(String deviceId) {
        String address = deviceDao.getClientAddress(deviceId);
        String msgCode = msgNumberUtil.getNumber();
        String key = address + msgCode;
        SocketThread socketThread = (SocketThread) ThreadUtil.getThreadByName(address);
        if (null == socketThread) {
            return "电桩未连接到服务器";
        }
        //拼接并格式化 server 发送的消息
        String msgContent = new SimpleDateFormat("yyMMddHHMMSS").format(new Date());
        String hexStr = "0E " + substrs(msgCode) + "00 06 " + substrs(msgContent);
        byte[] serverStr = CRC16.hexToByte(hexStr.replace(" ", ""));
        String serverstrCrc = HexConvert.BinaryToHexString(CRC16.GetCRC(serverStr));
        String hexserverStr = "68 " + HexConvert.BinaryToHexString(serverStr) + serverstrCrc;
        //给 sendThread 线程设置 发送消息
        SendThread sendThread = new SendThread(socketThread.getSocket());
        sendThread.setCmd(hexserverStr);
        sendThread.start();
        MQUtil.putMsg(key, "");
        String msg = null;
        int currentWaitTimes = 0;
        int maxWaitTimes = 5000;
        //有回复才会或者超过等待时间会跳出
        while (StrUtil.isBlank(msg)) {
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            msg = MQUtil.getMsg().get(key);
            currentWaitTimes += 10;
            //这里面写超时处理
            if (currentWaitTimes >= maxWaitTimes) {
                System.out.println("超时处理");
                return "联通超时";
            }
        }
        MQUtil.removeMsg(key);//删掉无用KV
        //电桩回复处理
        String msgResult = msg.substring(0, 2);
        if ("00".equals(msgResult)) {
            //这里应该有一个数据库交互--device 的计费规则ID和版本--没有建表
            return "设置成功";
        } else {
            return "设置失败";
        }
    }

    @Override
    public void proofTimeToDevice(Socket socket) {
        String msgCode = msgNumberUtil.getNumber();
        //拼接并格式化 server 发送的消息
        String msgContent = new SimpleDateFormat("yyMMddHHMMSS").format(new Date());
        String hexStr = "0E " + substrs(msgCode) + "00 06 " + substrs(msgContent);
        byte[] serverStr = CRC16.hexToByte(hexStr.replace(" ", ""));
        String serverstrCrc = HexConvert.BinaryToHexString(CRC16.GetCRC(serverStr));
        String hexserverStr = "68 " + HexConvert.BinaryToHexString(serverStr) + serverstrCrc;
        //给 sendThread 线程设置 发送消息
        SendThread sendThread = new SendThread(socket);
        sendThread.setCmd(hexserverStr);
        try {
            sendThread.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public String upgrade(String deviceId, int versionId) {
        Version version = versionDao.getVersion(versionId);
        if (null == version) {
            return "版本号异常";
        }
        //
        String address = deviceDao.getClientAddress(deviceId);
        String msgCode = msgNumberUtil.getNumber();
        String key = address + msgCode;
        SocketThread socketThread = (SocketThread) ThreadUtil.getThreadByName(address);
        if (null == socketThread) {
            return "电桩未连接到服务器";
        }
        String[] arrStr = version.getSoftwareVer().split("\\.");
        String msgSoftwareVer = fullZeroStr(Integer.parseInt(arrStr[0], 16) + "", 2) + fullZeroStr(Integer.parseInt(arrStr[1], 16) + "", 2);
        arrStr = version.getSoftwareVer().split("\\.");
        String msgComminVer = fullZeroStr(Integer.parseInt(arrStr[0], 16) + "", 2) + fullZeroStr(Integer.parseInt(arrStr[1], 16) + "", 2);
        String md5 = version.getMd5();
        String linkAddress = version.getLinkAddress();
        String linkLength = fullZeroStr(Integer.toHexString(linkAddress.getBytes().length), 4);
        //拼接并格式化server发送的消息
//        String msgContent = msgSoftwareVer + msgComminVer + md5 + linkLength + linkAddress;
        String msgLength = linkAddress.getBytes().length + "";
        String hexStr02 = "1E "
                + substrs(msgCode)
                + substrs((HexConvert.convertStringToHex(msgLength))
                + substrs(HexConvert.convertStringToHex(msgSoftwareVer))
                + substrs(HexConvert.convertStringToHex(msgComminVer))
                + substrs(HexConvert.convertStringToHex(md5))
                + substrs(linkLength)
                + substrs(HexConvert.convertStringToHex(linkAddress)));
        byte[] serverStr = CRC16.hexToByte(hexStr02.replace(" ", ""));
        String serverstrCrc = HexConvert.BinaryToHexString(CRC16.GetCRC(serverStr));
        String hexserverStr = "68 " + HexConvert.BinaryToHexString(serverStr) + serverstrCrc;
        //给 sendThread 线程设置 发送消息
        SendThread sendThread = new SendThread(socketThread.getSocket());
        sendThread.setCmd(hexserverStr);

        MQUtil.putMsg(key, "");
        sendThread.start();
        String msg = null;
        int currentWaitTimes = 0;
        int maxWaitTimes = 2000;
        //有回复才会或者超过等待时间会跳出
        while (StrUtil.isBlank(msg)) {
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            msg = MQUtil.getMsg().get(key);
            currentWaitTimes += 10;
            //这里面写超时处理
            if (currentWaitTimes >= maxWaitTimes) {
                System.out.println("超时处理");
                return "联通超时";
            }
        }
        MQUtil.removeMsg(key);//删掉无用KV
        //电桩回复处理
        String msgResult = msg.substring(0, 2);
        if ("00".equals(msgResult)) {
            //这里应该有一个数据库交互--device 的计费规则ID和版本--没有建表
            return "设置成功";
        } else {
            return "设置失败";
        }
    }

    @Override
    public String login(String reqContent) {
        String deviceId = reqContent.substring(0, 16);
        String deviceType = reqContent.substring(16, 18);
        String gunNum = reqContent.substring(18, 20);
        String opreatorCode = reqContent.substring(20, 28);
        String pwd = reqContent.substring(28, 34);
        String softwareVer = reqContent.substring(34, 38);
        String msgComminVer = reqContent.substring(38, 42);
        String ruleId = reqContent.substring(42, 50);
        String ruleVer = reqContent.substring(50, 58);
        String resContent = deviceId;
        Device device = deviceDao.getById(deviceId);
        if (null != device) {
            if (pwd.equals(device.getPwd())) {
                resContent += "00";
            } else {
                resContent += "01";
            }
        } else {
            resContent += "01";
        }
        return resContent;
    }

    @Override
    public String setQRcode(String msg) {
        String msgCode = msg.substring(4, 8);
        String resContent = "";
        String deviceId = "0000050104001001";
        String gunNo = "01";
        String linkStart = "http://blxny/96";
        String linkMid = "93/12";
        String linkEnd = "3/scanQrcode?0926";
        String QRcode = linkStart + deviceId + linkMid + gunNo + linkEnd;
        String codeLength = Integer.toHexString(QRcode.getBytes().length);
        resContent = gunNo + fullZeroStr(codeLength, 4);
        int contentLength = (QRcode.getBytes().length + 3);
        String hexStr02 = "9C " + msgCode
                + substrs(fullZeroStr(Integer.toHexString(contentLength), 4))
                + substrs(resContent)
                + substrs(HexConvert.convertStringToHex(QRcode));
        byte[] serverStr = CRC16.hexToByte(hexStr02.replace(" ", ""));
        String serverstrCrc = HexConvert.BinaryToHexString(CRC16.GetCRC(serverStr));
        String hexserverStr = "68 " + HexConvert.BinaryToHexString(serverStr) + serverstrCrc;
        return hexserverStr;
    }

    @Override
    public boolean isLogin(String address) {
        String ip = address.substring(1, address.length() - 4);
        int port = Integer.parseInt(address.substring(address.length() - 4));
        if (deviceDao.getIsLogin(ip, port) > 0) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean setAddress(String deviceId, String address) {
        if (deviceDao.updateClientAddress(deviceId, address) > 0) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String reStart(String deviceId) {
        String address = deviceDao.getClientAddress(deviceId);
        String msgCode = msgNumberUtil.getNumber();
        String key = address + msgCode;
        SocketThread socketThread = (SocketThread) ThreadUtil.getThreadByName(address);
        if (null == socketThread) {
            return "电桩未连接到服务器";
        }
        SendThread sendThread = new SendThread(socketThread.getSocket());
        //拼接并格式化server发送的消息
        String hexStr02 = "1D " + substrs(msgCode) + "00 00";
        byte[] serverStr02 = CRC16.hexToByte(hexStr02.replace(" ", ""));
        String serverstrCrc02 = HexConvert.BinaryToHexString(CRC16.GetCRC(serverStr02));
        String hexserverStr02 = "68 " + HexConvert.BinaryToHexString(serverStr02) + serverstrCrc02;
        //给send线程设置 发送消息
        sendThread.setCmd(hexserverStr02);
        MQUtil.putMsg(key, "");
        try {
            sendThread.start();
        }catch (Exception e){
            e.printStackTrace();
        }
        String msg = null;
        int currentWaitTimes = 0;
        int maxWaitTimes = 2000;
        //有回复才会或者超过等待时间会跳出
        while (StrUtil.isBlank(msg)) {
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            msg = MQUtil.getMsg().get(key);
            currentWaitTimes += 10;
            //这里面写超时处理
            if (currentWaitTimes >= maxWaitTimes) {
                System.out.println("超时处理");
                return "联通超时";
            }
        }
        MQUtil.removeMsg(key);  //删掉无用KV
        //电桩回复处理
        String msgResult = msg.substring(0, 2);
        if ("00".equals(msgResult)) {
            //这里应该有一个数据库交互--device 的计费规则ID和版本--没有建表
            return "重启成功";
        } else {
            return "重启失败";
        }
    }

    @Override
    public Object addAppointment(String uid, String time, String deviceId, String much, String payType) {
        Device device = null;
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("money", userDao.getMoney(uid));
        jsonObject.put("result", "000000");
        JsonConfig jsonConfig = new JsonConfig();
        jsonConfig.registerJsonValueProcessor(Date.class, new DateJsonValueProcessor("yyyy-MM-dd HH:mm:ss"));
        jsonObject.put("appointment", JSONObject.fromObject(new Appointment(), jsonConfig));
        //先找到桩，返回桩名/桩号/充电方式
        if (deviceId.length() != 16) {
            deviceId = deviceId + "%";
            device = orderDao.findDeviceByLike(deviceId);
        } else {
            device = orderDao.findDeviceById(deviceId);
        }
        //获取 socketThread 线程
        String address = device.getClientAddress();
        String msgCode = msgNumberUtil.getNumber();
        String key = address + msgCode;
        SocketThread socketThread = (SocketThread) ThreadUtil.getThreadByName(address);
        if (null == socketThread) {
            jsonObject.put("result", "电桩未连接到服务器");
            return jsonObject;
        }
        //计算时间
        Date beginTime = new Date();
        Date endTime = DateUtil.offset(beginTime, DateField.MINUTE, 30);
        //用户信息一起add
        Appointment appointment = new Appointment();
        appointment.setType(0);
        appointment.setBeginTime(beginTime);
        appointment.setEndTime(endTime);
        appointment.setDevice(device.getDeviceId());
        appointment.setDeviceType(String.valueOf(device.getChargeType()));
        appointment.setName(device.getName());
        appointment.setUseruuid(uid);
        String appouid = deviceId.substring(4, 10) + new SimpleDateFormat("yyMMdd").format(beginTime);
        String num = orderDao.findId(appouid + "%");
        if (null == num) {
            appouid = appouid + "0001";
        } else {
            String temp = "00000" + (Integer.valueOf(num.substring(12)) + 1);
            num = temp.substring(temp.length() - 4);
            appouid = appouid + num;
        }
        appointment.setAppoUid(appouid);
        if (orderDao.addAppointment(appointment) > 0 && orderDao.changeDeviceUse(device.getDeviceId()) > 0) {
            if ("ye".equals(payType)) {
                BigDecimal yeMoney = new BigDecimal(userDao.getMoney(uid));
                BigDecimal nowMoney = yeMoney.subtract(new BigDecimal(much));
                if (orderDao.updateMoney(nowMoney, uid) > 0) {
                    jsonObject.put("result", "success");
                    jsonObject.put("money", nowMoney);
                    jsonObject.put("appointment", JSONObject.fromObject(appointment, jsonConfig));
                } else {
                    jsonObject.put("result", "扣费失败");
                }
            } else {
                jsonObject.put("result", "success");
                jsonObject.put("appointment", JSONObject.fromObject(appointment, jsonConfig));
            }
            //发消息
            Information information = new Information();
            information.setTitle("预约成功");
            information.setInfo("尊敬的用户，桩号为" + deviceId + "的充电桩已被您预约成功，感谢您的使用。");
            information.setUserUuid(uid);
            userDao.addInformation(information);
        } else {
            jsonObject.put("result", "添加订单失败");
            //发消息
            Information information = new Information();
            information.setTitle("预约失败");
            information.setInfo("尊敬的用户，桩号为" + deviceId + "的充电桩预约失败，请联系管理员。");
            information.setUserUuid(uid);
            userDao.addInformation(information);
        }
        return jsonObject;
    }

    @Override
    @Transactional
    public String cancelAppointment(String appoId) {
        //找到对应的桩ip和端口号
        Appointment appo = orderDao.findAppoByAppoUid(appoId);
        if (null == appo) {
            return "数据异常，预约号错误";
        }
        Device device = orderDao.findDeviceById(appo.getDevice());
        if (device == null) {
            return "数据异常，未找到对应电桩";
        }
        if (orderDao.del(appoId) > 0 && deviceDao.updateStatus(device.getDeviceId(), "0") > 0) {
            return "success";
        } else {
            return "记录更新失败";
        }
    }

    /**
     * 每隔两位填空格
     * @param replace
     * @return
     */
    private static String substrs(String replace) {
        String regex = "(.{2})";
        replace = replace.replaceAll(regex, "$1 ");
        return replace;
    }

    /**
     * 16进制字符串转10进制FLOAT
     * @param str
     * @return
     */
    private float stringToFloat(String str) {
        return Long.parseLong(str) / 1000;
    }


    /**
     * YYMMDDHHMMSS格式的时间字符串转换成date
     * @param str
     * @return
     */
    private static Date stringToDate(String str) {
        SimpleDateFormat df = new SimpleDateFormat("yyMMddhhmmss");
        try {
            Date date = df.parse(str);
            System.out.println(date.getTime());
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return new Date();
    }

    /**
     * 字符串左边 补0
     * @param str
     * @param zero
     * @return
     */
    private static String fullZeroStr(String str, int zero) {
        String msg = "0000000000000000000" + str;
        return msg.substring(msg.length() - zero);
    }

    /**
     * 字符转ascii
     *
     * @param str
     * @return
     */
    private static String stringToAscii(String str) {
        StringBuilder sb = new StringBuilder("");
        char[] chars = str.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            sb.append(((int) chars[i] + ""));
        }
        return sb.toString();
    }

}