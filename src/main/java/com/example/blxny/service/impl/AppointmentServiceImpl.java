package com.example.blxny.service.impl;

import com.example.blxny.dao.AppointmentDao;
import com.example.blxny.service.AppointmentService;
import com.example.blxny.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AppointmentServiceImpl implements AppointmentService {

    @Autowired
    private AppointmentDao appointmentDao;

    @Override
    public String dealAppoCharge(String appoId,String result) {
//        int res = Integer.parseInt(result);
//        appointmentDao.updateStatus(appoId, res);

        //需要一个设备ID;

        SocketThread socketThread = (SocketThread) ThreadUtil.getThreadByName("9010");

        if (null == socketThread) {
            //写一个socket连接 并加入线程池
            return "连接已经被断开,正在重连";
        }

        String hexStr02 = "02 " + "00 00 " + "00 0D " + substrs("") + "00 00 05 01 04 00 10 01 00 01 00";
        byte[] serverStr02 = CRC16.hexToByte(hexStr02.replace(" ", ""));
        String serverstrCrc02 = HexConvert.BinaryToHexString(CRC16.GetCRC(serverStr02));
        String hexserverStr02 = "68 " + HexConvert.BinaryToHexString(serverStr02) + serverstrCrc02;
        System.out.println(socketThread.getName());
        MQUtil.putMsg(hexserverStr02, "");//设置消息key
        String flg = null;
        int currentWaitTimes = 0;
        //有回复才会或者超过等待时间会跳出
        while (null == flg || "".equals(flg)) {
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            flg = MQUtil.getMsg().get(hexserverStr02);
            //这里面写超时处理
            if (currentWaitTimes >= 5) {
                System.out.println("超时处理");
                return "超时";
            }
        }
        return result;
    }

    @Override
    public void cancelAppoCharge(String appoId, String result) {
        int res = Integer.parseInt(result);
        switch (res) {
            case 0:
                appointmentDao.updateStatus(appoId, 5);
                break;
            case 1:
                appointmentDao.updateStatus(appoId, 6);
                break;
            default:
                appointmentDao.updateStatus(appoId, 7);
        }
    }



    public static String substrs(String replace) {
        String regex = "(.{2})";
        replace = replace.replaceAll(regex, "$1 ");
        return replace;
    }
}
