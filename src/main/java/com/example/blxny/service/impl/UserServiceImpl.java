package com.example.blxny.service.impl;

import com.example.blxny.dao.*;
import com.example.blxny.model.*;
import com.example.blxny.service.UserLoginService;
import com.example.blxny.tool.BankImgTool;
import com.example.blxny.tool.DistanceTool;
import com.example.blxny.tool.JPushTool;
import com.example.blxny.tool.PayCommonUtil;
import com.example.blxny.util.ErrorInfomation;
import com.example.blxny.util.ShortMessage;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;

import static com.example.blxny.tool.JPushTool.ALL;

@Service
public class UserServiceImpl implements UserLoginService {
    @Autowired
    private UserDao dao;
    @Autowired
    private CarInfoDao carInfoDao;
    @Autowired
    private CarDao carDao;
    @Autowired
    private StationDao stationDao;
    @Autowired
    private PowerDao power;
    @Autowired
    private OrderDao orderdao;
    @Autowired
    private PartnerDao partner;
    @Autowired
    private VersionDao versionDao;
    public static String MD5(String strSrc, MessageDigest md) {
        byte[] bt = strSrc.getBytes();
        md.update(bt);
        String strDes = bytes2Hex(md.digest());
        return strDes;
    }

    private static String bytes2Hex(byte[] bts) {
        StringBuffer des = new StringBuffer();
        String tmp = null;
        for (int i = 0; i < bts.length; i++) {
            tmp = (Integer.toHexString(bts[i] & 0xFF));
            if (tmp.length() == 1) {
                des.append("0");
            }
            des.append(tmp);
        }
        return des.toString();
    }

    @Override
    public Object update(String version) {
        AppVersion app = dao.update();
        return app;
    }

    @Override
    public List news() {
        List js = new ArrayList();
        List<News> news = dao.news();
        for (News news1 : news) {
            String img = news1.getImg();
            if (img == null || img.equals("")) {
                img = "null";
            }
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("headline", news1.getHeadline());
            jsonObject.put("source", news1.getSource());
            jsonObject.put("time", news1.getUpdateTime());
            jsonObject.put("content", news1.getContent());
            jsonObject.put("img", img);
            js.add(jsonObject);
        }
        return js;
    }

    @Override
    public boolean isHavaBankCard(String number,String where) {
        boolean boo = true;
        // System.out.println(dao.isHavaBankCard(number));
        if (dao.isHavaBankCard(number,where).equals("0")) {
            boo = false;
        }
        return boo;
    }

    @Override
    public List<Bank> fingAllBank(String uuid, String where) {
        return dao.fingAllBank(uuid, where);
    }

    @Override
    public int closeBankCard(String number, String uuid, String where) {
        return dao.closeBankCard(number, uuid, where);
    }

    @Override
    public int addBank(Bank bank) {
        BankImgTool img=new BankImgTool();
        String url=img.BankImgTool(bank.getBackName());
        bank.setImg(url);
        if (dao.AddBank(bank) > 0) {
            return 0;
        } else {
            return 1;
        }
    }

    @Override
    public int didHaveInfo(String uuid, String jpid) {
        dao.addJP(jpid, uuid);
        return dao.didHaveInfo(uuid);
    }

    @Override
    public List<String> findCity(String pro, String city) {
        City city1 = new City();
        if (!pro.equals("0")) {
            city1.setProvince(pro);
        }
        if (!city.equals("0")) {
            city1.setCity(city);
        }
        return dao.findCity(city1);
    }

    @Override
    public String addMoneyToUser(String money, String uuid) {
        if (dao.userRecharge(Double.valueOf(money), uuid) > 0) {
            //发送充值信息
            Information information = new Information();
            information.setTitle("账户充值");
            information.setInfo("尊敬的用户您好，您当前充值金额为" + money + "元，请扫码充电，感谢您的使用。");
            information.setUserUuid(uuid);
            dao.addInformation(information);
            return dao.findmoneyFromUser(uuid);
        } else {
            return "lost";
        }

    }

    @Override
    public int closeOtherLogin(String code, String uuid) {
        String alibaba = null;
        String wx = null;
        if ("1".equals(code)) {
            wx = code;
        } else {
            alibaba = code;
        }
        return dao.closeOtherLogin(alibaba, wx, uuid);
    }

    @Override
    public int changeOtherUid(String uuid, String phone) {
        //System.out.println(uuid + phone);
        String chase = dao.isphoneAndOther("ZF" + uuid);
        System.out.println(chase);
        if (chase.equals("0")) {
            return dao.changeUUidss("ZF" + uuid, phone);
        } else {
            return 0;
        }
    }

    /*微信绑定号码*/
    @Override
    public User wxRegWithFhone(String nickname, String img, String uuid, String phone) {
        uuid = "WX" + uuid;
        String chase = dao.isphoneAndOther(uuid);
        String reall = dao.isphone(phone);
        User retuser = new User();
        System.out.println(chase);
        if (chase.equals("0")) {
            if (reall.equals("0")) {
                User user = new User();
                user.setPhone(phone);
                user.setImages(img);
                user.setIntegral(uuid);
                user.setUserUuid("BL" + UUID.randomUUID().toString().replaceAll("-", "").substring(0, 10));
                user.setNickname(nickname);
                if (dao.WXnewuser(user) > 0) {
                    retuser = dao.FindUserAll(phone);
                }
            } else {
                if (dao.changeUUids(uuid, phone) > 0) {
                    retuser = dao.FindUserAll(phone);
                }
            }
        } else {
            System.out.println("不能注册");
        }
        return retuser;
    }

    @Override
    public User ZFRegWithFhone(String uuid, String phone) {
        uuid = "ZF" + uuid;
        String reall = dao.isphone(phone);
        User retuser = new User();
        if (reall.equals("0")) {
            User user = new User();
            user.setPhone(phone);
            user.setToken(uuid);
            user.setUserUuid("BL" + UUID.randomUUID().toString().replaceAll("-", "").substring(0, 10));
            user.setNickname("BL" + UUID.randomUUID().toString().replace("-", "").toUpperCase().substring(0, 6));
            if (dao.ZFnewuser(user) > 0) {
                retuser = dao.FindUserAll(phone);
            }
        } else {
            if (dao.changeUUidss(uuid, phone) > 0) {
                retuser = dao.FindUserAll(phone);
            }
        }

        return retuser;
    }

    @Override
    public User wxlogin(String uuid) {
        User user = dao.findWX("WX" + uuid);
        return user;
    }


    @Override
    public User zfblogin(String uuid) {
        User user = dao.findZF("ZF" + uuid);
        return user;
    }

    @Override
    public String insertCar(String vin, String card, String year, String uuid) {
        Car car = new Car();
        car.setVintage(vin);
        car.setCard(card);
        String[] strs = year.split("-");
        year = strs[0] + strs[1];
        car.setYear(Integer.valueOf(year));
        car.setUuid(uuid);
        //查看有没有录入车辆信息
        Car car1 = dao.findCar(uuid);
        if (car1 == null) {
            if (dao.insertCar(car) > 0) {
                return "OK";
            } else {
                return "error";
            }
        } else {
            if (dao.updateCar(car)> 0) {
                return "OK";
            } else {
                return "error";
            }
        }
    }

    @Override
    public Car findCar(String uuid) {
        return dao.findCar(uuid);
    }

    @Override
    public int changepaypassword(String password, String uuid) {
        return dao.changepaypassword(password, uuid);
    }

    @Override
    public String changePhone(String phone, String useruuid) {
        //查看此手机号是否可用
        if (dao.isphone(phone).equals("0")) {
            dao.changephone(phone, useruuid);
            return "1";
        } else {
            return "0";
        }
    }

    @Override
    public IDcard findrealman(String useruuid) {
        IDcard id = dao.findrealman(useruuid);
        if (id == null) {
            IDcard is = new IDcard();
            is.setType("0");
            return is;
        }
        return id;
    }

    @Override
    public String newIdCard(IDcard iDcard) {
        if (dao.newIdCard(iDcard) > 0) {
            return "Ok";
        } else {
            return "error";
        }
    }

    @Override
    public String verification(String phone) {
        String returnMessage;
        try {
            // System.out.println("====" + phone);
            returnMessage = ShortMessage.dxapi(phone);
        } catch (Exception e) {
            e.printStackTrace();
            returnMessage = ErrorInfomation.Err1002.toString();
        }
        System.out.println("====" + returnMessage);
        return returnMessage;
    }

    @Override
    public String reguster(String phone, String password) {
        String returnMessage;
        String userNickname = "BL" + UUID.randomUUID().toString().replace("-", "").toUpperCase().substring(0, 6);
        String useruuid = "BL" + UUID.randomUUID().toString().replaceAll("-", "").substring(0, 10);
        System.out.println(useruuid);
        String reall = dao.isphone(phone);
        if (reall.equals("0")) {
            String passwd = String.valueOf(password);
            MessageDigest md = null;
            try {
                md = MessageDigest.getInstance("MD5");
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
            String md5pass = MD5(passwd, md);

            int change = dao.newuser(phone, md5pass, userNickname, useruuid);
            if (change > 0) {
                returnMessage = "注册成功";
            } else {
                returnMessage = "注册失败";
            }
        } else {
            returnMessage = ErrorInfomation.Err1042.toString();//用户存在
        }
        return returnMessage;
    }

    @Override
    public User FindUserAll(String phone, String password) {
        //System.out.println("service get phone" + phone + password);
        User findOneUser = new User();
        findOneUser = dao.FindUserAll(phone);
        //System.out.println("find user is" + findOneUser);
        if (findOneUser != null) {
            String passwd = String.valueOf(password);
            MessageDigest md = null;
            try {
                md = MessageDigest.getInstance("MD5");
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
            String md5pass = MD5(passwd, md);
            if (findOneUser.getPassword().equals(md5pass)) {
                System.out.println(findOneUser.getPhone() + "登录成功");
                return findOneUser;
            } else {
                findOneUser.setPhone("密码错误");
                return findOneUser;
            }
        } else {
            User no = new User();
            no.setPhone("账号不存在");
            return no;
        }

    }

    @Override
    public User FindUserAllByUuid(String uuid) {
        User findOneUser = new User();
        findOneUser = dao.FindUserAllByUid(uuid);
        return findOneUser;
    }

    /**
     * 修改密码--改密
     *
     * @param uid
     * @param newPwd
     * @return
     */
    @Override
    public String modifyPwd(String uid, String newPwd) {
        String returnMessage;
        Integer hasFlg = dao.hasUserByUid(uid);
        if (hasFlg == 0) {
            returnMessage = "用户不存在";
        } else {
            String password = String.valueOf(newPwd);
            MessageDigest md = null;
            try {
                md = MessageDigest.getInstance("MD5");
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
            String md5pass = MD5(password, md);
            if (dao.updatePwdByUuid(uid, md5pass) > 0) {
                returnMessage = "修改成功";
            } else {
                returnMessage = "修改失败";
            }
        }
        return returnMessage;
    }

    @Override
    public User PhoneLogin(String phone) {
        //System.out.println("得到短信登录用户" + phone);
        User reall = dao.FindUserAll(phone);
        return reall;
    }

    @Override
    public String modifyNickNmae(String uid, String newName) {
        if (dao.modifyNickName(uid, newName) > 0) {
            return "success";
        } else {
            return "fail";
        }
    }

    @Override
    public int newUserInfor(String address, String lat, String lng, String homepro, String homecity, String phone) {
        int plan = dao.newUserInfor(address, lat, lng, homepro, homecity, phone);
        return plan;
    }

    @Override
    public int workInfor(String address, String lat, String lng, String workpro, String workcity, String phone) {
        int plan = dao.workInfor(address, lat, lng, workpro, workcity, phone);
        return plan;
    }


    /**
     * 修改密码--验证码--验证
     *
     * @param phone
     * @return
     */
    @Override
    public String checkPhone(String phone) {
        String returnMessage;
        if ("0".equals(dao.isphone(phone))) {
            returnMessage = "手机号不存在";
        } else {
            returnMessage = "";
        }
        return returnMessage;
    }

    @Override
    public Map<String, Object> getUserCarInfo(int uid) {
        Map<String, Object> map = new HashMap<>();
        map.put("plate_num", "");
        map.put("car", "");
        map.put("created", new Date());
//        CarInfo carInfo =  carInfoDao.getCarInfoByUid(uid);
//        map.put("plateNum",carInfo.getPlateNum());
//        map.put("created",carInfo.getCarCreated());
//        if (null!=carInfo) {
//            Car car = carDao.getById(carInfo.getCarID());
//            if (null!=car){
//            }
//        }
        return map;
    }

    @Override
    public List<Station> getStationByUid(String uid) {
        List<Station> list = new ArrayList<>();
//        User user = dao.FindUserAll(phone);
        String stationsStr = dao.getStaionByUid(uid);
        if (null == stationsStr || "".equals(stationsStr)) {
            return list;
        }
        //System.out.println(stationsStr);
        String[] arr;
        if (stationsStr.indexOf(",") != -1) {
            arr = stationsStr.split(",");
        } else {
            arr = new String[1];
            arr[0] = stationsStr;
        }
        for (int i = 0; i < arr.length; i++) {
            Station station = stationDao.getInfoById(arr[i]);
            //System.out.println("stationId:" + arr[i]);
            //System.out.println("stationInfo:" + station);
            if (null != station) {
                list.add(station);
                // System.out.println("list.add(station);");
            }
        }
        return list;
    }

    @Override
    public String removeStationCollected(String uid, String stationCollected) {
        String stationsStr = dao.findStationCollectedByUuid(uid);
        String[] arr;
        String newStr = "";
        if (null == stationsStr) {
            return "fail";
        }
        if (stationsStr.indexOf(",") != -1) {
            arr = stationsStr.split(",");
        } else {
            arr = new String[1];
            arr[0] = stationsStr;
        }
        for (int i = 0; i < arr.length; i++) {
            if (!stationCollected.equals(arr[i])) {
                newStr += arr[i] + ",";
            }
        }
        newStr = newStr.substring(0, newStr.length() - 1);
        //System.out.println(newStr);
        if (dao.updateStationCollected(uid, newStr) > 0) {
            return "success";
        } else {
            return "fail";
        }
    }

    @Override/*收藏驿站*/
    public String addCollected(String uid, String stationCollected) {
        String stationsStr = dao.findStationCollectedByUuid(uid);
        int chose = dao.updateStationCollected(uid, stationCollected + "," + stationsStr);
        if (chose > 0) {
            return "Ok";
        } else {
            return "No";
        }

    }

    @Override
    public int upphoto(String img, String uuid) {
        return dao.upphoto(img, uuid);
    }

    @Override
    public Object findOrderNutPay(String uuid) {
        String string = dao.findOrderNotPay(uuid);
        if (string != null) {
            if (string.equals("1")) {
                string = dao.findNotPay(uuid);
            } else {
                string = "null";
            }
        } else {
            string = "null";
        }
        return string;

    }

    @Override
    public List findDeviceType(String station, String code) {
        if (code.equals("0")) {
            code = "1";
        } else if (code.equals("1")) {
            code = "0";
        }
        List<Device> device = power.findDeviceType(code, station);
        //System.out.println(device);
        List retMassage = new ArrayList();
        for (Device device1 : device) {
            JSONObject js = new JSONObject();
            js.put("deviceId", device1.getDeviceId());
            js.put("type", device1.getState());
            //此处应put一个time。为剩余时间
            js.put("time", "");
            retMassage.add(js);
        }
        return retMassage;
    }

    @Override/*更新app文件*/
    public boolean addUpdate(String url, String info, String version) {
       boolean v= versionDao.getAppVersion().getVersion().equals(version);
       if(!v) {
           if (dao.addNewVersion(version, info, url) > 0) {
               Information information = new Information();
               information.setTitle("更新");
               information.setInfo("尊敬的用户，为了您的使用方便，我们优化了充电服务功能，请你及时更新哦");
               information.setUserUuid("2");
               dao.addInformation(information);
               //极光
               JPushTool.JPush("", "尊敬的用户，为了您的使用方便，我们优化了充电服务功能，请你及时更新哦", "更新提醒", ALL);
               return true;
           }
       }
        return false;
    }

    @Override
    public String payOver(String id, String money) {
        //订单是否正确
        Order order = orderdao.getOrderById(id);
        if (order != null) {
            //扣钱
//            dao.updateMoney(order.getUid(),Double.valueOf(money));
            dao.userRecharge(-Double.valueOf(money), order.getUid());
            //更改订单状态
            orderdao.updateStatus(id, 2);
        }
        return dao.findmoneyFromUser(order.getUid());
    }

    @Override
    public int addgold(String uid, String type,String money) {
        money=money.split("\\.")[0];
        int score=0;
        int returnScore=0;
        if(type.equals("0")||Integer.valueOf(money)>=1){
            score=10*Integer.valueOf(money);
        }
        if(dao.addScore(uid,score)>0){
            returnScore= score;
        }
        return returnScore;
    }

    @Override
    @Transactional
    public String pos(String uuid, String money,String card) {
        Pos pos=new Pos();
        String num="error";
        if(dao.fingONEBank(uuid,card).getNumber().equals(card)){
            pos.setNeed(BigDecimal.valueOf(Double.valueOf(money)));
            pos.setUseruuid(uuid);
            pos.setPosid(PayCommonUtil.getRandomNumber(12));
            pos.setCard(card);
        }
        if(0<dao.AddPos(pos)){
            partner.updatemoney(pos);
            num=String.valueOf(partner.findPartner(uuid).getMoney());
        }
        return num;
    }


}
