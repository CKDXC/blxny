package com.example.blxny.service.impl;

import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;
import com.example.blxny.dao.*;
import com.example.blxny.model.*;
import com.example.blxny.service.PartnerService;
import com.example.blxny.tool.DoubleUtil;
import com.example.blxny.tool.PayCommonUtil;
import com.example.blxny.tool.UploadTool;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.FileNotFoundException;
import java.math.BigDecimal;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class PartnerServiceImpl implements PartnerService {
    @Autowired
    private PartnerDao partnerDao;
    @Autowired
    private UserDao userDao;
    @Autowired
    private PartnerApplyDao partnerApplyDao;
    @Autowired
    private PartnerDeviceDao partnerDeviceDao;
    @Autowired
    private OrderDao orderDao;
    @Autowired
    private StationDao stationDao;
    @Autowired
    private DeviceDao deviceDao;

    @Override
    public boolean password(String password, String pwd) {
        String pas = MD5(password);
        if (pwd.equals(pas)) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public List<PartnerUpdate> findPartnerUpdate(String id) {
        return partnerDao.findPartnerUpdate(id);
    }

    @Override
    public int partnerUpdate(PartnerUpdate partnerUpdate) {
        partnerUpdate.setUpdateId("33100005014" + PayCommonUtil.getRandomNumber(12));//暂时无法做城市编码
        return partnerDao.addPartnerUpdate(partnerUpdate);
    }

    @Override
    public Partner findPartner(String uuid) {
        return partnerDao.findPartner(uuid);
    }

    public static String MD5(String string) {/*
        if (TextUtils.isEmpty(string)) {
            return "";
        }*/
        MessageDigest md5 = null;
        try {
            md5 = MessageDigest.getInstance("MD5");
            byte[] bytes = md5.digest(string.getBytes());
            String result = "";
            for (byte b : bytes) {
                String temp = Integer.toHexString(b & 0xff);
                if (temp.length() == 1) {
                    temp = "0" + temp;
                }
                result += temp;
            }
            return result;
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    @Override
    public String addPartner(String uid, String partnerName, String opreatorName, String userName, String partnerPwd) {
        //检测运营商名称是否重复
        int flg = partnerDao.hasOpreatorName(opreatorName);
        if (flg > 0) {
            return "运营商名称重复";
        }
        //检测uuid 是否存在
        flg = userDao.hasUserByUid(uid);
        if (flg == 0) {
            return "用户id不存在";
        }
        Partner partner = new Partner();
        partner.setUserUuid(uid);
        partner.setPartnerName(partnerName);
        partner.setOpreatorName(opreatorName);
        partner.setUserName(userName);
        //md5
        String pwdStr = String.valueOf(partnerPwd);
       /* MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        String md5pass = MD5(pwdStr, md);*/
        String md5pass = MD5(pwdStr);
        partner.setPartnerPwd(md5pass);
        //UUID partnerId
        String str = UUID.randomUUID().toString().replace("-", "").substring(0, 10);
        partner.setCreated(new Date());
        if (partnerDao.addPartner(partner) > 0) {
            return "success";
        } else {
            return "fail";
        }
    }

    @Override
    public String addPartnerApply(String uid, String partnerName, String opreatorName, String userName, String partnerPwd) {
        //是否已经是合伙人
        if (partnerDao.hasPartnerByUid(uid) > 0) {
            return "已经是合伙人";
        }
        PartnerApply partnerApply = new PartnerApply();
        partnerApply.setUserUuid(uid);
        partnerApply.setPartnerName(partnerName);
        partnerApply.setOpreatorName(opreatorName);
        partnerApply.setUserName(userName);

        String pwdStr = String.valueOf(partnerPwd);
        String md5pass = MD5(pwdStr);
      /*  MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        String md5pass = MD5(pwdStr, md);*/
        partnerApply.setPartnerPwd(md5pass);
        partnerApply.setCreated(new Date());
        partnerApply.setPass(0);
        if (partnerApplyDao.addOne(partnerApply) > 0) {
            return "success";
        } else {
            return "fail";
        }
    }

    @Override
    public String login(String uid, String partnerPwd) {
        String pwd = partnerDao.getPwdByUid(uid);
        String pwdStr = String.valueOf(partnerPwd);
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        String md5pass = MD5(pwdStr, md);
        if (pwd.equals(md5pass)) {
            return "success";
        } else {
            return "error";
        }
    }

    @Override
    public String modifyPwd(int partnerId, String newPwd) {
        String returnMessage;
        Integer hasFlg = partnerDao.hasPartnerById(partnerId);
        if (hasFlg == 0) {
            returnMessage = "用户不存在";
        } else {
            String password = String.valueOf(newPwd);
            MessageDigest md = null;
            try {
                md = MessageDigest.getInstance("MD5");
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
            String md5pass = MD5(password);
            if (partnerDao.updatePwdById(partnerId, md5pass) > 0) {
                returnMessage = "修改成功";
            } else {
                returnMessage = "修改失败";
            }
        }
        return returnMessage;
    }

    @Override
    public String modifyPayPwd(int partnerId, String payPwd) {
        String password = String.valueOf(payPwd);
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        String md5pass = MD5(password);
        if (partnerDao.updatePayPwdById(partnerId, md5pass) > 0) {
            return  partnerDao.getPwdById(partnerId);
        } else {
            return "fail";
        }
    }

    @Override
    public String bindPhone(int partnerId, String phone) {
        if (partnerDao.hasPhone(phone) <= 0) {
            return "手机号已被绑定";
        }
        if (partnerDao.updatePhone(partnerId, phone) > 0) {
            return "success";
        }
        return "处理异常";
    }

    @Override
    public Object getStations(int partnerId) {
        JSONObject jsonObject = new JSONObject();
        //通过合伙人id获取合伙人的所有驿站信息
        List<PartnerDevice> partnerDevices = partnerDeviceDao.getDevicesByPartnerId(partnerId);
        //通过合伙人id找到stationId
        List<String> stationIds = partnerDeviceDao.getStationsByPartnerId(partnerId);
        List<Map<String, Object>> stations = new ArrayList<>();
        for (String id : stationIds) {
            Map<String, Object> map = new HashMap<>();
            //通过stationId找到驿站
            String name = stationDao.getNameById(id);
            map.put("id", id);
            map.put("stationName", name);
            //得到驿站所对应的设备
            stations.add(map);
        }
        JSONArray stationsJson = JSONArray.fromObject(stations);//构建stations json数组
        jsonObject.put("station", stationsJson);
        //默认的驿站的电桩列表
        List<Map<String, Object>> devicelist = new ArrayList<>();
        String id = "";
        if (stations.size() > 0) {
            id = (String) stations.get(0).get("id");
        }
        List<PartnerDevice> devices = partnerDeviceDao.getPartnerDevices(partnerId, id);
        String isUsedDeviceId = "";

        if (devices.size() > 0) {
            for (PartnerDevice device : devices) {
                Map<String, Object> map = new HashMap<>();
                map.put("deviceId", device.getDeviceId());
                int isUsed = Integer.valueOf(deviceDao.geTypeByDeviceId(device.getDeviceId()));
                map.put("isUsed", isUsed);
                devicelist.add(map);
                if (isUsed == 1) {
                    isUsedDeviceId = device.getDeviceId();
                }
            }
        } else {
            Map<String, Object> map = new HashMap<>();
            map.put("deviceId", "");
            map.put("isUsed", "");
        }
        JSONArray devicesJson = JSONArray.fromObject(devicelist);
        jsonObject.put("device", devicesJson);
        //指定电桩
        if (devices.size() > 0) {
            if ("".equals(isUsedDeviceId)) {
                PartnerDevice d = devices.get(0);
                isUsedDeviceId = d.getDeviceId();
            }
        }
        //order的详细信息
        List<Map<String, Object>> orderlist = new ArrayList<>();
        List<Order> orders = orderDao.getPageById(isUsedDeviceId, 1, 10);
        Map<String, Object> orderMap = new HashMap<>();
        if (orders.size() > 0) {
            for (Order order : orders) {
                orderMap = new HashMap<>();
                orderMap.put("orderID", order.getOrderId());
                Date start = order.getStartTime();
                Date end = order.getEndTime();
                double time = DateUtil.between(start, end, DateUnit.MINUTE);
                if (time < 60) {
                    time = DoubleUtil.round(time / 60, 2);
                }
                orderMap.put("chargeTime", time);
                orderMap.put("chargeAmount", order.getChargeAmount());
                orderMap.put("totalAmount", order.getTotalAmount());
                orderlist.add(orderMap);
            }
        } else {
            orderMap.put("orderID", 0);
            orderMap.put("chargeTime", 0);
            orderMap.put("chargeAmount", 0);
            orderMap.put("totalAmount", 0);
            orderlist.add(orderMap);
        }
        JSONArray ordersJson = JSONArray.fromObject(orderlist);
        jsonObject.put("order", ordersJson);

        //order 月账单
        //Date lastMonth = getDateTime(DateUtil.month(new Date()));
        Date currentMonth = getCurrentDateTime();
        /* List<Order> monthOrders = orderDao.getOrder(isUsedDeviceId, lastMonth, currentMonth);*/
        int month = Integer.valueOf(new SimpleDateFormat("MM").format(new Date()));
        List<Order> monthOrders = orderDao.getOrder1(isUsedDeviceId, month);
        long totalTime = 0;//充电总时长
        float totalMeter = 0;//充电总电量
        BigDecimal totalMoney = new BigDecimal(0.0);
        for (Order order : monthOrders) {
            Date start = order.getStartTime();
            Date end = order.getEndTime();
            long min = DateUtil.between(start, end, DateUnit.MINUTE);
            totalTime += min;
            totalMeter += order.getChargeAmount();
            totalMoney = totalMoney.add(order.getTotalAmount());
        }
        JSONObject monthOrderJson = new JSONObject();
        monthOrderJson.put("totalTime", totalTime);
        monthOrderJson.put("totalMeter", totalMeter);
        monthOrderJson.put("totalMoney", totalMoney);
        jsonObject.put("monthOrder", monthOrderJson);
        return jsonObject;
    }

    @Override
    public Object getStations(int partnerId, String stationId) {
        System.out.println(stationId);
        JSONObject jsonObject = new JSONObject();
        //电桩列表
        List<Map<String, Object>> deviceList = deviceList(partnerId, stationId);
        JSONArray devices = JSONArray.fromObject(deviceList);
        jsonObject.put("devices", devices);
        //System.out.println("devices" + devices);
        //指定电桩
        List<PartnerDevice> partnerDevices = partnerDeviceDao.getPartnerDevices(partnerId, stationId);
        String isUsedDeviceId = getIsUsedDeviceId(partnerId, stationId);

        //order的详细信息
        List<Map<String, Object>> orderlist = orderList(isUsedDeviceId, 1, 10);
        JSONArray ordersJson = JSONArray.fromObject(orderlist);
        jsonObject.put("order", ordersJson);

        //order 月账单
        Date startMonth = getDateTime(DateUtil.month(new Date()));
        Date currentMonth = getCurrentDateTime();
        int months = Integer.valueOf(new SimpleDateFormat("MM").format(new Date()));
        JSONObject monthOrderJson = orderMonth(isUsedDeviceId, months, currentMonth);
        jsonObject.put("monthOrder", monthOrderJson);
        return jsonObject;
    }

    @Override
    public Object getDevices(String deviceId) {
        JSONObject jsonObject = new JSONObject();
        //order的详细信息
        List<Map<String, Object>> orderlist = orderList(deviceId, 1, 10);
        JSONArray ordersJson = JSONArray.fromObject(orderlist);
        jsonObject.put("order", ordersJson);

        //order 月账单
        //Date lastMonth = getDateTime(DateUtil.month(new Date()));
        Date currentMonth = getCurrentDateTime();
        int month = Integer.valueOf(new SimpleDateFormat("MM").format(new Date()));
        JSONObject monthOrderJson = orderMonth(deviceId, month, currentMonth);
        jsonObject.put("monthOrder", monthOrderJson);
        return jsonObject;
    }

    @Override
    public Object getCount(String deviceId, int month, int year) {
        JSONObject jsonObject = new JSONObject();
        List<Order> monthOrders = orderDao.getOrder(deviceId, month, year);
        long totalTime = 0;//充电总时长
        float totalMeter = 0;//充电总电量
        BigDecimal totalMoney = new BigDecimal(0.0);
        for (Order order : monthOrders) {
            Date start = order.getStartTime();
            Date end = order.getEndTime();
            long min = DateUtil.between(start, end, DateUnit.MINUTE);
            totalTime += min;
            totalMeter += order.getChargeAmount();
            totalMoney = totalMoney.add(order.getTotalAmount());
        }
        JSONObject monthOrderJson = new JSONObject();
        monthOrderJson.put("totalTime", totalTime);
        monthOrderJson.put("totalMeter", totalMeter);
        monthOrderJson.put("totalMoney", totalMoney);
        jsonObject.put("monthOrder", monthOrderJson);
        return jsonObject;
    }

    @Override
    public Object getPartnerApplyStatus(String uid) {
        PartnerApply partnerApply = partnerApplyDao.getApplyByUid(uid);
        JSONObject jsonObject = new JSONObject();
        if (null == partnerApply) {
            jsonObject.put("pass", "3");//未申请成为合伙人
            jsonObject.put("partnerId", "0");
        } else {
            if (partnerApply.getPass() == 1) {
                jsonObject.put("pass", "1");//审核通过
                int partnerId = partnerDao.getPartnerIdByUid(uid);
                jsonObject.put("partnerId", partnerId);//设置partnerId
            } else {
                jsonObject.put("pass", partnerApply.getPass());
                jsonObject.put("partnerId", "0");
            }
        }
        return jsonObject;
    }

    @Override
    public Object addImg(String file, String pid) throws FileNotFoundException {
        UploadTool up = new UploadTool();
        String img = up.upOne(file);
        if (img.equals("error")) {
            img = "error";
        } else {
            if (partnerDao.addImg(img, pid) <= 0) {
                img = "error";
            }
        }
        return img;
    }

    private List<Map<String, Object>> deviceList(int partnerId, String stationId) {
        List<Map<String, Object>> devicelist = new ArrayList<>();
        List<PartnerDevice> devices = partnerDeviceDao.getPartnerDevices(partnerId, stationId);
        for (PartnerDevice device : devices) {
            Map<String, Object> map = new HashMap<>();
            map.put("deviceId", device.getDeviceId());
            map.put("isUsed", device.getIsUsed());
            devicelist.add(map);
        }
        return devicelist;
    }

    private String getIsUsedDeviceId(int partnerId, String stationId) {
        List<PartnerDevice> devices = partnerDeviceDao.getPartnerDevices(partnerId, stationId);
        if (devices.size() > 0) {

            for (PartnerDevice device : devices) {
                if (device.getIsUsed() == 1) {
                    return device.getDeviceId();
                }
            }
            return devices.get(0).getDeviceId();
        } else {
            return "";
        }
    }

    private List<Map<String, Object>> orderList(String deviceId, int pageNumber, int pageSize) {
        List<Map<String, Object>> orderlist = new ArrayList<>();
        List<Order> orders = orderDao.getPageById(deviceId, pageNumber, pageSize);
        for (Order order : orders) {
            Map<String, Object> map = new HashMap<>();
            map.put("orderID", order.getOrderId());
            Date start = order.getStartTime();
            Date end = order.getEndTime();
            map.put("chargeTime", DateUtil.between(start, end, DateUnit.MINUTE));
            map.put("chargeAmount", order.getChargeAmount());
            map.put("totalAmount", order.getTotalAmount());
            orderlist.add(map);
        }
        return orderlist;
    }

    private JSONObject orderMonth(String deviceId, int startMonth, Date endMonth) {
        List<Order> monthOrders = orderDao.getOrder1(deviceId, startMonth);
        long totalTime = 0;//充电总时长
        float totalMeter = 0;//充电总电量
        BigDecimal totalMoney = new BigDecimal(0.0);
        for (Order order : monthOrders) {
            Date start = order.getStartTime();
            Date end = order.getEndTime();
            long min = DateUtil.between(start, end, DateUnit.MINUTE);
            totalTime += min;
            totalMeter += order.getChargeAmount();
            totalMoney = totalMoney.add(order.getTotalAmount());
        }
        JSONObject monthOrderJson = new JSONObject();
        monthOrderJson.put("totalTime", totalTime);
        monthOrderJson.put("totalMeter", totalMeter);
        monthOrderJson.put("totalMoney", totalMoney);
        return monthOrderJson;
    }


    //---------------util
    private static Date getDateTime(int month) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.MONTH, month - 1);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        //calstar.set(Calendar.DAY_OF_MONTH, 0);//最后一天
        return calendar.getTime();
    }

    private static Date getCurrentDateTime() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DATE, 1);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }

    public static String MD5(String strSrc, MessageDigest md) {
        byte[] bt = strSrc.getBytes();
        md.update(bt);
        String strDes = bytes2Hex(md.digest());
        return strDes;
    }

    private static String bytes2Hex(byte[] bts) {
        StringBuffer des = new StringBuffer();
        String tmp = null;
        for (int i = 0; i < bts.length; i++) {
            tmp = (Integer.toHexString(bts[i] & 0xFF));
            if (tmp.length() == 1) {
                des.append("0");
            }
            des.append(tmp);
        }
        return des.toString();
    }

}
