package com.example.blxny.service.impl;

import com.example.blxny.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TestServiceImpl {
    public String test() {
        SocketThread socketThread = (SocketThread) ThreadUtil.getThreadByName("9010");

        if (null==socketThread){
            //写一个socket连接 并加入线程池
            return "连接已经被断开,正在重连";
        }

        String hexStr02 = "82 " + "00 00 " + "00 0D " + substrs("1120190411000005");
        byte[] serverStr02 = CRC16.hexToByte(hexStr02.replace(" ", ""));
        String serverstrCrc02 = HexConvert.BinaryToHexString(CRC16.GetCRC(serverStr02));
        String hexserverStr02 = "68 " + HexConvert.BinaryToHexString(serverStr02) + serverstrCrc02;
        System.out.println(socketThread.getName());
        MQUtil.putMsg(hexserverStr02, "");//设置消息key
        String result = null;
        int currentWaitTimes = 0;
        //有回复才会或者超过等待时间会跳出
        while (null == result || "".equals(result)) {
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            result = MQUtil.getMsg().get(hexserverStr02);
            //这里面写超时处理
            if (currentWaitTimes >= 5) {
                System.out.println("超时处理");
                return "超时";
            }
        }
        return result;
    }

    public static String substrs(String replace) {
        String regex = "(.{2})";
        replace = replace.replaceAll(regex, "$1 ");
        return replace;
    }
}
