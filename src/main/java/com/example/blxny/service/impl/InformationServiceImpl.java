package com.example.blxny.service.impl;

import com.example.blxny.dao.UserDao;
import com.example.blxny.model.Information;
import com.example.blxny.service.InformationService;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class InformationServiceImpl implements InformationService {
    @Autowired
    private UserDao dao;


    @Override
    public List findInformation(String uuid, String code) {
        dao.adOverTime();//先清空过期的系统消息
        List<Information> infor = dao.findInformation(uuid, Integer.valueOf(code) * 10);
        List returnMassage = new ArrayList();
        for (Information info : infor) {
            JSONObject js = new JSONObject();
            js.put("up", info.getTitle());
            js.put("body", info.getInfo());
            js.put("type", info.getType());
            Date date = new Date();
            String returnTime = null;
            String infoYear = new SimpleDateFormat("yyyy").format(info.getTime());
            String year = new SimpleDateFormat("yyyy").format(date);
            String infoMon = new SimpleDateFormat("MM-dd").format(info.getTime());
            String mon = new SimpleDateFormat("MM-dd").format(date);
            if (infoYear.equals(year)) {
                if (infoMon.equals(mon)) {
                    returnTime = "今天 "+new SimpleDateFormat("HH:ss").format(info.getTime());
                } else {
                    returnTime = infoMon;
                }
            } else {
                returnTime = infoYear;
            }
            js.put("time", returnTime);
            returnMassage.add(js);
            dao.changeAllInformation(uuid);
        }
        return returnMassage;
    }

    @Override
    public int informationClean(String uuid) {
        System.out.println("uuid"+uuid);
        System.out.println(dao.informationClean(uuid));
        return dao.informationClean(uuid);
    }

    @Override
    public String didHaveInfo(String uuid) {
        return null;
    }
}
