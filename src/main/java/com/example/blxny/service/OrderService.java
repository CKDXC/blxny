package com.example.blxny.service;

import com.example.blxny.model.Appointment;
import com.example.blxny.model.Order;
import java.util.List;

public interface OrderService {
    Object findAllOrderInfor(String id);

    String changeMonththree(String uuid);

    Object findorder(String useruuid,String code);

    Object findorderIng(String useruuid);

    String quxiao(String id);

    boolean addOne(Order order);

    boolean addOrder(Order order);

    List<Order> getPageByUid(int uid, int orderType, int pageNumber, int pageSize);

    Object addAppointment(String uid,String time, String device,String much,String parType);

    List<Appointment> findAppo(String number,String uuid);

    List<Appointment> findAppoOne(String uuid);

    String cleanAppo(String id);

    String isAppo(String uuid);
}
