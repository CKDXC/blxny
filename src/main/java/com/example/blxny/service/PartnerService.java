package com.example.blxny.service;

import com.example.blxny.model.Partner;
import com.example.blxny.model.PartnerUpdate;

import java.io.FileNotFoundException;
import java.util.List;

public interface PartnerService {
    boolean password(String password,String pwd);

    List<PartnerUpdate> findPartnerUpdate(String id);

    int partnerUpdate(PartnerUpdate partnerUpdate);

    Partner findPartner(String uuid);

    String addPartner(String uid, String partnerName, String opreatorName, String userName, String partnerPwd);

    String addPartnerApply(String uid, String partnerName, String opreatorName, String userName, String partnerPwd);

    String login(String uid, String partnerPwd);

    String modifyPwd(int partnerId, String newPwd);

    String modifyPayPwd(int partnerId, String payPwd);

    //获取合伙人所拥有的驿站
    Object getStations(int partnerId);

    //绑定手机号
    String bindPhone(int partnerId, String phone);

    //切换驿站,返回页面信息
    Object getStations(int partnerId, String stationId);

    //切换设备
    Object getDevices(String deviceId);

    //切换月份
    Object getCount(String deviceId, int month ,int year);

    //获取申请情况
    Object getPartnerApplyStatus(String uid);

    //添加头像
    Object addImg(String file,String pid) throws FileNotFoundException;
}
