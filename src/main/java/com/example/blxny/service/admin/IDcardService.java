package com.example.blxny.service.admin;

public interface IDcardService {
    /**查询所有身份证审批申请*/
    Object findAllIdcardApproval(Integer type,Integer size,Integer number);

    /**对一个用户的身份证进行审批*/
    Object ApprovalIdCard(String uuid,int type);

}
