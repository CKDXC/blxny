package com.example.blxny.service.admin;

import com.example.blxny.model.Coupon;
import com.example.blxny.model.PartnerApply;
import com.github.pagehelper.PageInfo;

import java.util.List;

/*
 *项目名: blxny
 *文件名: CouponService
 *创建者: SCH
 *创建时间:2019/5/14 9:35
 *描述: TODO
 */
public interface CouponService {
    //默认获取指定 页码 和 页数 按时间排序 的 PartnerApply
    List<Coupon> list(int pageNumber, int pageSize);
    PageInfo findPage(int pageNumber, int pageSize);
}
