package com.example.blxny.service.admin;

import com.example.blxny.model.Device;
import com.example.blxny.model.vo.DeviceVO;

import java.util.List;

public interface DeviceService {

    /*** 设备信息查询*/
    List<Device> adminFindAllDevice(int pageSize,int pageNumber);

    /** 驿站内新增设备*/
    int addDevice(DeviceVO device);

    //修改设备信息
    String updateDevice(DeviceVO deviceVo);

}
