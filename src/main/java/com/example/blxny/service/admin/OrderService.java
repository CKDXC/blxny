package com.example.blxny.service.admin;

public interface OrderService {
    /**
     * 查询所有订单
     */
    Object findAllOrderAdmin(int number, int size);

    /**
     * 通过桩查询订单
     */
    Object findDeviceOrder(int number, int size, String id);

}
