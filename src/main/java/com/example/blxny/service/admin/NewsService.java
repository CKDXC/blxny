package com.example.blxny.service.admin;

public interface NewsService {
    /**
     * 刷新一次新闻
     */
    boolean flashNewS() throws InterruptedException;

    /**
     * 新建一个新闻
     */
    String newNews(String time);

    /**
     * 删除一个新闻
     */
    String delNews();
    /**
     * 查询一个新闻
     */
    String getNews();
}
