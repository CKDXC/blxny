package com.example.blxny.service.admin;

import com.example.blxny.model.Partner;
import com.example.blxny.model.PartnerUpdate;
import com.github.pagehelper.PageInfo;

import java.util.List;

public interface partnerUpdateService {
    //查询合伙人驿站扩建申请单
    PageInfo<PartnerUpdate> findPartnerUpdate(Integer number,Integer type);
    //修改合伙人驿站扩建状态
    Object changPartnerUpdate(Integer type,String updateId);
}
