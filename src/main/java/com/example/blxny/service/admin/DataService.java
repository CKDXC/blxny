package com.example.blxny.service.admin;

import java.util.List;

public interface DataService {
    /**
     * 获取城市驿站数量
     */
    List getCityStationNumber();

    /**度数与时间关系ZY*/
    List getSettlement(String city, String start, String end);
}
