package com.example.blxny.service.admin;
/*
 *项目名: blxny
 *文件名: PartnerApplyService
 *创建者: SCH
 *创建时间:2019/5/6 15:08
 *描述: TODO
 */

import com.example.blxny.model.PartnerApply;
import com.github.pagehelper.PageInfo;

import java.util.List;

public interface PartnerApplyService {
    //默认获取指定 页码 和 页数 按时间排序 的 PartnerApply
    List<PartnerApply> list(int pageNumber, int pageSize);
    PageInfo findPage(int pageNumber, int pageSize);
    //合伙人申请审核通过
    String updatePartnerAppoly(String id, int state,int type);
}
