package com.example.blxny.service.admin;

import com.example.blxny.model.Rule;
import com.example.blxny.model.Station;
import org.springframework.stereotype.Service;

import java.util.List;

public interface StationService {
    /*** 驿站信息查询*/
    List<Station> adminFindAllStation(int pageSize, int pageNumber);

    /**新增一个空驿站*/
    Object adminAddNewStation(Station station);

    /**修改一个驿站基本信息*/
    Object updateBasicStationInfo(Station station);

}
