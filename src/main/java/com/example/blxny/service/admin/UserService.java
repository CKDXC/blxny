package com.example.blxny.service.admin;
/*
 *项目名: blxny
 *文件名: UserService
 *创建者: SCH
 *创建时间:2019/5/7 9:24
 *描述: TODO
 */

import com.github.pagehelper.PageInfo;

public interface UserService {
    //默认获取指定 页码 和 页数 按时间排序 的 User
    PageInfo findPage(int pageNumber, int pageSize);
    //master的权限赋予
    Object authorization(String master,String gave,Integer level);
}
