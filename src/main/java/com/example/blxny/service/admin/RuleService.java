package com.example.blxny.service.admin;

import com.example.blxny.model.Rule;
import com.example.blxny.model.vo.RuleVo;
import com.github.pagehelper.PageInfo;

import java.util.List;

/*
 *项目名: blxny
 *文件名: RuleService
 *创建者: SCH
 *创建时间:2019/5/14 10:42
 *描述: TODO
 */
public interface RuleService {
    //分页
    List<Rule> list(int pageNumber, int pageSize);
    PageInfo<Rule> findPage(int pageNumber, int pageSize);
    /**
     * 新建驿站后添加新的计费规则
     */
    Object addNewRule(RuleVo vo);
    /**
     * 修改计费规则
     */
    Object changeRule(int type,double number,String id,String type2);
}