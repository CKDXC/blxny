package com.example.blxny.service.admin;

import com.example.blxny.model.vo.UserSysRoleVO;
import com.github.pagehelper.PageInfo;

/*
 *项目名: blxny
 *文件名: SysUserService
 *创建者: SCH
 *创建时间:2019/5/7 11:26
 *描述: TODO
 */
public interface SysUserService {
    //默认获取指定 页码 和 页数 按时间排序 的 User
    PageInfo<UserSysRoleVO> findPage(int pageNumber, int pageSize);
    //修改管理员密码
    String updatePwd(String uid, String oldPwd, String newPwd);
}
