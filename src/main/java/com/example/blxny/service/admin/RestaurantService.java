package com.example.blxny.service.admin;

import com.example.blxny.model.CateringProduct;
import com.example.blxny.model.Hotel;
import com.example.blxny.model.Restaurant;
import com.example.blxny.model.dto.CateringProductDTO;
import com.github.pagehelper.PageInfo;
import com.example.blxny.model.Shop;

import java.io.FileNotFoundException;
import java.util.List;

public interface RestaurantService {
    //下架一个店铺
    boolean delList(int id);

    //修改积分兑换率
    boolean change(String code);

    //查看积分兑换率
    String get();

    //新增店铺
    String addNewStore(Restaurant restaurant) throws FileNotFoundException;

    //修改店铺信息
    String changeStore(Restaurant restaurant);

    //根据restaurant_id获取
    PageInfo<CateringProduct> findPage(int restaurantId, int pageNumber, int pageSize);

    //新增菜品
    String addCateringProduct(CateringProduct cateringProduct);

    //修改菜品
    String updateCateringProduct(CateringProductDTO cateringProductDTO);

    //查询酒店信息通过酒店id
    List<Hotel> listHotel(String message);

    //通过id新增一个酒店的图片
    String addHotelImg(String img, String info, int id) throws FileNotFoundException;

    //上架一个商店商品
    String addNewShop(Shop shop,int id) throws FileNotFoundException;

    //下架一个商品
    String delShop(int id);
}
