package com.example.blxny.service.admin;

import java.io.FileNotFoundException;

public interface UpdateVersionService {
    //获取当前最新版本信息
    Object updateVersion();
    //删除一个版本
    Object delVersion(String id) throws FileNotFoundException;
}
