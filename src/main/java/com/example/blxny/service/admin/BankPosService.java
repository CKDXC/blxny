package com.example.blxny.service.admin;

import com.example.blxny.model.vo.BankPosVO;
import com.github.pagehelper.PageInfo;

import java.util.List;

public interface BankPosService {
    //查询提现申请
    PageInfo<BankPosVO> findAllBankPos(Integer pageNumber,Integer type,String phone);
    //提现审批
    String sureBankPos(String posId);
}
