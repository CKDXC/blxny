package com.example.blxny.service.admin;

import com.example.blxny.model.Partner;
import com.github.pagehelper.PageInfo;

/*
 *项目名: blxny
 *文件名: PartnerService
 *创建者: SCH
 *创建时间:2019/5/7 10:24
 *描述: TODO
 */
public interface PartnerService {
    //默认获取指定 页码 和 页数 按时间排序 的 User
    PageInfo<Partner> findPage(int pageNumber, int pageSize);
}
