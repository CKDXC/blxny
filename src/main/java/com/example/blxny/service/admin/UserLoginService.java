package com.example.blxny.service.admin;

import net.sf.json.JSONObject;

public interface UserLoginService {

    /*** 权限用户admin 登录*/
    JSONObject AdminUserLogin(String user, String password);
}
