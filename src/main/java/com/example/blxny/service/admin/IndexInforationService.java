package com.example.blxny.service.admin;

import org.springframework.stereotype.Service;


public interface IndexInforationService {
    /**发布一个系统消息*/
    String addNewInformation(String info,String time);
    /**下架一个系统消息*/
    int oneInformationDown(int id);
    /**获取系统消息*/
    Object getAdminInforMation(int size,int number);
    /**系统极光推送消息*/
    String AdminJPinformation(String info);
}
