package com.example.blxny.service.admin;

import com.example.blxny.model.Ad;

import java.io.FileNotFoundException;

public interface AdService {
    //新增一个智行广告
    Object addNewAD(String file, Ad ad) throws FileNotFoundException;

    //删除一个广告
    String delAD(int id);
}
