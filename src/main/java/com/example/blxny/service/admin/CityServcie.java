package com.example.blxny.service.admin;

import com.example.blxny.model.City;

public interface CityServcie {
    /**添加新的城市*/
    Object AddNewCity(City city);

    /**通过id删除一个城市*/
    int delCity(String id);
}
