package com.example.blxny.service;

import java.net.Socket;

public interface ChargeService {
    //扫码
    String sweetCode(String deviceId,int gunNo);
    //远程启动充电--S-C
    Object startCharge(String deivceId,String gunNo,String chargeType,int chargeData,String uid);
    //电桩上传充电数据--C-S
    void upChargeData(String resContent);
    //获取充电数据--从redis数据库获取
    Object getChargeData(String orderId);
    //电桩上传充电记录--C-S
    int upChargeLog(String resContent);
    //远程断电--S-C
    Object endCharge(String orderId,String gunNo);
    //电桩设置计费规则--S-C
    String ruleSetToDevice(String deviceId,int ruleId);
    //电桩设置计费规则--S-C
    void ruleSetToDevice(Socket socket, String deviceId, String deviceType);
    //处理计费规则设置结果
    void dealRuleSet(String response,String address);
    //电桩告警--C-S
    void alarmDevice(String resContent);
    //电桩校时--S-C
    String proofTime(String deviceId);
    //电桩校时
    void proofTimeToDevice(Socket socket);
    //电桩远程升级--S-C
    String upgrade(String deviceId, int versionId);
    //电桩登录服务器--C-S
    String login(String reqContent);
    //电桩请求设置二维码--C-S
    String setQRcode(String reqContent);
    //电桩是否在数据库上记录登录状态
    boolean isLogin(String address);
    //电桩登录时,在数据库记录下电桩的通信地址
    boolean setAddress(String deviceId, String address);
    //远程重启电桩--C-S
    String reStart(String deviceId);
    //添加预约号
    Object addAppointment(String uid, String time, String deviceId, String much,String payType);
    //取消预约
    String cancelAppointment(String appoId);
}
