package com.example.blxny.service;

import com.example.blxny.model.Ad;
import com.example.blxny.model.Service;

import java.text.ParseException;
import java.util.List;

public interface FindSalesService {
    String findOrderInfo(String user);

    List<Ad> adEndless();

    Object findSales(String lat,String lng);

    int addService(Service service);

    int changeSalesType(String type,String id);

    String isSales(String uuid);

    String changeSalesTypeNow(String uuid,int type,String JPid,String lng,String lat);

    Object oderInfo(String uuid,String type,String lat,String lng);

    Object findSalesForUs(String beginTime, String endTime, String money, String carCard, String phone,  String serviceInfo, String other,
                          String stationId,String  useruuid,String car) throws ParseException;

    Object fate(String uuid,String id);

    Object findSalesEndliss(String uuid,String id);

    /**
     * webSocket
     */
    //查看他现在的上班状态
    String findTypeNow(String uuid);

    //业务员看现在的订单
    String findFateOrder(String uuid);
}
