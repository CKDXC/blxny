package com.example.blxny.service;

import com.example.blxny.model.*;

import java.util.List;
import java.util.Map;

public interface UserLoginService {
    Object update(String version);

    List news();

    boolean isHavaBankCard(String number,String where);

    List<Bank> fingAllBank(String uuid,String where);

    int closeBankCard(String number,String uuid,String where);

    int addBank(Bank bank);

    int didHaveInfo(String uuid,String jpid);

    List<String> findCity(String pro,String city);

    String addMoneyToUser(String money,String uuid);

    int closeOtherLogin(String code,String uuid);

    int changeOtherUid(String uuid,String phone);

    User wxRegWithFhone(String nickname,String img,String uuid,String phone);

    User ZFRegWithFhone(String uuid,String phone);

    User  wxlogin(String uuid);

    User zfblogin(String uuid);

    String insertCar(String vin,String card,String year,String uuid);

    Car findCar(String uuid);

    int changepaypassword(String password,String uuid);

    String changePhone(String phone,String useruuid);

    IDcard findrealman(String useruuid);

    String newIdCard(IDcard iDcard);

    String verification(String phone);

    String reguster(String phone,String password);

    User FindUserAll(String phone, String password);

    User FindUserAllByUuid(String phone);

    String modifyPwd(String uid, String newPwd);

    String checkPhone(String phone);

    User PhoneLogin(String phone);

    int newUserInfor(String address,String lat,String lng,String homepro,String homecity,String phone);

    int workInfor(String address,String lat,String lng,String workpro,String workcity,String phone);

    String modifyNickNmae(String uid, String newName);

    Map<String,Object> getUserCarInfo(int uid);

    List<Station> getStationByUid(String uid);

    String removeStationCollected(String uid, String stationCollected);

    String addCollected(String uid, String stationCollected);

    int upphoto(String img,String uuid);

    Object findOrderNutPay(String uuid);

    List findDeviceType(String station,String code);

    boolean addUpdate(String url,String info,String version);

    String payOver(String id,String money);

    int addgold(String uid,String type,String money);

    String pos(String uuid,String money,String card);
}
