package com.example.blxny.service;

import com.example.blxny.model.Information;

import java.util.List;

public interface InformationService {
        List findInformation(String uuid,String code);

        int informationClean(String uuid);

        String didHaveInfo(String uuid);

}
