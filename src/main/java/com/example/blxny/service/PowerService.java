package com.example.blxny.service;

import com.example.blxny.model.Charging;
import com.example.blxny.model.Device;

import java.util.List;

public interface PowerService {
    Object findGPS(String id);

    Object  powerWorking(String type,String usetime,String usepower,String usemuch,String uuid);

    Object findOtherPower(String uuid,String code,String lat,String lng);

    Object getAllPower(String uuid,String lat,String lng,String city);

    Object findlowpower(String id);

    Object findfastpower(String id);

    Object findPowerPay(String id);

    Object findPay(String type, String station);

    Object endPower(String orderid);

    Object getPowerNum(String id,String num);
}
