package com.example.blxny.service;

import com.example.blxny.model.Coupon;
import com.example.blxny.model.ScoreProduct;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public interface CouponService {

    //获取代金卷
    List<Coupon> getCoupon(String uid,int type, int isUsed);

    //兑换
    String exchange(String uid, int scoreProductId);

    //获取每日积分商品信息
    List<ScoreProduct> getScoreProducts();

    Object getGold(String uid,
                   String money,
                   String cold,
                   String type,
                   String day);
    Object getGoldUse(String uuid);

    String cleanGoldUse(String uuid);
}
