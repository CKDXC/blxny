package com.example.blxny.model;

public class Car {
    private int id;
    private String vintage;
    private String card;
    private int year;
    private String uuid;

    @Override
    public String toString() {
        return "Car{" +
                "id=" + id +
                ", vintage='" + vintage + '\'' +
                ", card='" + card + '\'' +
                ", year=" + year +
                ", uuid='" + uuid + '\'' +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getVintage() {
        return vintage;
    }

    public void setVintage(String vintage) {
        this.vintage = vintage;
    }

    public String getCard() {
        return card;
    }

    public void setCard(String card) {
        this.card = card;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
}
