package com.example.blxny.model;

import java.util.Date;

//预约表
public class Appointment {
    private int id;
    private int type;
    private String device;
    private String deviceType;
    private String name;
    private Date beginTime;
    private Date endTime;
    private String useruuid;
    private String appoUid;
    private String appoCode;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(Date beginTime) {
        this.beginTime = beginTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String getUseruuid() {
        return useruuid;
    }

    public void setUseruuid(String useruuid) {
        this.useruuid = useruuid;
    }

    public String getAppoUid() {
        return appoUid;
    }

    public void setAppoUid(String appoUid) {
        this.appoUid = appoUid;
    }

    public String getAppoCode() {
        return appoCode;
    }

    public void setAppoCode(String appoCode) {
        this.appoCode = appoCode;
    }

    @Override
    public String toString() {
        return "Appointment{" +
                "id=" + id +
                ", type=" + type +
                ", device='" + device + '\'' +
                ", deviceType='" + deviceType + '\'' +
                ", name='" + name + '\'' +
                ", beginTime=" + beginTime +
                ", endTime=" + endTime +
                ", useruuid='" + useruuid + '\'' +
                ", appoUid='" + appoUid + '\'' +
                ", appoCode='" + appoCode + '\'' +
                '}';
    }
}