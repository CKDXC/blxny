package com.example.blxny.model;

import java.util.Date;

public class PartnerUpdate {
    private Integer id;
    private String username;
    private String phone;
    private String workname;
    private String workaddress;
    private String type;
    private Integer number;
    private String useruuid;
    private String  partnerid;
    private String power;
    private Date time;
    private String updateId;
    private Integer updateType;

    @Override
    public String toString() {
        return "PartnerUpdate{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", phone='" + phone + '\'' +
                ", workname='" + workname + '\'' +
                ", workaddress='" + workaddress + '\'' +
                ", type='" + type + '\'' +
                ", number=" + number +
                ", useruuid='" + useruuid + '\'' +
                ", partnerid='" + partnerid + '\'' +
                ", power='" + power + '\'' +
                ", time=" + time +
                ", updateId='" + updateId + '\'' +
                ", updateType=" + updateType +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getWorkname() {
        return workname;
    }

    public void setWorkname(String workname) {
        this.workname = workname;
    }

    public String getWorkaddress() {
        return workaddress;
    }

    public void setWorkaddress(String workaddress) {
        this.workaddress = workaddress;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public String getUseruuid() {
        return useruuid;
    }

    public void setUseruuid(String useruuid) {
        this.useruuid = useruuid;
    }

    public String getPartnerid() {
        return partnerid;
    }

    public void setPartnerid(String partnerid) {
        this.partnerid = partnerid;
    }

    public String getPower() {
        return power;
    }

    public void setPower(String power) {
        this.power = power;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public String getUpdateId() {
        return updateId;
    }

    public void setUpdateId(String updateId) {
        this.updateId = updateId;
    }

    public Integer getUpdateType() {
        return updateType;
    }

    public void setUpdateType(Integer updateType) {
        this.updateType = updateType;
    }
}
