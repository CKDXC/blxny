package com.example.blxny.model;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;
@Data
public class SalesOder {
    private Integer id;
    private String salesId;//订单号*
    private Date salesTime;//订单时间（右上角）*
    private Date beginTime;//开始时间*
    private Date endTime;//结束时间*
    private BigDecimal money;//多少钱*
    private String carCard;//车牌*
    private String phone;//电话*
    private Integer serviceType;//服务类型 1自助 2增值*
    private Integer bill;//发票，0不要1要*
    private Integer serviceInfo;//服务项目，1汽车保养2随带充*
    private String other;//订单备注*
    private String stationId;//驿站号*
    private String useruuid;//用户id*
    private String worker;//业务员id
    private Integer type;//0生效订单1被取消的订单2失效订单
    private String car;//车辆信息


}
