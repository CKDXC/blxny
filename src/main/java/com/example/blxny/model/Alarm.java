package com.example.blxny.model;

import java.io.Serializable;

public class Alarm implements Serializable {
    private int id;
    private int gunNo;
    private String code;
    private String alarmTime;
    private int status;
    private double alarmValue;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getGunNo() {
        return gunNo;
    }

    public void setGunNo(int gunNo) {
        this.gunNo = gunNo;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getAlarmTime() {
        return alarmTime;
    }

    public void setAlarmTime(String alarmTime) {
        this.alarmTime = alarmTime;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public double getAlarmValue() {
        return alarmValue;
    }

    public void setAlarmValue(double alarmValue) {
        this.alarmValue = alarmValue;
    }

    @Override
    public String toString() {
        return "Alarm{" +
                "id=" + id +
                ", gunNo=" + gunNo +
                ", code='" + code + '\'' +
                ", alarmTime='" + alarmTime + '\'' +
                ", status=" + status +
                ", alarmValue=" + alarmValue +
                '}';
    }
}
