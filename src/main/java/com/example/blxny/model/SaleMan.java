package com.example.blxny.model;

public class SaleMan {
    private Integer id;
    private Integer type;
    private String lat;
    private String lng;
    private String phone;
    private String seleid;
    private String serviceOffice;
    private String useruuid;
    public SaleMan(int id, String seleid){
        super();
        this.id = id;
        this.seleid = seleid;
    }
    @Override
    public String toString() {
        return "SaleMan{" +
                "id=" + id +
                ", type=" + type +
                ", lat='" + lat + '\'' +
                ", lng='" + lng + '\'' +
                ", phone='" + phone + '\'' +
                ", seleid='" + seleid + '\'' +
                ", serviceOffice='" + serviceOffice + '\'' +
                ", useruuid='" + useruuid + '\'' +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getSeleid() {
        return seleid;
    }

    public void setSeleid(String seleid) {
        this.seleid = seleid;
    }

    public String getServiceOffice() {
        return serviceOffice;
    }

    public void setServiceOffice(String serviceOffice) {
        this.serviceOffice = serviceOffice;
    }

    public String getUseruuid() {
        return useruuid;
    }

    public void setUseruuid(String useruuid) {
        this.useruuid = useruuid;
    }
}
