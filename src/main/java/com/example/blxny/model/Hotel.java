package com.example.blxny.model;

import lombok.Data;

import java.util.Date;

@Data
public class Hotel {
    private Integer id;
    private Integer restaurantId;
    private String img;
    private String imgInfo;
    private Date created;
}
