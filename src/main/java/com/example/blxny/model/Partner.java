package com.example.blxny.model;

import lombok.Data;

import java.util.Date;
@Data
public class Partner {

    private int id;
    private int partnerId;
    private String partnerName;
    private String opreatorName;
    private String userName;
    private String partnerPwd;
    private String userUuid;
    private Date created;
    private String payPwd;
    private String phone;
    private String img;
    private Double money;
    private Integer type;


}