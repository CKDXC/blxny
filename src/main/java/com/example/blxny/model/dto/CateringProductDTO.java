package com.example.blxny.model.dto;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.math.BigDecimal;

/*
 *项目名: blxny
 *文件名: CateringProductDTO
 *创建者: SCH
 *创建时间:2019/5/31 16:57
 *描述: TODO
 */
@Data
public class CateringProductDTO {

        private Integer id;
        private String name;
        private String img;
        private BigDecimal price;
        private Integer type;
        private Integer sort;
        private Integer restaurantId;
        private Integer from;
}
