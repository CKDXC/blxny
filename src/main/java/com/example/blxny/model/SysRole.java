package com.example.blxny.model;

import lombok.Data;

import java.io.Serializable;

/**
 * admin权限
 */
@Data
public class SysRole implements Serializable {
    private Integer id;
    private String userUuid;
    private Integer power;
    private String password;

}
