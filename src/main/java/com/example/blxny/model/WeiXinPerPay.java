package com.example.blxny.model;

public class WeiXinPerPay {
        private String AppId;
        private String MchId;
        private String TimeStamp;
        private String NonceStr;
        private String PrepayId;
        private String SignType;
        private String PaySign;

    @Override
    public String toString() {
        return "WeiXinPerPay{" +
                "AppId='" + AppId + '\'' +
                ", MchId='" + MchId + '\'' +
                ", TimeStamp='" + TimeStamp + '\'' +
                ", NonceStr='" + NonceStr + '\'' +
                ", PrepayId='" + PrepayId + '\'' +
                ", SignType='" + SignType + '\'' +
                ", PaySign='" + PaySign + '\'' +
                '}';
    }

    public String getAppId() {
        return AppId;
    }

    public void setAppId(String appId) {
        AppId = appId;
    }

    public String getMchId() {
        return MchId;
    }

    public void setMchId(String mchId) {
        MchId = mchId;
    }

    public String getTimeStamp() {
        return TimeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        TimeStamp = timeStamp;
    }

    public String getNonceStr() {
        return NonceStr;
    }

    public void setNonceStr(String nonceStr) {
        NonceStr = nonceStr;
    }

    public String getPrepayId() {
        return PrepayId;
    }

    public void setPrepayId(String prepayId) {
        PrepayId = prepayId;
    }

    public String getSignType() {
        return SignType;
    }

    public void setSignType(String signType) {
        SignType = signType;
    }

    public String getPaySign() {
        return PaySign;
    }

    public void setPaySign(String paySign) {
        PaySign = paySign;
    }
}
