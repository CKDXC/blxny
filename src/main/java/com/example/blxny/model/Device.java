package com.example.blxny.model;

import java.math.BigDecimal;

public class Device {
    private int id;
    private String deviceId;
    private String name;
    private String state;
    private int port;
    private String stationId;
    private int chargeType;
    private int chargeTimes;
    private long chargeTime;
    private double meterAmout;
    private BigDecimal chargeMoney;
    private int isShoot;
    private int isLogin;
    private String pwd;
    private String ip;
    private String voltage;
    private String electric;
    private String power;
    private String clientAddress;
    private String powerRating;
    private String ratedVoltage;
    private String currentRated;

    @Override
    public String toString() {
        return "Device{" +
                "id=" + id +
                ", deviceId='" + deviceId + '\'' +
                ", name='" + name + '\'' +
                ", state='" + state + '\'' +
                ", port=" + port +
                ", stationId='" + stationId + '\'' +
                ", chargeType=" + chargeType +
                ", chargeTimes=" + chargeTimes +
                ", chargeTime=" + chargeTime +
                ", meterAmout=" + meterAmout +
                ", chargeMoney=" + chargeMoney +
                ", isShoot=" + isShoot +
                ", isLogin=" + isLogin +
                ", pwd='" + pwd + '\'' +
                ", ip='" + ip + '\'' +
                ", voltage='" + voltage + '\'' +
                ", electric='" + electric + '\'' +
                ", power='" + power + '\'' +
                ", clientAddress='" + clientAddress + '\'' +
                ", powerRating='" + powerRating + '\'' +
                ", ratedVoltage='" + ratedVoltage + '\'' +
                ", currentRated='" + currentRated + '\'' +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getStationId() {
        return stationId;
    }

    public void setStationId(String stationId) {
        this.stationId = stationId;
    }

    public int getChargeType() {
        return chargeType;
    }

    public void setChargeType(int chargeType) {
        this.chargeType = chargeType;
    }

    public int getChargeTimes() {
        return chargeTimes;
    }

    public void setChargeTimes(int chargeTimes) {
        this.chargeTimes = chargeTimes;
    }

    public long getChargeTime() {
        return chargeTime;
    }

    public void setChargeTime(long chargeTime) {
        this.chargeTime = chargeTime;
    }

    public double getMeterAmout() {
        return meterAmout;
    }

    public void setMeterAmout(double meterAmout) {
        this.meterAmout = meterAmout;
    }

    public BigDecimal getChargeMoney() {
        return chargeMoney;
    }

    public void setChargeMoney(BigDecimal chargeMoney) {
        this.chargeMoney = chargeMoney;
    }

    public int getIsShoot() {
        return isShoot;
    }

    public void setIsShoot(int isShoot) {
        this.isShoot = isShoot;
    }

    public int getIsLogin() {
        return isLogin;
    }

    public void setIsLogin(int isLogin) {
        this.isLogin = isLogin;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getVoltage() {
        return voltage;
    }

    public void setVoltage(String voltage) {
        this.voltage = voltage;
    }

    public String getElectric() {
        return electric;
    }

    public void setElectric(String electric) {
        this.electric = electric;
    }

    public String getPower() {
        return power;
    }

    public void setPower(String power) {
        this.power = power;
    }

    public String getClientAddress() {
        return clientAddress;
    }

    public void setClientAddress(String clientAddress) {
        this.clientAddress = clientAddress;
    }

    public String getPowerRating() {
        return powerRating;
    }

    public void setPowerRating(String powerRating) {
        this.powerRating = powerRating;
    }

    public String getRatedVoltage() {
        return ratedVoltage;
    }

    public void setRatedVoltage(String ratedVoltage) {
        this.ratedVoltage = ratedVoltage;
    }

    public String getCurrentRated() {
        return currentRated;
    }

    public void setCurrentRated(String currentRated) {
        this.currentRated = currentRated;
    }
}