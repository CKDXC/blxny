package com.example.blxny.model;

public class Service {
    private int id;
    private String userUuid;
    private String userPhone;
    private String userName;
    private String serviceStation;
    private String serviceAddress;
    private String service;
    private String serviceOffice;
    private String salesId;
    private String serviceType;
    private Integer invoice;
    private String money;
    private String serviceId;

    @Override
    public String toString() {
        return "Service{" +
                "id=" + id +
                ", userUuid='" + userUuid + '\'' +
                ", userPhone='" + userPhone + '\'' +
                ", userName='" + userName + '\'' +
                ", serviceStation='" + serviceStation + '\'' +
                ", serviceAddress='" + serviceAddress + '\'' +
                ", service='" + service + '\'' +
                ", serviceOffice='" + serviceOffice + '\'' +
                ", salesId='" + salesId + '\'' +
                ", serviceType='" + serviceType + '\'' +
                ", invoice=" + invoice +
                ", money='" + money + '\'' +
                ", serviceId='" + serviceId + '\'' +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserUuid() {
        return userUuid;
    }

    public void setUserUuid(String userUuid) {
        this.userUuid = userUuid;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getServiceStation() {
        return serviceStation;
    }

    public void setServiceStation(String serviceStation) {
        this.serviceStation = serviceStation;
    }

    public String getServiceAddress() {
        return serviceAddress;
    }

    public void setServiceAddress(String serviceAddress) {
        this.serviceAddress = serviceAddress;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getServiceOffice() {
        return serviceOffice;
    }

    public void setServiceOffice(String serviceOffice) {
        this.serviceOffice = serviceOffice;
    }

    public String getSalesId() {
        return salesId;
    }

    public void setSalesId(String salesId) {
        this.salesId = salesId;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public Integer getInvoice() {
        return invoice;
    }

    public void setInvoice(Integer invoice) {
        this.invoice = invoice;
    }

    public String getMoney() {
        return money;
    }

    public void setMoney(String money) {
        this.money = money;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }
}
