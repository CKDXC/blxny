package com.example.blxny.model;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/*
 *项目名: blxny
 *文件名: CoffeeProduct
 *创建者: SCH
 *创建时间:2019/5/28 16:30
 *描述: TODO
 */
@Data
public class CateringProduct {
    private int id;
    private String name;
    private String img;
    private BigDecimal price;
    private int type;
    private int sort;
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Date created;
    private int restaurant_id;
    private int from;
}
