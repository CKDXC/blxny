package com.example.blxny.model.vo;

import lombok.Data;

@Data
public class RuleVo {
    private Double slowBookingPrice;
    private Double slowServicePrice;
    private Double slowPickPrice;
    private String stationId;
    private Double fastBookingPrice;
    private Double fastServicePrice;
    private Double fastPickPrice;
    private Double slowCuspPrice;
    private Double slowHighPrice;
    private Double slowNormalPrice;
    private Double slowValleyPrice;
    private Double fastCuspPrice;
    private Double fastHighPrice;
    private Double fastNormalPrice;
    private Double fastValleyPrice;
}
