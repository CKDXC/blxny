package com.example.blxny.model.vo;

import com.alipay.api.domain.DataEntry;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class BankPosVO {
    private String phone;
    private String userName;
    private String opreatorName;
    private String partnerName;
    private BigDecimal need;
    private Date time;
    private Integer type;
    private Date endtime;
    private String  posid;
    private String card;
}
