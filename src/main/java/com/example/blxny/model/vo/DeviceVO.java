package com.example.blxny.model.vo;

import lombok.Data;

/*
 *项目名: blxny
 *文件名: DeviceVo
 *创建者: SCH
 *创建时间:2019/5/8 15;56
 *描述: TODO
 */
@Data
public class DeviceVO {
    private String deviceId;
    private String stationId;
    private String name;
    private Integer chargeType;
    private String powerRating;
    private String ratedVoltage;
    private String currentRated;
    private String state;
    private String ip;
    private Integer port;
}