package com.example.blxny.model.vo;

import lombok.Data;

/*
 *项目名: blxny
 *文件名: UserSysRole
 *创建者: SCH
 *创建时间:2019/5/7 13:45
 *描述: TODO
 */
@Data
public class UserSysRoleVO {
    private Integer id;
    private String userUuid;
    private Integer power;
    private String phone;
    private String nickname;
    private String customerCard;
    private Integer role;
    private String address;
    private Integer voucher;
    private double money;
    private String homeLat;
    private String homeLng;
    private String stationCollected;
    private String workLat;
    private String workLng;
    private String workAddress;
    private String homeProvince;
    private String homeCity;
    private String workProvince;
    private String workCity;
    private int score;
    private int isAuthentication;
}
