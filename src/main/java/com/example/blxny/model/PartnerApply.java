package com.example.blxny.model;

import lombok.Data;

import java.util.Date;

@Data
public class PartnerApply {
    private int id;
    private String partnerName;
    private String opreatorName;
    private String userName;
    private String partnerPwd;
    private String userUuid;
    private Date created;
    private int pass;

}
