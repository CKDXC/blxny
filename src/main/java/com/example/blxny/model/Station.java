package com.example.blxny.model;

import lombok.Data;

@Data
public class Station {
    private Integer id;
    private String stationId;
    private String name;
    private int deviceNum;
    private int grade;
    private String ip;
    private String addressInfo;
    private int range;
    private int workTime;
    private int type;
    private String lat;
    private String lng;


}
