package com.example.blxny.model;

import lombok.Data;

@Data
public class City {
    private String province;
    private String city;
    private String area;
    private String number;
}
