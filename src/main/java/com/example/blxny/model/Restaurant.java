package com.example.blxny.model;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/*
 *项目名: blxny
 *文件名: Restaurant
 *创建者: SCH
 *创建时间:2019/5/28 14:02
 *描述: TODO
 */
@Data
public class Restaurant {
    private int id;
    private String name;
    private String address;
    private BigDecimal star;
    private BigDecimal price;
    private String lat;
    private String lng;
    private String phone;
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Date created;
    private int type;
    private int sort;
    private String img;
}
