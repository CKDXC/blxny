package com.example.blxny.model;

public class Rule {
    private int id;
    private int ruleVersion;
    private String effectiveTime;
    private double bookingPrice;
    private double servicePrice;
    private double pickPrice;
    private double cuspPrice;
    private double highPrice;
    private double normalPrice;
    private double valleyPrice;
    private int timeSlot;
    private String startTime;
    private String endTime;
    private String timeType;
    private String stationId;
    private int type;

    @Override
    public String toString() {
        return "Rule{" +
                "id=" + id +
                ", ruleVersion=" + ruleVersion +
                ", effectiveTime='" + effectiveTime + '\'' +
                ", bookingPrice=" + bookingPrice +
                ", servicePrice=" + servicePrice +
                ", pickPrice=" + pickPrice +
                ", cuspPrice=" + cuspPrice +
                ", highPrice=" + highPrice +
                ", normalPrice=" + normalPrice +
                ", valleyPrice=" + valleyPrice +
                ", timeSlot=" + timeSlot +
                ", startTime='" + startTime + '\'' +
                ", endTime='" + endTime + '\'' +
                ", timeType='" + timeType + '\'' +
                ", stationId='" + stationId + '\'' +
                ", type=" + type +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRuleVersion() {
        return ruleVersion;
    }

    public void setRuleVersion(int ruleVersion) {
        this.ruleVersion = ruleVersion;
    }

    public String getEffectiveTime() {
        return effectiveTime;
    }

    public void setEffectiveTime(String effectiveTime) {
        this.effectiveTime = effectiveTime;
    }

    public double getBookingPrice() {
        return bookingPrice;
    }

    public void setBookingPrice(double bookingPrice) {
        this.bookingPrice = bookingPrice;
    }

    public double getServicePrice() {
        return servicePrice;
    }

    public void setServicePrice(double servicePrice) {
        this.servicePrice = servicePrice;
    }

    public double getPickPrice() {
        return pickPrice;
    }

    public void setPickPrice(double pickPrice) {
        this.pickPrice = pickPrice;
    }

    public double getCuspPrice() {
        return cuspPrice;
    }

    public void setCuspPrice(double cuspPrice) {
        this.cuspPrice = cuspPrice;
    }

    public double getHighPrice() {
        return highPrice;
    }

    public void setHighPrice(double highPrice) {
        this.highPrice = highPrice;
    }

    public double getNormalPrice() {
        return normalPrice;
    }

    public void setNormalPrice(double normalPrice) {
        this.normalPrice = normalPrice;
    }

    public double getValleyPrice() {
        return valleyPrice;
    }

    public void setValleyPrice(double valleyPrice) {
        this.valleyPrice = valleyPrice;
    }

    public int getTimeSlot() {
        return timeSlot;
    }

    public void setTimeSlot(int timeSlot) {
        this.timeSlot = timeSlot;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getTimeType() {
        return timeType;
    }

    public void setTimeType(String timeType) {
        this.timeType = timeType;
    }

    public String getStationId() {
        return stationId;
    }

    public void setStationId(String stationId) {
        this.stationId = stationId;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
