package com.example.blxny.model;

import java.math.BigDecimal;
import java.util.Date;

public class Pos {
    private Integer id;
    private String useruuid;
    private BigDecimal need;
    private Date time;
    private Integer type;
    private Date endtime;
    private String posid;
    private String card;

    @Override
    public String toString() {
        return "Pos{" +
                "id=" + id +
                ", useruuid='" + useruuid + '\'' +
                ", need=" + need +
                ", time=" + time +
                ", type=" + type +
                ", endtime=" + endtime +
                ", posid='" + posid + '\'' +
                ", card='" + card + '\'' +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUseruuid() {
        return useruuid;
    }

    public void setUseruuid(String useruuid) {
        this.useruuid = useruuid;
    }

    public BigDecimal getNeed() {
        return need;
    }

    public void setNeed(BigDecimal need) {
        this.need = need;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Date getEndtime() {
        return endtime;
    }

    public void setEndtime(Date endtime) {
        this.endtime = endtime;
    }

    public String getPosid() {
        return posid;
    }

    public void setPosid(String posid) {
        this.posid = posid;
    }

    public String getCard() {
        return card;
    }

    public void setCard(String card) {
        this.card = card;
    }
}
