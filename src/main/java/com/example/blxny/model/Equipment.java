package com.example.blxny.model;

public class Equipment {
    private Integer equipmentId;
    private String equipmentName;
    private  String equipmentNum;
    private  String equipmentChargeState;
    private  String equipmentRepairState;
    private String equipmentGps;
    private String stationId;

    @Override
    public String toString() {
        return "Equipment{" +
                "equipmentId=" + equipmentId +
                ", equipmentName='" + equipmentName + '\'' +
                ", equipmentNum='" + equipmentNum + '\'' +
                ", equipmentChargeState='" + equipmentChargeState + '\'' +
                ", equipmentRepairState='" + equipmentRepairState + '\'' +
                ", equipmentGps='" + equipmentGps + '\'' +
                ", stationId='" + stationId + '\'' +
                '}';
    }

    public Integer getEquipmentId() {
        return equipmentId;
    }

    public void setEquipmentId(Integer equipmentId) {
        this.equipmentId = equipmentId;
    }

    public String getEquipmentName() {
        return equipmentName;
    }

    public void setEquipmentName(String equipmentName) {
        this.equipmentName = equipmentName;
    }

    public String getEquipmentNum() {
        return equipmentNum;
    }

    public void setEquipmentNum(String equipmentNum) {
        this.equipmentNum = equipmentNum;
    }

    public String getEquipmentChargeState() {
        return equipmentChargeState;
    }

    public void setEquipmentChargeState(String equipmentChargeState) {
        this.equipmentChargeState = equipmentChargeState;
    }

    public String getEquipmentRepairState() {
        return equipmentRepairState;
    }

    public void setEquipmentRepairState(String equipmentRepairState) {
        this.equipmentRepairState = equipmentRepairState;
    }

    public String getEquipmentGps() {
        return equipmentGps;
    }

    public void setEquipmentGps(String equipmentGps) {
        this.equipmentGps = equipmentGps;
    }

    public String getStationId() {
        return stationId;
    }

    public void setStationId(String stationId) {
        this.stationId = stationId;
    }
}
