package com.example.blxny.model;

import java.util.Date;

public class Ad {
    private Integer id;
    private Integer form;
    private String img;
    private String url;
    private Date time;
    private Date endtime;

    @Override
    public String toString() {
        return "Ad{" +
                "id=" + id +
                ", form=" + form +
                ", img='" + img + '\'' +
                ", url='" + url + '\'' +
                ", time=" + time +
                ", endtime=" + endtime +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getForm() {
        return form;
    }

    public void setForm(Integer form) {
        this.form = form;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public Date getEndtime() {
        return endtime;
    }

    public void setEndtime(Date endtime) {
        this.endtime = endtime;
    }
}
