package com.example.blxny.model;

public class Bank {
    private Integer id;
    private String number;
    private String backName;
    private String  cardType;
    private String cardVar;
    private String cardAddress;
    private String phone;
    private String userUuid;
    private Integer type;
    private String where;
    private String img;
    private String idcard;

    @Override
    public String toString() {
        return "Bank{" +
                "id=" + id +
                ", number='" + number + '\'' +
                ", backName='" + backName + '\'' +
                ", cardType='" + cardType + '\'' +
                ", cardVar='" + cardVar + '\'' +
                ", cardAddress='" + cardAddress + '\'' +
                ", phone='" + phone + '\'' +
                ", userUuid='" + userUuid + '\'' +
                ", type=" + type +
                ", where='" + where + '\'' +
                ", img='" + img + '\'' +
                ", idcard='" + idcard + '\'' +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getBackName() {
        return backName;
    }

    public void setBackName(String backName) {
        this.backName = backName;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public String getCardVar() {
        return cardVar;
    }

    public void setCardVar(String cardVar) {
        this.cardVar = cardVar;
    }

    public String getCardAddress() {
        return cardAddress;
    }

    public void setCardAddress(String cardAddress) {
        this.cardAddress = cardAddress;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getUserUuid() {
        return userUuid;
    }

    public void setUserUuid(String userUuid) {
        this.userUuid = userUuid;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getWhere() {
        return where;
    }

    public void setWhere(String where) {
        this.where = where;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getIdcard() {
        return idcard;
    }

    public void setIdcard(String idcard) {
        this.idcard = idcard;
    }
}
