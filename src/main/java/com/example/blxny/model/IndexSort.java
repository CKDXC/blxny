package com.example.blxny.model;

public class IndexSort {
    private Integer far;
    private String id;
    private String name;
    private String null_device;
    private String powerpay;
    private String servicepay;
    private String address;
    private Integer timetool;
    private Integer range;
    private String lat;
    private String lng;
    private Integer type;
    private String isCollected;

    @Override
    public String toString() {
        return "IndexSort{" +
                "far=" + far +
                ", id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", null_device='" + null_device + '\'' +
                ", powerpay='" + powerpay + '\'' +
                ", servicepay='" + servicepay + '\'' +
                ", address='" + address + '\'' +
                ", timetool=" + timetool +
                ", range=" + range +
                ", lat='" + lat + '\'' +
                ", lng='" + lng + '\'' +
                ", type=" + type +
                ", isCollected='" + isCollected + '\'' +
                '}';
    }

    public Integer getFar() {
        return far;
    }

    public void setFar(Integer far) {
        this.far = far;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNull_device() {
        return null_device;
    }

    public void setNull_device(String null_device) {
        this.null_device = null_device;
    }

    public String getPowerpay() {
        return powerpay;
    }

    public void setPowerpay(String powerpay) {
        this.powerpay = powerpay;
    }

    public String getServicepay() {
        return servicepay;
    }

    public void setServicepay(String servicepay) {
        this.servicepay = servicepay;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getTimetool() {
        return timetool;
    }

    public void setTimetool(Integer timetool) {
        this.timetool = timetool;
    }

    public Integer getRange() {
        return range;
    }

    public void setRange(Integer range) {
        this.range = range;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getIsCollected() {
        return isCollected;
    }

    public void setIsCollected(String isCollected) {
        this.isCollected = isCollected;
    }
    public IndexSort() {

    }


    public IndexSort(int far,
                     String id,
                     String name,
                     String null_device,
                     String powerpay,
                     String servicepay,
                     String address,
                     int timetool,
                     int range,
                     String lat,
                     String lng,
                     int type,
                     String isCollected) {
        super();
        this.id = id;
        this.name = name;
        this.null_device = null_device;
        this.powerpay = powerpay;
        this.servicepay = servicepay;
        this.address = address;
        this.timetool = timetool;
        this.range = range;
        this.lat = lat;
        this.lng = lng;
        this.type=type;
        this.isCollected=isCollected;
    }


}
