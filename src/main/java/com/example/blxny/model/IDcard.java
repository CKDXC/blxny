package com.example.blxny.model;

import java.util.Queue;

public class IDcard {
    private Integer id;
    private String face;//身份证正面图
    private String back;//身份证背面图
    private String name;//用户真实姓名
    private String idcard;//用户身份证号
    private String useruuid;//用户uuid
    private String aspeople;//用户持证照片
    private String type;//认证状态

    @Override
    public String toString() {
        return "IDcard{" +
                "id=" + id +
                ", face='" + face + '\'' +
                ", back='" + back + '\'' +
                ", name='" + name + '\'' +
                ", idcard='" + idcard + '\'' +
                ", useruuid='" + useruuid + '\'' +
                ", aspeople='" + aspeople + '\'' +
                ", type='" + type + '\'' +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFace() {
        return face;
    }

    public void setFace(String face) {
        this.face = face;
    }

    public String getBack() {
        return back;
    }

    public void setBack(String back) {
        this.back = back;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIdcard() {
        return idcard;
    }

    public void setIdcard(String idcard) {
        this.idcard = idcard;
    }

    public String getUseruuid() {
        return useruuid;
    }

    public void setUseruuid(String useruuid) {
        this.useruuid = useruuid;
    }

    public String getAspeople() {
        return aspeople;
    }

    public void setAspeople(String aspeople) {
        this.aspeople = aspeople;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
