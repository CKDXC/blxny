package com.example.blxny.model;

import java.util.Date;

public class Version {
    private int id;
    private String softwareVer;
    private String communicateVer;
    private String md5;
    private String linkAddress;
    private Date create;
    private String note;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSoftwareVer() {
        return softwareVer;
    }

    public void setSoftwareVer(String softwareVer) {
        this.softwareVer = softwareVer;
    }

    public String getCommunicateVer() {
        return communicateVer;
    }

    public void setCommunicateVer(String communicateVer) {
        this.communicateVer = communicateVer;
    }

    public String getMd5() {
        return md5;
    }

    public void setMd5(String md5) {
        this.md5 = md5;
    }

    public String getLinkAddress() {
        return linkAddress;
    }

    public void setLinkAddress(String linkAddress) {
        this.linkAddress = linkAddress;
    }

    public Date getCreate() {
        return create;
    }

    public void setCreate(Date create) {
        this.create = create;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    @Override
    public String toString() {
        return "Version{" +
                "id=" + id +
                ", softwareVer='" + softwareVer + '\'' +
                ", communicateVer='" + communicateVer + '\'' +
                ", md5='" + md5 + '\'' +
                ", linkAddress='" + linkAddress + '\'' +
                ", create=" + create +
                ", note='" + note + '\'' +
                '}';
    }
}