package com.example.blxny.model;

import java.math.BigDecimal;

public class User {
    private Integer id;
    private String phone;
    private String password;
    private String nickname;
    private String customercard;
    private String token;
    private Integer role;
    private String starttime;
    private String images;
    private String address;
    private String integral;
    private Integer voucher;
    private BigDecimal money;
    private String homeLat;
    private String homeLng;
    private String stationCollected;
    private String userUuid;
    private String workLat;
    private String workLng;
    private String workAddress;
    private String homeProvince;
    private String homeCity;
    private String workProvince;
    private String workCity;
    private int score;
    private int isAuthentication;

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", phone='" + phone + '\'' +
                ", password='" + password + '\'' +
                ", nickname='" + nickname + '\'' +
                ", customercard='" + customercard + '\'' +
                ", token='" + token + '\'' +
                ", role=" + role +
                ", starttime='" + starttime + '\'' +
                ", images='" + images + '\'' +
                ", address='" + address + '\'' +
                ", integral='" + integral + '\'' +
                ", voucher=" + voucher +
                ", money=" + money +
                ", homeLat='" + homeLat + '\'' +
                ", homeLng='" + homeLng + '\'' +
                ", stationCollected='" + stationCollected + '\'' +
                ", userUuid='" + userUuid + '\'' +
                ", workLat='" + workLat + '\'' +
                ", workLng='" + workLng + '\'' +
                ", workAddress='" + workAddress + '\'' +
                ", homeProvince='" + homeProvince + '\'' +
                ", homeCity='" + homeCity + '\'' +
                ", workProvince='" + workProvince + '\'' +
                ", workCity='" + workCity + '\'' +
                ", score=" + score +
                ", isAuthentication=" + isAuthentication +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getCustomercard() {
        return customercard;
    }

    public void setCustomercard(String customercard) {
        this.customercard = customercard;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Integer getRole() {
        return role;
    }

    public void setRole(Integer role) {
        this.role = role;
    }

    public String getStarttime() {
        return starttime;
    }

    public void setStarttime(String starttime) {
        this.starttime = starttime;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getIntegral() {
        return integral;
    }

    public void setIntegral(String integral) {
        this.integral = integral;
    }

    public Integer getVoucher() {
        return voucher;
    }

    public void setVoucher(Integer voucher) {
        this.voucher = voucher;
    }

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }

    public String getHomeLat() {
        return homeLat;
    }

    public void setHomeLat(String homeLat) {
        this.homeLat = homeLat;
    }

    public String getHomeLng() {
        return homeLng;
    }

    public void setHomeLng(String homeLng) {
        this.homeLng = homeLng;
    }

    public String getStationCollected() {
        return stationCollected;
    }

    public void setStationCollected(String stationCollected) {
        this.stationCollected = stationCollected;
    }

    public String getUserUuid() {
        return userUuid;
    }

    public void setUserUuid(String userUuid) {
        this.userUuid = userUuid;
    }

    public String getWorkLat() {
        return workLat;
    }

    public void setWorkLat(String workLat) {
        this.workLat = workLat;
    }

    public String getWorkLng() {
        return workLng;
    }

    public void setWorkLng(String workLng) {
        this.workLng = workLng;
    }

    public String getWorkAddress() {
        return workAddress;
    }

    public void setWorkAddress(String workAddress) {
        this.workAddress = workAddress;
    }

    public String getHomeProvince() {
        return homeProvince;
    }

    public void setHomeProvince(String homeProvince) {
        this.homeProvince = homeProvince;
    }

    public String getHomeCity() {
        return homeCity;
    }

    public void setHomeCity(String homeCity) {
        this.homeCity = homeCity;
    }

    public String getWorkProvince() {
        return workProvince;
    }

    public void setWorkProvince(String workProvince) {
        this.workProvince = workProvince;
    }

    public String getWorkCity() {
        return workCity;
    }

    public void setWorkCity(String workCity) {
        this.workCity = workCity;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getIsAuthentication() {
        return isAuthentication;
    }

    public void setIsAuthentication(int isAuthentication) {
        this.isAuthentication = isAuthentication;
    }
}