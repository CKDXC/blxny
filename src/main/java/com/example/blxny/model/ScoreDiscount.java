package com.example.blxny.model;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class ScoreDiscount {
    private Integer id;
    private Integer amount;
    private Date created;
    private String uid;
    private Integer from;
    private String provider;
    private String scoreId;
    private BigDecimal price;
    private int score;
}
