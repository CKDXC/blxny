package com.example.blxny.model;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class Shop {
    private Integer id;
    private Integer type;
    private String name;
    private BigDecimal price;
    private String img;
    private Date create;
    private Integer deleted;
    private Integer restaurantId;
}
