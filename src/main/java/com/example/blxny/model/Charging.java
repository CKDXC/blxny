package com.example.blxny.model;

public class Charging {
    private Integer id;
    private String stationId;
    private String powerNum;
    private String serviceNum;
    private String time;

    @Override
    public String toString() {
        return "Charging{" +
                "id=" + id +
                ", stationId='" + stationId + '\'' +
                ", powerNum='" + powerNum + '\'' +
                ", serviceNum='" + serviceNum + '\'' +
                ", time='" + time + '\'' +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStationId() {
        return stationId;
    }

    public void setStationId(String stationId) {
        this.stationId = stationId;
    }

    public String getPowerNum() {
        return powerNum;
    }

    public void setPowerNum(String powerNum) {
        this.powerNum = powerNum;
    }

    public String getServiceNum() {
        return serviceNum;
    }

    public void setServiceNum(String serviceNum) {
        this.serviceNum = serviceNum;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
