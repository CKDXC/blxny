package com.example.blxny.model;

import java.util.Date;

public class PartnerDevice {

    private int id;
    private int partnerId;
    private String stationId;
    private String deviceId;
    private Date created;
    private int isUsed;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(int partnerId) {
        this.partnerId = partnerId;
    }

    public String getStationId() {
        return stationId;
    }

    public void setStationId(String stationId) {
        this.stationId = stationId;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public int getIsUsed() {
        return isUsed;
    }

    public void setIsUsed(int isUsed) {
        this.isUsed = isUsed;
    }

    @Override
    public String toString() {
        return "PartnerDevice{" +
                "id=" + id +
                ", partnerId=" + partnerId +
                ", stationId='" + stationId + '\'' +
                ", deviceId='" + deviceId + '\'' +
                ", created=" + created +
                ", isUsed=" + isUsed +
                '}';
    }
}
