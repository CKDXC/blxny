package com.example.blxny.model;


import com.alibaba.fastjson.annotation.JSONField;

import java.math.BigDecimal;
import java.util.Date;

public class Order {

	private long id;
	private String orderId;
	private int orderType;
	private int chargeType;
	private int gunNo;
	private String cardId;
	private String vin;
	private int soc;
	private int endReason;
	@JSONField(format = "yyyy-MM-dd HH:mm:ss")
	private Date startTime;
	@JSONField(format = "yyyy-MM-dd HH:mm:ss")
	private Date endTime;
	private float startMeter;
	private float endMeter;
	private float chargeAmount;
	private float cuspElectricity;
	private float highElectricity;
	private float normalElectricity;
	private float valleyElectricity;
	private BigDecimal totalAmount;
	private BigDecimal cuspAmount;
	private BigDecimal highAmount;
	private BigDecimal normalAmount;
	private BigDecimal valleyAmount;
	private BigDecimal bookingAmount;
	private BigDecimal serviceAmount;
	private BigDecimal pickAmount;
	private String uid;
	private int orderStatus;
	private int payStatus;
	private int payment;
	private String deviceId;
	@JSONField(format = "yyyy-MM-dd HH:mm:ss")
	private Date created;
	private BigDecimal overPay;
	private String stopCode;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public int getOrderType() {
		return orderType;
	}

	public void setOrderType(int orderType) {
		this.orderType = orderType;
	}

	public int getChargeType() {
		return chargeType;
	}

	public void setChargeType(int chargeType) {
		this.chargeType = chargeType;
	}

	public int getGunNo() {
		return gunNo;
	}

	public void setGunNo(int gunNo) {
		this.gunNo = gunNo;
	}

	public String getCardId() {
		return cardId;
	}

	public void setCardId(String cardId) {
		this.cardId = cardId;
	}

	public String getVin() {
		return vin;
	}

	public void setVin(String vin) {
		this.vin = vin;
	}

	public int getSoc() {
		return soc;
	}

	public void setSoc(int soc) {
		this.soc = soc;
	}

	public int getEndReason() {
		return endReason;
	}

	public void setEndReason(int endReason) {
		this.endReason = endReason;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public float getStartMeter() {
		return startMeter;
	}

	public void setStartMeter(float startMeter) {
		this.startMeter = startMeter;
	}

	public float getEndMeter() {
		return endMeter;
	}

	public void setEndMeter(float endMeter) {
		this.endMeter = endMeter;
	}

	public float getChargeAmount() {
		return chargeAmount;
	}

	public void setChargeAmount(float chargeAmount) {
		this.chargeAmount = chargeAmount;
	}

	public float getCuspElectricity() {
		return cuspElectricity;
	}

	public void setCuspElectricity(float cuspElectricity) {
		this.cuspElectricity = cuspElectricity;
	}

	public float getHighElectricity() {
		return highElectricity;
	}

	public void setHighElectricity(float highElectricity) {
		this.highElectricity = highElectricity;
	}

	public float getNormalElectricity() {
		return normalElectricity;
	}

	public void setNormalElectricity(float normalElectricity) {
		this.normalElectricity = normalElectricity;
	}

	public float getValleyElectricity() {
		return valleyElectricity;
	}

	public void setValleyElectricity(float valleyElectricity) {
		this.valleyElectricity = valleyElectricity;
	}

	public BigDecimal getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	public BigDecimal getCuspAmount() {
		return cuspAmount;
	}

	public void setCuspAmount(BigDecimal cuspAmount) {
		this.cuspAmount = cuspAmount;
	}

	public BigDecimal getHighAmount() {
		return highAmount;
	}

	public void setHighAmount(BigDecimal highAmount) {
		this.highAmount = highAmount;
	}

	public BigDecimal getNormalAmount() {
		return normalAmount;
	}

	public void setNormalAmount(BigDecimal normalAmount) {
		this.normalAmount = normalAmount;
	}

	public BigDecimal getValleyAmount() {
		return valleyAmount;
	}

	public void setValleyAmount(BigDecimal valleyAmount) {
		this.valleyAmount = valleyAmount;
	}

	public BigDecimal getBookingAmount() {
		return bookingAmount;
	}

	public void setBookingAmount(BigDecimal bookingAmount) {
		this.bookingAmount = bookingAmount;
	}

	public BigDecimal getServiceAmount() {
		return serviceAmount;
	}

	public void setServiceAmount(BigDecimal serviceAmount) {
		this.serviceAmount = serviceAmount;
	}

	public BigDecimal getPickAmount() {
		return pickAmount;
	}

	public void setPickAmount(BigDecimal pickAmount) {
		this.pickAmount = pickAmount;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public int getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(int orderStatus) {
		this.orderStatus = orderStatus;
	}

	public int getPayStatus() {
		return payStatus;
	}

	public void setPayStatus(int payStatus) {
		this.payStatus = payStatus;
	}

	public int getPayment() {
		return payment;
	}

	public void setPayment(int payment) {
		this.payment = payment;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public BigDecimal getOverPay() {
		return overPay;
	}

	public void setOverPay(BigDecimal overPay) {
		this.overPay = overPay;
	}

	public String getStopCode() {
		return stopCode;
	}

	public void setStopCode(String stopCode) {
		this.stopCode = stopCode;
	}

	@Override
	public String toString() {
		return "Order{" +
				"id=" + id +
				", orderId='" + orderId + '\'' +
				", orderType=" + orderType +
				", chargeType=" + chargeType +
				", gunNo=" + gunNo +
				", cardId='" + cardId + '\'' +
				", vin='" + vin + '\'' +
				", soc=" + soc +
				", endReason=" + endReason +
				", startTime=" + startTime +
				", endTime=" + endTime +
				", startMeter=" + startMeter +
				", endMeter=" + endMeter +
				", chargeAmount=" + chargeAmount +
				", cuspElectricity=" + cuspElectricity +
				", highElectricity=" + highElectricity +
				", normalElectricity=" + normalElectricity +
				", valleyElectricity=" + valleyElectricity +
				", totalAmount=" + totalAmount +
				", cuspAmount=" + cuspAmount +
				", highAmount=" + highAmount +
				", normalAmount=" + normalAmount +
				", valleyAmount=" + valleyAmount +
				", bookingAmount=" + bookingAmount +
				", serviceAmount=" + serviceAmount +
				", pickAmount=" + pickAmount +
				", uid='" + uid + '\'' +
				", orderStatus=" + orderStatus +
				", payStatus=" + payStatus +
				", payment=" + payment +
				", deviceId='" + deviceId + '\'' +
				", created=" + created +
				", overPay=" + overPay +
				", stopCode='" + stopCode + '\'' +
				'}';
	}
}