package com.example.blxny.model;

import java.math.BigDecimal;
import java.util.Date;

public class Coupon {

    private int id;
    private String uid;
    private int  scoreProductId;
    private int type;
    private int isUsed;
    private Date usedTime;
    private String note;
    private Date created;
    private String integral;
    private Double discount;
    private Date expireDate;
    private int display;
    private int condition;

    @Override
    public String toString() {
        return "Coupon{" +
                "id=" + id +
                ", uid='" + uid + '\'' +
                ", scoreProductId=" + scoreProductId +
                ", type=" + type +
                ", isUsed=" + isUsed +
                ", usedTime=" + usedTime +
                ", note='" + note + '\'' +
                ", created=" + created +
                ", integral='" + integral + '\'' +
                ", discount=" + discount +
                ", expireDate=" + expireDate +
                ", display=" + display +
                ", condition=" + condition +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public int getScoreProductId() {
        return scoreProductId;
    }

    public void setScoreProductId(int scoreProductId) {
        this.scoreProductId = scoreProductId;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getIsUsed() {
        return isUsed;
    }

    public void setIsUsed(int isUsed) {
        this.isUsed = isUsed;
    }

    public Date getUsedTime() {
        return usedTime;
    }

    public void setUsedTime(Date usedTime) {
        this.usedTime = usedTime;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public String getIntegral() {
        return integral;
    }

    public void setIntegral(String integral) {
        this.integral = integral;
    }

    public Double getDiscount() {
        return discount;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }

    public Date getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(Date expireDate) {
        this.expireDate = expireDate;
    }

    public int getDisplay() {
        return display;
    }

    public void setDisplay(int display) {
        this.display = display;
    }

    public int getCondition() {
        return condition;
    }

    public void setCondition(int condition) {
        this.condition = condition;
    }
}
