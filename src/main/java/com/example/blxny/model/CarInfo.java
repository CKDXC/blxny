package com.example.blxny.model;

import java.util.Date;

public class CarInfo {
    private int id;
    private int uid;
    private String plateNum;
    private int carID;
    private Date carCreated;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getPlateNum() {
        return plateNum;
    }

    public void setPlateNum(String plateNum) {
        this.plateNum = plateNum;
    }

    public int getCarID() {
        return carID;
    }

    public void setCarID(int carID) {
        this.carID = carID;
    }

    public Date getCarCreated() {
        return carCreated;
    }

    public void setCarCreated(Date carCreated) {
        this.carCreated = carCreated;
    }

    @Override
    public String toString() {
        return "CarInfo{" +
                "id=" + id +
                ", uid=" + uid +
                ", plateNum='" + plateNum + '\'' +
                ", carID=" + carID +
                ", carCreated=" + carCreated +
                '}';
    }
}
