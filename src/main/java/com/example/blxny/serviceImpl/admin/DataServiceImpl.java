package com.example.blxny.serviceImpl.admin;

import cn.hutool.core.date.DateField;
import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;
import com.example.blxny.dao.OrderDao;
import com.example.blxny.dao.UserDao;
import com.example.blxny.model.Order;
import com.example.blxny.service.admin.DataService;
import com.example.blxny.tool.StringUtil;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Service(value = "DataService")
public class DataServiceImpl implements DataService {
    @Autowired
    private UserDao userDao;
    @Autowired
    private OrderDao orderDao;

    @Override
    public List getCityStationNumber() {
        //先获取所有城市
        List<String> address = userDao.AdminFindAllCity();
        Set set = new LinkedHashSet();
        for (String str : address) {
            String city = str.split("市")[0] + "市";
            JSONObject js = new JSONObject();
            int num = StringUtil.getSubCount_2(address.toString(), city);
            js.put("city", city);
            js.put("number", num);
            set.add(js);
        }
        List message = new ArrayList();
        for (Object s : set) {
            message.add(s);
        }
        return message;
    }

    @Override
    public List getSettlement(String city, String start, String end) {
        //先找到对应的订单
        Date startTime = new Date();
        Date endTime = new Date();
        try {
            startTime = new SimpleDateFormat("yyyyMMdd").parse(start);
            endTime = new SimpleDateFormat("yyyyMMdd").parse(end);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        List<Order> orders = orderDao.getOrderByTime(startTime, endTime, city + "%");
        System.out.println(orders);
        //通过日期计算时间
        long betweenDay = DateUtil.between(startTime, endTime, DateUnit.DAY);
        List message = new ArrayList();

        for (int i = 0; i < betweenDay; i++) {
            JSONObject js = new JSONObject();
            Date date = DateUtil.offset(startTime, DateField.DAY_OF_MONTH, i);
            js.put("time", new SimpleDateFormat("yyyy-MM-dd").format(date));//给时间
            //给总电流
            double chargeAmount = 0;
            //给总费用
            BigDecimal totalAmount = new BigDecimal("0.000");
            String money = "0";
            for (Order orderOne : orders) {
                if (DateUtil.between(orderOne.getCreated(), date, DateUnit.DAY) == 0) {
                    chargeAmount += orderOne.getChargeAmount();
                    BigDecimal totalAmount1 = new BigDecimal(String.valueOf(orderOne.getTotalAmount()));
                    money = totalAmount.add(totalAmount1).toString();
                }
            }
            js.put("allPower", chargeAmount);
            js.put("allMoney", money);
            message.add(js);
        }
        return message;
    }


}
