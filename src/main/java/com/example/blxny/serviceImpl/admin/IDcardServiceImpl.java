package com.example.blxny.serviceImpl.admin;

import com.example.blxny.dao.IDcardDao;
import com.example.blxny.model.IDcard;
import com.example.blxny.model.PartnerApply;
import com.example.blxny.service.admin.IDcardService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service(value = "IDcard")
public class IDcardServiceImpl implements IDcardService {
    @Autowired
    private IDcardDao dao;

    @Override
    public Object findAllIdcardApproval(Integer type, Integer size, Integer number) {
        PageHelper.startPage(number,size);
        PageInfo<IDcard> pageInfo = new PageInfo<>(dao.findAllIDcardApproval(type));
        return pageInfo;
    }

    @Override
    public Object ApprovalIdCard(String uuid,int type) {
        if(dao.ApprovalIdCard(type,uuid)>0){
            return "OK";
        }else{
        return "ERROR";}
    }
}
