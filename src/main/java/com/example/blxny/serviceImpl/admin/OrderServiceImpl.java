package com.example.blxny.serviceImpl.admin;

import com.example.blxny.dao.OrderDao;
import com.example.blxny.model.Order;
import com.example.blxny.service.admin.OrderService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;



@Service(value = "adminOrder")
public class OrderServiceImpl implements OrderService {
    @Autowired
    private OrderDao orderDao;

    @Override
    public Object findAllOrderAdmin(int number, int size) {
        PageHelper.startPage(number,size);
        PageInfo<Order> order=new PageInfo<>(orderDao.findAdminAllOrder());
        return order;
    }

    @Override
    public Object findDeviceOrder(int number, int size, String id) {
        PageHelper.startPage(number,size);
        PageInfo<Order> order=new PageInfo<>(orderDao.findDeviceOrder(id));
        return order;
    }
}
