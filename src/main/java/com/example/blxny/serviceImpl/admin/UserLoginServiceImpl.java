package com.example.blxny.serviceImpl.admin;

import com.example.blxny.dao.UserDao;
import com.example.blxny.model.SysRole;
import com.example.blxny.model.User;
import com.example.blxny.service.admin.UserLoginService;
import com.example.blxny.service.impl.UserServiceImpl;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

@Service(value = "adminUserLogin")
public class UserLoginServiceImpl implements UserLoginService {
    @Autowired
    private UserDao userDao;

    @Override
    public JSONObject AdminUserLogin(String user, String password) {
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        String md5pass = UserServiceImpl.MD5(password, md);
        //先看有没有这个用户
        User user1 = userDao.FindUserAll(user);
        JSONObject js = new JSONObject();
        int result = 3;
        int power = 3;
        String useruuid = "";
        String nickname = "";
        String img = "";
        String phone = "";
        String jpid = "";
        String money = "0";
        int score = 0;
        int card = 0;
        if (user1 == null) {
            result = 1;//没有此账户
        } else {
            useruuid = user1.getUserUuid();
            nickname = user1.getNickname();
            img = user1.getImages();
            phone = user1.getPhone();
            jpid = user1.getStarttime();
            money = String.valueOf(user1.getMoney());
            score = user1.getScore();
            card = user1.getIsAuthentication();
            //查看用户等级
            SysRole powers = userDao.AdminUserLoginPower(user1.getUserUuid());
            if (powers == null) {
                result = 4;//无权限登录
            } else{
                if(powers.getPassword()==null){//只有第一次登录才为null
                    if(user1.getPassword().equals(md5pass)){
                        result = 0;
                        power=powers.getPower();
                    }else{
                        result=2;
                    }
                }else{
                    if(powers.getPassword().equals(md5pass)){
                        result = 0;
                        power=powers.getPower();
                    }else{
                        result=2;
                    }
                }
            }
        }
        //返回用户信息
        js.put("result", result);
        js.put("power", power);
        js.put("useruuid", useruuid);
        js.put("nickname", nickname);
        js.put("img", img);
        js.put("phone", phone);
        js.put("jpid", jpid);
        js.put("money", money);
        js.put("score", score);
        js.put("card", card);
        return js;
    }
}
