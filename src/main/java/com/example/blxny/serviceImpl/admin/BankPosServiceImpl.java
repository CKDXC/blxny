package com.example.blxny.serviceImpl.admin;

import com.example.blxny.dao.BankDao;
import com.example.blxny.dao.UserDao;
import com.example.blxny.model.Information;
import com.example.blxny.model.Partner;
import com.example.blxny.model.vo.BankPosVO;
import com.example.blxny.service.admin.BankPosService;
import com.example.blxny.tool.JPushTool;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static com.example.blxny.tool.JPushTool.ONE;

@Service(value = "posService")
public class BankPosServiceImpl implements BankPosService {
    @Autowired
    private BankDao bankDao;
    @Autowired
    private UserDao userDao;

    @Override
    public PageInfo<BankPosVO> findAllBankPos(Integer pageNumber,Integer type,String phone) {
        PageHelper.startPage(pageNumber, 10);
        List<BankPosVO> bankPosVOS=bankDao.FindPos(type,phone);
        PageInfo<BankPosVO> pageInfo = new PageInfo<>(bankPosVOS);
        return pageInfo;
    }

    @Override
    @Transactional
    public String sureBankPos(String posId) {
        //修改pos状态
        String message="系统错误";
        if(bankDao.changePosType(posId)>0){
        //发送推送
            String JPid=bankDao.findJPidFromPosId(posId);//查到极光id
            JPushTool.JPush(JPid,"您单号为"+posId+"的提现申请已提交银行处理，预计24内小时到账。","提现进度通知",ONE);
        //发送消息
            Information information=new Information();
            information.setTitle("提现进度通知");
            information.setInfo("您单号为"+posId+"的提现申请已提交银行处理，预计24内小时到账。");
            information.setUserUuid(bankDao.findPosByPosId(posId));
            userDao.addInformation(information);
            message="OK";
            }
        return message;
    }


}
