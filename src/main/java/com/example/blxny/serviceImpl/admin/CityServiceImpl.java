package com.example.blxny.serviceImpl.admin;

import com.example.blxny.dao.CityDao;
import com.example.blxny.model.City;
import com.example.blxny.service.admin.CityServcie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service(value = "CityAdmin")
public class CityServiceImpl implements CityServcie {
    @Autowired
    private CityDao cityDao;

    @Override
    public Object AddNewCity(City city) {
        String messge = "ERROR";
        int a=cityDao.isHaveOne(city);
        if (0 == a && cityDao.AddNewCity(city) > 0) {
            messge = "OK";
        }
        return messge;
    }

    @Override
    public int delCity(String id) {
        return cityDao.delCity(id);
    }
}
