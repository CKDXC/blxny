package com.example.blxny.serviceImpl.admin;

import com.example.blxny.dao.CateringProductDao;
import com.example.blxny.dao.RestaurantDao;
import com.example.blxny.model.CateringProduct;
import com.example.blxny.model.Hotel;
import com.example.blxny.model.Restaurant;
import com.example.blxny.model.dto.CateringProductDTO;
import com.example.blxny.model.Shop;
import com.example.blxny.service.admin.RestaurantService;
import com.example.blxny.util.RedisUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.FileNotFoundException;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;

import static com.example.blxny.tool.UploadTool.upOneByYourUrlUseBase64;

@Service(value = "Restaurant")
public class RestaurantServiceImpl implements RestaurantService {
    @Autowired
    private RedisUtil redisUtil;
    @Autowired
    private RestaurantDao restaurantDao;
    @Autowired
    private CateringProductDao cateringProductDao;


    @Override
    public boolean delList(int id) {
        return restaurantDao.delList(id) > 0;
    }

    @Override
    public boolean change(String code) {
        double d = Double.valueOf(code);
        redisUtil.delete("theInterestRateOfqrCode");
        if (d < 1 || d > 0) {
            redisUtil.set("theInterestRateOfqrCode", code);
            return true;
        }
        return false;
    }

    @Override
    public String get() {
        return redisUtil.get("theInterestRateOfqrCode");
    }

    @Override
    public String addNewStore(Restaurant restaurant) throws FileNotFoundException {
        String img = upOneByYourUrlUseBase64(restaurant.getImg(), "restaurant");
        restaurant.setImg(img);
        if (restaurant.getType() != 1) {
            restaurant.setStar(new BigDecimal(5));
        }
        if (restaurantDao.addNewList(restaurant) > 0) {
            return "OK";
        }
        return "ERROR";
    }

    @Override
    public String changeStore(Restaurant restaurant) {
        if (restaurantDao.findList(restaurant.getId()) == 0) {
            return "没有找到此店铺";
        }
        if (restaurantDao.changeNewList(restaurant) > 0) {
            return "OK";
        }
        return "ERROR";
    }

    @Override
    public PageInfo<CateringProduct> findPage(int restaurantId, int pageNumber, int pageSize) {
        PageHelper.startPage(pageNumber, pageSize);
        List<CateringProduct> list = cateringProductDao.findAll(restaurantId);
        return new PageInfo<>(list);
    }

    @Override
    public String addCateringProduct(CateringProduct cateringProduct) {
        cateringProduct.setCreated(new Date());
        if (cateringProductDao.addCateringProducat(cateringProduct) > 0) {
            return "添加成功";
        } else {
            return "添加失败";
        }
    }

    @Override
    public String updateCateringProduct(CateringProductDTO cateringProductDTO) {
        if (restaurantDao.getRestaurantById(cateringProductDTO.getRestaurantId())==null){
            return "店铺ID不存在";
        }
        if (cateringProductDao.updateCateringProducat(cateringProductDTO) > 0) {
            return "修改成功";
        } else {
            return "修改失败";
        }
    }

    @Override
    public List<Hotel> listHotel(String message) {
        List<Hotel> hotels = new ArrayList<>();
        String str = "^[0-9]*$";
        if (message.matches(str)) {
            hotels = restaurantDao.findHotelInfo(Integer.valueOf(message));
        } else {
            String id = restaurantDao.findHotelIdFromRestaurant("%" + message + "%");
            if (id == null) {
                return null;
            }
            hotels = restaurantDao.findHotelInfo(Integer.valueOf(id));
        }
        return hotels;
    }

    @Override
    public String addHotelImg(String img, String info, int id) throws FileNotFoundException {
        //先看是不是大堂,不是的话看这个info写过没有
        boolean boo = "大堂".equals(info) || "宴会厅".equals(info) || "豪华套房".equals(info) ||
                "游泳池".equals(info) || "酒吧".equals(info) || "健身房".equals(info) || "休闲区".equals(info);
        if (!boo) {
            return "图片说明必须为:大堂，宴会厅，豪华套房，游泳池，酒吧，健身房，休闲区";
        } else if (!info.equals("大堂")) {
            if (restaurantDao.findCountByImgInfo(info, id) > 0) {
                return info + "只能存在一次";
            }
        }
        Hotel hotel = new Hotel();
        hotel.setImgInfo(info);
        hotel.setRestaurantId(id);
        hotel.setImg(upOneByYourUrlUseBase64(img, "restaurant/hotel"));
        if (restaurantDao.addNewImgFromHotel(hotel) > 0) {
            return "OK";
        }
        return "ERROR";
    }

    @Override
    public String addNewShop(Shop shop, int id) throws FileNotFoundException {
        //想看有没有这个id的商店
        if (!"4".equals(restaurantDao.findIShaveShop(id))) {
            return "没有找到此商店";
        }
        //先上传这个图片
        String img = upOneByYourUrlUseBase64(shop.getImg(), "restaurant/shop");
        shop.setImg(img);
        if (restaurantDao.addNewShop(shop) > 0) {
            return "OK";
        }
        return "ERROR";
    }

    @Override
    public String delShop(int id) {
        if (restaurantDao.delShopOne(id) > 0) {
            return "OK";
        }
        return "ERROR";
    }
}
