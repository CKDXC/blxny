package com.example.blxny.serviceImpl.admin;

import com.example.blxny.dao.CityDao;
import com.example.blxny.dao.RuleDao;
import com.example.blxny.dao.StationDao;
import com.example.blxny.model.City;
import com.example.blxny.model.Rule;
import com.example.blxny.model.Station;
import com.example.blxny.service.admin.StationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class StationServiceImpl implements StationService {
    @Autowired
    private RuleDao ruleDao;
    @Autowired
    private StationDao stationDao;
    @Autowired
    private CityDao cityDao;
    @Override
    public List<Station> adminFindAllStation(int pageSize, int pageNumber) {
        pageNumber=pageSize*(pageNumber-1);
        return stationDao.findAllStation(pageSize,pageNumber);
    }

    @Override
    public Object adminAddNewStation(Station station) {
        //查找城市id
        String[] strArr1 = station.getAddressInfo().split("区")[0].split("市");
        String[] strArr2=strArr1[0].split("省");
        City city=new City();
        city.setProvince(strArr2[0]+"省");
        city.setCity(strArr2[1]+"市");
        city.setArea(strArr1[1]+"区");
        System.out.println(city);
        String cityId=cityDao.findCityId(city);
        if (cityId==null){
            return "库中没有找到城市";
        }
        //拼接驿站id
        String bigId=stationDao.findBigId(cityId+"%");
        if(bigId!=null){
            String endId="00"+String.valueOf(Long.valueOf(bigId.substring(10, bigId.length()))+1);
            bigId=bigId.substring(0, 10)+endId.substring(endId.length() - 3);
        }else {
            bigId=cityId+"001";
        }
        station.setStationId(bigId);
        //添加驿站
        if(stationDao.AddNewStation(station)>0){
            return "OK";
        }
        return "ERROR";
    }

    @Override
    public Object updateBasicStationInfo(Station station) {
        if (stationDao.updateStationInfo(station)>0){
            return "OK";
        }else{
            return "ERROR";
        }
    }
}
