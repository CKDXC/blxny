package com.example.blxny.serviceImpl.admin;

import com.example.blxny.dao.UserDao;
import com.example.blxny.model.Information;
import com.example.blxny.service.admin.IndexInforationService;
import com.example.blxny.tool.JPushTool;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static com.example.blxny.tool.JPushTool.ALL;

@Service(value = "information")
public class IndexInformationServiceImpl implements IndexInforationService {
    @Autowired
    private UserDao userDao;

    @Override
    public String addNewInformation(String info, String time) {
        Information information = new Information();
        Date date = null;//获取过期时间
        try {
            date = new SimpleDateFormat("yyyyMMdd").parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if(date.before(new Date())){
            return "请输入正确的过期日期";
        }
        information.setTime(date);
        information.setTitle("系统消息");
        information.setInfo(info);
        if (0 < userDao.addNewAdminInfomation(information)) {
            return "OK";
        } else {
            return "ERROR";
        }
    }

    @Override
    public int oneInformationDown(int id) {
        return userDao.oneInformationDown(id);
    }

    @Override
    public Object getAdminInforMation(int size, int number) {
        PageHelper.startPage(number,size);
        PageInfo<Information> pageInfo = new PageInfo<>(userDao.findAdminInformation());
        return pageInfo;
    }

    @Override
    public String AdminJPinformation(String info) {
        return String.valueOf(JPushTool.JPush("1",info,"易智充",ALL));
    }
}
