package com.example.blxny.serviceImpl.admin;

import com.example.blxny.dao.PartnerDao;
import com.example.blxny.dao.UserDao;
import com.example.blxny.model.Information;
import com.example.blxny.model.PartnerUpdate;
import com.example.blxny.service.admin.partnerUpdateService;
import com.example.blxny.tool.JPInformation;
import com.example.blxny.tool.JPushTool;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.example.blxny.tool.JPushTool.ONE;

@Service(value = "partnerUpdateService")
public class partnerUpdateServiceImpl implements partnerUpdateService {
    @Autowired
    private PartnerDao partnerDao;
    @Autowired
    private UserDao userDao;

    @Override
    public PageInfo<PartnerUpdate> findPartnerUpdate(Integer number, Integer type) {
        PageHelper.startPage(number, 10);
        List<PartnerUpdate> list = partnerDao.findPartnerUpdateByType(type);
        PageInfo<PartnerUpdate> pageInfo = new PageInfo<>(list);
        return pageInfo;
    }

    @Override
    @Transactional
    public Object changPartnerUpdate(Integer type, String updateId) {
        if (0 < partnerDao.changePartnerUpdateType(type, updateId)) {
            //找到这个updateId的人的jpid发送消息
            Map user = partnerDao.findJpidBypartnerUpdateId(updateId);
            String jpid = (String) user.get("starttime");
            String uuid = (String) user.get("user_uuid");
            Information information = new Information();
            if (type == 1) {
                JPushTool.JPush(jpid, JPInformation.PARTNER_UPDATE_SUCCESS.getInfo(), JPInformation.PARTNER_UPDATE_SUCCESS.getName(), ONE);
                information.setTitle(JPInformation.PARTNER_UPDATE_SUCCESS.getName());
                information.setInfo(JPInformation.PARTNER_UPDATE_SUCCESS.getInfo());
            } else if (type == 2) {
                JPushTool.JPush(jpid, JPInformation.PARTNER_UPDATE_ERROR.getInfo(), JPInformation.PARTNER_UPDATE_ERROR.getName(), ONE);
                information.setTitle(JPInformation.PARTNER_UPDATE_ERROR.getName());
                information.setInfo(JPInformation.PARTNER_UPDATE_ERROR.getInfo());
            } else {
                return "ERROR";
            }
            information.setUserUuid(uuid);
            userDao.addInformation(information);
        }
        return "OK";
    }


}
