package com.example.blxny.serviceImpl.admin;

import com.example.blxny.dao.SysRoleDao;
import com.example.blxny.dao.UserDao;
import com.example.blxny.model.SysRole;
import com.example.blxny.model.vo.UserSysRoleVO;
import com.example.blxny.service.admin.SysUserService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

/*
 *项目名: blxny
 *文件名: SysUserServiceImpl
 *创建者: SCH
 *创建时间:2019/5/7 11:30
 *描述: TODO
 */
@Service(value = "adminSysUserService")
public class SysUserServiceImpl implements SysUserService {

    @Autowired
    private UserDao userDao;
    @Autowired
    private SysRoleDao sysRoleDao;

    @Override
    public PageInfo<UserSysRoleVO> findPage(int pageNumber, int pageSize) {
        PageHelper.startPage(pageNumber, pageSize);
        List<UserSysRoleVO> userSysRoles = userDao.findAllUserSysRole();
        PageInfo<UserSysRoleVO> pageInfo = new PageInfo<>(userSysRoles);
        return pageInfo;
    }

    @Override
    public String updatePwd(String uid, String oldPwd, String newPwd) {
        String password = String.valueOf(oldPwd);
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        String oldMd5Pwd = MD5(password, md);
        SysRole sysRole = sysRoleDao.getSysRole(uid, oldMd5Pwd);
        if (null == sysRole) {
            return "用户名或密码错误";
        }
        password = String.valueOf(newPwd);
        String newMd5Pwd = MD5(password, md);
        if (sysRoleDao.updatePwd(uid, oldMd5Pwd, newMd5Pwd) > 0) {
            return "修改密码成功";
        } else {
            return "修改密码失败";
        }
    }



    private static String MD5(String strSrc, MessageDigest md) {
        byte[] bt = strSrc.getBytes();
        md.update(bt);
        String strDes = bytes2Hex(md.digest());
        return strDes;
    }

    private static String bytes2Hex(byte[] bts) {
        StringBuffer des = new StringBuffer();
        String tmp = null;
        for (int i = 0; i < bts.length; i++) {
            tmp = (Integer.toHexString(bts[i] & 0xFF));
            if (tmp.length() == 1) {
                des.append("0");
            }
            des.append(tmp);
        }
        return des.toString();
    }
}
