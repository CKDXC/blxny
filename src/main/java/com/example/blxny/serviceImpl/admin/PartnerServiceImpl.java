package com.example.blxny.serviceImpl.admin;

import com.example.blxny.dao.PartnerDao;
import com.example.blxny.model.Partner;
import com.example.blxny.service.admin.PartnerService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/*
 *项目名: blxny
 *文件名: PartnerServiceImpl
 *创建者: SCH
 *创建时间:2019/5/7 10:34
 *描述: TODO
 */
@Service(value = "adminPartnerService")
public class PartnerServiceImpl implements PartnerService {

    @Autowired
    private PartnerDao partnerDao;

    @Override
    public PageInfo<Partner> findPage(int pageNumber, int pageSize) {
        PageHelper.startPage(pageNumber, pageSize);
        List<Partner> partners = partnerDao.findAll();
        PageInfo<Partner> pageInfo = new PageInfo<>(partners);
        return pageInfo;
    }
}
