package com.example.blxny.serviceImpl.admin;

import com.example.blxny.dao.AdDao;
import com.example.blxny.model.Ad;
import com.example.blxny.service.admin.AdService;
import com.example.blxny.tool.DelFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.FileNotFoundException;

import static com.example.blxny.tool.UploadTool.upOneByYourUrlUseBase64;

@Service(value = "AD")
public class AdServiceImpl implements AdService {
@Autowired
private AdDao adDao;
    @Override
    public Object addNewAD(String file, Ad ad) throws FileNotFoundException {
        String img=upOneByYourUrlUseBase64(file,"adimg");
        if(img.equals("error")){
            return "ERROR";
        }
        ad.setImg(img);
        if(0<adDao.addAdNew(ad)){
            return img;
        }
        return "ERROR";
    }

    @Override
    public String delAD(int id) {
        if(adDao.setOneAdOver(id)>0){
            Ad ad=adDao.findOneAd(id);
            String[] img=ad.getImg().split("/");
            if(DelFile.del("adimg",img[img.length-1])){
                return "OK";
            }
        }
        return "ERROR";
    }
}
