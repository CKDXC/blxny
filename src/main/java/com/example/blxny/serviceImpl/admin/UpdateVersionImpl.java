package com.example.blxny.serviceImpl.admin;

import com.example.blxny.dao.VersionDao;
import com.example.blxny.model.AppVersion;
import com.example.blxny.model.Version;
import com.example.blxny.service.UserLoginService;
import com.example.blxny.service.admin.UpdateVersionService;
import io.netty.util.internal.SuppressJava6Requirement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.FileNotFoundException;

@Service(value = "UpdateVersion")
public class UpdateVersionImpl implements UpdateVersionService {
@Autowired
private VersionDao versionDao;
    @Override
    public Object updateVersion() {
        return versionDao.getAppVersion();
    }

    @Override
    public Object delVersion(String id) throws FileNotFoundException {
        boolean bool=versionDao.getAppVersion().getVersion().equals(id)&&versionDao.delAppVersion(id)>0;
        if(bool){
            File path = new File(ResourceUtils.getURL("classpath:").getPath());
            File file = new File(path.getAbsolutePath(), "static/APP/"+id+".apk");
            // 如果文件路径所对应的文件存在，并且是一个文件，则直接删除
            if (file.exists() && file.isFile()) {
                if (file.delete()) {
                    System.out.println("删除单个文件" + file + "成功！");
                    return "OK";
                } else {
                    System.out.println("删除单个文件" + file + "失败！");
                    return "ERROR";
                }
            } else {
                System.out.println("删除单个文件失败：" + file + "不存在！");
                return "ERROR";
            }
        }
        return "ERROR";
    }
}
