package com.example.blxny.serviceImpl.admin;

import com.example.blxny.dao.UserDao;
import com.example.blxny.model.SysRole;
import com.example.blxny.model.User;
import com.example.blxny.service.admin.UserService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/*
 *项目名: blxny
 *文件名: UserServiceImpl
 *创建者: SCH
 *创建时间:2019/5/7 9:28
 *描述: TODO
 */
@Service(value = "adminUserService")
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;


    @Override
    public PageInfo findPage(int pageNumber, int pageSize) {
        PageHelper.startPage(pageNumber, pageSize);
        List<User> users = userDao.findAll();
        System.out.println("usersize:\t" + users.size());
        PageInfo<User> pageInfo = new PageInfo<>(users);
        return pageInfo;
    }

    @Override
    public Object authorization(String master, String gave, Integer level) {
        if(master.equals(gave)){
            return "你给自己赋权限干吗？";
        }
        //先看这个是不是master用户
        String message = "修改失败";
        int i = 0;
        SysRole sys = userDao.AdminUserLoginPower(master);
        User user = userDao.FindUserAllByUid(gave);
        if (sys != null & user != null) {
            if (sys.getPower() == 0 ) {
                SysRole sysRole = new SysRole();
                sysRole.setUserUuid(gave);
                sysRole.setPower(level);
                sys = userDao.AdminUserLoginPower(gave);
                if (sys == null) {//如果gave是普通用户
                    i = userDao.newSysRole(sysRole);
                } else {//如果gave是等级用户
                    i = userDao.updateSysRolePower(sysRole);
                }
            } else {
                message = "没有权限";
            }
            if (i > 0) {
                message = "修改成功";
            }
        }
        return message;
    }
}
