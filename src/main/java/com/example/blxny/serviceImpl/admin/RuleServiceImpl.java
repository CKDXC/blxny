package com.example.blxny.serviceImpl.admin;

import com.example.blxny.dao.RuleDao;
import com.example.blxny.model.Rule;
import com.example.blxny.model.vo.RuleVo;
import com.example.blxny.service.admin.RuleService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/*
 *项目名: blxny
 *文件名: RuleServiceImpl
 *创建者: SCH
 *创建时间:2019/5/14 10:45
 *描述: TODO
 */
@Service(value = "adminRuleService")
public class RuleServiceImpl implements RuleService {

    @Autowired
    private RuleDao ruleDao;

    @Override
    public List<Rule> list(int pageNumber, int pageSize) {
        return null;
    }

    @Override
    public PageInfo<Rule> findPage(int pageNumber, int pageSize) {
        PageHelper.startPage(pageNumber, pageSize);
        List<Rule> rules = ruleDao.findAll();
        PageInfo<Rule> pageInfo = new PageInfo<>(rules);
        return pageInfo;
    }

    @Transactional
    public Object addNewRule(RuleVo vo) {
        if (ruleDao.getRule(vo.getStationId(), "0") != null) {
            return "已有此驿站规则";
        }
        if (ruleDao.addFastRule(vo) > 0 && ruleDao.addSlowRule(vo) > 0) {
            return "OK";
        } else {
            return "ERROR";
        }
    }

    @Override
    public Object changeRule(int type, double number, String id, String type2) {
        if (ruleDao.changeRule(type, number, id, Integer.valueOf(type2)) > 0) {
            return "OK";
        } else {
            return "ERROR";
        }
    }
}
