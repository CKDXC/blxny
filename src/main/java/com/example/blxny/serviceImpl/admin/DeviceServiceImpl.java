package com.example.blxny.serviceImpl.admin;

import com.example.blxny.dao.DeviceDao;
import com.example.blxny.dao.DeviceDao;
import com.example.blxny.dao.PartnerDao;
import com.example.blxny.dao.RuleDao;
import com.example.blxny.dao.StationDao;
import com.example.blxny.model.Device;
import com.example.blxny.model.vo.DeviceVO;
import com.example.blxny.service.admin.DeviceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service(value = "adminDeviceService")
public class DeviceServiceImpl implements DeviceService {

    @Autowired
    private StationDao stationDao;
    @Autowired
    private DeviceDao deviceDao;


    @Override
    public List<Device> adminFindAllDevice(int pageSize, int pageNumber) {
        pageNumber = pageSize * (pageNumber - 1);
        return stationDao.findAllDevice(pageSize, pageNumber);
    }

    @Override
    public int addDevice(DeviceVO devicevo) {
        String id = deviceDao.findBigstId(devicevo.getStationId());
        if (id != null) {
            Long lo = new Long(id);
            devicevo.setDeviceId("00000" + String.valueOf(lo + 1));
        } else {
            devicevo.setDeviceId(devicevo.getStationId() + "001");
        }
        if (0 < stationDao.stationDeviceNumUp(devicevo.getStationId())) {
            return deviceDao.addDevice(devicevo);
        } else {
            return 0;
        }
    }

    @Override
    public String updateDevice(DeviceVO deviceVO) {
        System.out.println(deviceVO);
        if (deviceDao.updateDevice(deviceVO) > 0) {
            return "修改成功";
        } else {
            return "修改失败";
        }
    }
}
