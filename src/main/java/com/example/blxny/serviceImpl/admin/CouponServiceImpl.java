package com.example.blxny.serviceImpl.admin;

import com.example.blxny.dao.CouponDao;
import com.example.blxny.model.Coupon;
import com.example.blxny.model.PartnerApply;
import com.example.blxny.service.admin.CouponService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.naming.Name;
import java.util.List;

/*
 *项目名: blxny
 *文件名: CouponServiceImpl
 *创建者: SCH
 *创建时间:2019/5/14 9:39
 *描述: TODO
 */
@Service(value = "adminCouponService")
public class CouponServiceImpl implements CouponService {

    @Autowired
    private CouponDao couponDao;

    @Override
    public List<Coupon> list(int pageNumber, int pageSize) {
        return couponDao.findAll(pageNumber, pageSize);
    }

    @Override
    public PageInfo findPage(int pageNumber, int pageSize) {
        PageHelper.startPage(pageNumber, pageSize);
        List<Coupon> coupons = couponDao.findAll(pageNumber, pageSize);
        PageInfo<Coupon> pageInfo = new PageInfo<>(coupons);
        return pageInfo;
    }
}
