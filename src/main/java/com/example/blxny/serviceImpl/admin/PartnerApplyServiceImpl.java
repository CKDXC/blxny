package com.example.blxny.serviceImpl.admin;
/*
 *项目名: blxny
 *文件名: PartnerApplyServiceImpl
 *创建者: SCH
 *创建时间:2019/5/6 15:20
 *描述: TODO
 */

import com.example.blxny.dao.PartnerApplyDao;
import com.example.blxny.dao.PartnerDao;
import com.example.blxny.dao.SysRoleDao;
import com.example.blxny.dao.UserDao;
import com.example.blxny.model.Partner;
import com.example.blxny.model.PartnerApply;
import com.example.blxny.model.SysRole;
import com.example.blxny.service.admin.PartnerApplyService;
import com.example.blxny.tool.PayCommonUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.List;

@Service
public class PartnerApplyServiceImpl implements PartnerApplyService {

    @Autowired
    private PartnerApplyDao partnerApplyDao;
    @Autowired
    private PartnerDao partnerDao;
    @Autowired
    private UserDao userDao;
    @Autowired
    private SysRoleDao sysRoleDao;

    @Override
    public List<PartnerApply> list(int pageNumber, int pageSize) {
        return null;
    }

    @Override
    public PageInfo findPage(int pageNumber, int pageSize) {
        PageHelper.startPage(pageNumber, pageSize);
        List<PartnerApply> partnerApplies = partnerApplyDao.findAll();
        PageInfo<PartnerApply> pageInfo = new PageInfo<>(partnerApplies);
        return pageInfo;
    }

    @Override
    @Transactional
    public String updatePartnerAppoly(String id, int state,int type) {
        PartnerApply partnerApply = partnerApplyDao.getPartnerApply(id);
        System.out.println(partnerApply);
        if (null == partnerApply) {
            return "数据异常,合伙人申请不存在";
        }
        if (partnerApplyDao.updatePass(id, state) <= 0) {
            return "处理失败";
        }
        if (state == 2) {
            return "处理成功";
        }
        if (state == 0) {
            return "处理成功";
        }
        Partner partner = new Partner();
        partner.setPartnerId(Integer.parseInt(PayCommonUtil.getRandomNumber(8)));
        partner.setPartnerName(partnerApply.getPartnerName());
        partner.setOpreatorName(partnerApply.getOpreatorName());
        partner.setUserName(partnerApply.getUserName());
        partner.setPartnerPwd(partnerApply.getPartnerPwd());
        String phone = userDao.getPhoneByUserUuid(partnerApply.getUserUuid());//获取手机号
        partner.setPhone(phone);
        partner.setMoney(0D);
        partner.setType(type);
        partner.setUserUuid(partnerApply.getUserUuid());
        partner.setCreated(new Date());
        //md5密码
        String password = "000000";
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        String md5pass = MD5(password, md);
        partner.setPayPwd(md5pass);
        if (partnerDao.insertPartner(partner) <= 0) {
            return "处理失败";
        }
        SysRole sysRole = new SysRole();
        sysRole.setUserUuid(partnerApply.getUserUuid());
        sysRole.setPower(2);//合伙人等级
        if (sysRoleDao.insertSysRole(sysRole) <= 0) {
            return "处理失败";
        }
        return "处理成功";
    }


    private static String MD5(String strSrc, MessageDigest md) {
        byte[] bt = strSrc.getBytes();
        md.update(bt);
        String strDes = bytes2Hex(md.digest());
        return strDes;
    }

    private static String bytes2Hex(byte[] bts) {
        StringBuffer des = new StringBuffer();
        String tmp = null;
        for (int i = 0; i < bts.length; i++) {
            tmp = (Integer.toHexString(bts[i] & 0xFF));
            if (tmp.length() == 1) {
                des.append("0");
            }
            des.append(tmp);
        }
        return des.toString();
    }
}
