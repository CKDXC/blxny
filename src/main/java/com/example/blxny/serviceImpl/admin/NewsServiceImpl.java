package com.example.blxny.serviceImpl.admin;

import com.example.blxny.service.admin.NewsService;
import com.example.blxny.util.RedisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.List;

@Service(value = "NewsService")
public class NewsServiceImpl implements NewsService {
    private static String REDIS_KEY = "pythonNews";
    private static String REFRESH_NEWS = "cmd.exe /k start C:\\Users\\Administrator\\Desktop\\test\\test.py";
    private static String SET_NEW_TASK = " schtasks /create /tn thisIsATagForNews /tr C:\\Users\\Administrator\\Desktop\\test\\test.py /sc minute /mo ";
    //private static String SET_NEW_TASK = "schtasks /create /tn thisIsATagForNewsSoYouDoNotHaveToCareTheMeaning /tr calc /sc minute /mo ";
    private static String DELETE_NEW_TASK = " schtasks /Delete /tn thisIsATagForNews -f ";
    private static String FIND_ONE_TASK = " schtasks /query";
    @Autowired
    private RedisUtil redis;

    @Override
    public boolean flashNewS() {
        if (cmd(REFRESH_NEWS).indexOf("1") != -1) {
            return true;
        } else {
            return true;
        }
    }

    @Override
    public String newNews(String time) {
        if (redis.get(REDIS_KEY) != null) {
            return "已存在任务，请先删除";
        }
        String message = cmd(SET_NEW_TASK + time);
        if(message.indexOf("成功")!=-1) {
            redis.set(REDIS_KEY, "thisIsATagForNews");
            return "OK";
        }else{
            return "ERROR";
        }
    }

    @Override
    public String delNews() {
        if (redis.get(REDIS_KEY) == null) {
            return "现在还没有任务";
        }else {
            redis.delete(REDIS_KEY);
            cmd(DELETE_NEW_TASK);
            return "OK";
        }
    }

    @Override
    public String getNews() {
        String message = cmd(FIND_ONE_TASK);
        String string = null;
        if (message.indexOf("thisIsATagForNews") == -1) {
            return "当前还没有任务";
        }
        List<String> list = Arrays.asList(message.split("\n"));
        for (String str : list) {
            if (str.indexOf("thisIsATagForNews") != -1) {
                string = str.split("thisIsATagForNews")[1].trim();
                break;
            }
        }
        if (string != null || redis.get(REDIS_KEY) != null) {
            return string;
        } else {
            return "ERROR";
        }
    }


    private String cmd(String path) {
        StringBuilder sb = new StringBuilder();
        try {
            Process process = Runtime.getRuntime().exec(path);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(process.getInputStream(), Charset.forName("GBK")));
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                if (sb.indexOf("thisIsATagForNews") != -1) {
                    sb.append(line + "\n");
                    break;
                }
                sb.append(line + "\n");
            }
            System.out.println("PY" + sb.toString());
        } catch (Exception e) {
            e.toString();
        }
        return sb.toString();
    }
}
