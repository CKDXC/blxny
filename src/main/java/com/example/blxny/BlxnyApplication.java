package com.example.blxny;

import com.example.blxny.util.MsgNumberUtil;
import com.example.blxny.util.*;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.ApplicationContext;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@EnableConfigurationProperties({MsgNumberUtil.class})
@EnableCaching
@SpringBootApplication(scanBasePackages = {"com.example.blxny"})
public class BlxnyApplication extends SpringBootServletInitializer {
    public static MsgNumberUtil msgNumberUtil;

    public static void main(String[] args) {
        SpringApplication.run(BlxnyApplication.class, args);
        ApplicationContext applicationContext = SpringUtil.getApplicationContext();
        msgNumberUtil = new MsgNumberUtil();
        try {
            ExecutorService pool = Executors.newCachedThreadPool();
            ServerSocket serverSocket = new ServerSocket(9010);
            System.out.println("服务端已启动，等待客户端连接..");
            while (true) {
                Socket socket = serverSocket.accept();// 侦听并接受到此套接字的连接,返回一个Socket对象
                SocketThread socketThread = new SocketThread(socket, applicationContext);
                String address = socket.getRemoteSocketAddress().toString();
                socketThread.setName(address);
                socketThread.start();//开始线程
//                pool.submit(socketThread);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
