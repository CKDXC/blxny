package com.example.blxny.dao;

import com.example.blxny.model.Device;
import com.example.blxny.model.Station;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface StationDao {
    int updateStationInfo(Station station);

    int stationDeviceNumUp(String id);

    List<Station> findAllStation(Integer pageSize,Integer pageNumber);

    List<Device> findAllDevice(Integer pageSize, Integer pageNumber);

    Station getInfoById(String stationId);

    String getNameById(String stationId);

    String findBigId(String id);

    int AddNewStation(Station station);



}
