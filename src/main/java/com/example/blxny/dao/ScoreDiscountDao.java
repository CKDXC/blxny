package com.example.blxny.dao;

import com.example.blxny.model.ScoreDiscount;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface ScoreDiscountDao {
    int newTable(ScoreDiscount scoreDiscount);

    String getAmount(String uuid);

    String getByScoreId(String id);

    int getCountById(@Param("id") int id);

    int updateScore(@Param("id") int id, @Param("score") int score);

    String getSocreAvg(int id);
}
