package com.example.blxny.dao;

import com.example.blxny.model.Device;
import com.example.blxny.model.vo.DeviceVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface DeviceDao {
    String findBigstId(String stationId);

    int addDevice(DeviceVO deviceVO);

    String geTypeByDeviceId(String id);

    List<String> geDeviceIdtByStationId(String id);

    Device getById(String deviceId);

    int getIsLogin(@Param("ip") String ip, @Param("port") int port);

    int updateClientAddress(String deviceId, String address);

    String getClientAddress(String deviceId);

    int updateStatus(@Param("deviceId") String deviceId, @Param("status") String status);

    //通过通信地址来修改devicestatus
    int updateStatusByAddress(String address, String state);

    //获取电桩充电类型
    int getChargeType(String deviceId);

    //修改设备信息
    int updateDevice(DeviceVO deviceVO);
    //查找驿站ID
    String getStationByDeviceId(String deviceId);
}
