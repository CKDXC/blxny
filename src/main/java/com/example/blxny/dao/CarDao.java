package com.example.blxny.dao;

import com.example.blxny.model.Car;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface CarDao {

    List<String> getAllFirm();

    Car getById(int id);
}
