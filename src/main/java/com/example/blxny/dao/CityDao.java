package com.example.blxny.dao;

import com.example.blxny.model.City;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface CityDao {
    int delCity(String id);

    int AddNewCity(City city);

    int isHaveOne(City cty);

    String findCityId(City city);

}
