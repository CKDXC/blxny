package com.example.blxny.dao;

import com.example.blxny.model.*;
import com.example.blxny.model.vo.UserSysRoleVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Mapper
@Repository
public interface UserDao {
    List AdminFindAllCity();

    List<Information> findAdminInformation();

    int oneInformationDown(int id);

    int addNewAdminInfomation(Information information);

    int adOverTime();

    int updateSysRolePower(SysRole role);

    int newSysRole(SysRole role);

    SysRole AdminUserLoginPower(String useruuid);

    int AddPos(Pos pos);

    int updateCar(Car acr);

    String findJP(String uuid);

    int addJP(String jp,String uuid);

    int addInformation(Information information);

    AppVersion update();

    String findmoneyFromUser(String uuid);

    String isHavaBankCard(String number,String where);

    List<News> news();

    List<Bank> fingAllBank(String uuid,String where);

    Bank fingONEBank(String uuid,String number);

    int closeBankCard(String number,String uuid,String where);

    int AddBank(Bank bank);

    void changeAllInformation(String uuid);

    int informationClean(String uuid);

    String findOrderNotPay(String uuid);

    String findNotPay(String uuid);

    int didHaveInfo(String uuid);

    int addDaijinjuan(String uuid);

    List<String> findCity(City city);

    int userRecharge(@RequestParam("money")Double money,@RequestParam("uuid") String uuid);

    int closeOtherLogin(String alibaba,String wx,String uuid);

    int changeUUidss(String uuid,String phone);

    int changeUUids(String uuid,String phone);

    User findWX(String integral);

    User findZF(String token);

    int changeUUid(String uuid,String phone);

    int WXnewuser(User user);

    int ZFnewuser(User user);

    int updateUserScore(@Param("golds")int golds,@Param("uid")String uid);

    int findUserScore(String uuid);

    int insertCar(Car car);

    Car findCar(String uuid);

    int changepaypassword(String password,String uuid);

    int changephone(String phone,String uuid);

    IDcard findrealman(String useruuid);

    int newIdCard(IDcard iDcard);

    String findStation(String uuid);

    String isphone(String phone);

    String isphoneAndOther(String phone);

    Integer newuser(String phone,String password,String name,String useruuid);

    User FindUserAll(String phone);

    User FindUserAllByUid(String uuid);

    Integer updatePwdByUuid(String uuid, String newpwd);

    String check(String phone,String password);

    String hasId(Integer id);

    Integer newUserInfor(String address,String lat,String lng,String homepro,String homecity,String phone);

    Integer workInfor(String address,String lat,String lng,String workpro,String workcity,String phone);

    Integer modifyNickName(@Param("uuid") String uuid,@Param("newName") String newName);

    String getStaionByUid(String uid);

    Integer updateStationCollected(String uid, String stationCollected);

    String findStationCollectedByUuid(String uid);

    Integer hasUserByUid(String uuid);

    //获取用户积分
    Integer getScoreByUid(String uid);

    //减少积分
    Integer updateScore(@Param("uid") String uid, @Param("score") Integer score);

    Integer addScore(@Param("uid") String uid, @Param("score") Integer score);

    Integer upphoto(String img,String uuid);

    List<Information> findInformation(@Param("uuid")String uuid, @Param("code")int code);

    Double getMoney(String uid);

    int updateMoney(@Param("uid") String uid, @Param("money") double money);

    int addNewVersion(@Param("version") String version, @Param("info") String info, @Param("url") String url);
    //获取User
    List<User> findAll();
    //获取所有的管理员
    List<UserSysRoleVO> findAllUserSysRole();
    //获取手机号
    String getPhoneByUserUuid(String userUuid);
}
