package com.example.blxny.dao;

import com.example.blxny.model.*;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Mapper
@Repository
public interface OrderDao {
    List<Order> getOrderByTime(@Param("start") Date start,@Param("end") Date end,@Param("city")String city);

    List<Order> findDeviceOrder(String id);

    List<Order> findAdminAllOrder();

    Appointment findAppoByAppoUid(String uid);

    String findId(String id);

    Charging findPowerPay(String stationid, String time, String type);

    Order findAllOrderInfor(String id);

    int changeMonththree(String uuid);

    List<Order> findorder(@Param("useruuid") String useruuid ,@Param("code") int code);

    String findTimeOne(String orderid);

    Station findStationOne(String stationid);

    List<Order> findorderIng(String useruuid);

    int updateMoney(@Param("money") BigDecimal money, @Param("uid") String uid);

    int isAppo(String uuid);

    int del(String id);

    Device findDeviceByLike(String idlike);

    int clean(String id);

    List<Appointment> findAppo(@Param("number") int number, @Param("uuid") String uuid);

     List<Appointment> findAppoOne(@Param("uuid") String uuid);

    int changeDeviceUse(String id);

    Device findDeviceById(String id);

    Integer addOne(Order order);

    Integer addOrder(Order order);

    List<Order> getPageByUid(@Param("uid") int uid,@Param("orderType") int orderType, @Param("pageNumber") int pageNumber, @Param("pageSize") int pageSize);

    List<Order> getOrdersByDeviceId(String deviceId);

    int addAppointment(Appointment appointment);
    //获取指定设备在指定月份的order
    List<Order> getOrder(@Param("deviceId") String deviceId, @Param("month")int month, @Param("year") int year);

    List<Order> getOrder1(@Param("deviceId") String deviceId, @Param("currentMonth") int currentMonth);
    //获取order
    List<Order> getPageById(@Param("deviceId") String deviceId,@Param("pageNumber") int pageNumber,@Param("pageSize") int pageSize);
    //修改订单状态
    int updateStatus(@Param("orderId") String orderId,@Param("status") int status);

    //通过id 查找订单
    Order getOrderById(String orderId);

    //获取最后一个order
    String getLastOrder(String orderIdHeader);

    //更新充电数据
    int update(Order order);
    //获取deviceId
    String getDeviceIdByOrderId(String orderId);
    //获取用户指定订单状态的记录数
    Order getOrderByStatus(@Param("uid")String uid, @Param("status") int status);
}
