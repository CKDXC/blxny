package com.example.blxny.dao;

import com.example.blxny.model.Hotel;
import com.example.blxny.model.Restaurant;
import com.example.blxny.model.Shop;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;

/*
 *项目名: blxny
 *文件名: RestaurantDao
 *创建者: SCH
 *创建时间:2019/5/28 14:29
 *描述: TODO
 */
@Mapper
@Repository
public interface RestaurantDao {
    int delShopOne(int id);

    int addNewShop(Shop shop);

    String findIShaveShop(int i);

    int delList(int id);

    int addNewImgFromHotel(Hotel s);

    int findCountByImgInfo(@Param("info") String info,@Param("id")int id);

    String findHotelIdFromRestaurant(String name);

    int changeNewList(Restaurant restaurant);

    int findList(int id);

    List<Restaurant> getList(@Param("type") int type, @Param("lastId") int lastId, @Param("pageSize") int pageSize);

    List<Restaurant> getPopular(@Param("type") int type,@Param("lastId") int lastId, @Param("pageSize") int pageSize);

    List<Hotel> findHotelInfo(int id);

    List<Shop> findShopInfo(@Param("id") int id,@Param("size")int size,@Param("number")int number,@Param("type")int type);

    int updateStar(@Param("id") int id, @Param("newstar") BigDecimal newstar);

    int addNewList(Restaurant restaurant);

    Restaurant getRestaurantById(int id);
}
