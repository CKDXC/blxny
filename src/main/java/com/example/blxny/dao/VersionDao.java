package com.example.blxny.dao;

import com.example.blxny.model.AppVersion;
import com.example.blxny.model.Version;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface VersionDao {
    Version getVersion(int versionId);

    AppVersion getAppVersion();

    int delAppVersion(String id);
}
