package com.example.blxny.dao;

import com.example.blxny.model.vo.BankPosVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface BankDao {
    String findPosByPosId(String id);

    List<BankPosVO> FindPos(@Param("type") Integer type, @Param("phone") String phone);

    int changePosType(String id);

    String findJPidFromPosId(String id);


}
