package com.example.blxny.dao;

import com.example.blxny.model.CateringProduct;
import com.example.blxny.model.dto.CateringProductDTO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

/*
 *项目名: blxny
 *文件名: CoffeeProductDao
 *创建者: SCH
 *创建时间:2019/5/28 16:38
 *描述: TODO
 */
@Mapper
@Repository
public interface CateringProductDao {
    List<CateringProduct> getList(int restaurantId, int lastId, int type, int ps);

//    List<CateringProduct> getCateringProduct(int from, int restaurantId, int type);

    //获取所有CateringProduct
    List<CateringProduct> findAll(int restaurantId);
    //添加CateringProduct
    int addCateringProducat(CateringProduct cateringProduct);

    int updateCateringProducat(CateringProductDTO cateringProductDTO);
}
