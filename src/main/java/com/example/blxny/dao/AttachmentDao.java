package com.example.blxny.dao;

import com.example.blxny.model.Attachment;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface AttachmentDao {
    boolean addImg(Attachment attachment);
}
