package com.example.blxny.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface AppointmentDao {

    void updateStatus(@Param("appoId") String appoId,@Param("status") int status);

}
