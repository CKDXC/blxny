package com.example.blxny.dao;

import com.example.blxny.model.Partner;
import com.example.blxny.model.PartnerUpdate;
import com.example.blxny.model.Pos;
import com.example.blxny.model.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface PartnerDao {
    String getPwdById(Integer id);

    Map findJpidBypartnerUpdateId(String id);

    int changePartnerUpdateType(@Param("type") Integer type, @Param("id") String id);

    List<PartnerUpdate> findPartnerUpdateByType(Integer type);

    int updatemoney(Pos pos);

    List<PartnerUpdate> findPartnerUpdate(String id);

    int addPartnerUpdate(PartnerUpdate update);

    int addImg(String img, String pid);

    Partner findPartner(String id);

    int hasOpreatorName(String opreatorName);

    int addPartner(@Param("partner") Partner partner);

    int hasPartnerByUid(String uid);

    int hasPartnerById(int partnerId);

    int updatePwdById(@Param("partnerId") int partnerId, @Param("newPwd") String newPwd);

    String getPwdByUid(String uuid);

    int updatePayPwdById(@Param("partnerId") int partnerId, @Param("payPwd") String payPwd);

    int updatePhone(@Param("partnerId") int partnerId, @Param("phone") String phone);

    int hasPhone(String phone);

    int getPartnerIdByUid(String uid);

    //Partner
    List<Partner> findAll();

    int insertPartner(Partner partner);
}
