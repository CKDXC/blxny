package com.example.blxny.dao;

import com.example.blxny.model.Coupon;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Mapper
@Repository
public interface CouponDao {
    int changeUserVoucher(@Param("number")int number,@Param("uuid")String uuid);

    int changeDate();

    int cleanGoldUse(String uuid);

    List<Coupon> getGoldUse(String uuid);

    //获取优惠券通过uid,是否使用,过期时间
    List<Coupon> getCoupon(@Param("uid") String uid,@Param("type") int type, @Param("isUsed") int isUsed, @Param("nowDate") long nowDate);

    //添加优惠券
    Integer addOne(@Param("uuid") String uuid,
                   @Param("scoreid") Integer scoreid,
                   @Param("type") Integer type,
                   @Param("nowDate") Date nowDate,
                   @Param("integral")String integral,
                   @Param("money")Double money,
                   @Param("overtime")Date overtime);

    //获取所有优惠券
    List<Coupon> findAll(int pageNumber, int pageSize);


}
