package com.example.blxny.dao;

import com.example.blxny.model.ScoreProduct;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface ScoreProductDao {

    int getPriceById(int productId);

    int getStockById(int productId);

    ScoreProduct getProductById(int productId);

}
