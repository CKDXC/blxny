package com.example.blxny.dao;

import com.example.blxny.model.PartnerApply;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface PartnerApplyDao {

    int addOne(PartnerApply partnerApply);

    PartnerApply getApplyByUid(String uid);

    //获取PartnerApply
    List<PartnerApply> findAll();

    //修改申请状态
    int updatePass(@Param("id") String id, @Param("pass") int pass);

    //通过id 获取PartnerApply
    PartnerApply getPartnerApply(String id);
}
