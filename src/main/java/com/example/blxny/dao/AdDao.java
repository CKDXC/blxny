package com.example.blxny.dao;

import com.example.blxny.model.Ad;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface AdDao {
    int addAdNew(Ad ad);

    Ad findOneAd(int id);

   int setOneAdOver(int id);
}
