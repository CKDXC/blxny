package com.example.blxny.dao;

import com.example.blxny.model.PartnerDevice;
import com.example.blxny.model.Station;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface PartnerDeviceDao {

    List<String> getStationsByPartnerId(int partnerId);

    List<PartnerDevice> getDevicesByPartnerId(int partnerId);

    //获取正在运行的电桩
    //获取合伙人指定驿站下的电桩
    List<PartnerDevice> getPartnerDevices(@Param("partnerId") int partnerId, @Param("stationId") String stationId);
}
