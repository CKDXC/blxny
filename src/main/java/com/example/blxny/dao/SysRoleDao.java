package com.example.blxny.dao;

import com.example.blxny.model.SysRole;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/*
 *项目名: blxny
 *文件名: SysRoleDao
 *创建者: SCH
 *创建时间:2019/5/7 15:35
 *描述: TODO
 */
@Mapper
@Repository
public interface SysRoleDao {
    //修改密码
    int updatePwd(String uid, String oldPwd, String newPwd);
    //获取SysRole
    SysRole getSysRole(String uid, String pwd);
    //添加系统管理员
    int insertSysRole(SysRole sysRole);
}
