package com.example.blxny.dao;

import com.example.blxny.model.Alarm;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface AlarmDao {
    void addOne(Alarm caveat);
}
