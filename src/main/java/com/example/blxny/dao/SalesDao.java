package com.example.blxny.dao;

import com.alipay.api.domain.Sale;
import com.example.blxny.model.Ad;
import com.example.blxny.model.SaleMan;
import com.example.blxny.model.SalesOder;
import com.example.blxny.model.Service;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface SalesDao {

    List<Ad> findAd();

    SalesOder getOrderSales(String id);

    int getFate(String id, String salesid);

    int addSalesOder(SalesOder oder);

    String isSales(String useruuid);

    int changeSalesType(@Param("type") int type, @Param("id") String id);

    List<SaleMan> findSales();

    SaleMan findSalesOne(String uuid);

    int addService(Service service);

    int updateWork(String uuid, String JPid,String lng,String lat);

    int offWork(String uuid);

    int goToWork(String uuid);

    SaleMan findSalesNew(String uuid);

    SaleMan findSalesWork(String uuid);
}
