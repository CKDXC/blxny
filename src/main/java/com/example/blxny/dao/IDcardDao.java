package com.example.blxny.dao;

import com.example.blxny.model.IDcard;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface IDcardDao {
   int  ApprovalIdCard(@Param("type")Integer type,@Param("uuid")String uuid);

    List<IDcard> findAllIDcardApproval(Integer type);
}
