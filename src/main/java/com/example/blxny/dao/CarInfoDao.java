package com.example.blxny.dao;

import com.example.blxny.model.CarInfo;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface CarInfoDao {

    CarInfo getCarInfoByUid(int uid);
}
