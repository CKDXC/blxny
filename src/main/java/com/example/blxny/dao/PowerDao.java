package com.example.blxny.dao;

import com.example.blxny.model.Charging;
import com.example.blxny.model.Device;
import com.example.blxny.model.Rule;
import com.example.blxny.model.Station;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface PowerDao {
   Station findGPS(String id);

   Station findOneStation(String stationid);

   List<Station> findAllStationNoMore();

   List<Device> findDeviceType(String code,String station);

   Device finddevice(String id);

   int setAllPay(String allpay,String id);

   Rule findPay(String type,String station);

   String findStationOne(String uuid);

   Station findStation(String type,String stationid);

   List<Station> findAllStation(String city);

   List findType(String type);

   Rule changeMoney(String addressid,String nowtime,String type);

   Charging changeMoneys(String addressid, String time);

   String countNull(String id);

   List<Device> findlowpower(String id);

   List<Device> findfastpower(String id);
}
