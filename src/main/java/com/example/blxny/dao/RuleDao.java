package com.example.blxny.dao;

import com.example.blxny.model.Rule;
import com.example.blxny.model.vo.RuleVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface RuleDao {
    //通过ruleId获取rule
    //Rule getRuleById(int ruleId);//现在不可用
    //通过驿站id和电桩类型获取rule
    Rule getRule(String stationId, String deviceType);
    //添加规则
    int addFastRule(RuleVo o);
    int addSlowRule(RuleVo o);
    //修改rule
    int changeRule(@Param("key") int key, @Param("value")double value, @Param("id")String id, @Param("type")int type);
    //获取所有rule
    List<Rule> findAll();
}
